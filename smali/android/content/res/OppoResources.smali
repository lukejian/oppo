.class public final Landroid/content/res/OppoResources;
.super Landroid/content/res/Resources;
.source "OppoResources.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/content/res/OppoResources$OppoTheme;
    }
.end annotation


# static fields
.field public static final COOKIE_TYPE_FRAMEWORK:I = 0x1

.field public static final COOKIE_TYPE_OPPO:I = 0x2

.field public static final COOKIE_TYPE_OTHERPACKAGE:I = 0x3

.field private static final TAG:Ljava/lang/String; = "OppoResources"

.field private static themeChanged:Z


# instance fields
.field private final DEBUG:Z

.field protected mCharSequences:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field protected mCookies:Landroid/util/SparseIntArray;

.field protected mIntegers:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected mIsHasValues:Z

.field protected mSkipFiles:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mThemeChangeEnable:Z

.field protected mThemeResources:Loppo/content/res/OppoThemeResources;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    sput-boolean v0, Landroid/content/res/OppoResources;->themeChanged:Z

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 119
    invoke-direct {p0}, Landroid/content/res/Resources;-><init>()V

    .line 51
    iput-boolean v1, p0, Landroid/content/res/OppoResources;->DEBUG:Z

    .line 56
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/content/res/OppoResources;->mCharSequences:Landroid/util/SparseArray;

    .line 57
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Landroid/content/res/OppoResources;->mCookies:Landroid/util/SparseIntArray;

    .line 58
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/content/res/OppoResources;->mSkipFiles:Landroid/util/SparseArray;

    .line 61
    iput-boolean v1, p0, Landroid/content/res/OppoResources;->mIsHasValues:Z

    .line 63
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/content/res/OppoResources;->mIntegers:Landroid/util/SparseArray;

    .line 69
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/content/res/OppoResources;->mThemeChangeEnable:Z

    .line 120
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/content/res/OppoResources;->init(Ljava/lang/String;)V

    .line 121
    return-void
.end method

.method public constructor <init>(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;)V
    .locals 2
    .parameter "assetmanager"
    .parameter "displaymetrics"
    .parameter "configuration"

    .prologue
    const/4 v1, 0x0

    .line 125
    invoke-direct {p0, p1, p2, p3}, Landroid/content/res/Resources;-><init>(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;)V

    .line 51
    iput-boolean v1, p0, Landroid/content/res/OppoResources;->DEBUG:Z

    .line 56
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/content/res/OppoResources;->mCharSequences:Landroid/util/SparseArray;

    .line 57
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Landroid/content/res/OppoResources;->mCookies:Landroid/util/SparseIntArray;

    .line 58
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/content/res/OppoResources;->mSkipFiles:Landroid/util/SparseArray;

    .line 61
    iput-boolean v1, p0, Landroid/content/res/OppoResources;->mIsHasValues:Z

    .line 63
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/content/res/OppoResources;->mIntegers:Landroid/util/SparseArray;

    .line 69
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/content/res/OppoResources;->mThemeChangeEnable:Z

    .line 126
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/content/res/OppoResources;->init(Ljava/lang/String;)V

    .line 127
    return-void
.end method

.method public constructor <init>(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)V
    .locals 2
    .parameter "assetmanager"
    .parameter "displaymetrics"
    .parameter "configuration"
    .parameter "compatibilityinfo"

    .prologue
    const/4 v1, 0x0

    .line 131
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/content/res/Resources;-><init>(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)V

    .line 51
    iput-boolean v1, p0, Landroid/content/res/OppoResources;->DEBUG:Z

    .line 56
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/content/res/OppoResources;->mCharSequences:Landroid/util/SparseArray;

    .line 57
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Landroid/content/res/OppoResources;->mCookies:Landroid/util/SparseIntArray;

    .line 58
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/content/res/OppoResources;->mSkipFiles:Landroid/util/SparseArray;

    .line 61
    iput-boolean v1, p0, Landroid/content/res/OppoResources;->mIsHasValues:Z

    .line 63
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/content/res/OppoResources;->mIntegers:Landroid/util/SparseArray;

    .line 69
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/content/res/OppoResources;->mThemeChangeEnable:Z

    .line 132
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/content/res/OppoResources;->init(Ljava/lang/String;)V

    .line 133
    return-void
.end method

.method static synthetic access$000(Landroid/content/res/OppoResources;Landroid/content/res/TypedArray;)Landroid/content/res/TypedArray;
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/content/res/OppoResources;->replaceTypedArray(Landroid/content/res/TypedArray;)Landroid/content/res/TypedArray;

    move-result-object v0

    return-object v0
.end method

.method static clearPreloadedCache()V
    .locals 1

    .prologue
    .line 80
 #   sget-object v0, Landroid/content/res/OppoResources;->sPreloadedDrawables:Landroid/util/LongSparseArray;

 #   invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V

    .line 81
    sget-object v0, Landroid/content/res/OppoResources;->sPreloadedColorStateLists:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V

    .line 82
    sget-object v0, Landroid/content/res/OppoResources;->sPreloadedColorDrawables:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V

    .line 83
    return-void
.end method

.method private getCookieType(I)I
    .locals 3
    .parameter "id"

    .prologue
    .line 189
    iget-object v2, p0, Landroid/content/res/OppoResources;->mCookies:Landroid/util/SparseIntArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    .line 190
    .local v0, i:I
    const/4 v1, 0x0

    .line 191
    .local v1, packageName:Ljava/lang/String;
    if-nez v0, :cond_0

    .line 192
    iget-object v2, p0, Landroid/content/res/OppoResources;->mAssets:Landroid/content/res/AssetManager;

    invoke-virtual {v2, p1}, Landroid/content/res/AssetManager;->getCookieName(I)Ljava/lang/String;

    move-result-object v1

    .line 196
    const-string v2, "/system/framework/framework-res.apk"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 197
    const/4 v0, 0x1

    .line 204
    :goto_0
    iget-object v2, p0, Landroid/content/res/OppoResources;->mCookies:Landroid/util/SparseIntArray;

    invoke-virtual {v2, p1, v0}, Landroid/util/SparseIntArray;->put(II)V

    .line 206
    :cond_0
    return v0

    .line 198
    :cond_1
    const-string v2, "/system/framework/oppo-framework-res.apk"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 199
    const/4 v0, 0x2

    goto :goto_0

    .line 201
    :cond_2
    const/4 v0, 0x3

    goto :goto_0
.end method

.method private replaceTypedArray(Landroid/content/res/TypedArray;)Landroid/content/res/TypedArray;
    .locals 7
    .parameter "typedarray"

    .prologue
    .line 373
    iget-boolean v5, p0, Landroid/content/res/OppoResources;->mIsHasValues:Z

    if-eqz v5, :cond_3

    iget-boolean v5, p0, Landroid/content/res/OppoResources;->mThemeChangeEnable:Z

    if-eqz v5, :cond_3

    .line 374
    iget-object v0, p1, Landroid/content/res/TypedArray;->mData:[I

    .line 375
    .local v0, ai:[I
    const/4 v1, 0x0

    .line 376
    .local v1, i:I
    :goto_0
    array-length v5, v0

    if-ge v1, v5, :cond_3

    .line 377
    add-int/lit8 v5, v1, 0x0

    aget v4, v0, v5

    .line 378
    .local v4, type:I
    add-int/lit8 v5, v1, 0x3

    aget v2, v0, v5

    .line 381
    .local v2, id:I
    const/16 v5, 0x10

    if-lt v4, v5, :cond_0

    const/16 v5, 0x1f

    if-le v4, v5, :cond_1

    :cond_0
    const/4 v5, 0x5

    if-ne v4, v5, :cond_2

    .line 383
    :cond_1
    invoke-virtual {p0, v2}, Landroid/content/res/OppoResources;->getThemeInt(I)Ljava/lang/Integer;

    move-result-object v3

    .line 384
    .local v3, res:Ljava/lang/Integer;
    if-eqz v3, :cond_2

    .line 385
    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    aput v6, v0, v5

    .line 388
    .end local v3           #res:Ljava/lang/Integer;
    :cond_2
    add-int/lit8 v1, v1, 0x6

    .line 389
    goto :goto_0

    .line 392
    .end local v0           #ai:[I
    .end local v1           #i:I
    .end local v2           #id:I
    .end local v4           #type:I
    :cond_3
    return-object p1
.end method


# virtual methods
.method public clearCache()V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x1

    sput-boolean v0, Landroid/content/res/OppoResources;->themeChanged:Z

    .line 111
    iget-object v0, p0, Landroid/content/res/OppoResources;->mDrawableCache:Landroid/util/LongSparseArray;

    invoke-virtual {p0, v0}, Landroid/content/res/OppoResources;->clearRefuse(Landroid/util/LongSparseArray;)V

    .line 112
    iget-object v0, p0, Landroid/content/res/OppoResources;->mColorStateListCache:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V

    .line 113
    iget-object v0, p0, Landroid/content/res/OppoResources;->mColorDrawableCache:Landroid/util/LongSparseArray;

    invoke-virtual {p0, v0}, Landroid/content/res/OppoResources;->clearRefuse(Landroid/util/LongSparseArray;)V

    .line 114
    invoke-static {}, Landroid/content/res/OppoResources;->clearPreloadedCache()V

    .line 115
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 116
    return-void
.end method

.method public clearRefuse(Landroid/util/LongSparseArray;)V
    .locals 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/drawable/Drawable$ConstantState;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .local p1, cache:Landroid/util/LongSparseArray;,"Landroid/util/LongSparseArray<Ljava/lang/ref/WeakReference<Landroid/graphics/drawable/Drawable$ConstantState;>;>;"
    const/4 v5, 0x0

    .line 95
    invoke-virtual {p1}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    .line 96
    .local v0, N:I
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 97
    invoke-virtual {p1, v2}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    .line 98
    .local v3, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/graphics/drawable/Drawable$ConstantState;>;"
    if-eqz v3, :cond_0

    .line 99
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable$ConstantState;

    .line 100
    .local v1, cs:Landroid/graphics/drawable/Drawable$ConstantState;
    if-eqz v1, :cond_0

    .line 101
    invoke-virtual {v1, p0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 102
    invoke-virtual {p1, v2, v5}, Landroid/util/LongSparseArray;->setValueAt(ILjava/lang/Object;)V

    .line 96
    .end local v1           #cs:Landroid/graphics/drawable/Drawable$ConstantState;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 106
    .end local v3           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/graphics/drawable/Drawable$ConstantState;>;"
    :cond_1
    return-void
.end method

.method public getText(I)Ljava/lang/CharSequence;
    .locals 1
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    .prologue
    .line 210
    invoke-virtual {p0, p1}, Landroid/content/res/OppoResources;->getThemeCharSequence(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 212
    .local v0, res:Ljava/lang/CharSequence;
    if-nez v0, :cond_0

    .line 213
    invoke-super {p0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 216
    :cond_0
    return-object v0
.end method

.method public getText(ILjava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 1
    .parameter "id"
    .parameter "def"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    .prologue
    .line 220
    invoke-virtual {p0, p1}, Landroid/content/res/OppoResources;->getThemeCharSequence(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 222
    .local v0, res:Ljava/lang/CharSequence;
    if-nez v0, :cond_0

    .line 223
    invoke-super {p0, p1, p2}, Landroid/content/res/Resources;->getText(ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 226
    :cond_0
    return-object v0
.end method

.method public getThemeChanged()Z
    .locals 1

    .prologue
    .line 90
    sget-boolean v0, Landroid/content/res/OppoResources;->themeChanged:Z

    return v0
.end method

.method public getThemeCharSequence(I)Ljava/lang/CharSequence;
    .locals 3
    .parameter "id"

    .prologue
    .line 230
    const/4 v1, 0x0

    .line 231
    .local v1, res:Ljava/lang/CharSequence;
    iget-boolean v2, p0, Landroid/content/res/OppoResources;->mIsHasValues:Z

    if-nez v2, :cond_0

    .line 232
    const/4 v1, 0x0

    .line 243
    :goto_0
    return-object v1

    .line 234
    :cond_0
    iget-object v2, p0, Landroid/content/res/OppoResources;->mCharSequences:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    .line 235
    .local v0, index:I
    if-ltz v0, :cond_1

    .line 236
    iget-object v2, p0, Landroid/content/res/OppoResources;->mCharSequences:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1           #res:Ljava/lang/CharSequence;
    check-cast v1, Ljava/lang/CharSequence;

    .restart local v1       #res:Ljava/lang/CharSequence;
    goto :goto_0

    .line 238
    :cond_1
    iget-object v2, p0, Landroid/content/res/OppoResources;->mThemeResources:Loppo/content/res/OppoThemeResources;

    invoke-virtual {v2, p1}, Loppo/content/res/OppoThemeResources;->getThemeCharSequence(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 239
    iget-object v2, p0, Landroid/content/res/OppoResources;->mCharSequences:Landroid/util/SparseArray;

    invoke-virtual {v2, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public getThemeInt(I)Ljava/lang/Integer;
    .locals 3
    .parameter "id"

    .prologue
    .line 248
    iget-boolean v2, p0, Landroid/content/res/OppoResources;->mIsHasValues:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Landroid/content/res/OppoResources;->mThemeChangeEnable:Z

    if-nez v2, :cond_1

    .line 249
    :cond_0
    const/4 v1, 0x0

    .line 260
    .local v1, res:Ljava/lang/Integer;
    :goto_0
    return-object v1

    .line 251
    .end local v1           #res:Ljava/lang/Integer;
    :cond_1
    iget-object v2, p0, Landroid/content/res/OppoResources;->mIntegers:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    .line 252
    .local v0, index:I
    if-ltz v0, :cond_2

    .line 253
    iget-object v2, p0, Landroid/content/res/OppoResources;->mIntegers:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .restart local v1       #res:Ljava/lang/Integer;
    goto :goto_0

    .line 255
    .end local v1           #res:Ljava/lang/Integer;
    :cond_2
    iget-object v2, p0, Landroid/content/res/OppoResources;->mThemeResources:Loppo/content/res/OppoThemeResources;

    invoke-virtual {v2, p1}, Loppo/content/res/OppoThemeResources;->getThemeInt(I)Ljava/lang/Integer;

    move-result-object v1

    .line 256
    .restart local v1       #res:Ljava/lang/Integer;
    iget-object v2, p0, Landroid/content/res/OppoResources;->mIntegers:Landroid/util/SparseArray;

    invoke-virtual {v2, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public getValue(ILandroid/util/TypedValue;Z)V
    .locals 3
    .parameter "id"
    .parameter "outValue"
    .parameter "resolveRefs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    .prologue
    .line 265
    invoke-super {p0, p1, p2, p3}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 266
    iget v1, p2, Landroid/util/TypedValue;->type:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    iget v1, p2, Landroid/util/TypedValue;->type:I

    const/16 v2, 0x1f

    if-le v1, v2, :cond_1

    :cond_0
    iget v1, p2, Landroid/util/TypedValue;->type:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    .line 267
    :cond_1
    invoke-virtual {p0, p1}, Landroid/content/res/OppoResources;->getThemeInt(I)Ljava/lang/Integer;

    move-result-object v0

    .line 268
    .local v0, res:Ljava/lang/Integer;
    if-eqz v0, :cond_2

    .line 269
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p2, Landroid/util/TypedValue;->data:I

    .line 272
    .end local v0           #res:Ljava/lang/Integer;
    :cond_2
    return-void
.end method

.method public init(Ljava/lang/String;)V
    .locals 1
    .parameter "name"

    .prologue
    .line 136
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "android"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "oppo"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 137
    :cond_0
    invoke-static {p0}, Loppo/content/res/OppoThemeResources;->getSystem(Landroid/content/res/Resources;)Loppo/content/res/OppoThemeResources;

    move-result-object v0

    iput-object v0, p0, Landroid/content/res/OppoResources;->mThemeResources:Loppo/content/res/OppoThemeResources;

    .line 142
    :goto_0
    iget-object v0, p0, Landroid/content/res/OppoResources;->mThemeResources:Loppo/content/res/OppoThemeResources;

    if-eqz v0, :cond_1

    .line 143
    iget-object v0, p0, Landroid/content/res/OppoResources;->mThemeResources:Loppo/content/res/OppoThemeResources;

    invoke-virtual {v0}, Loppo/content/res/OppoThemeResources;->hasValues()Z

    move-result v0

    iput-boolean v0, p0, Landroid/content/res/OppoResources;->mIsHasValues:Z

    .line 145
    :cond_1
    return-void

    .line 139
    :cond_2
    invoke-static {p0, p1}, Loppo/content/res/OppoThemeResourcesPackage;->getThemeResources(Landroid/content/res/Resources;Ljava/lang/String;)Loppo/content/res/OppoThemeResourcesPackage;

    move-result-object v0

    iput-object v0, p0, Landroid/content/res/OppoResources;->mThemeResources:Loppo/content/res/OppoThemeResources;

    goto :goto_0
.end method

.method public loadIcon(I)Landroid/graphics/drawable/Drawable;
    .locals 1
    .parameter "id"

    .prologue
    .line 297
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/content/res/OppoResources;->loadIcon(ILjava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public loadIcon(ILjava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 9
    .parameter "id"
    .parameter "str"

    .prologue
    const/4 v8, 0x1

    .line 302
    const/4 v0, 0x0

    .line 303
    .local v0, drawable:Landroid/graphics/drawable/Drawable;
    const/4 v3, 0x0

    .line 304
    .local v3, path:Ljava/lang/String;
    new-instance v6, Landroid/util/TypedValue;

    invoke-direct {v6}, Landroid/util/TypedValue;-><init>()V

    .line 305
    .local v6, value:Landroid/util/TypedValue;
    invoke-virtual {p0, p1, v6, v8}, Landroid/content/res/OppoResources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 306
    iget-object v7, v6, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 308
    if-eqz p2, :cond_0

    .line 309
    const-string v7, "/"

    invoke-virtual {v3, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v3, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 310
    .local v4, temp:Ljava/lang/String;
    invoke-virtual {v3, v4, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 314
    .end local v4           #temp:Ljava/lang/String;
    :cond_0
    iget-object v7, p0, Landroid/content/res/OppoResources;->mSkipFiles:Landroid/util/SparseArray;

    invoke-virtual {v7, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_3

    iget-object v7, p0, Landroid/content/res/OppoResources;->mThemeResources:Loppo/content/res/OppoThemeResources;

    if-eqz v7, :cond_3

    .line 315
    iget-object v7, p0, Landroid/content/res/OppoResources;->mThemeResources:Loppo/content/res/OppoThemeResources;

    invoke-static {}, Loppo/content/res/OppoThemeResources;->getSystem()Loppo/content/res/OppoThemeResourcesSystem;

    move-result-object v7

    invoke-virtual {v7, v3}, Loppo/content/res/OppoThemeResourcesSystem;->getIconStream(Ljava/lang/String;)Loppo/content/res/OppoThemeZipFile$ThemeFileInfo;

    move-result-object v5

    .line 317
    .local v5, themeFileInfo:Loppo/content/res/OppoThemeZipFile$ThemeFileInfo;
    if-nez v5, :cond_1

    .line 318
    iget-object v7, p0, Landroid/content/res/OppoResources;->mSkipFiles:Landroid/util/SparseArray;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, p1, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 319
    const/4 v7, 0x0

    .line 340
    .end local v5           #themeFileInfo:Loppo/content/res/OppoThemeZipFile$ThemeFileInfo;
    :goto_0
    return-object v7

    .line 323
    .restart local v5       #themeFileInfo:Loppo/content/res/OppoThemeZipFile$ThemeFileInfo;
    :cond_1
    if-eqz v5, :cond_2

    .line 324
    :try_start_0
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 325
    .local v2, options:Landroid/graphics/BitmapFactory$Options;
    iget v7, v5, Loppo/content/res/OppoThemeZipFile$ThemeFileInfo;->mDensity:I

    iput v7, v2, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    .line 326
    iget-object v1, v5, Loppo/content/res/OppoThemeZipFile$ThemeFileInfo;->mInput:Ljava/io/InputStream;

    .line 327
    .local v1, input:Ljava/io/InputStream;
    invoke-static {p0, v6, v1, v3, v2}, Landroid/graphics/drawable/Drawable;->createFromResourceStream(Landroid/content/res/Resources;Landroid/util/TypedValue;Ljava/io/InputStream;Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 333
    .end local v1           #input:Ljava/io/InputStream;
    .end local v2           #options:Landroid/graphics/BitmapFactory$Options;
    :cond_2
    if-eqz v5, :cond_3

    .line 334
    :try_start_1
    iget-object v7, v5, Loppo/content/res/OppoThemeZipFile$ThemeFileInfo;->mInput:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .end local v5           #themeFileInfo:Loppo/content/res/OppoThemeZipFile$ThemeFileInfo;
    :cond_3
    :goto_1
    move-object v7, v0

    .line 340
    goto :goto_0

    .line 329
    .restart local v5       #themeFileInfo:Loppo/content/res/OppoThemeZipFile$ThemeFileInfo;
    :catch_0
    move-exception v7

    .line 333
    if-eqz v5, :cond_3

    .line 334
    :try_start_2
    iget-object v7, v5, Loppo/content/res/OppoThemeZipFile$ThemeFileInfo;->mInput:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 336
    :catch_1
    move-exception v7

    goto :goto_1

    .line 332
    :catchall_0
    move-exception v7

    .line 333
    if-eqz v5, :cond_4

    .line 334
    :try_start_3
    iget-object v8, v5, Loppo/content/res/OppoThemeZipFile$ThemeFileInfo;->mInput:Ljava/io/InputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 337
    :cond_4
    :goto_2
    throw v7

    .line 336
    :catch_2
    move-exception v8

    goto :goto_2

    :catch_3
    move-exception v7

    goto :goto_1
.end method

.method public loadOverlayDrawable(Landroid/util/TypedValue;I)Landroid/graphics/drawable/Drawable;
    .locals 8
    .parameter "value"
    .parameter "id"

    .prologue
    .line 148
    iget-boolean v6, p0, Landroid/content/res/OppoResources;->mThemeChangeEnable:Z

    if-nez v6, :cond_1

    const/4 v0, 0x0

    .line 185
    :cond_0
    :goto_0
    return-object v0

    .line 150
    :cond_1
    const/4 v0, 0x0

    .line 151
    .local v0, drawable:Landroid/graphics/drawable/Drawable;
    iget-object v6, p0, Landroid/content/res/OppoResources;->mSkipFiles:Landroid/util/SparseArray;

    invoke-virtual {v6, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_0

    iget-object v6, p0, Landroid/content/res/OppoResources;->mThemeResources:Loppo/content/res/OppoThemeResources;

    if-eqz v6, :cond_0

    .line 152
    iget-object v6, p1, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 156
    .local v4, path:Ljava/lang/String;
    iget-object v6, p0, Landroid/content/res/OppoResources;->mThemeResources:Loppo/content/res/OppoThemeResources;

    iget v7, p1, Landroid/util/TypedValue;->assetCookie:I

    invoke-direct {p0, v7}, Landroid/content/res/OppoResources;->getCookieType(I)I

    move-result v7

    invoke-virtual {v6, v7, v4}, Loppo/content/res/OppoThemeResources;->getThemeFileStream(ILjava/lang/String;)Loppo/content/res/OppoThemeZipFile$ThemeFileInfo;

    move-result-object v5

    .line 158
    .local v5, themeFileInfo:Loppo/content/res/OppoThemeZipFile$ThemeFileInfo;
    if-nez v5, :cond_2

    .line 159
    iget-object v6, p0, Landroid/content/res/OppoResources;->mSkipFiles:Landroid/util/SparseArray;

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, p2, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 168
    :cond_2
    :try_start_0
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 169
    .local v3, options:Landroid/graphics/BitmapFactory$Options;
    iget v6, v5, Loppo/content/res/OppoThemeZipFile$ThemeFileInfo;->mDensity:I

    iput v6, v3, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    .line 170
    iget-object v1, v5, Loppo/content/res/OppoThemeZipFile$ThemeFileInfo;->mInput:Ljava/io/InputStream;

    .line 171
    .local v1, input:Ljava/io/InputStream;
    const-string v6, ".9.png"

    invoke-virtual {v4, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 173
    :cond_3
    invoke-static {p0, p1, v1, v4, v3}, Landroid/graphics/drawable/Drawable;->createFromResourceStream(Landroid/content/res/Resources;Landroid/util/TypedValue;Ljava/io/InputStream;Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 178
    if-eqz v5, :cond_0

    .line 179
    :try_start_1
    iget-object v6, v5, Loppo/content/res/OppoThemeZipFile$ThemeFileInfo;->mInput:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 181
    :catch_0
    move-exception v6

    goto :goto_0

    .line 174
    .end local v1           #input:Ljava/io/InputStream;
    .end local v3           #options:Landroid/graphics/BitmapFactory$Options;
    :catch_1
    move-exception v2

    .line 175
    .local v2, localOutOfMemoryError:Ljava/lang/OutOfMemoryError;
    :try_start_2
    const-string v6, "OppoResources"

    const-string v7, "out of memory !!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 178
    if-eqz v5, :cond_0

    .line 179
    :try_start_3
    iget-object v6, v5, Loppo/content/res/OppoThemeZipFile$ThemeFileInfo;->mInput:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 181
    :catch_2
    move-exception v6

    goto :goto_0

    .line 177
    .end local v2           #localOutOfMemoryError:Ljava/lang/OutOfMemoryError;
    :catchall_0
    move-exception v6

    .line 178
    if-eqz v5, :cond_4

    .line 179
    :try_start_4
    iget-object v7, v5, Loppo/content/res/OppoThemeZipFile$ThemeFileInfo;->mInput:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 182
    :cond_4
    :goto_1
    throw v6

    .line 181
    :catch_3
    move-exception v7

    goto :goto_1
.end method

.method public newTheme()Landroid/content/res/Resources$Theme;
    .locals 1

    .prologue
    .line 423
    new-instance v0, Landroid/content/res/OppoResources$OppoTheme;

    invoke-direct {v0, p0}, Landroid/content/res/OppoResources$OppoTheme;-><init>(Landroid/content/res/OppoResources;)V

    return-object v0
.end method

.method public obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    .locals 1
    .parameter "set"
    .parameter "attrs"

    .prologue
    .line 419
    invoke-super {p0, p1, p2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/content/res/OppoResources;->replaceTypedArray(Landroid/content/res/TypedArray;)Landroid/content/res/TypedArray;

    move-result-object v0

    return-object v0
.end method

.method public openRawResource(ILandroid/util/TypedValue;)Ljava/io/InputStream;
    .locals 7
    .parameter "id"
    .parameter "outValue"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 276
    const/4 v3, 0x0

    .line 277
    .local v3, themeFileInfo:Loppo/content/res/OppoThemeZipFile$ThemeFileInfo;
    const/4 v0, 0x0

    .line 278
    .local v0, input:Ljava/io/InputStream;
    iget-boolean v4, p0, Landroid/content/res/OppoResources;->mThemeChangeEnable:Z

    if-eqz v4, :cond_1

    iget-boolean v4, p0, Landroid/content/res/OppoResources;->mIsHasValues:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Landroid/content/res/OppoResources;->mSkipFiles:Landroid/util/SparseArray;

    invoke-virtual {v4, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_1

    iget-object v4, p0, Landroid/content/res/OppoResources;->mThemeResources:Loppo/content/res/OppoThemeResources;

    if-eqz v4, :cond_1

    .line 279
    invoke-virtual {p0, p1, p2, v6}, Landroid/content/res/OppoResources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 280
    iget-object v4, p2, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 281
    .local v2, path:Ljava/lang/String;
    iget-object v4, p0, Landroid/content/res/OppoResources;->mThemeResources:Loppo/content/res/OppoThemeResources;

    iget v5, p2, Landroid/util/TypedValue;->assetCookie:I

    invoke-direct {p0, v5}, Landroid/content/res/OppoResources;->getCookieType(I)I

    move-result v5

    invoke-virtual {v4, v5, v2}, Loppo/content/res/OppoThemeResources;->getThemeFileStream(ILjava/lang/String;)Loppo/content/res/OppoThemeZipFile$ThemeFileInfo;

    move-result-object v3

    .line 283
    if-eqz v3, :cond_0

    .line 284
    iget-object v0, v3, Loppo/content/res/OppoThemeZipFile$ThemeFileInfo;->mInput:Ljava/io/InputStream;

    move-object v1, v0

    .line 293
    .end local v0           #input:Ljava/io/InputStream;
    .end local v2           #path:Ljava/lang/String;
    .local v1, input:Ljava/io/InputStream;
    :goto_0
    return-object v1

    .line 288
    .end local v1           #input:Ljava/io/InputStream;
    .restart local v0       #input:Ljava/io/InputStream;
    .restart local v2       #path:Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Landroid/content/res/OppoResources;->mSkipFiles:Landroid/util/SparseArray;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, p1, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 291
    .end local v2           #path:Ljava/lang/String;
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/content/res/Resources;->openRawResource(ILandroid/util/TypedValue;)Ljava/io/InputStream;

    move-result-object v0

    move-object v1, v0

    .line 293
    .end local v0           #input:Ljava/io/InputStream;
    .restart local v1       #input:Ljava/io/InputStream;
    goto :goto_0
.end method

.method public setIsThemeChanged(Z)V
    .locals 0
    .parameter "changed"

    .prologue
    .line 86
    sput-boolean p1, Landroid/content/res/OppoResources;->themeChanged:Z

    .line 87
    return-void
.end method

.method public setThemeChangeEnable(Ljava/lang/String;)V
    .locals 1
    .parameter "packageName"

    .prologue
    .line 75
    iget-boolean v0, p0, Landroid/content/res/OppoResources;->mIsHasValues:Z

    invoke-static {v0, p1}, Lcom/oppo/theme/OppoApkChangedInfo;->themeChangeEnable(ZLjava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Landroid/content/res/OppoResources;->mThemeChangeEnable:Z

    .line 76
    return-void
.end method

.method public setThemeChangeEnable(Z)V
    .locals 0
    .parameter "changeThemeEnable"

    .prologue
    .line 71
    iput-boolean p1, p0, Landroid/content/res/OppoResources;->mThemeChangeEnable:Z

    .line 72
    return-void
.end method

.method public updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;Landroid/content/res/CompatibilityInfo;)V
    .locals 3
    .parameter "config"
    .parameter "displayMetrics"
    .parameter "compatibilityInfo"

    .prologue
    .line 347
    const/4 v1, 0x0

    .line 349
    .local v1, i:I
    invoke-virtual {p0}, Landroid/content/res/OppoResources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 351
    .local v0, configuration:Landroid/content/res/Configuration;
    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    .line 352
    invoke-virtual {v0, p1}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    move-result v1

    .line 357
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;Landroid/content/res/CompatibilityInfo;)V

    .line 359
    iget-object v2, p0, Landroid/content/res/OppoResources;->mThemeResources:Loppo/content/res/OppoThemeResources;

    if-eqz v2, :cond_1

    and-int/lit16 v2, v1, 0x200

    if-nez v2, :cond_0

    invoke-static {v1}, Loppo/content/res/OppoExtraConfiguration;->needNewResources(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 362
    :cond_0
    invoke-virtual {p0}, Landroid/content/res/OppoResources;->clearCache()V

    .line 363
    iget-object v2, p0, Landroid/content/res/OppoResources;->mIntegers:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->clear()V

    .line 364
    iget-object v2, p0, Landroid/content/res/OppoResources;->mCharSequences:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->clear()V

    .line 365
    iget-object v2, p0, Landroid/content/res/OppoResources;->mSkipFiles:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->clear()V

    .line 366
    iget-object v2, p0, Landroid/content/res/OppoResources;->mThemeResources:Loppo/content/res/OppoThemeResources;

    invoke-virtual {v2}, Loppo/content/res/OppoThemeResources;->checkUpdate()Z

    .line 367
    iget-object v2, p0, Landroid/content/res/OppoResources;->mThemeResources:Loppo/content/res/OppoThemeResources;

    invoke-virtual {v2}, Loppo/content/res/OppoThemeResources;->hasValues()Z

    move-result v2

    iput-boolean v2, p0, Landroid/content/res/OppoResources;->mIsHasValues:Z

    .line 370
    :cond_1
    return-void

    .line 354
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
