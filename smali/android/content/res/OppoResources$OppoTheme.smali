.class public Landroid/content/res/OppoResources$OppoTheme;
.super Landroid/content/res/Resources$Theme;
.source "OppoResources.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/res/OppoResources;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OppoTheme"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/content/res/OppoResources;


# direct methods
.method constructor <init>(Landroid/content/res/OppoResources;)V
    .locals 0
    .parameter

    .prologue
    .line 399
    iput-object p1, p0, Landroid/content/res/OppoResources$OppoTheme;->this$0:Landroid/content/res/OppoResources;

    .line 400
    invoke-direct {p0, p1}, Landroid/content/res/Resources$Theme;-><init>(Landroid/content/res/Resources;)V

    .line 401
    return-void
.end method


# virtual methods
.method public obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;
    .locals 2
    .parameter "resid"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    .prologue
    .line 410
    iget-object v0, p0, Landroid/content/res/OppoResources$OppoTheme;->this$0:Landroid/content/res/OppoResources;

    invoke-super {p0, p1, p2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v1

    #calls: Landroid/content/res/OppoResources;->replaceTypedArray(Landroid/content/res/TypedArray;)Landroid/content/res/TypedArray;
    invoke-static {v0, v1}, Landroid/content/res/OppoResources;->access$000(Landroid/content/res/OppoResources;Landroid/content/res/TypedArray;)Landroid/content/res/TypedArray;

    move-result-object v0

    return-object v0
.end method

.method public obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
    .locals 2
    .parameter "set"
    .parameter "attrs"
    .parameter "defStyleAttr"
    .parameter "defStyleRes"

    .prologue
    .line 415
    iget-object v0, p0, Landroid/content/res/OppoResources$OppoTheme;->this$0:Landroid/content/res/OppoResources;

    invoke-super {p0, p1, p2, p3, p4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    #calls: Landroid/content/res/OppoResources;->replaceTypedArray(Landroid/content/res/TypedArray;)Landroid/content/res/TypedArray;
    invoke-static {v0, v1}, Landroid/content/res/OppoResources;->access$000(Landroid/content/res/OppoResources;Landroid/content/res/TypedArray;)Landroid/content/res/TypedArray;

    move-result-object v0

    return-object v0
.end method

.method public obtainStyledAttributes([I)Landroid/content/res/TypedArray;
    .locals 2
    .parameter "attrs"

    .prologue
    .line 404
    iget-object v0, p0, Landroid/content/res/OppoResources$OppoTheme;->this$0:Landroid/content/res/OppoResources;

    invoke-super {p0, p1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    #calls: Landroid/content/res/OppoResources;->replaceTypedArray(Landroid/content/res/TypedArray;)Landroid/content/res/TypedArray;
    invoke-static {v0, v1}, Landroid/content/res/OppoResources;->access$000(Landroid/content/res/OppoResources;Landroid/content/res/TypedArray;)Landroid/content/res/TypedArray;

    move-result-object v0

    return-object v0
.end method
