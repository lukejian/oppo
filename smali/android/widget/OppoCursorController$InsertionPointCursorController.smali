.class public Landroid/widget/OppoCursorController$InsertionPointCursorController;
.super Landroid/widget/OppoCursorController;
.source "OppoCursorController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/OppoCursorController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InsertionPointCursorController"
.end annotation


# instance fields
.field private mCoords:[I

.field private mDownScrollX:I

.field private mDownScrollY:I

.field private mFousedFlag:Z

.field private final mHandle:Landroid/widget/OppoCursorController$OppoHandleView;

.field private mPanel:Landroid/widget/OppoCursorController$FloatPanelViewController;

.field private mPreviousDownTime:J

.field mShowOnOneShot:Z

.field private mTextOffset:I


# direct methods
.method constructor <init>(Landroid/widget/OppoEditor;Landroid/content/Context;)V
    .locals 3
    .parameter "editor"
    .parameter "context"

    .prologue
    .line 183
    invoke-direct {p0, p1, p2}, Landroid/widget/OppoCursorController;-><init>(Landroid/widget/OppoEditor;Landroid/content/Context;)V

    .line 184
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, p0}, Landroid/widget/OppoCursorController$InsertionPointCursorController;->initHandleView(Landroid/widget/OppoEditor;ILandroid/widget/OppoCursorController;)Landroid/widget/OppoCursorController$OppoHandleView;

    move-result-object v1

    iput-object v1, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mHandle:Landroid/widget/OppoCursorController$OppoHandleView;

    .line 185
    const v1, 0xc09042d

    invoke-virtual {p0, v1}, Landroid/widget/OppoCursorController$InsertionPointCursorController;->createFloatPanelViewController(I)Landroid/widget/OppoCursorController$FloatPanelViewController;

    move-result-object v1

    iput-object v1, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mPanel:Landroid/widget/OppoCursorController$FloatPanelViewController;

    .line 186
    const/4 v1, 0x2

    new-array v1, v1, [I

    iput-object v1, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mCoords:[I

    .line 187
    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mShowOnOneShot:Z

    .line 189
    new-instance v0, Landroid/widget/OppoCursorController$InsertionPointCursorController$1;

    invoke-direct {v0, p0}, Landroid/widget/OppoCursorController$InsertionPointCursorController$1;-><init>(Landroid/widget/OppoCursorController$InsertionPointCursorController;)V

    .line 198
    .local v0, onClickListener:Landroid/view/View$OnClickListener;
    iget-object v1, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mPanel:Landroid/widget/OppoCursorController$FloatPanelViewController;

    const v2, 0xc020450

    invoke-virtual {v1, v2, v0}, Landroid/widget/OppoCursorController$FloatPanelViewController;->setButtonOnClickListener(ILandroid/view/View$OnClickListener;)V

    .line 199
    iget-object v1, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mPanel:Landroid/widget/OppoCursorController$FloatPanelViewController;

    const v2, 0xc020451

    invoke-virtual {v1, v2, v0}, Landroid/widget/OppoCursorController$FloatPanelViewController;->setButtonOnClickListener(ILandroid/view/View$OnClickListener;)V

    .line 200
    iget-object v1, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mPanel:Landroid/widget/OppoCursorController$FloatPanelViewController;

    const v2, 0xc02044f

    invoke-virtual {v1, v2, v0}, Landroid/widget/OppoCursorController$FloatPanelViewController;->setButtonOnClickListener(ILandroid/view/View$OnClickListener;)V

    .line 201
    iget-object v1, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mPanel:Landroid/widget/OppoCursorController$FloatPanelViewController;

    const v2, 0xc020452

    invoke-virtual {v1, v2, v0}, Landroid/widget/OppoCursorController$FloatPanelViewController;->setButtonOnClickListener(ILandroid/view/View$OnClickListener;)V

    .line 202
    return-void
.end method

.method static synthetic access$000(Landroid/widget/OppoCursorController$InsertionPointCursorController;)Landroid/widget/OppoCursorController$FloatPanelViewController;
    .locals 1
    .parameter "x0"

    .prologue
    .line 170
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mPanel:Landroid/widget/OppoCursorController$FloatPanelViewController;

    return-object v0
.end method

.method private showInsertionPanel()V
    .locals 2

    .prologue
    .line 311
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mPanel:Landroid/widget/OppoCursorController$FloatPanelViewController;

    invoke-virtual {v0}, Landroid/widget/OppoCursorController$FloatPanelViewController;->showAndUpdatePositionAsync()V

    .line 313
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mEditor:Landroid/widget/OppoEditor;

    invoke-virtual {v0}, Landroid/widget/OppoEditor;->getOppoSelectionController()Landroid/widget/OppoCursorController;

    move-result-object v0

    iget v1, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mTextOffset:I

    invoke-virtual {v0, v1}, Landroid/widget/OppoCursorController;->setMinMaxOffset(I)V

    .line 314
    return-void
.end method


# virtual methods
.method public computePanelPosition([I)V
    .locals 3
    .parameter "postion"

    .prologue
    .line 220
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mHandle:Landroid/widget/OppoCursorController$OppoHandleView;

    iget-object v1, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mHandle:Landroid/widget/OppoCursorController$OppoHandleView;

    invoke-virtual {v1}, Landroid/widget/OppoCursorController$OppoHandleView;->getCurrentCursorOffset()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/OppoCursorController$OppoHandleView;->updatePositionXY(IZ)V

    .line 221
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mHandle:Landroid/widget/OppoCursorController$OppoHandleView;

    invoke-virtual {v0, p1}, Landroid/widget/OppoCursorController$OppoHandleView;->getHotspotLocationOnScreen([I)V

    .line 222
    return-void
.end method

.method public computePanelPositionOnBottom()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 225
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mHandle:Landroid/widget/OppoCursorController$OppoHandleView;

    iget-object v1, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mHandle:Landroid/widget/OppoCursorController$OppoHandleView;

    invoke-virtual {v1}, Landroid/widget/OppoCursorController$OppoHandleView;->getCurrentCursorOffset()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/OppoCursorController$OppoHandleView;->updatePositionXY(IZ)V

    .line 226
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mHandle:Landroid/widget/OppoCursorController$OppoHandleView;

    iget-object v1, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mCoords:[I

    invoke-virtual {v0, v1}, Landroid/widget/OppoCursorController$OppoHandleView;->getHotspotLocationOnScreen([I)V

    .line 227
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mHandle:Landroid/widget/OppoCursorController$OppoHandleView;

    invoke-virtual {v0}, Landroid/widget/OppoCursorController$OppoHandleView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 228
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mCoords:[I

    aget v0, v0, v3

    iget-object v1, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mHandle:Landroid/widget/OppoCursorController$OppoHandleView;

    invoke-virtual {v1}, Landroid/widget/OppoCursorController$OppoHandleView;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 230
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mCoords:[I

    aget v0, v0, v3

    goto :goto_0
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mPanel:Landroid/widget/OppoCursorController$FloatPanelViewController;

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mPanel:Landroid/widget/OppoCursorController$FloatPanelViewController;

    invoke-virtual {v0}, Landroid/widget/OppoCursorController$FloatPanelViewController;->hide()V

    .line 392
    :cond_0
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mHandle:Landroid/widget/OppoCursorController$OppoHandleView;

    if-eqz v0, :cond_1

    .line 393
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mHandle:Landroid/widget/OppoCursorController$OppoHandleView;

    invoke-virtual {v0}, Landroid/widget/OppoCursorController$OppoHandleView;->hide()V

    .line 395
    :cond_1
    return-void
.end method

.method public isShowing()Z
    .locals 2

    .prologue
    .line 399
    iget-object v1, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mHandle:Landroid/widget/OppoCursorController$OppoHandleView;

    invoke-virtual {v1}, Landroid/widget/OppoCursorController$OppoHandleView;->isShowing()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mPanel:Landroid/widget/OppoCursorController$FloatPanelViewController;

    invoke-virtual {v1}, Landroid/widget/OppoCursorController$FloatPanelViewController;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 400
    :cond_0
    const/4 v0, 0x1

    .line 404
    .local v0, flag:Z
    :goto_0
    return v0

    .line 402
    .end local v0           #flag:Z
    :cond_1
    const/4 v0, 0x0

    .restart local v0       #flag:Z
    goto :goto_0
.end method

.method onClipBoardPancelClick()V
    .locals 0

    .prologue
    .line 408
    return-void
.end method

.method public onHandleTouchEvent(Landroid/widget/OppoCursorController$OppoHandleView;Landroid/view/MotionEvent;)Z
    .locals 4
    .parameter "oppohandleview"
    .parameter "ev"

    .prologue
    .line 281
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 307
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 283
    :pswitch_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mPreviousDownTime:J

    goto :goto_0

    .line 287
    :pswitch_1
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mPanel:Landroid/widget/OppoCursorController$FloatPanelViewController;

    invoke-virtual {v0}, Landroid/widget/OppoCursorController$FloatPanelViewController;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 288
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mPanel:Landroid/widget/OppoCursorController$FloatPanelViewController;

    invoke-virtual {v0}, Landroid/widget/OppoCursorController$FloatPanelViewController;->hide()V

    .line 290
    :cond_1
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mHandle:Landroid/widget/OppoCursorController$OppoHandleView;

    invoke-virtual {v0}, Landroid/widget/OppoCursorController$OppoHandleView;->onHandleMoved()V

    goto :goto_0

    .line 295
    :pswitch_2
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mPanel:Landroid/widget/OppoCursorController$FloatPanelViewController;

    invoke-virtual {v0}, Landroid/widget/OppoCursorController$FloatPanelViewController;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mPreviousDownTime:J

    sub-long/2addr v0, v2

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_2

    .line 298
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mPanel:Landroid/widget/OppoCursorController$FloatPanelViewController;

    invoke-virtual {v0}, Landroid/widget/OppoCursorController$FloatPanelViewController;->hide()V

    goto :goto_0

    .line 300
    :cond_2
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mEditor:Landroid/widget/OppoEditor;

    invoke-virtual {v0}, Landroid/widget/OppoEditor;->needInsertPanel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 301
    invoke-direct {p0}, Landroid/widget/OppoCursorController$InsertionPointCursorController;->showInsertionPanel()V

    goto :goto_0

    .line 281
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .parameter "ev"

    .prologue
    const/4 v4, 0x0

    .line 235
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 277
    :cond_0
    :goto_0
    return v4

    .line 237
    :pswitch_0
    iget-object v3, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getScrollX()I

    move-result v3

    iput v3, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mDownScrollX:I

    .line 238
    iget-object v3, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getScrollY()I

    move-result v3

    iput v3, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mDownScrollY:I

    goto :goto_0

    .line 241
    :pswitch_1
    iget-object v3, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mPanel:Landroid/widget/OppoCursorController$FloatPanelViewController;

    invoke-virtual {v3}, Landroid/widget/OppoCursorController$FloatPanelViewController;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 242
    invoke-virtual {p0}, Landroid/widget/OppoCursorController$InsertionPointCursorController;->hide()V

    goto :goto_0

    .line 246
    :pswitch_2
    iget-object v3, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getScrollY()I

    move-result v2

    .line 247
    .local v2, upScrollY:I
    iget-object v3, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getScrollX()I

    move-result v1

    .line 248
    .local v1, upScrollX:I
    const/4 v0, 0x1

    .line 249
    .local v0, isScroll:Z
    iget v3, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mDownScrollY:I

    if-ne v2, v3, :cond_1

    iget v3, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mDownScrollX:I

    if-eq v1, v3, :cond_2

    .line 250
    :cond_1
    const/4 v0, 0x0

    .line 252
    :cond_2
    iget-object v3, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->isFocused()Z

    move-result v3

    iput-boolean v3, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mFousedFlag:Z

    .line 261
    iget-object v3, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mEditor:Landroid/widget/OppoEditor;

    invoke-virtual {v3}, Landroid/widget/OppoEditor;->getOppoSelectionController()Landroid/widget/OppoCursorController;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/OppoCursorController;->isShowing()Z

    move-result v3

    if-nez v3, :cond_3

    .line 262
    if-eqz v0, :cond_3

    iget-object v3, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mEditor:Landroid/widget/OppoEditor;

    invoke-virtual {v3}, Landroid/widget/OppoEditor;->needInsertPanel()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 265
    invoke-virtual {p0}, Landroid/widget/OppoCursorController$InsertionPointCursorController;->show()V

    .line 268
    :cond_3
    iget-object v3, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mEditor:Landroid/widget/OppoEditor;

    invoke-virtual {v3, v4}, Landroid/widget/OppoEditor;->setLongPressed(Z)V

    .line 270
    iput v4, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mDownScrollX:I

    .line 271
    iput v4, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mDownScrollY:I

    goto :goto_0

    .line 235
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public show()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x4

    const/4 v7, 0x1

    .line 340
    new-array v3, v8, [I

    fill-array-data v3, :array_0

    .line 342
    .local v3, selectButton:[I
    const/4 v6, 0x3

    new-array v4, v6, [I

    fill-array-data v4, :array_1

    .line 345
    .local v4, separatorButton:[I
    iget-object v6, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mEditor:Landroid/widget/OppoEditor;

    invoke-virtual {v6, v3}, Landroid/widget/OppoEditor;->getFloatPanelShowHides([I)Ljava/util/ArrayList;

    move-result-object v0

    .line 346
    .local v0, arraylist:Ljava/util/ArrayList;
    iget-object v6, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mPanel:Landroid/widget/OppoCursorController$FloatPanelViewController;

    invoke-virtual {v6, v3, v4, v0}, Landroid/widget/OppoCursorController$FloatPanelViewController;->showHideButtons([I[ILjava/util/ArrayList;)V

    .line 347
    iget-object v6, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mPanel:Landroid/widget/OppoCursorController$FloatPanelViewController;

    invoke-virtual {v6}, Landroid/widget/OppoCursorController$FloatPanelViewController;->hide()V

    .line 351
    iget-object v6, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getSelectionStart()I

    move-result v5

    .line 352
    .local v5, start:I
    iget v6, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mTextOffset:I

    if-eq v5, v6, :cond_0

    iget-object v6, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->length()I

    move-result v6

    if-gtz v6, :cond_5

    .line 353
    :cond_0
    const/4 v1, 0x1

    .line 358
    .local v1, canSelectFlag:Z
    :goto_0
    iput v5, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mTextOffset:I

    .line 359
    iget-object v6, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mEditor:Landroid/widget/OppoEditor;

    invoke-virtual {v6}, Landroid/widget/OppoEditor;->hasMagnifierController()Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v6, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mEditor:Landroid/widget/OppoEditor;

    invoke-virtual {v6}, Landroid/widget/OppoEditor;->getMagnifierController()Landroid/widget/OppoMagnifierController;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/OppoMagnifierController;->isMagnifierShowing()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 361
    move v2, v1

    .line 365
    .local v2, magnifierFlag:Z
    :goto_1
    iget-object v6, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mHandle:Landroid/widget/OppoCursorController$OppoHandleView;

    invoke-virtual {v6, v9}, Landroid/widget/OppoCursorController$OppoHandleView;->setVisibility(I)V

    .line 366
    iget-object v6, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mHandle:Landroid/widget/OppoCursorController$OppoHandleView;

    invoke-virtual {v6}, Landroid/widget/OppoCursorController$OppoHandleView;->show()V

    .line 367
    iget-object v6, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mEditor:Landroid/widget/OppoEditor;

    invoke-virtual {v6}, Landroid/widget/OppoEditor;->getLongPressed()Z

    move-result v6

    if-ne v7, v6, :cond_1

    .line 368
    iput-boolean v7, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mFousedFlag:Z

    .line 370
    :cond_1
    if-eqz v1, :cond_4

    if-nez v2, :cond_4

    .line 371
    iget-boolean v6, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mFousedFlag:Z

    if-eqz v6, :cond_7

    iget-object v6, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mEditor:Landroid/widget/OppoEditor;

    invoke-virtual {v6}, Landroid/widget/OppoEditor;->needInsertPanel()Z

    move-result v6

    if-eqz v6, :cond_7

    iget-boolean v6, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mShowOnOneShot:Z

    if-eqz v6, :cond_7

    .line 372
    invoke-direct {p0}, Landroid/widget/OppoCursorController$InsertionPointCursorController;->showInsertionPanel()V

    .line 373
    iget-object v6, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->length()I

    move-result v6

    if-nez v6, :cond_2

    .line 374
    iget-object v6, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mHandle:Landroid/widget/OppoCursorController$OppoHandleView;

    invoke-virtual {v6, v8}, Landroid/widget/OppoCursorController$OppoHandleView;->setVisibility(I)V

    .line 380
    :cond_2
    :goto_2
    iget-boolean v6, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mShowOnOneShot:Z

    if-eqz v6, :cond_3

    .line 381
    iput-boolean v9, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mShowOnOneShot:Z

    .line 383
    :cond_3
    iput-boolean v7, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mShowOnOneShot:Z

    .line 386
    :cond_4
    return-void

    .line 355
    .end local v1           #canSelectFlag:Z
    .end local v2           #magnifierFlag:Z
    :cond_5
    const/4 v1, 0x0

    .restart local v1       #canSelectFlag:Z
    goto :goto_0

    .line 363
    :cond_6
    const/4 v2, 0x0

    .restart local v2       #magnifierFlag:Z
    goto :goto_1

    .line 378
    :cond_7
    iget-object v6, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mHandle:Landroid/widget/OppoCursorController$OppoHandleView;

    invoke-virtual {v6, v8}, Landroid/widget/OppoCursorController$OppoHandleView;->setVisibility(I)V

    goto :goto_2

    .line 340
    nop

    :array_0
    .array-data 0x4
        0x50t 0x4t 0x2t 0xct
        0x51t 0x4t 0x2t 0xct
        0x4ft 0x4t 0x2t 0xct
        0x52t 0x4t 0x2t 0xct
    .end array-data

    .line 342
    :array_1
    .array-data 0x4
        0x53t 0x4t 0x2t 0xct
        0x54t 0x4t 0x2t 0xct
        0x55t 0x4t 0x2t 0xct
    .end array-data
.end method

.method public updatePosition()V
    .locals 3

    .prologue
    .line 205
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getSelectionStart()I

    move-result v0

    iput v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mTextOffset:I

    .line 206
    iget v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mTextOffset:I

    if-gez v0, :cond_1

    .line 207
    invoke-virtual {p0}, Landroid/widget/OppoCursorController$InsertionPointCursorController;->hide()V

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 209
    :cond_1
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mPanel:Landroid/widget/OppoCursorController$FloatPanelViewController;

    invoke-virtual {v0}, Landroid/widget/OppoCursorController$FloatPanelViewController;->isShowing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 210
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mHandle:Landroid/widget/OppoCursorController$OppoHandleView;

    iget v1, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mTextOffset:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/OppoCursorController$OppoHandleView;->positionAtCursorOffset(IZ)V

    .line 213
    :cond_2
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mPanel:Landroid/widget/OppoCursorController$FloatPanelViewController;

    invoke-virtual {v0}, Landroid/widget/OppoCursorController$FloatPanelViewController;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Landroid/widget/OppoCursorController$InsertionPointCursorController;->mPanel:Landroid/widget/OppoCursorController$FloatPanelViewController;

    invoke-virtual {v0}, Landroid/widget/OppoCursorController$FloatPanelViewController;->showAndUpdatePositionAsync()V

    goto :goto_0
.end method
