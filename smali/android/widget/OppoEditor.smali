.class public Landroid/widget/OppoEditor;
.super Landroid/widget/Editor;
.source "OppoEditor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/OppoEditor$PastePanelOnClickListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "OppoEditor"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCursrOffset:I

.field private mFlag:I

.field private mHasOnTouchListener:Z

.field private mIsInTextSelectionMode:Z

.field private mLongPressed:Z

.field private mMagnifierController:Landroid/widget/OppoMagnifierController;

.field mOppoInsertionCursorController:Landroid/widget/OppoCursorController;

.field mOppoSelectionCursorController:Landroid/widget/OppoCursorController;

.field private mSelectHandleCenter:Landroid/graphics/drawable/Drawable;

.field private mSelectHandleLeft:Landroid/graphics/drawable/Drawable;

.field private mSelectHandleRight:Landroid/graphics/drawable/Drawable;

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/widget/TextView;)V
    .locals 2
    .parameter "textView"

    .prologue
    const/4 v0, 0x0

    .line 82
    invoke-direct {p0, p1}, Landroid/widget/Editor;-><init>(Landroid/widget/TextView;)V

    .line 68
    iput-boolean v0, p0, Landroid/widget/OppoEditor;->mIsInTextSelectionMode:Z

    .line 76
    iput v0, p0, Landroid/widget/OppoEditor;->mFlag:I

    .line 77
    iput-boolean v0, p0, Landroid/widget/OppoEditor;->mHasOnTouchListener:Z

    .line 78
    iput-boolean v0, p0, Landroid/widget/OppoEditor;->mLongPressed:Z

    .line 83
    iput-object p1, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    .line 84
    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Landroid/widget/OppoEditor;->mContext:Landroid/content/Context;

    .line 85
    iget-object v0, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0xc050431

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Landroid/widget/OppoEditor;->mCursrOffset:I

    .line 87
    return-void
.end method

.method static synthetic access$000(Landroid/widget/OppoEditor;)Landroid/widget/TextView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 60
    iget-object v0, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method private canSelectText()Z
    .locals 1

    .prologue
    .line 483
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->hasSelectionController()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getPrimaryHorizontal(Landroid/text/Layout;Landroid/text/Layout;I)F
    .locals 1
    .parameter "layout"
    .parameter "hintLayout"
    .parameter "offset"

    .prologue
    .line 326
    invoke-virtual {p1}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 328
    invoke-virtual {p2, p3}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v0

    .line 330
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1, p3}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method getButtonShowHides(ZI)Z
    .locals 5
    .parameter "isTextEditable"
    .parameter "id"

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 488
    :try_start_0
    iget-object v4, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getTransformationMethod()Landroid/text/method/TransformationMethod;

    move-result-object v4

    instance-of v1, v4, Landroid/text/method/PasswordTransformationMethod;

    .line 489
    .local v1, passwordTransformed:Z
    sparse-switch p2, :sswitch_data_0

    .line 533
    .end local v1           #passwordTransformed:Z
    :cond_0
    :goto_0
    return v3

    .line 492
    .restart local v1       #passwordTransformed:Z
    :sswitch_0
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->isSelectionToolbarEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 495
    invoke-direct {p0}, Landroid/widget/OppoEditor;->canSelectText()Z

    move-result v4

    if-eqz v4, :cond_1

    if-nez v1, :cond_1

    :goto_1
    move v3, v2

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1

    .line 498
    :sswitch_1
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->isCutAndPasteEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 501
    if-eqz p1, :cond_2

    iget-object v4, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->canPaste()Z

    move-result v4

    if-eqz v4, :cond_2

    :goto_2
    move v3, v2

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_2

    .line 512
    :sswitch_2
    iget-object v2, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->canCopy()Z

    move-result v3

    goto :goto_0

    .line 515
    :sswitch_3
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->isCutAndPasteEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 518
    if-eqz p1, :cond_3

    iget-object v4, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->canCut()Z

    move-result v4

    if-eqz v4, :cond_3

    :goto_3
    move v3, v2

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_3

    .line 520
    :sswitch_4
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->isImSwitcherEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    move v3, v2

    .line 522
    goto :goto_0

    .line 527
    :sswitch_5
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->isSearchEnabled()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    goto :goto_0

    .line 531
    .end local v1           #passwordTransformed:Z
    :catch_0
    move-exception v0

    .line 532
    .local v0, e:Ljava/lang/SecurityException;
    const-string v2, "OppoEditor"

    invoke-virtual {v0}, Ljava/lang/SecurityException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 489
    nop

    :sswitch_data_0
    .sparse-switch
        0xc02044d -> :sswitch_2
        0xc02044e -> :sswitch_3
        0xc02044f -> :sswitch_1
        0xc020450 -> :sswitch_0
        0xc020451 -> :sswitch_0
        0xc020452 -> :sswitch_4
        0xc0204a7 -> :sswitch_5
    .end sparse-switch
.end method

.method getFloatPanelShowHides([I)Ljava/util/ArrayList;
    .locals 7
    .parameter "buttons"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 448
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 449
    .local v5, showHides:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    iget-object v6, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->isTextEditable()Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 450
    .local v3, isTextEditable:Ljava/lang/Boolean;
    move-object v0, p1

    .local v0, arr$:[I
    array-length v4, v0

    .local v4, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_0
    if-ge v2, v4, :cond_0

    aget v1, v0, v2

    .line 451
    .local v1, button:I
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    invoke-virtual {p0, v6, v1}, Landroid/widget/OppoEditor;->getButtonShowHides(ZI)Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 450
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 453
    .end local v1           #button:I
    :cond_0
    return-object v5
.end method

.method getLastTapPositionWrap()I
    .locals 2

    .prologue
    .line 285
    iget-object v1, p0, Landroid/widget/OppoEditor;->mOppoSelectionCursorController:Landroid/widget/OppoCursorController;

    if-eqz v1, :cond_1

    .line 286
    iget-object v1, p0, Landroid/widget/OppoEditor;->mOppoSelectionCursorController:Landroid/widget/OppoCursorController;

    invoke-virtual {v1}, Landroid/widget/OppoCursorController;->getMinTouchOffset()I

    move-result v0

    .line 287
    .local v0, lastTapPosition:I
    if-ltz v0, :cond_1

    .line 289
    iget-object v1, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 290
    iget-object v1, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 295
    .end local v0           #lastTapPosition:I
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method getLastTouchOffsetsWrap()J
    .locals 5

    .prologue
    .line 266
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->getOppoSelectionController()Landroid/widget/OppoCursorController;

    move-result-object v2

    .line 267
    .local v2, selectionController:Landroid/widget/OppoCursorController;
    invoke-virtual {v2}, Landroid/widget/OppoCursorController;->getMinTouchOffset()I

    move-result v1

    .line 268
    .local v1, minOffset:I
    invoke-virtual {v2}, Landroid/widget/OppoCursorController;->getMaxTouchOffset()I

    move-result v0

    .line 269
    .local v0, maxOffset:I
    invoke-static {v1, v0}, Landroid/text/TextUtils;->packRangeInLong(II)J

    move-result-wide v3

    return-wide v3
.end method

.method public getLongPressed()Z
    .locals 1

    .prologue
    .line 412
    iget-boolean v0, p0, Landroid/widget/OppoEditor;->mLongPressed:Z

    return v0
.end method

.method getMagnifierController()Landroid/widget/OppoMagnifierController;
    .locals 3

    .prologue
    .line 723
    const/4 v0, 0x0

    .line 724
    .local v0, controller:Landroid/widget/OppoMagnifierController;
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->hasMagnifierController()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 725
    iget-object v1, p0, Landroid/widget/OppoEditor;->mMagnifierController:Landroid/widget/OppoMagnifierController;

    if-nez v1, :cond_0

    .line 726
    new-instance v1, Landroid/widget/OppoMagnifierController;

    iget-object v2, p0, Landroid/widget/OppoEditor;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, p0}, Landroid/widget/OppoMagnifierController;-><init>(Landroid/content/Context;Landroid/widget/OppoEditor;)V

    iput-object v1, p0, Landroid/widget/OppoEditor;->mMagnifierController:Landroid/widget/OppoMagnifierController;

    .line 728
    :cond_0
    iget-object v0, p0, Landroid/widget/OppoEditor;->mMagnifierController:Landroid/widget/OppoMagnifierController;

    .line 730
    :cond_1
    return-object v0
.end method

.method getOppoInsertionController()Landroid/widget/OppoCursorController;
    .locals 3

    .prologue
    .line 374
    iget-boolean v1, p0, Landroid/widget/OppoEditor;->mInsertionControllerEnabled:Z

    if-nez v1, :cond_0

    .line 375
    const/4 v1, 0x0

    .line 385
    :goto_0
    return-object v1

    .line 378
    :cond_0
    iget-object v1, p0, Landroid/widget/OppoEditor;->mOppoInsertionCursorController:Landroid/widget/OppoCursorController;

    if-nez v1, :cond_1

    .line 379
    iget-object v1, p0, Landroid/widget/OppoEditor;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Landroid/widget/OppoCursorController;->create(Landroid/widget/OppoEditor;Landroid/content/Context;I)Landroid/widget/OppoCursorController;

    move-result-object v1

    iput-object v1, p0, Landroid/widget/OppoEditor;->mOppoInsertionCursorController:Landroid/widget/OppoCursorController;

    .line 381
    iget-object v1, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 382
    .local v0, observer:Landroid/view/ViewTreeObserver;
    iget-object v1, p0, Landroid/widget/OppoEditor;->mOppoInsertionCursorController:Landroid/widget/OppoCursorController;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 385
    .end local v0           #observer:Landroid/view/ViewTreeObserver;
    :cond_1
    iget-object v1, p0, Landroid/widget/OppoEditor;->mOppoInsertionCursorController:Landroid/widget/OppoCursorController;

    goto :goto_0
.end method

.method getOppoSelectionController()Landroid/widget/OppoCursorController;
    .locals 3

    .prologue
    .line 389
    iget-boolean v1, p0, Landroid/widget/OppoEditor;->mSelectionControllerEnabled:Z

    if-nez v1, :cond_0

    .line 390
    const/4 v1, 0x0

    .line 400
    :goto_0
    return-object v1

    .line 393
    :cond_0
    iget-object v1, p0, Landroid/widget/OppoEditor;->mOppoSelectionCursorController:Landroid/widget/OppoCursorController;

    if-nez v1, :cond_1

    .line 394
    iget-object v1, p0, Landroid/widget/OppoEditor;->mContext:Landroid/content/Context;

    const/4 v2, 0x2

    invoke-static {p0, v1, v2}, Landroid/widget/OppoCursorController;->create(Landroid/widget/OppoEditor;Landroid/content/Context;I)Landroid/widget/OppoCursorController;

    move-result-object v1

    iput-object v1, p0, Landroid/widget/OppoEditor;->mOppoSelectionCursorController:Landroid/widget/OppoCursorController;

    .line 396
    iget-object v1, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 397
    .local v0, observer:Landroid/view/ViewTreeObserver;
    iget-object v1, p0, Landroid/widget/OppoEditor;->mOppoSelectionCursorController:Landroid/widget/OppoCursorController;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 400
    .end local v0           #observer:Landroid/view/ViewTreeObserver;
    :cond_1
    iget-object v1, p0, Landroid/widget/OppoEditor;->mOppoSelectionCursorController:Landroid/widget/OppoCursorController;

    goto :goto_0
.end method

.method getSelectHandleCenterRes()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 424
    iget-object v0, p0, Landroid/widget/OppoEditor;->mSelectHandleCenter:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 425
    iget-object v0, p0, Landroid/widget/OppoEditor;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    iget v1, v1, Landroid/widget/TextView;->mTextSelectHandleRes:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/widget/OppoEditor;->mSelectHandleCenter:Landroid/graphics/drawable/Drawable;

    .line 428
    :cond_0
    iget-object v0, p0, Landroid/widget/OppoEditor;->mSelectHandleCenter:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method getSelectHandleLeftRes()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 432
    iget-object v0, p0, Landroid/widget/OppoEditor;->mSelectHandleLeft:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 433
    iget-object v0, p0, Landroid/widget/OppoEditor;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    iget v1, v1, Landroid/widget/TextView;->mTextSelectHandleLeftRes:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/widget/OppoEditor;->mSelectHandleLeft:Landroid/graphics/drawable/Drawable;

    .line 436
    :cond_0
    iget-object v0, p0, Landroid/widget/OppoEditor;->mSelectHandleLeft:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method getSelectHandleRightRes()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 440
    iget-object v0, p0, Landroid/widget/OppoEditor;->mSelectHandleRight:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 441
    iget-object v0, p0, Landroid/widget/OppoEditor;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    iget v1, v1, Landroid/widget/TextView;->mTextSelectHandleRightRes:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/widget/OppoEditor;->mSelectHandleRight:Landroid/graphics/drawable/Drawable;

    .line 444
    :cond_0
    iget-object v0, p0, Landroid/widget/OppoEditor;->mSelectHandleRight:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method getSelectHandleWindowStyle()I
    .locals 1

    .prologue
    .line 416
    const v0, 0x10102c8

    return v0
.end method

.method handleFloatPanelClick(Landroid/view/View;Landroid/widget/OppoCursorController;)V
    .locals 16
    .parameter "v"
    .parameter "cc"

    .prologue
    .line 601
    const/4 v8, 0x0

    .line 602
    .local v8, min:I
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->length()I

    move-result v7

    .line 604
    .local v7, max:I
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->isFocused()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 605
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getSelectionStart()I

    move-result v12

    .line 606
    .local v12, selStart:I
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v11

    .line 608
    .local v11, selEnd:I
    const/4 v13, 0x0

    invoke-static {v12, v11}, Ljava/lang/Math;->min(II)I

    move-result v14

    invoke-static {v13, v14}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 609
    const/4 v13, 0x0

    invoke-static {v12, v11}, Ljava/lang/Math;->max(II)I

    move-result v14

    invoke-static {v13, v14}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 612
    .end local v11           #selEnd:I
    .end local v12           #selStart:I
    :cond_0
    const/4 v2, 0x0

    .line 613
    .local v2, data:Landroid/content/ClipData;
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/widget/OppoEditor;->mContext:Landroid/content/Context;

    const-string v14, "clipboard"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ClipboardManager;

    .line 616
    .local v1, clip:Landroid/content/ClipboardManager;
    :try_start_0
    invoke-virtual {v1}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 621
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v13

    sparse-switch v13, :sswitch_data_0

    .line 715
    .end local p2
    :cond_1
    :goto_1
    return-void

    .line 617
    .restart local p2
    :catch_0
    move-exception v3

    .line 618
    .local v3, e:Ljava/lang/SecurityException;
    const-string v13, "OppoEditor"

    invoke-virtual {v3}, Ljava/lang/SecurityException;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 623
    .end local v3           #e:Ljava/lang/SecurityException;
    :sswitch_0
    move-object/from16 v0, p0

    iget-boolean v13, v0, Landroid/widget/OppoEditor;->mSelectionControllerEnabled:Z

    if-eqz v13, :cond_1

    .line 624
    invoke-virtual/range {p0 .. p0}, Landroid/widget/OppoEditor;->startTextSelectionMode()Z

    move-result v13

    if-eqz v13, :cond_1

    .line 625
    invoke-virtual/range {p0 .. p0}, Landroid/widget/OppoEditor;->getOppoSelectionController()Landroid/widget/OppoCursorController;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/OppoCursorController;->show()V

    goto :goto_1

    .line 632
    :sswitch_1
    move-object/from16 v0, p0

    iget-boolean v13, v0, Landroid/widget/OppoEditor;->mSelectionControllerEnabled:Z

    if-eqz v13, :cond_1

    .line 633
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v13

    check-cast v13, Landroid/text/Spannable;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v15}, Landroid/widget/TextView;->length()I

    move-result v15

    invoke-static {v13, v14, v15}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 634
    invoke-virtual/range {p0 .. p0}, Landroid/widget/OppoEditor;->startTextSelectionMode()Z

    move-result v13

    if-eqz v13, :cond_1

    .line 635
    invoke-virtual/range {p0 .. p0}, Landroid/widget/OppoEditor;->getOppoSelectionController()Landroid/widget/OppoCursorController;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/OppoCursorController;->show()V

    goto :goto_1

    .line 641
    :sswitch_2
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->isHandleEditAction(I)Z

    move-result v13

    if-nez v13, :cond_1

    if-eqz v2, :cond_1

    .line 645
    const/4 v9, 0x0

    .line 646
    .local v9, paste:Ljava/lang/CharSequence;
    invoke-virtual {v2}, Landroid/content/ClipData;->getItemCount()I

    move-result v13

    if-lez v13, :cond_2

    .line 647
    const/4 v13, 0x0

    invoke-virtual {v2, v13}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/widget/OppoEditor;->mContext:Landroid/content/Context;

    invoke-virtual {v13, v14}, Landroid/content/ClipData$Item;->coerceToText(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v9

    .line 650
    :cond_2
    if-eqz v9, :cond_1

    invoke-interface {v9}, Ljava/lang/CharSequence;->length()I

    move-result v13

    if-lez v13, :cond_1

    .line 651
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v13

    check-cast v13, Landroid/text/Spannable;

    invoke-static {v13, v7}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 652
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v13, v8, v7, v9}, Landroid/widget/TextView;->replaceText_internal(IILjava/lang/CharSequence;)V

    .line 653
    invoke-virtual/range {p0 .. p0}, Landroid/widget/OppoEditor;->stopTextSelectionMode()V

    goto/16 :goto_1

    .line 665
    .end local v9           #paste:Ljava/lang/CharSequence;
    :sswitch_3
    check-cast p2, Landroid/widget/OppoCursorController$SelectionModifierCursorController;

    .end local p2
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v13, v8, v7}, Landroid/widget/TextView;->getTransformedText(II)Ljava/lang/CharSequence;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2, v13}, Landroid/widget/OppoCursorController$SelectionModifierCursorController;->addClipData(Landroid/content/ClipboardManager;Landroid/content/ClipData;Ljava/lang/CharSequence;)V

    .line 667
    invoke-virtual/range {p0 .. p0}, Landroid/widget/OppoEditor;->stopTextSelectionMode()V

    .line 668
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->isHandleEditAction(I)Z

    .line 669
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/widget/OppoEditor;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/widget/OppoEditor;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0xc040525

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-static {v13, v14, v15}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 675
    .restart local p2
    :sswitch_4
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    const/4 v14, 0x2

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->isHandleEditAction(I)Z

    .line 676
    check-cast p2, Landroid/widget/OppoCursorController$SelectionModifierCursorController;

    .end local p2
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v13, v8, v7}, Landroid/widget/TextView;->getTransformedText(II)Ljava/lang/CharSequence;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2, v13}, Landroid/widget/OppoCursorController$SelectionModifierCursorController;->addClipData(Landroid/content/ClipboardManager;Landroid/content/ClipData;Ljava/lang/CharSequence;)V

    .line 678
    invoke-virtual/range {p0 .. p0}, Landroid/widget/OppoEditor;->stopTextSelectionMode()V

    .line 679
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v13, v8, v7}, Landroid/widget/TextView;->deleteText_internal(II)V

    .line 680
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/widget/OppoEditor;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/widget/OppoEditor;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0xc040525

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-static {v13, v14, v15}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 686
    .restart local p2
    :sswitch_5
    invoke-virtual/range {p0 .. p0}, Landroid/widget/OppoEditor;->stopTextSelectionMode()V

    .line 687
    new-instance v5, Landroid/content/Intent;

    const-string v13, "android.intent.action.WEB_SEARCH"

    invoke-direct {v5, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 688
    .local v5, intent:Landroid/content/Intent;
    const/4 v6, 0x0

    .line 689
    .local v6, intentTemp:Landroid/content/Intent;
    const-string v13, "new_search"

    const/4 v14, 0x1

    invoke-virtual {v5, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 690
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v13, v8, v7}, Landroid/widget/TextView;->getTransformedText(II)Ljava/lang/CharSequence;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    .line 691
    .local v10, query:Ljava/lang/String;
    const-string v13, "query"

    invoke-virtual {v5, v13, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 692
    const/high16 v13, 0x1000

    invoke-virtual {v5, v13}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 695
    :try_start_1
    const-string v13, "com.android.quicksearchbox"

    invoke-virtual {v5, v13}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 696
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13, v5}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 697
    :catch_1
    move-exception v3

    .line 698
    .local v3, e:Ljava/lang/Exception;
    const-string v13, "OppoEditor"

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 699
    new-instance v6, Landroid/content/Intent;

    .end local v6           #intentTemp:Landroid/content/Intent;
    const-string v13, "android.intent.action.WEB_SEARCH"

    invoke-direct {v6, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 700
    .restart local v6       #intentTemp:Landroid/content/Intent;
    const-string v13, "new_search"

    const/4 v14, 0x1

    invoke-virtual {v6, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 701
    const-string v13, "query"

    invoke-virtual {v6, v13, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 702
    const/high16 v13, 0x1000

    invoke-virtual {v6, v13}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 703
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13, v6}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 709
    .end local v3           #e:Ljava/lang/Exception;
    .end local v5           #intent:Landroid/content/Intent;
    .end local v6           #intentTemp:Landroid/content/Intent;
    .end local v10           #query:Ljava/lang/String;
    :sswitch_6
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v4

    .line 710
    .local v4, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v4, :cond_1

    .line 711
    invoke-virtual {v4}, Landroid/view/inputmethod/InputMethodManager;->showInputMethodPicker()V

    goto/16 :goto_1

    .line 621
    :sswitch_data_0
    .sparse-switch
        0xc02044d -> :sswitch_3
        0xc02044e -> :sswitch_4
        0xc02044f -> :sswitch_2
        0xc020450 -> :sswitch_0
        0xc020451 -> :sswitch_1
        0xc020452 -> :sswitch_6
        0xc0204a7 -> :sswitch_5
    .end sparse-switch
.end method

.method hasMagnifierController()Z
    .locals 1

    .prologue
    .line 718
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->isMagnifierEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/widget/OppoEditor;->mInsertionControllerEnabled:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/widget/OppoEditor;->mSelectionControllerEnabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOnTouchListener()Z
    .locals 1

    .prologue
    .line 868
    iget-boolean v0, p0, Landroid/widget/OppoEditor;->mHasOnTouchListener:Z

    return v0
.end method

.method hasPasswordTransformationMethodWrap()Z
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTransformationMethod()Landroid/text/method/TransformationMethod;

    move-result-object v0

    instance-of v0, v0, Landroid/text/method/PasswordTransformationMethod;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/widget/OppoEditor;->isAllTextSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method hideInsertionPointCursorControllerWrap()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Landroid/widget/OppoEditor;->mOppoInsertionCursorController:Landroid/widget/OppoCursorController;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Landroid/widget/OppoEditor;->mOppoInsertionCursorController:Landroid/widget/OppoCursorController;

    invoke-virtual {v0}, Landroid/widget/OppoCursorController;->hide()V

    .line 148
    :cond_0
    return-void
.end method

.method hideSelectionModifierCursorController()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Landroid/widget/OppoEditor;->mOppoSelectionCursorController:Landroid/widget/OppoCursorController;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Landroid/widget/OppoEditor;->mOppoSelectionCursorController:Landroid/widget/OppoCursorController;

    invoke-virtual {v0}, Landroid/widget/OppoCursorController;->hide()V

    .line 141
    :cond_0
    return-void
.end method

.method public isAllTextSelected()Z
    .locals 2

    .prologue
    .line 848
    iget v0, p0, Landroid/widget/OppoEditor;->mFlag:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    .line 850
    const/4 v0, 0x1

    .line 852
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCutAndPasteEnabled()Z
    .locals 2

    .prologue
    .line 856
    iget v0, p0, Landroid/widget/OppoEditor;->mFlag:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    .line 858
    const/4 v0, 0x0

    .line 860
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isImSwitcherEnabled()Z
    .locals 2

    .prologue
    .line 840
    iget v0, p0, Landroid/widget/OppoEditor;->mFlag:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    .line 842
    const/4 v0, 0x0

    .line 844
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isInsertToolbarEnabled()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 822
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->isToolbarEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 823
    iget v1, p0, Landroid/widget/OppoEditor;->mFlag:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    .line 829
    :cond_0
    :goto_0
    return v0

    .line 827
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isMagnifierEnabled()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 795
    iget v0, p0, Landroid/widget/OppoEditor;->mFlag:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 799
    :cond_0
    return v2
.end method

.method public isSearchEnabled()Z
    .locals 2

    .prologue
    .line 833
    iget v0, p0, Landroid/widget/OppoEditor;->mFlag:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    .line 835
    const/4 v0, 0x0

    .line 837
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isSelectionToolbarEnabled()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 811
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->isToolbarEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 812
    iget v1, p0, Landroid/widget/OppoEditor;->mFlag:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 818
    :cond_0
    :goto_0
    return v0

    .line 816
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isToolbarEnabled()Z
    .locals 2

    .prologue
    .line 803
    iget v0, p0, Landroid/widget/OppoEditor;->mFlag:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 805
    const/4 v0, 0x0

    .line 807
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method needInsertPanel()Z
    .locals 1

    .prologue
    .line 461
    iget-boolean v0, p0, Landroid/widget/OppoEditor;->mIsInTextSelectionMode:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/widget/OppoEditor;->isInsertToolbarEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method onAttachedToWindow()V
    .locals 2

    .prologue
    .line 91
    iget-object v1, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 92
    .local v0, observer:Landroid/view/ViewTreeObserver;
    iget-object v1, p0, Landroid/widget/OppoEditor;->mOppoInsertionCursorController:Landroid/widget/OppoCursorController;

    if-eqz v1, :cond_0

    .line 93
    iget-object v1, p0, Landroid/widget/OppoEditor;->mOppoInsertionCursorController:Landroid/widget/OppoCursorController;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 95
    :cond_0
    iget-object v1, p0, Landroid/widget/OppoEditor;->mOppoSelectionCursorController:Landroid/widget/OppoCursorController;

    if-eqz v1, :cond_1

    .line 96
    iget-object v1, p0, Landroid/widget/OppoEditor;->mOppoSelectionCursorController:Landroid/widget/OppoCursorController;

    invoke-virtual {v1}, Landroid/widget/OppoCursorController;->resetTouchOffsets()V

    .line 97
    iget-object v1, p0, Landroid/widget/OppoEditor;->mOppoSelectionCursorController:Landroid/widget/OppoCursorController;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 99
    :cond_1
    invoke-super {p0}, Landroid/widget/Editor;->onAttachedToWindow()V

    .line 100
    return-void
.end method

.method onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Landroid/widget/OppoEditor;->mOppoInsertionCursorController:Landroid/widget/OppoCursorController;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Landroid/widget/OppoEditor;->mOppoInsertionCursorController:Landroid/widget/OppoCursorController;

    invoke-virtual {v0}, Landroid/widget/OppoCursorController;->onDetached()V

    .line 108
    :cond_0
    iget-object v0, p0, Landroid/widget/OppoEditor;->mOppoSelectionCursorController:Landroid/widget/OppoCursorController;

    if-eqz v0, :cond_1

    .line 109
    iget-object v0, p0, Landroid/widget/OppoEditor;->mOppoSelectionCursorController:Landroid/widget/OppoCursorController;

    invoke-virtual {v0}, Landroid/widget/OppoCursorController;->onDetached()V

    .line 111
    :cond_1
    invoke-super {p0}, Landroid/widget/Editor;->onDetachedFromWindow()V

    .line 112
    return-void
.end method

.method onFocusChanged(ZI)V
    .locals 1
    .parameter "focused"
    .parameter "direction"

    .prologue
    .line 274
    invoke-super {p0, p1, p2}, Landroid/widget/Editor;->onFocusChanged(ZI)V

    .line 275
    if-nez p1, :cond_0

    .line 276
    iget-object v0, p0, Landroid/widget/OppoEditor;->mOppoSelectionCursorController:Landroid/widget/OppoCursorController;

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Landroid/widget/OppoEditor;->mOppoSelectionCursorController:Landroid/widget/OppoCursorController;

    invoke-virtual {v0}, Landroid/widget/OppoCursorController;->resetTouchOffsets()V

    .line 280
    :cond_0
    return-void
.end method

.method onTapUpEvent()V
    .locals 1

    .prologue
    .line 538
    iget-boolean v0, p0, Landroid/widget/OppoEditor;->mDiscardNextActionUp:Z

    if-nez v0, :cond_0

    .line 539
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->stopTextSelectionMode()V

    .line 541
    :cond_0
    return-void
.end method

.method onTouchEventWrap(Landroid/view/MotionEvent;)Z
    .locals 3
    .parameter "event"

    .prologue
    const/4 v0, 0x0

    .line 302
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->hasSelectionController()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/widget/OppoEditor;->isToolbarEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 303
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->getOppoSelectionController()Landroid/widget/OppoCursorController;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/widget/OppoCursorController;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 304
    const/4 v0, 0x1

    .line 322
    :cond_0
    :goto_0
    return v0

    .line 308
    :cond_1
    iget-object v1, p0, Landroid/widget/OppoEditor;->mShowSuggestionRunnable:Ljava/lang/Runnable;

    if-eqz v1, :cond_2

    .line 309
    iget-object v1, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    iget-object v2, p0, Landroid/widget/OppoEditor;->mShowSuggestionRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 310
    const/4 v1, 0x0

    iput-object v1, p0, Landroid/widget/OppoEditor;->mShowSuggestionRunnable:Ljava/lang/Runnable;

    .line 313
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    if-nez v1, :cond_0

    .line 314
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Landroid/widget/OppoEditor;->mLastDownPositionX:F

    .line 315
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Landroid/widget/OppoEditor;->mLastDownPositionY:F

    .line 319
    iput-boolean v0, p0, Landroid/widget/OppoEditor;->mTouchFocusSelected:Z

    .line 320
    iput-boolean v0, p0, Landroid/widget/OppoEditor;->mIgnoreActionUpEvent:Z

    goto :goto_0
.end method

.method onTouchUpEvent(Landroid/view/MotionEvent;)V
    .locals 7
    .parameter "event"

    .prologue
    const/4 v1, 0x0

    .line 166
    iput-boolean v1, p0, Landroid/widget/OppoEditor;->mLongPressed:Z

    .line 167
    iget-boolean v3, p0, Landroid/widget/OppoEditor;->mSelectAllOnFocus:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->didTouchFocusSelect()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x1

    .line 168
    .local v1, selectAllGotFocus:Z
    :cond_0
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->hideControllers()V

    .line 169
    iget-object v3, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 170
    .local v2, text:Ljava/lang/CharSequence;
    if-nez v1, :cond_2

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 172
    iget-object v3, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/TextView;->getOffsetForPosition(FF)I

    move-result v0

    .line 173
    .local v0, offset:I
    check-cast v2, Landroid/text/Spannable;

    .end local v2           #text:Ljava/lang/CharSequence;
    invoke-static {v2, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 174
    iget-object v3, p0, Landroid/widget/OppoEditor;->mSpellChecker:Landroid/widget/SpellChecker;

    if-eqz v3, :cond_1

    .line 176
    iget-object v3, p0, Landroid/widget/OppoEditor;->mSpellChecker:Landroid/widget/SpellChecker;

    invoke-virtual {v3}, Landroid/widget/SpellChecker;->onSelectionChanged()V

    .line 178
    :cond_1
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->extractedTextModeWillBeStartedWrap()Z

    move-result v3

    if-nez v3, :cond_2

    .line 179
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->isCursorInsideEasyCorrectionSpanWrap()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 180
    new-instance v3, Landroid/widget/OppoEditor$1;

    invoke-direct {v3, p0}, Landroid/widget/OppoEditor$1;-><init>(Landroid/widget/OppoEditor;)V

    iput-object v3, p0, Landroid/widget/OppoEditor;->mShowSuggestionRunnable:Ljava/lang/Runnable;

    .line 188
    iget-object v3, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    iget-object v4, p0, Landroid/widget/OppoEditor;->mShowSuggestionRunnable:Ljava/lang/Runnable;

    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v5

    int-to-long v5, v5

    invoke-virtual {v3, v4, v5, v6}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 195
    .end local v0           #offset:I
    :cond_2
    :goto_0
    return-void

    .line 190
    .restart local v0       #offset:I
    :cond_3
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->hasInsertionController()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 191
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->getOppoInsertionController()Landroid/widget/OppoCursorController;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/OppoCursorController;->show()V

    goto :goto_0
.end method

.method public performLongClick(Z)Z
    .locals 4
    .parameter "handled"

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 735
    const/4 p1, 0x0

    .line 736
    const/4 v0, 0x0

    .line 737
    .local v0, vibrate:Z
    iput-boolean v2, p0, Landroid/widget/OppoEditor;->mLongPressed:Z

    .line 739
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->hasMagnifierController()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 740
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->stopTextSelectionMode()V

    .line 741
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->getOppoSelectionController()Landroid/widget/OppoCursorController;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/OppoCursorController;->hide()V

    .line 742
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->getOppoInsertionController()Landroid/widget/OppoCursorController;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/OppoCursorController;->hide()V

    .line 743
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->getMagnifierController()Landroid/widget/OppoMagnifierController;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/OppoMagnifierController;->show()V

    .line 744
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->getOppoInsertionController()Landroid/widget/OppoCursorController;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/OppoCursorController;->show()V

    .line 745
    iput-boolean v2, p0, Landroid/widget/OppoEditor;->mDiscardNextActionUp:Z

    .line 746
    const/4 p1, 0x1

    .line 749
    :cond_0
    if-nez p1, :cond_1

    .line 750
    iget-boolean v1, p0, Landroid/widget/OppoEditor;->mIsInTextSelectionMode:Z

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Landroid/widget/OppoEditor;->touchPositionIsInSelectionWrap()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 751
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->stopTextSelectionMode()V

    .line 752
    const/4 p1, 0x1

    .line 771
    :cond_1
    :goto_0
    if-eqz v0, :cond_5

    .line 772
    iget-object v1, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->performHapticFeedback(I)Z

    .line 779
    :cond_2
    :goto_1
    if-eqz p1, :cond_3

    .line 780
    iput-boolean v2, p0, Landroid/widget/OppoEditor;->mDiscardNextActionUp:Z

    .line 783
    :cond_3
    return p1

    .line 754
    :cond_4
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->stopTextSelectionMode()V

    .line 765
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->isToolbarEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/widget/OppoEditor;->isSelectionToolbarEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 766
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->startTextSelectionMode()Z

    move-result p1

    move v0, p1

    goto :goto_0

    .line 773
    :cond_5
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->isInsertToolbarEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Landroid/widget/OppoEditor;->hasInsertionController()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 775
    iput-boolean v2, p0, Landroid/widget/OppoEditor;->mDiscardNextActionUp:Z

    .line 776
    iget-object v1, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->performHapticFeedback(I)Z

    goto :goto_1
.end method

.method prepareCursorControllers()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 116
    invoke-super {p0}, Landroid/widget/Editor;->prepareCursorControllers()V

    .line 118
    iget-boolean v0, p0, Landroid/widget/OppoEditor;->mInsertionControllerEnabled:Z

    if-nez v0, :cond_0

    .line 119
    iget-object v0, p0, Landroid/widget/OppoEditor;->mOppoInsertionCursorController:Landroid/widget/OppoCursorController;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Landroid/widget/OppoEditor;->mOppoInsertionCursorController:Landroid/widget/OppoCursorController;

    invoke-virtual {v0}, Landroid/widget/OppoCursorController;->onDetached()V

    .line 121
    iput-object v1, p0, Landroid/widget/OppoEditor;->mOppoInsertionCursorController:Landroid/widget/OppoCursorController;

    .line 124
    :cond_0
    iget-boolean v0, p0, Landroid/widget/OppoEditor;->mSelectionControllerEnabled:Z

    if-nez v0, :cond_1

    .line 125
    iget-object v0, p0, Landroid/widget/OppoEditor;->mOppoSelectionCursorController:Landroid/widget/OppoCursorController;

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, Landroid/widget/OppoEditor;->mOppoSelectionCursorController:Landroid/widget/OppoCursorController;

    invoke-virtual {v0}, Landroid/widget/OppoCursorController;->onDetached()V

    .line 127
    iput-object v1, p0, Landroid/widget/OppoEditor;->mOppoSelectionCursorController:Landroid/widget/OppoCursorController;

    .line 131
    :cond_1
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->hasMagnifierController()Z

    move-result v0

    if-nez v0, :cond_2

    .line 132
    iput-object v1, p0, Landroid/widget/OppoEditor;->mMagnifierController:Landroid/widget/OppoMagnifierController;

    .line 134
    :cond_2
    return-void
.end method

.method public setEditFlag(I)V
    .locals 2
    .parameter "flag"

    .prologue
    .line 788
    iget v0, p0, Landroid/widget/OppoEditor;->mFlag:I

    or-int/2addr v0, p1

    iput v0, p0, Landroid/widget/OppoEditor;->mFlag:I

    .line 789
    iget v0, p0, Landroid/widget/OppoEditor;->mFlag:I

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    .line 790
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->hideControllers()V

    .line 792
    :cond_0
    return-void
.end method

.method public setHasOnTouchListener(Z)V
    .locals 0
    .parameter "b"

    .prologue
    .line 864
    iput-boolean p1, p0, Landroid/widget/OppoEditor;->mHasOnTouchListener:Z

    .line 865
    return-void
.end method

.method public setLongPressed(Z)V
    .locals 0
    .parameter "longPressed"

    .prologue
    .line 408
    iput-boolean p1, p0, Landroid/widget/OppoEditor;->mLongPressed:Z

    .line 409
    return-void
.end method

.method setTextSelectionWrap(II)V
    .locals 1
    .parameter "start"
    .parameter "end"

    .prologue
    .line 420
    iget-object v0, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spannable;

    invoke-static {v0, p1, p2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 421
    return-void
.end method

.method startSelectionActionMode()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 202
    iget-object v4, p0, Landroid/widget/OppoEditor;->mSelectionActionMode:Landroid/view/ActionMode;

    if-eqz v4, :cond_1

    move v1, v3

    .line 239
    :cond_0
    :goto_0
    return v1

    .line 207
    :cond_1
    invoke-direct {p0}, Landroid/widget/OppoEditor;->canSelectText()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->requestFocus()Z

    move-result v4

    if-nez v4, :cond_3

    .line 208
    :cond_2
    const-string v4, "TextView"

    const-string v5, "TextView does not support text selection. Action mode cancelled."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v3

    .line 210
    goto :goto_0

    .line 213
    :cond_3
    iget-object v4, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->hasSelection()Z

    move-result v4

    if-nez v4, :cond_4

    .line 215
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->selectCurrentWordWrap()Z

    move-result v4

    if-nez v4, :cond_4

    move v1, v3

    .line 217
    goto :goto_0

    .line 221
    :cond_4
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->extractedTextModeWillBeStartedWrap()Z

    move-result v2

    .line 225
    .local v2, willExtract:Z
    if-nez v2, :cond_5

    .line 230
    :cond_5
    iget-object v4, p0, Landroid/widget/OppoEditor;->mSelectionActionMode:Landroid/view/ActionMode;

    if-nez v4, :cond_6

    if-eqz v2, :cond_7

    :cond_6
    const/4 v1, 0x1

    .line 231
    .local v1, selectionStarted:Z
    :goto_1
    if-eqz v1, :cond_0

    iget-object v4, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->isTextSelectable()Z

    move-result v4

    if-nez v4, :cond_0

    iget-boolean v4, p0, Landroid/widget/OppoEditor;->mShowSoftInputOnFocus:Z

    if-eqz v4, :cond_0

    .line 233
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    .line 234
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 235
    iget-object v4, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v3, v5}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;ILandroid/os/ResultReceiver;)Z

    goto :goto_0

    .end local v0           #imm:Landroid/view/inputmethod/InputMethodManager;
    .end local v1           #selectionStarted:Z
    :cond_7
    move v1, v3

    .line 230
    goto :goto_1
.end method

.method startTextSelectionMode()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 554
    iget-boolean v3, p0, Landroid/widget/OppoEditor;->mIsInTextSelectionMode:Z

    if-nez v3, :cond_0

    .line 555
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->hasSelectionController()Z

    move-result v3

    if-nez v3, :cond_1

    .line 576
    :cond_0
    :goto_0
    return v1

    .line 559
    :cond_1
    invoke-direct {p0}, Landroid/widget/OppoEditor;->canSelectText()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->requestFocus()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 563
    iget-object v3, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->hasSelection()Z

    move-result v3

    if-nez v3, :cond_2

    .line 564
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->selectCurrentWordWrap()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 569
    :cond_2
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->getOppoSelectionController()Landroid/widget/OppoCursorController;

    move-result-object v0

    .line 570
    .local v0, mOppoSelectionCursorController:Landroid/widget/OppoCursorController;
    if-eqz v0, :cond_0

    .line 571
    invoke-virtual {v0}, Landroid/widget/OppoCursorController;->show()V

    .line 572
    iput-boolean v2, p0, Landroid/widget/OppoEditor;->mIsInTextSelectionMode:Z

    move v1, v2

    .line 573
    goto :goto_0
.end method

.method startTextSelectionModeIfDouleTap(JFFFF)V
    .locals 9
    .parameter "previousTapUpTime"
    .parameter "x"
    .parameter "y"
    .parameter "previousX"
    .parameter "previousY"

    .prologue
    .line 466
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    sub-long v3, v7, p1

    .line 467
    .local v3, duration:J
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v7

    int-to-long v7, v7

    cmp-long v7, v3, v7

    if-gtz v7, :cond_0

    invoke-virtual {p0, p3, p4}, Landroid/widget/OppoEditor;->isPositionOnTextWrap(FF)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 468
    sub-float v0, p3, p5

    .line 469
    .local v0, deltaX:F
    sub-float v1, p4, p6

    .line 470
    .local v1, deltaY:F
    mul-float v7, v0, v0

    mul-float v8, v1, v1

    add-float v2, v7, v8

    .line 471
    .local v2, distanceSquared:F
    iget-object v7, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v6

    .line 473
    .local v6, viewConfiguration:Landroid/view/ViewConfiguration;
    invoke-virtual {v6}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v5

    .line 474
    .local v5, touchSlop:I
    mul-int v7, v5, v5

    int-to-float v7, v7

    cmpg-float v7, v2, v7

    if-gez v7, :cond_0

    .line 475
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->startTextSelectionMode()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 476
    const/4 v7, 0x1

    iput-boolean v7, p0, Landroid/widget/OppoEditor;->mDiscardNextActionUp:Z

    .line 480
    .end local v0           #deltaX:F
    .end local v1           #deltaY:F
    .end local v2           #distanceSquared:F
    .end local v5           #touchSlop:I
    .end local v6           #viewConfiguration:Landroid/view/ViewConfiguration;
    :cond_0
    return-void
.end method

.method protected stopSelectionActionMode()V
    .locals 2

    .prologue
    .line 158
    invoke-super {p0}, Landroid/widget/Editor;->stopSelectionActionMode()V

    .line 159
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->hasSelectionController()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spannable;

    iget-object v1, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v1

    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 162
    :cond_0
    return-void
.end method

.method stopTextSelectionMode()V
    .locals 2

    .prologue
    .line 544
    iget-boolean v0, p0, Landroid/widget/OppoEditor;->mIsInTextSelectionMode:Z

    if-eqz v0, :cond_1

    .line 545
    iget-object v0, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spannable;

    iget-object v1, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v1

    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 546
    iget-object v0, p0, Landroid/widget/OppoEditor;->mOppoSelectionCursorController:Landroid/widget/OppoCursorController;

    if-eqz v0, :cond_0

    .line 547
    iget-object v0, p0, Landroid/widget/OppoEditor;->mOppoSelectionCursorController:Landroid/widget/OppoCursorController;

    invoke-virtual {v0}, Landroid/widget/OppoCursorController;->hide()V

    .line 549
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/widget/OppoEditor;->mIsInTextSelectionMode:Z

    .line 551
    :cond_1
    return-void
.end method

.method textview()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method touchPositionIsInSelectionWrap()Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 244
    iget-object v6, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getSelectionStart()I

    move-result v4

    .line 245
    .local v4, selectionStart:I
    iget-object v6, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result v3

    .line 247
    .local v3, selectionEnd:I
    if-ne v4, v3, :cond_0

    .line 261
    :goto_0
    return v7

    .line 251
    :cond_0
    if-le v4, v3, :cond_1

    .line 252
    move v5, v4

    .line 253
    .local v5, tmp:I
    move v4, v3

    .line 254
    move v3, v5

    .line 255
    iget-object v6, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    check-cast v6, Landroid/text/Spannable;

    invoke-static {v6, v4, v3}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 257
    .end local v5           #tmp:I
    :cond_1
    invoke-virtual {p0}, Landroid/widget/OppoEditor;->getOppoSelectionController()Landroid/widget/OppoCursorController;

    move-result-object v2

    .line 258
    .local v2, selectionController:Landroid/widget/OppoCursorController;
    invoke-virtual {v2}, Landroid/widget/OppoCursorController;->getMinTouchOffset()I

    move-result v1

    .line 259
    .local v1, minOffset:I
    invoke-virtual {v2}, Landroid/widget/OppoCursorController;->getMaxTouchOffset()I

    move-result v0

    .line 261
    .local v0, maxOffset:I
    if-lt v1, v4, :cond_2

    if-ge v0, v3, :cond_2

    const/4 v6, 0x1

    :goto_1
    move v7, v6

    goto :goto_0

    :cond_2
    move v6, v7

    goto :goto_1
.end method

.method updateCursorsPositions()V
    .locals 14

    .prologue
    .line 335
    iget-object v12, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    iget v12, v12, Landroid/widget/TextView;->mCursorDrawableRes:I

    if-nez v12, :cond_1

    .line 336
    const/4 v12, 0x0

    iput v12, p0, Landroid/widget/OppoEditor;->mCursorCount:I

    .line 371
    :cond_0
    :goto_0
    return-void

    .line 340
    :cond_1
    iget-object v12, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v12}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v4

    .line 341
    .local v4, layout:Landroid/text/Layout;
    iget-object v12, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v12}, Landroid/widget/TextView;->getHintLayout()Landroid/text/Layout;

    move-result-object v2

    .line 342
    .local v2, hintLayout:Landroid/text/Layout;
    iget-object v12, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v12}, Landroid/widget/TextView;->getSelectionStart()I

    move-result v8

    .line 343
    .local v8, offset:I
    invoke-virtual {v4, v8}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v6

    .line 344
    .local v6, line:I
    invoke-virtual {v4, v6}, Landroid/text/Layout;->getLineTop(I)I

    move-result v11

    .line 345
    .local v11, top:I
    add-int/lit8 v12, v6, 0x1

    invoke-virtual {v4, v12}, Landroid/text/Layout;->getLineTop(I)I

    move-result v0

    .line 347
    .local v0, bottom:I
    invoke-direct {p0, v4, v2, v8}, Landroid/widget/OppoEditor;->getPrimaryHorizontal(Landroid/text/Layout;Landroid/text/Layout;I)F

    move-result v9

    .line 348
    .local v9, primaryHorizontal:F
    invoke-virtual {v4, v6}, Landroid/text/Layout;->getLineRight(I)F

    move-result v12

    invoke-static {v12}, Landroid/util/FloatMath;->ceil(F)F

    move-result v12

    float-to-int v10, v12

    .line 349
    .local v10, right:I
    invoke-virtual {v4, v6}, Landroid/text/Layout;->getLineLeft(I)F

    move-result v12

    invoke-static {v12}, Landroid/util/FloatMath;->ceil(F)F

    move-result v12

    float-to-int v5, v12

    .line 350
    .local v5, left:I
    iget-object v12, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v12}, Landroid/widget/TextView;->getWidth()I

    move-result v12

    iget-object v13, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    move-result v13

    sub-int/2addr v12, v13

    iget-object v13, p0, Landroid/widget/OppoEditor;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    move-result v13

    sub-int v3, v12, v13

    .line 352
    .local v3, hspace:I
    const/4 v1, 0x0

    .line 353
    .local v1, cursorOffset:I
    sub-int v12, v10, v3

    if-lez v12, :cond_2

    float-to-int v12, v9

    if-ne v12, v10, :cond_2

    .line 354
    iget v1, p0, Landroid/widget/OppoEditor;->mCursrOffset:I

    .line 357
    :cond_2
    invoke-virtual {v4, v8}, Landroid/text/Layout;->isLevelBoundary(I)Z

    move-result v12

    if-eqz v12, :cond_4

    const/4 v12, 0x2

    :goto_1
    iput v12, p0, Landroid/widget/OppoEditor;->mCursorCount:I

    .line 359
    move v7, v0

    .line 360
    .local v7, middle:I
    iget v12, p0, Landroid/widget/OppoEditor;->mCursorCount:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_3

    .line 362
    add-int v12, v11, v0

    shr-int/lit8 v7, v12, 0x1

    .line 366
    :cond_3
    const/4 v12, 0x0

    int-to-float v13, v1

    sub-float v13, v9, v13

    invoke-virtual {p0, v12, v11, v7, v13}, Landroid/widget/OppoEditor;->updateCursorPositionWrap(IIIF)V

    .line 368
    iget v12, p0, Landroid/widget/OppoEditor;->mCursorCount:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_0

    .line 369
    const/4 v12, 0x1

    invoke-virtual {v4, v8}, Landroid/text/Layout;->getSecondaryHorizontal(I)F

    move-result v13

    invoke-virtual {p0, v12, v7, v0, v13}, Landroid/widget/OppoEditor;->updateCursorPositionWrap(IIIF)V

    goto :goto_0

    .line 357
    .end local v7           #middle:I
    :cond_4
    const/4 v12, 0x1

    goto :goto_1
.end method
