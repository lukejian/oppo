.class public Landroid/widget/OppoScroller;
.super Ljava/lang/Object;
.source "OppoScroller.java"


# static fields
.field private static final DEFAULT_DURATION:I = 0xfa

.field private static final DEFAULT_TIME_GAP:I = 0xf

.field private static final FLING_MODE:I = 0x1

.field private static final FLING_SCROLL_BACK_DURATION:I = 0x2ee

.field private static final FLING_SCROLL_BACK_MODE:I = 0x3

.field private static final FLING_SPRING_MODE:I = 0x2

.field private static final GALLERY_LIST_MODE:I = 0x5

.field private static final GALLERY_TIME_GAP:I = 0x19

.field private static final SCROLL_LIST_MODE:I = 0x4

.field private static final SCROLL_MODE:I


# instance fields
.field final DEBUG_SPRING:Z

.field private DeltaCurrV:I

.field final TAG:Ljava/lang/String;

.field private fmCurrY:I

.field private fmLastCurrY:I

.field private mCoeffX:F

.field private mCoeffY:F

.field private mCount:I

.field private mCurrV:I

.field private mCurrVX:I

.field private mCurrVY:I

.field private mCurrX:I

.field private mCurrY:I

.field public mDeceleration:F

.field private mDeltaX:F

.field private mDeltaY:F

.field private mDuration:I

.field private mDurationReciprocal:F

.field private mFinalX:I

.field private mFinalY:I

.field private mFinished:Z

.field private mInterpolator:Landroid/view/animation/Interpolator;

.field private mLastCurrV:I

.field private mLastCurrY:I

.field private mMaxX:I

.field private mMaxY:I

.field private mMinX:I

.field private mMinY:I

.field private mMode:I

.field private final mPpi:F

.field private mSpringOffsetX:I

.field private mSpringOffsetY:I

.field private mStartTime:J

.field private mStartX:I

.field private mStartY:I

.field private mVelocity:F

.field private mViscousFluidNormalize:F

.field private mViscousFluidScale:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 107
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/OppoScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    .line 108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V
    .locals 3
    .parameter "context"
    .parameter "interpolator"

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const-string v0, "OppoScroller"

    iput-object v0, p0, Landroid/widget/OppoScroller;->TAG:Ljava/lang/String;

    .line 41
    iput-boolean v1, p0, Landroid/widget/OppoScroller;->DEBUG_SPRING:Z

    .line 61
    iput v1, p0, Landroid/widget/OppoScroller;->mLastCurrV:I

    .line 62
    iput v1, p0, Landroid/widget/OppoScroller;->mCurrV:I

    .line 63
    iput v1, p0, Landroid/widget/OppoScroller;->DeltaCurrV:I

    .line 75
    const/4 v0, 0x0

    iput v0, p0, Landroid/widget/OppoScroller;->mCoeffX:F

    .line 76
    const/high16 v0, 0x3f80

    iput v0, p0, Landroid/widget/OppoScroller;->mCoeffY:F

    .line 88
    iput v2, p0, Landroid/widget/OppoScroller;->mCount:I

    .line 90
    iput v1, p0, Landroid/widget/OppoScroller;->mLastCurrY:I

    .line 100
    iput v1, p0, Landroid/widget/OppoScroller;->fmLastCurrY:I

    .line 115
    iput-boolean v2, p0, Landroid/widget/OppoScroller;->mFinished:Z

    .line 116
    iput-object p2, p0, Landroid/widget/OppoScroller;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 117
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x4320

    mul-float/2addr v0, v1

    iput v0, p0, Landroid/widget/OppoScroller;->mPpi:F

    .line 118
    const v0, 0x43c10b3d

    iget v1, p0, Landroid/widget/OppoScroller;->mPpi:F

    mul-float/2addr v0, v1

    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Landroid/widget/OppoScroller;->mDeceleration:F

    .line 120
    return-void
.end method

.method private computeDeceleration(F)F
    .locals 2
    .parameter "friction"

    .prologue
    .line 643
    const v0, 0x43c10b3d

    iget v1, p0, Landroid/widget/OppoScroller;->mPpi:F

    mul-float/2addr v0, v1

    mul-float/2addr v0, p1

    return v0
.end method

.method private getInterpolation(F)F
    .locals 4
    .parameter "x"

    .prologue
    const/high16 v3, 0x3f80

    .line 568
    const/high16 v1, 0x4140

    mul-float/2addr p1, v1

    .line 569
    const v0, 0x3ebc5ab2

    .line 570
    .local v0, start:F
    neg-float v1, p1

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->exp(D)D

    move-result-wide v1

    double-to-float v1, v1

    sub-float p1, v3, v1

    .line 571
    sub-float v1, v3, v0

    mul-float/2addr p1, v1

    .line 572
    iget v1, p0, Landroid/widget/OppoScroller;->mViscousFluidNormalize:F

    mul-float/2addr p1, v1

    .line 573
    return p1
.end method

.method private viscousFluid(F)F
    .locals 4
    .parameter "x"

    .prologue
    const/high16 v3, 0x3f80

    .line 555
    iget v1, p0, Landroid/widget/OppoScroller;->mViscousFluidScale:F

    mul-float/2addr p1, v1

    .line 556
    cmpg-float v1, p1, v3

    if-gez v1, :cond_0

    .line 557
    neg-float v1, p1

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->exp(D)D

    move-result-wide v1

    double-to-float v1, v1

    sub-float v1, v3, v1

    sub-float/2addr p1, v1

    .line 563
    :goto_0
    iget v1, p0, Landroid/widget/OppoScroller;->mViscousFluidNormalize:F

    mul-float/2addr p1, v1

    .line 564
    return p1

    .line 559
    :cond_0
    const v0, 0x3ebc5ab2

    .line 560
    .local v0, start:F
    sub-float v1, v3, p1

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->exp(D)D

    move-result-wide v1

    double-to-float v1, v1

    sub-float p1, v3, v1

    .line 561
    sub-float v1, v3, v0

    mul-float/2addr v1, p1

    add-float p1, v0, v1

    goto :goto_0
.end method


# virtual methods
.method public abortAnimation()V
    .locals 1

    .prologue
    .line 583
    iget v0, p0, Landroid/widget/OppoScroller;->mFinalX:I

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrX:I

    .line 584
    iget v0, p0, Landroid/widget/OppoScroller;->mFinalY:I

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    .line 585
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/widget/OppoScroller;->mFinished:Z

    .line 586
    return-void
.end method

.method public computeScrollOffset()Z
    .locals 15

    .prologue
    const/high16 v14, 0x447a

    const/high16 v13, 0x4000

    const/high16 v5, 0x3f80

    const/4 v0, 0x0

    const/4 v12, 0x1

    .line 228
    iget-boolean v1, p0, Landroid/widget/OppoScroller;->mFinished:Z

    if-eqz v1, :cond_0

    .line 387
    :goto_0
    return v0

    .line 232
    :cond_0
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Landroid/widget/OppoScroller;->mStartTime:J

    sub-long/2addr v1, v3

    long-to-int v7, v1

    .line 236
    .local v7, timePassed:I
    const/4 v1, 0x4

    iget v2, p0, Landroid/widget/OppoScroller;->mMode:I

    if-ne v1, v2, :cond_3

    .line 237
    iget v1, p0, Landroid/widget/OppoScroller;->mCount:I

    mul-int/lit8 v7, v1, 0xf

    .line 241
    :cond_1
    :goto_1
    iget v1, p0, Landroid/widget/OppoScroller;->mDuration:I

    if-ge v7, v1, :cond_a

    .line 242
    iget v1, p0, Landroid/widget/OppoScroller;->mMode:I

    packed-switch v1, :pswitch_data_0

    :cond_2
    :goto_2
    move v0, v12

    .line 387
    goto :goto_0

    .line 238
    :cond_3
    const/4 v1, 0x5

    iget v2, p0, Landroid/widget/OppoScroller;->mMode:I

    if-ne v1, v2, :cond_1

    .line 239
    iget v1, p0, Landroid/widget/OppoScroller;->mCount:I

    mul-int/lit8 v7, v1, 0x19

    goto :goto_1

    .line 245
    :pswitch_0
    int-to-float v0, v7

    iget v1, p0, Landroid/widget/OppoScroller;->mDurationReciprocal:F

    mul-float v11, v0, v1

    .line 246
    .local v11, x:F
    iget-object v0, p0, Landroid/widget/OppoScroller;->mInterpolator:Landroid/view/animation/Interpolator;

    if-nez v0, :cond_4

    .line 247
    invoke-direct {p0, v11}, Landroid/widget/OppoScroller;->viscousFluid(F)F

    move-result v11

    .line 251
    :goto_3
    iget v0, p0, Landroid/widget/OppoScroller;->mStartX:I

    iget v1, p0, Landroid/widget/OppoScroller;->mDeltaX:F

    mul-float/2addr v1, v11

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrX:I

    .line 252
    iget v0, p0, Landroid/widget/OppoScroller;->mStartY:I

    iget v1, p0, Landroid/widget/OppoScroller;->mDeltaY:F

    mul-float/2addr v1, v11

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    .line 253
    iget v0, p0, Landroid/widget/OppoScroller;->mCurrX:I

    iget v1, p0, Landroid/widget/OppoScroller;->mFinalX:I

    if-ne v0, v1, :cond_2

    iget v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    iget v1, p0, Landroid/widget/OppoScroller;->mFinalY:I

    if-ne v0, v1, :cond_2

    .line 254
    iput-boolean v12, p0, Landroid/widget/OppoScroller;->mFinished:Z

    goto :goto_2

    .line 249
    :cond_4
    iget-object v0, p0, Landroid/widget/OppoScroller;->mInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v0, v11}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v11

    goto :goto_3

    .line 266
    .end local v11           #x:F
    :pswitch_1
    add-int/lit16 v1, v7, 0xc8

    int-to-float v1, v1

    const/high16 v2, 0x44fa

    div-float v11, v1, v2

    .line 267
    .restart local v11       #x:F
    const/16 v1, 0xf

    if-ne v7, v1, :cond_5

    .line 268
    iput v0, p0, Landroid/widget/OppoScroller;->DeltaCurrV:I

    .line 269
    iput v0, p0, Landroid/widget/OppoScroller;->mLastCurrV:I

    .line 270
    iput v0, p0, Landroid/widget/OppoScroller;->fmLastCurrY:I

    .line 280
    :cond_5
    const/high16 v0, -0x3f80

    mul-float/2addr v0, v11

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    double-to-float v0, v0

    sub-float v0, v5, v0

    sub-float/2addr v0, v5

    const-wide v1, -0x4026666660000000L

    invoke-static {v1, v2}, Ljava/lang/Math;->exp(D)D

    move-result-wide v1

    double-to-float v1, v1

    add-float v11, v0, v1

    .line 292
    iget v0, p0, Landroid/widget/OppoScroller;->mStartX:I

    iget v1, p0, Landroid/widget/OppoScroller;->mDeltaX:F

    mul-float/2addr v1, v11

    float-to-int v1, v1

    add-int/2addr v0, v1

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrX:I

    .line 293
    iget v0, p0, Landroid/widget/OppoScroller;->mStartY:I

    iget v1, p0, Landroid/widget/OppoScroller;->mDeltaY:F

    mul-float/2addr v1, v11

    float-to-int v1, v1

    add-int/2addr v0, v1

    iget v1, p0, Landroid/widget/OppoScroller;->DeltaCurrV:I

    sub-int/2addr v0, v1

    iput v0, p0, Landroid/widget/OppoScroller;->fmCurrY:I

    .line 294
    iget v0, p0, Landroid/widget/OppoScroller;->fmCurrY:I

    iget v1, p0, Landroid/widget/OppoScroller;->fmLastCurrY:I

    sub-int/2addr v0, v1

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrV:I

    .line 295
    iget v0, p0, Landroid/widget/OppoScroller;->mLastCurrV:I

    if-eqz v0, :cond_6

    iget v0, p0, Landroid/widget/OppoScroller;->mLastCurrV:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Landroid/widget/OppoScroller;->mCurrV:I

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 296
    iget v0, p0, Landroid/widget/OppoScroller;->DeltaCurrV:I

    iget v1, p0, Landroid/widget/OppoScroller;->mCurrV:I

    iget v2, p0, Landroid/widget/OppoScroller;->mLastCurrV:I

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Landroid/widget/OppoScroller;->DeltaCurrV:I

    .line 297
    iget v0, p0, Landroid/widget/OppoScroller;->fmCurrY:I

    iget v1, p0, Landroid/widget/OppoScroller;->mCurrV:I

    iget v2, p0, Landroid/widget/OppoScroller;->mLastCurrV:I

    sub-int/2addr v1, v2

    sub-int/2addr v0, v1

    iput v0, p0, Landroid/widget/OppoScroller;->fmCurrY:I

    .line 298
    iget v0, p0, Landroid/widget/OppoScroller;->mLastCurrV:I

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrV:I

    .line 300
    :cond_6
    iget v0, p0, Landroid/widget/OppoScroller;->fmCurrY:I

    iput v0, p0, Landroid/widget/OppoScroller;->fmLastCurrY:I

    .line 301
    iget v0, p0, Landroid/widget/OppoScroller;->mCurrV:I

    iput v0, p0, Landroid/widget/OppoScroller;->mLastCurrV:I

    .line 302
    iget v0, p0, Landroid/widget/OppoScroller;->fmCurrY:I

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    .line 303
    iget v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    iget v1, p0, Landroid/widget/OppoScroller;->mLastCurrY:I

    sub-int/2addr v0, v1

    mul-int/lit16 v0, v0, 0xfa

    div-int/lit8 v9, v0, 0xf

    .line 304
    .local v9, v:I
    int-to-float v0, v9

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrVY:I

    .line 310
    iget v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    iput v0, p0, Landroid/widget/OppoScroller;->mLastCurrY:I

    .line 312
    iget v0, p0, Landroid/widget/OppoScroller;->mCurrX:I

    iget v1, p0, Landroid/widget/OppoScroller;->mFinalX:I

    if-ne v0, v1, :cond_7

    iget v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    iget v1, p0, Landroid/widget/OppoScroller;->mFinalY:I

    if-eq v0, v1, :cond_8

    :cond_7
    iget v0, p0, Landroid/widget/OppoScroller;->mCurrVY:I

    if-nez v0, :cond_2

    .line 313
    :cond_8
    iget v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    iput v0, p0, Landroid/widget/OppoScroller;->mFinalY:I

    .line 314
    iput-boolean v12, p0, Landroid/widget/OppoScroller;->mFinished:Z

    goto/16 :goto_2

    .line 320
    .end local v9           #v:I
    .end local v11           #x:F
    :pswitch_2
    int-to-float v0, v7

    div-float v8, v0, v14

    .line 321
    .local v8, timePassedSeconds:F
    iget v0, p0, Landroid/widget/OppoScroller;->mVelocity:F

    mul-float/2addr v0, v8

    iget v1, p0, Landroid/widget/OppoScroller;->mDeceleration:F

    mul-float/2addr v1, v8

    mul-float/2addr v1, v8

    div-float/2addr v1, v13

    sub-float v6, v0, v1

    .line 324
    .local v6, distance:F
    iget v0, p0, Landroid/widget/OppoScroller;->mStartX:I

    iget v1, p0, Landroid/widget/OppoScroller;->mCoeffX:F

    mul-float/2addr v1, v6

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrX:I

    .line 326
    iget v0, p0, Landroid/widget/OppoScroller;->mCurrX:I

    iget v1, p0, Landroid/widget/OppoScroller;->mMaxX:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrX:I

    .line 327
    iget v0, p0, Landroid/widget/OppoScroller;->mCurrX:I

    iget v1, p0, Landroid/widget/OppoScroller;->mMinX:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrX:I

    .line 329
    iget v0, p0, Landroid/widget/OppoScroller;->mStartY:I

    iget v1, p0, Landroid/widget/OppoScroller;->mCoeffY:F

    mul-float/2addr v1, v6

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    .line 331
    iget v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    iget v1, p0, Landroid/widget/OppoScroller;->mMaxY:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    .line 332
    iget v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    iget v1, p0, Landroid/widget/OppoScroller;->mMinY:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    .line 333
    iget v0, p0, Landroid/widget/OppoScroller;->mVelocity:F

    iget v1, p0, Landroid/widget/OppoScroller;->mDeceleration:F

    mul-float/2addr v1, v8

    sub-float v10, v0, v1

    .line 334
    .local v10, velocity:F
    iget v0, p0, Landroid/widget/OppoScroller;->mCoeffX:F

    mul-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrVX:I

    .line 335
    iget v0, p0, Landroid/widget/OppoScroller;->mCoeffY:F

    mul-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrVY:I

    .line 338
    iget v0, p0, Landroid/widget/OppoScroller;->mCurrX:I

    iget v1, p0, Landroid/widget/OppoScroller;->mFinalX:I

    if-ne v0, v1, :cond_2

    iget v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    iget v1, p0, Landroid/widget/OppoScroller;->mFinalY:I

    if-ne v0, v1, :cond_2

    .line 339
    iput-boolean v12, p0, Landroid/widget/OppoScroller;->mFinished:Z

    goto/16 :goto_2

    .line 343
    .end local v6           #distance:F
    .end local v8           #timePassedSeconds:F
    .end local v10           #velocity:F
    :pswitch_3
    int-to-float v0, v7

    div-float v8, v0, v14

    .line 344
    .restart local v8       #timePassedSeconds:F
    iget v0, p0, Landroid/widget/OppoScroller;->mVelocity:F

    mul-float/2addr v0, v8

    iget v1, p0, Landroid/widget/OppoScroller;->mDeceleration:F

    mul-float/2addr v1, v8

    mul-float/2addr v1, v8

    div-float/2addr v1, v13

    sub-float v6, v0, v1

    .line 347
    .restart local v6       #distance:F
    iget v0, p0, Landroid/widget/OppoScroller;->mStartX:I

    iget v1, p0, Landroid/widget/OppoScroller;->mCoeffX:F

    mul-float/2addr v1, v6

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrX:I

    .line 349
    iget v0, p0, Landroid/widget/OppoScroller;->mCurrX:I

    iget v1, p0, Landroid/widget/OppoScroller;->mMaxX:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrX:I

    .line 350
    iget v0, p0, Landroid/widget/OppoScroller;->mCurrX:I

    iget v1, p0, Landroid/widget/OppoScroller;->mMinX:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrX:I

    .line 352
    iget v0, p0, Landroid/widget/OppoScroller;->mStartY:I

    iget v1, p0, Landroid/widget/OppoScroller;->mCoeffY:F

    mul-float/2addr v1, v6

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    .line 354
    iget v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    iget v1, p0, Landroid/widget/OppoScroller;->mMaxY:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    .line 355
    iget v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    iget v1, p0, Landroid/widget/OppoScroller;->mMinY:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    .line 357
    iget v0, p0, Landroid/widget/OppoScroller;->mCurrX:I

    iget v1, p0, Landroid/widget/OppoScroller;->mFinalX:I

    if-eq v0, v1, :cond_9

    iget v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    iget v1, p0, Landroid/widget/OppoScroller;->mFinalY:I

    if-ne v0, v1, :cond_2

    .line 359
    :cond_9
    iget v1, p0, Landroid/widget/OppoScroller;->mCurrX:I

    iget v2, p0, Landroid/widget/OppoScroller;->mCurrY:I

    iget v0, p0, Landroid/widget/OppoScroller;->mDeltaX:F

    iget v3, p0, Landroid/widget/OppoScroller;->mFinalX:I

    iget v4, p0, Landroid/widget/OppoScroller;->mCurrX:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    add-float/2addr v0, v3

    float-to-int v3, v0

    iget v0, p0, Landroid/widget/OppoScroller;->mDeltaY:F

    iget v4, p0, Landroid/widget/OppoScroller;->mFinalY:I

    iget v5, p0, Landroid/widget/OppoScroller;->mCurrY:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    add-float/2addr v0, v4

    float-to-int v4, v0

    const/16 v5, 0x2ee

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/widget/OppoScroller;->startScroll(IIIII)V

    .line 362
    const/4 v0, 0x3

    iput v0, p0, Landroid/widget/OppoScroller;->mMode:I

    goto/16 :goto_2

    .line 366
    .end local v6           #distance:F
    .end local v8           #timePassedSeconds:F
    :pswitch_4
    int-to-float v0, v7

    iget v1, p0, Landroid/widget/OppoScroller;->mDurationReciprocal:F

    mul-float v11, v0, v1

    .line 367
    .restart local v11       #x:F
    invoke-direct {p0, v11}, Landroid/widget/OppoScroller;->viscousFluid(F)F

    move-result v11

    .line 369
    iget v0, p0, Landroid/widget/OppoScroller;->mStartX:I

    iget v1, p0, Landroid/widget/OppoScroller;->mDeltaX:F

    mul-float/2addr v1, v11

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrX:I

    .line 370
    iget v0, p0, Landroid/widget/OppoScroller;->mStartY:I

    iget v1, p0, Landroid/widget/OppoScroller;->mDeltaY:F

    mul-float/2addr v1, v11

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    .line 371
    iget v0, p0, Landroid/widget/OppoScroller;->mCurrX:I

    iget v1, p0, Landroid/widget/OppoScroller;->mFinalX:I

    if-ne v0, v1, :cond_2

    iget v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    iget v1, p0, Landroid/widget/OppoScroller;->mFinalY:I

    if-ne v0, v1, :cond_2

    .line 372
    iput-boolean v12, p0, Landroid/widget/OppoScroller;->mFinished:Z

    goto/16 :goto_2

    .line 376
    .end local v11           #x:F
    :cond_a
    iget v0, p0, Landroid/widget/OppoScroller;->mMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_b

    .line 377
    iget v1, p0, Landroid/widget/OppoScroller;->mCurrX:I

    iget v2, p0, Landroid/widget/OppoScroller;->mCurrY:I

    iget v0, p0, Landroid/widget/OppoScroller;->mDeltaX:F

    iget v3, p0, Landroid/widget/OppoScroller;->mFinalX:I

    iget v4, p0, Landroid/widget/OppoScroller;->mCurrX:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    add-float/2addr v0, v3

    float-to-int v3, v0

    iget v0, p0, Landroid/widget/OppoScroller;->mDeltaY:F

    iget v4, p0, Landroid/widget/OppoScroller;->mFinalY:I

    iget v5, p0, Landroid/widget/OppoScroller;->mCurrY:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    add-float/2addr v0, v4

    float-to-int v4, v0

    const/16 v5, 0x2ee

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/widget/OppoScroller;->startScroll(IIIII)V

    .line 379
    const/4 v0, 0x3

    iput v0, p0, Landroid/widget/OppoScroller;->mMode:I

    move v0, v12

    .line 380
    goto/16 :goto_0

    .line 383
    :cond_b
    iget v0, p0, Landroid/widget/OppoScroller;->mFinalX:I

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrX:I

    .line 384
    iget v0, p0, Landroid/widget/OppoScroller;->mFinalY:I

    iput v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    .line 385
    iput-boolean v12, p0, Landroid/widget/OppoScroller;->mFinished:Z

    goto/16 :goto_2

    .line 242
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public extendDuration(I)V
    .locals 3
    .parameter "extend"

    .prologue
    .line 597
    invoke-virtual {p0}, Landroid/widget/OppoScroller;->timePassed()I

    move-result v0

    .line 598
    .local v0, passed:I
    add-int v1, v0, p1

    iput v1, p0, Landroid/widget/OppoScroller;->mDuration:I

    .line 599
    const/high16 v1, 0x3f80

    iget v2, p0, Landroid/widget/OppoScroller;->mDuration:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Landroid/widget/OppoScroller;->mDurationReciprocal:F

    .line 600
    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid/widget/OppoScroller;->mFinished:Z

    .line 601
    return-void
.end method

.method public fling(IIIIIIII)V
    .locals 6
    .parameter "startX"
    .parameter "startY"
    .parameter "velocityX"
    .parameter "velocityY"
    .parameter "minX"
    .parameter "maxX"
    .parameter "minY"
    .parameter "maxY"

    .prologue
    .line 480
    const/4 v2, 0x1

    iput v2, p0, Landroid/widget/OppoScroller;->mMode:I

    .line 481
    const/4 v2, 0x0

    iput-boolean v2, p0, Landroid/widget/OppoScroller;->mFinished:Z

    .line 483
    int-to-double v2, p3

    int-to-double v4, p4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v2

    double-to-float v1, v2

    .line 485
    .local v1, velocity:F
    iput v1, p0, Landroid/widget/OppoScroller;->mVelocity:F

    .line 486
    const/high16 v2, 0x447a

    mul-float/2addr v2, v1

    iget v3, p0, Landroid/widget/OppoScroller;->mDeceleration:F

    div-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Landroid/widget/OppoScroller;->mDuration:I

    .line 488
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Landroid/widget/OppoScroller;->mStartTime:J

    .line 489
    iput p1, p0, Landroid/widget/OppoScroller;->mStartX:I

    .line 490
    iput p2, p0, Landroid/widget/OppoScroller;->mStartY:I

    .line 492
    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-nez v2, :cond_0

    const/high16 v2, 0x3f80

    :goto_0
    iput v2, p0, Landroid/widget/OppoScroller;->mCoeffX:F

    .line 493
    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-nez v2, :cond_1

    const/high16 v2, 0x3f80

    :goto_1
    iput v2, p0, Landroid/widget/OppoScroller;->mCoeffY:F

    .line 495
    mul-float v2, v1, v1

    const/high16 v3, 0x4000

    iget v4, p0, Landroid/widget/OppoScroller;->mDeceleration:F

    mul-float/2addr v3, v4

    div-float/2addr v2, v3

    float-to-int v0, v2

    .line 497
    .local v0, totalDistance:I
    iput p5, p0, Landroid/widget/OppoScroller;->mMinX:I

    .line 498
    iput p6, p0, Landroid/widget/OppoScroller;->mMaxX:I

    .line 499
    iput p7, p0, Landroid/widget/OppoScroller;->mMinY:I

    .line 500
    iput p8, p0, Landroid/widget/OppoScroller;->mMaxY:I

    .line 502
    int-to-float v2, v0

    iget v3, p0, Landroid/widget/OppoScroller;->mCoeffX:F

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    add-int/2addr v2, p1

    iput v2, p0, Landroid/widget/OppoScroller;->mFinalX:I

    .line 504
    iget v2, p0, Landroid/widget/OppoScroller;->mFinalX:I

    iget v3, p0, Landroid/widget/OppoScroller;->mMaxX:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, Landroid/widget/OppoScroller;->mFinalX:I

    .line 505
    iget v2, p0, Landroid/widget/OppoScroller;->mFinalX:I

    iget v3, p0, Landroid/widget/OppoScroller;->mMinX:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p0, Landroid/widget/OppoScroller;->mFinalX:I

    .line 507
    int-to-float v2, v0

    iget v3, p0, Landroid/widget/OppoScroller;->mCoeffY:F

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    add-int/2addr v2, p2

    iput v2, p0, Landroid/widget/OppoScroller;->mFinalY:I

    .line 509
    iget v2, p0, Landroid/widget/OppoScroller;->mFinalY:I

    iget v3, p0, Landroid/widget/OppoScroller;->mMaxY:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, Landroid/widget/OppoScroller;->mFinalY:I

    .line 510
    iget v2, p0, Landroid/widget/OppoScroller;->mFinalY:I

    iget v3, p0, Landroid/widget/OppoScroller;->mMinY:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p0, Landroid/widget/OppoScroller;->mFinalY:I

    .line 511
    return-void

    .line 492
    .end local v0           #totalDistance:I
    :cond_0
    int-to-float v2, p3

    div-float/2addr v2, v1

    goto :goto_0

    .line 493
    :cond_1
    int-to-float v2, p4

    div-float/2addr v2, v1

    goto :goto_1
.end method

.method public fling(IIIIIIIIII)V
    .locals 10
    .parameter "startX"
    .parameter "startY"
    .parameter "velocityX"
    .parameter "velocityY"
    .parameter "minX"
    .parameter "maxX"
    .parameter "minY"
    .parameter "maxY"
    .parameter "springOffsetX"
    .parameter "springOffsetY"

    .prologue
    .line 532
    sub-int v6, p5, p9

    add-int v7, p6, p9

    sub-int v8, p7, p10

    add-int v9, p8, p10

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v1 .. v9}, Landroid/widget/OppoScroller;->fling(IIIIIIII)V

    .line 534
    const/4 v1, 0x0

    iput v1, p0, Landroid/widget/OppoScroller;->mDeltaY:F

    iput v1, p0, Landroid/widget/OppoScroller;->mDeltaX:F

    .line 535
    iget v1, p0, Landroid/widget/OppoScroller;->mFinalX:I

    move/from16 v0, p6

    if-gt v1, v0, :cond_0

    iget v1, p0, Landroid/widget/OppoScroller;->mFinalX:I

    if-ge v1, p5, :cond_1

    .line 536
    :cond_0
    const/4 v1, 0x2

    iput v1, p0, Landroid/widget/OppoScroller;->mMode:I

    .line 537
    iget v1, p0, Landroid/widget/OppoScroller;->mFinalX:I

    move/from16 v0, p6

    if-le v1, v0, :cond_4

    .line 538
    iget v1, p0, Landroid/widget/OppoScroller;->mFinalX:I

    sub-int v1, p6, v1

    int-to-float v1, v1

    iput v1, p0, Landroid/widget/OppoScroller;->mDeltaX:F

    .line 544
    :cond_1
    :goto_0
    iget v1, p0, Landroid/widget/OppoScroller;->mFinalY:I

    move/from16 v0, p8

    if-gt v1, v0, :cond_2

    iget v1, p0, Landroid/widget/OppoScroller;->mFinalY:I

    move/from16 v0, p7

    if-ge v1, v0, :cond_3

    .line 545
    :cond_2
    const/4 v1, 0x2

    iput v1, p0, Landroid/widget/OppoScroller;->mMode:I

    .line 546
    iget v1, p0, Landroid/widget/OppoScroller;->mFinalY:I

    move/from16 v0, p8

    if-le v1, v0, :cond_5

    .line 547
    iget v1, p0, Landroid/widget/OppoScroller;->mFinalY:I

    sub-int v1, p8, v1

    int-to-float v1, v1

    iput v1, p0, Landroid/widget/OppoScroller;->mDeltaY:F

    .line 552
    :cond_3
    :goto_1
    return-void

    .line 540
    :cond_4
    iget v1, p0, Landroid/widget/OppoScroller;->mFinalX:I

    sub-int v1, p5, v1

    int-to-float v1, v1

    iput v1, p0, Landroid/widget/OppoScroller;->mDeltaX:F

    goto :goto_0

    .line 549
    :cond_5
    iget v1, p0, Landroid/widget/OppoScroller;->mFinalY:I

    sub-int v1, p7, v1

    int-to-float v1, v1

    iput v1, p0, Landroid/widget/OppoScroller;->mDeltaY:F

    goto :goto_1
.end method

.method public final forceFinished(Z)V
    .locals 0
    .parameter "finished"

    .prologue
    .line 138
    iput-boolean p1, p0, Landroid/widget/OppoScroller;->mFinished:Z

    .line 139
    return-void
.end method

.method public final getCurrVX()I
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Landroid/widget/OppoScroller;->mCurrVX:I

    return v0
.end method

.method public final getCurrVY()I
    .locals 1

    .prologue
    .line 220
    iget v0, p0, Landroid/widget/OppoScroller;->mCurrVY:I

    return v0
.end method

.method public getCurrVelocity()F
    .locals 3

    .prologue
    .line 174
    iget v0, p0, Landroid/widget/OppoScroller;->mVelocity:F

    iget v1, p0, Landroid/widget/OppoScroller;->mDeceleration:F

    invoke-virtual {p0}, Landroid/widget/OppoScroller;->timePassed()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x44fa

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    return v0
.end method

.method public final getCurrX()I
    .locals 1

    .prologue
    .line 156
    iget v0, p0, Landroid/widget/OppoScroller;->mCurrX:I

    return v0
.end method

.method public final getCurrY()I
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Landroid/widget/OppoScroller;->mCurrY:I

    return v0
.end method

.method public final getDuration()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Landroid/widget/OppoScroller;->mDuration:I

    return v0
.end method

.method public final getFinalX()I
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Landroid/widget/OppoScroller;->mFinalX:I

    return v0
.end method

.method public final getFinalY()I
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Landroid/widget/OppoScroller;->mFinalY:I

    return v0
.end method

.method public final getStartX()I
    .locals 1

    .prologue
    .line 183
    iget v0, p0, Landroid/widget/OppoScroller;->mStartX:I

    return v0
.end method

.method public final getStartY()I
    .locals 1

    .prologue
    .line 192
    iget v0, p0, Landroid/widget/OppoScroller;->mStartY:I

    return v0
.end method

.method public final isFinished()Z
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Landroid/widget/OppoScroller;->mFinished:Z

    return v0
.end method

.method public setCount(I)V
    .locals 0
    .parameter "n"

    .prologue
    .line 431
    iput p1, p0, Landroid/widget/OppoScroller;->mCount:I

    .line 432
    return-void
.end method

.method public setFinalX(I)V
    .locals 2
    .parameter "newX"

    .prologue
    .line 620
    iput p1, p0, Landroid/widget/OppoScroller;->mFinalX:I

    .line 621
    iget v0, p0, Landroid/widget/OppoScroller;->mFinalX:I

    iget v1, p0, Landroid/widget/OppoScroller;->mStartX:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p0, Landroid/widget/OppoScroller;->mDeltaX:F

    .line 622
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/widget/OppoScroller;->mFinished:Z

    .line 623
    return-void
.end method

.method public setFinalY(I)V
    .locals 2
    .parameter "newY"

    .prologue
    .line 633
    iput p1, p0, Landroid/widget/OppoScroller;->mFinalY:I

    .line 634
    iget v0, p0, Landroid/widget/OppoScroller;->mFinalY:I

    iget v1, p0, Landroid/widget/OppoScroller;->mStartY:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p0, Landroid/widget/OppoScroller;->mDeltaY:F

    .line 635
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/widget/OppoScroller;->mFinished:Z

    .line 636
    return-void
.end method

.method public final setFriction(F)V
    .locals 1
    .parameter "friction"

    .prologue
    .line 639
    invoke-direct {p0, p1}, Landroid/widget/OppoScroller;->computeDeceleration(F)F

    move-result v0

    iput v0, p0, Landroid/widget/OppoScroller;->mDeceleration:F

    .line 640
    return-void
.end method

.method public startGalleryList(IIIII)V
    .locals 1
    .parameter "startX"
    .parameter "startY"
    .parameter "dx"
    .parameter "dy"
    .parameter "duration"

    .prologue
    .line 422
    invoke-virtual/range {p0 .. p5}, Landroid/widget/OppoScroller;->startScroll(IIIII)V

    .line 423
    const/4 v0, 0x5

    iput v0, p0, Landroid/widget/OppoScroller;->mMode:I

    .line 424
    const/4 v0, 0x1

    iput v0, p0, Landroid/widget/OppoScroller;->mCount:I

    .line 425
    return-void
.end method

.method public startScroll(IIII)V
    .locals 6
    .parameter "startX"
    .parameter "startY"
    .parameter "dx"
    .parameter "dy"

    .prologue
    .line 403
    const/16 v5, 0xfa

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/widget/OppoScroller;->startScroll(IIIII)V

    .line 404
    return-void
.end method

.method public startScroll(IIIII)V
    .locals 3
    .parameter "startX"
    .parameter "startY"
    .parameter "dx"
    .parameter "dy"
    .parameter "duration"

    .prologue
    const/4 v0, 0x0

    const/high16 v2, 0x3f80

    .line 447
    iput v0, p0, Landroid/widget/OppoScroller;->mMode:I

    .line 448
    iput-boolean v0, p0, Landroid/widget/OppoScroller;->mFinished:Z

    .line 449
    iput p5, p0, Landroid/widget/OppoScroller;->mDuration:I

    .line 450
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/widget/OppoScroller;->mStartTime:J

    .line 451
    iput p1, p0, Landroid/widget/OppoScroller;->mStartX:I

    .line 452
    iput p2, p0, Landroid/widget/OppoScroller;->mStartY:I

    .line 453
    add-int v0, p1, p3

    iput v0, p0, Landroid/widget/OppoScroller;->mFinalX:I

    .line 454
    add-int v0, p2, p4

    iput v0, p0, Landroid/widget/OppoScroller;->mFinalY:I

    .line 455
    int-to-float v0, p3

    iput v0, p0, Landroid/widget/OppoScroller;->mDeltaX:F

    .line 456
    int-to-float v0, p4

    iput v0, p0, Landroid/widget/OppoScroller;->mDeltaY:F

    .line 457
    iget v0, p0, Landroid/widget/OppoScroller;->mDuration:I

    int-to-float v0, v0

    div-float v0, v2, v0

    iput v0, p0, Landroid/widget/OppoScroller;->mDurationReciprocal:F

    .line 459
    const/high16 v0, 0x4100

    iput v0, p0, Landroid/widget/OppoScroller;->mViscousFluidScale:F

    .line 461
    iput v2, p0, Landroid/widget/OppoScroller;->mViscousFluidNormalize:F

    .line 462
    invoke-direct {p0, v2}, Landroid/widget/OppoScroller;->viscousFluid(F)F

    move-result v0

    div-float v0, v2, v0

    iput v0, p0, Landroid/widget/OppoScroller;->mViscousFluidNormalize:F

    .line 463
    return-void
.end method

.method public startScrollList(IIIII)V
    .locals 2
    .parameter "startX"
    .parameter "startY"
    .parameter "dx"
    .parameter "dy"
    .parameter "duration"

    .prologue
    const/high16 v1, 0x3f80

    .line 410
    invoke-virtual/range {p0 .. p5}, Landroid/widget/OppoScroller;->startScroll(IIIII)V

    .line 411
    const/4 v0, 0x4

    iput v0, p0, Landroid/widget/OppoScroller;->mMode:I

    .line 412
    iput v1, p0, Landroid/widget/OppoScroller;->mViscousFluidNormalize:F

    .line 413
    invoke-direct {p0, v1}, Landroid/widget/OppoScroller;->getInterpolation(F)F

    move-result v0

    div-float v0, v1, v0

    iput v0, p0, Landroid/widget/OppoScroller;->mViscousFluidNormalize:F

    .line 414
    const/4 v0, 0x0

    iput v0, p0, Landroid/widget/OppoScroller;->mLastCurrY:I

    .line 415
    const/4 v0, 0x1

    iput v0, p0, Landroid/widget/OppoScroller;->mCount:I

    .line 416
    return-void
.end method

.method public timePassed()I
    .locals 4

    .prologue
    .line 609
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Landroid/widget/OppoScroller;->mStartTime:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method
