.class public interface abstract Landroid/view/OppoWindowManagerPolicy;
.super Ljava/lang/Object;
.source "OppoWindowManagerPolicy.java"

# interfaces
.implements Landroid/view/WindowManagerPolicy;


# virtual methods
.method public abstract keyguardSetApkLockScreenShowing(Z)V
    .annotation build Landroid/annotation/OppoHook;
        level = .enum Landroid/annotation/OppoHook$OppoHookType;->NEW_METHOD:Landroid/annotation/OppoHook$OppoHookType;
        note = "zhangkai add for apklock"
    .end annotation
.end method

.method public abstract keyguardShowSecureApkLock(Z)V
    .annotation build Landroid/annotation/OppoHook;
        level = .enum Landroid/annotation/OppoHook$OppoHookType;->NEW_METHOD:Landroid/annotation/OppoHook$OppoHookType;
        note = "zhangkai add for apklock"
    .end annotation
.end method
