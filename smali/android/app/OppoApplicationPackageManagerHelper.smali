.class final Landroid/app/OppoApplicationPackageManagerHelper;
.super Ljava/lang/Object;
.source "OppoApplicationPackageManager.java"


# static fields
.field private static final COLUMN_NAME:Ljava/lang/String; = "package_name"

.field private static final CONTENT_URI_APP_LIST:Landroid/net/Uri; = null

.field private static final CONTENT_URI_MARKET_LIST:Landroid/net/Uri; = null

.field private static final DEBUG:Z = true

.field private static final FILTRATE_APP_FEATURE_NAME:Ljava/lang/String; = "oppo.filtrated.app"

.field private static final TAG:Ljava/lang/String; = "OppoApplicationPackageManagerHelper"

.field private static final UID:I = 0x2710

.field private static mFiltrateAppSwitch:Z


# instance fields
.field private final mContext:Landroid/app/ContextImpl;

.field private mFiltrateAppNameList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFiltrateMarketNameList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    sput-boolean v0, Landroid/app/OppoApplicationPackageManagerHelper;->mFiltrateAppSwitch:Z

    .line 40
    const-string v0, "content://com.oppo.market/rom_update_filter_market_list"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Landroid/app/OppoApplicationPackageManagerHelper;->CONTENT_URI_MARKET_LIST:Landroid/net/Uri;

    .line 41
    const-string v0, "content://com.oppo.market/rom_update_filter_app_list"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Landroid/app/OppoApplicationPackageManagerHelper;->CONTENT_URI_APP_LIST:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/app/ContextImpl;)V
    .locals 3
    .parameter "Context"

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/app/OppoApplicationPackageManagerHelper;->mFiltrateMarketNameList:Ljava/util/ArrayList;

    .line 38
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/app/OppoApplicationPackageManagerHelper;->mFiltrateAppNameList:Ljava/util/ArrayList;

    .line 46
    iput-object p1, p0, Landroid/app/OppoApplicationPackageManagerHelper;->mContext:Landroid/app/ContextImpl;

    .line 47
    iget-object v1, p0, Landroid/app/OppoApplicationPackageManagerHelper;->mContext:Landroid/app/ContextImpl;

    invoke-virtual {v1}, Landroid/app/ContextImpl;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 48
    .local v0, pms:Landroid/content/pm/PackageManager;
    const-string v1, "oppo.filtrated.app"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    const-string v1, "OppoApplicationPackageManagerHelper"

    const-string v2, "OppoApplicationPackageManagerHelper hasSystemFeature oppo.filtrated.app"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    const/4 v1, 0x1

    sput-boolean v1, Landroid/app/OppoApplicationPackageManagerHelper;->mFiltrateAppSwitch:Z

    .line 53
    :cond_0
    const-string v1, "OppoApplicationPackageManagerHelper"

    const-string v2, "OppoApplicationPackageManagerHelper()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    return-void
.end method

.method private getDataFromProvider()V
    .locals 10

    .prologue
    .line 57
    const/4 v7, 0x0

    .line 60
    .local v7, cursor:Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Landroid/app/OppoApplicationPackageManagerHelper;->mContext:Landroid/app/ContextImpl;

    invoke-virtual {v0}, Landroid/app/ContextImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/app/OppoApplicationPackageManagerHelper;->CONTENT_URI_MARKET_LIST:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 61
    if-eqz v7, :cond_2

    .line 62
    const-string v0, "package_name"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 63
    .local v9, marketcolumnIndex:I
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 64
    iget-object v0, p0, Landroid/app/OppoApplicationPackageManagerHelper;->mFiltrateMarketNameList:Ljava/util/ArrayList;

    invoke-interface {v7, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    const-string v0, "OppoApplicationPackageManagerHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cursor.getString(MarketcolumnIndex)="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v7, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 70
    .end local v9           #marketcolumnIndex:I
    :catch_0
    move-exception v8

    .line 71
    .local v8, e:Ljava/lang/Exception;
    :try_start_1
    const-string v0, "OppoApplicationPackageManagerHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "We can not get MarketName data from provider,because of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 73
    if-eqz v7, :cond_0

    .line 74
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 75
    const/4 v7, 0x0

    .line 80
    .end local v8           #e:Ljava/lang/Exception;
    :cond_0
    :goto_1
    :try_start_2
    iget-object v0, p0, Landroid/app/OppoApplicationPackageManagerHelper;->mContext:Landroid/app/ContextImpl;

    invoke-virtual {v0}, Landroid/app/ContextImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/app/OppoApplicationPackageManagerHelper;->CONTENT_URI_APP_LIST:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 81
    if-eqz v7, :cond_5

    .line 82
    const-string v0, "package_name"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 83
    .local v6, appcolumnIndex:I
    :goto_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 84
    iget-object v0, p0, Landroid/app/OppoApplicationPackageManagerHelper;->mFiltrateAppNameList:Ljava/util/ArrayList;

    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    const-string v0, "OppoApplicationPackageManagerHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cursor.getString(appcolumnIndex)="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 90
    .end local v6           #appcolumnIndex:I
    :catch_1
    move-exception v8

    .line 91
    .restart local v8       #e:Ljava/lang/Exception;
    :try_start_3
    const-string v0, "OppoApplicationPackageManagerHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "We can not get AppName data from provider,because of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 93
    if-eqz v7, :cond_1

    .line 94
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 95
    const/4 v7, 0x0

    .line 98
    .end local v8           #e:Ljava/lang/Exception;
    :cond_1
    :goto_3
    return-void

    .line 68
    :cond_2
    :try_start_4
    const-string v0, "OppoApplicationPackageManagerHelper"

    const-string v1, "The MarketName cursor is null !!!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 73
    :cond_3
    if-eqz v7, :cond_0

    .line 74
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 75
    const/4 v7, 0x0

    goto :goto_1

    .line 73
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_4

    .line 74
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 75
    const/4 v7, 0x0

    :cond_4
    throw v0

    .line 88
    :cond_5
    :try_start_5
    const-string v0, "OppoApplicationPackageManagerHelper"

    const-string v1, "The AppName cursor is null !!!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    .line 93
    :cond_6
    if-eqz v7, :cond_1

    .line 94
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 95
    const/4 v7, 0x0

    goto :goto_3

    .line 93
    :catchall_1
    move-exception v0

    if-eqz v7, :cond_7

    .line 94
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 95
    const/4 v7, 0x0

    :cond_7
    throw v0
.end method


# virtual methods
.method public filtratingApp(Landroid/content/pm/IPackageManager;Ljava/util/List;)V
    .locals 11
    .parameter "pm"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/IPackageManager;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/PackageInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 102
    .local p2, packageInfos:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    const-string v8, "OppoApplicationPackageManagerHelper"

    const-string v9, "filtratingApp"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :try_start_0
    sget-boolean v8, Landroid/app/OppoApplicationPackageManagerHelper;->mFiltrateAppSwitch:Z

    if-eqz v8, :cond_7

    .line 106
    invoke-direct {p0}, Landroid/app/OppoApplicationPackageManagerHelper;->getDataFromProvider()V

    .line 108
    iget-object v8, p0, Landroid/app/OppoApplicationPackageManagerHelper;->mFiltrateAppNameList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_6

    iget-object v8, p0, Landroid/app/OppoApplicationPackageManagerHelper;->mFiltrateMarketNameList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_6

    .line 109
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    .line 110
    .local v1, callingUid:I
    const/4 v4, 0x0

    .line 113
    .local v4, isBadMarket:Z
    const-string v8, "OppoApplicationPackageManagerHelper"

    const-string v9, "mFiltrateMarketNameListt is not empty!"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    const/4 v6, 0x0

    .local v6, m:I
    :goto_0
    iget-object v8, p0, Landroid/app/OppoApplicationPackageManagerHelper;->mFiltrateMarketNameList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v6, v8, :cond_0

    .line 115
    const-string v9, "OppoApplicationPackageManagerHelper"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "mFiltrateMarketNameList["

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "]="

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v8, p0, Landroid/app/OppoApplicationPackageManagerHelper;->mFiltrateMarketNameList:Ljava/util/ArrayList;

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 118
    :cond_0
    const-string v8, "OppoApplicationPackageManagerHelper"

    const-string v9, "mFiltrateAppNameList is not empty!"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    const/4 v7, 0x0

    .local v7, n:I
    :goto_1
    iget-object v8, p0, Landroid/app/OppoApplicationPackageManagerHelper;->mFiltrateAppNameList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v7, v8, :cond_1

    .line 120
    const-string v9, "OppoApplicationPackageManagerHelper"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "mFiltrateAppNameList["

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "]="

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v8, p0, Landroid/app/OppoApplicationPackageManagerHelper;->mFiltrateAppNameList:Ljava/util/ArrayList;

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 124
    :cond_1
    const/16 v8, 0x2710

    if-le v1, v8, :cond_5

    .line 125
    invoke-interface {p1, v1}, Landroid/content/pm/IPackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    .line 126
    .local v0, MarketName:[Ljava/lang/String;
    const/4 v5, 0x0

    .local v5, j:I
    :goto_2
    array-length v8, v0

    if-ge v5, v8, :cond_2

    .line 127
    iget-object v8, p0, Landroid/app/OppoApplicationPackageManagerHelper;->mFiltrateMarketNameList:Ljava/util/ArrayList;

    aget-object v9, v0, v5

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    .line 128
    const-string v8, "OppoApplicationPackageManagerHelper"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "The MarketName["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    aget-object v10, v0, v5

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " called by Binder.getCallingUid()= "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    const-string v8, "OppoApplicationPackageManagerHelper"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "The market isBadMarket = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    if-eqz v4, :cond_4

    .line 136
    :cond_2
    if-eqz v4, :cond_5

    .line 137
    const/4 v3, 0x0

    .local v3, i:I
    :goto_3
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v8

    if-ge v3, v8, :cond_5

    .line 138
    iget-object v9, p0, Landroid/app/OppoApplicationPackageManagerHelper;->mFiltrateAppNameList:Ljava/util/ArrayList;

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/pm/PackageInfo;

    iget-object v8, v8, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 139
    const-string v9, "OppoApplicationPackageManagerHelper"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "The "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/pm/PackageInfo;

    iget-object v8, v8, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " is filtrated!!!"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    invoke-interface {p2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 137
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 126
    .end local v3           #i:I
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    .line 146
    .end local v0           #MarketName:[Ljava/lang/String;
    .end local v5           #j:I
    :cond_5
    iget-object v8, p0, Landroid/app/OppoApplicationPackageManagerHelper;->mFiltrateMarketNameList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 147
    iget-object v8, p0, Landroid/app/OppoApplicationPackageManagerHelper;->mFiltrateAppNameList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 157
    .end local v1           #callingUid:I
    .end local v4           #isBadMarket:Z
    .end local v6           #m:I
    .end local v7           #n:I
    :goto_4
    return-void

    .line 149
    :cond_6
    const-string v8, "OppoApplicationPackageManagerHelper"

    const-string v9, "mFiltrateAppNameList or mFiltrateMarketNameList is empty !!!!"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    .line 154
    :catch_0
    move-exception v2

    .line 155
    .local v2, e:Landroid/os/RemoteException;
    new-instance v8, Ljava/lang/RuntimeException;

    const-string v9, "Package manager has died"

    invoke-direct {v8, v9, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8

    .line 152
    .end local v2           #e:Landroid/os/RemoteException;
    :cond_7
    :try_start_1
    const-string v8, "OppoApplicationPackageManagerHelper"

    const-string v9, "OppoApplicationPackageManagerHelper do not hasSystemFeature oppo.filtrated.app !!!"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4
.end method
