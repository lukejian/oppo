.class public interface abstract Landroid/app/IOppoActivityManager;
.super Ljava/lang/Object;
.source "IOppoActivityManager.java"


# static fields
.field public static final GET_TOP_ACTIVITY_COMPONENTNAME_TRANSACTION:I = 0x2717

.field public static final KILL_PID_FORCE:I = 0x2718

.field public static final OPPO_CALL_TRANSACTION_INDEX:I = 0x2710

.field public static final OPPO_FIRST_CALL_TRANSACTION:I = 0x2711

.field public static final SET_PROPERTIES_TRANSACTION:I = 0x2716


# virtual methods
.method public abstract getTopActivityComponentName()Landroid/content/ComponentName;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract killPidForce(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract setSystemProperties(Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
