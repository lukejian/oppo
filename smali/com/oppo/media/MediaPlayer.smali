.class public Lcom/oppo/media/MediaPlayer;
.super Ljava/lang/Object;
.source "MediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/oppo/media/MediaPlayer$OnInfoListener;,
        Lcom/oppo/media/MediaPlayer$OnErrorListener;,
        Lcom/oppo/media/MediaPlayer$OnVideoSizeChangedListener;,
        Lcom/oppo/media/MediaPlayer$OnBufferingUpdateListener;,
        Lcom/oppo/media/MediaPlayer$OnCompletionListener;,
        Lcom/oppo/media/MediaPlayer$OnSeekCompleteListener;,
        Lcom/oppo/media/MediaPlayer$OnPreparedListener;,
        Lcom/oppo/media/MediaPlayer$TrackInfo;
    }
.end annotation


# static fields
.field public static final MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:I = 0xc8

.field public static final MEDIA_ERROR_SERVER_DIED:I = 0x64

.field public static final MEDIA_ERROR_UNKNOWN:I = 0x1

.field public static final MEDIA_INFO_HAS_UNSUPPORT_VIDEO:I = 0x35c

.field public static final MEDIA_PLAYER_IDLE:I = 0x1

.field public static final MEDIA_PLAYER_INITIALIZED:I = 0x2

.field public static final MEDIA_PLAYER_PAUSED:I = 0x20

.field public static final MEDIA_PLAYER_PLAYBACK_COMPLETE:I = 0x80

.field public static final MEDIA_PLAYER_PREPARED:I = 0x8

.field public static final MEDIA_PLAYER_PREPARING:I = 0x4

.field public static final MEDIA_PLAYER_STARTED:I = 0x10

.field public static final MEDIA_PLAYER_STOPPED:I = 0x40

.field private static TAG:Ljava/lang/String;


# instance fields
.field private isOppoCreat:Z

.field private mAudioSessionId:I

.field private mAudioStreamType:I

.field private mContext:Landroid/content/Context;

.field private mCurrentState:I

.field private mHeaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mNeedSeeking:Z

.field private mOnBufferingUpdateListener:Lcom/oppo/media/MediaPlayer$OnBufferingUpdateListener;

.field private mOnCompletionListener:Lcom/oppo/media/MediaPlayer$OnCompletionListener;

.field private mOnErrorListener:Lcom/oppo/media/MediaPlayer$OnErrorListener;

.field private mOnInfoListener:Lcom/oppo/media/MediaPlayer$OnInfoListener;

.field private mOnPreparedListener:Lcom/oppo/media/MediaPlayer$OnPreparedListener;

.field private mOnSeekCompleteListener:Lcom/oppo/media/MediaPlayer$OnSeekCompleteListener;

.field private mOnVideoSizeChangedListener:Lcom/oppo/media/MediaPlayer$OnVideoSizeChangedListener;

.field private mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

.field private mPath:Ljava/lang/String;

.field private mPrepareAsync:Z

.field private mScreenOn:Z

.field private mSeekMs:I

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mUri:Landroid/net/Uri;

.field private mfd:Ljava/io/FileDescriptor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-string v0, "com_oppo_media_MediaPlayer"

    sput-object v0, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-boolean v2, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    .line 62
    iput-boolean v3, p0, Lcom/oppo/media/MediaPlayer;->mPrepareAsync:Z

    .line 63
    iput-boolean v2, p0, Lcom/oppo/media/MediaPlayer;->mNeedSeeking:Z

    .line 66
    iput-object v0, p0, Lcom/oppo/media/MediaPlayer;->mUri:Landroid/net/Uri;

    .line 67
    iput-object v0, p0, Lcom/oppo/media/MediaPlayer;->mPath:Ljava/lang/String;

    .line 68
    iput-object v0, p0, Lcom/oppo/media/MediaPlayer;->mContext:Landroid/content/Context;

    .line 69
    iput-object v0, p0, Lcom/oppo/media/MediaPlayer;->mfd:Ljava/io/FileDescriptor;

    .line 70
    iput-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 71
    iput-object v0, p0, Lcom/oppo/media/MediaPlayer;->mHeaders:Ljava/util/Map;

    .line 72
    iput-object v0, p0, Lcom/oppo/media/MediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 73
    iput-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    .line 77
    iput v1, p0, Lcom/oppo/media/MediaPlayer;->mSeekMs:I

    .line 78
    iput v1, p0, Lcom/oppo/media/MediaPlayer;->mAudioSessionId:I

    .line 79
    iput v1, p0, Lcom/oppo/media/MediaPlayer;->mAudioStreamType:I

    .line 80
    iput-boolean v2, p0, Lcom/oppo/media/MediaPlayer;->mScreenOn:Z

    .line 83
    sget-object v0, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    const-string v1, "com_oppo_media_MediaPlayer is running"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    iput v3, p0, Lcom/oppo/media/MediaPlayer;->mCurrentState:I

    .line 86
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 88
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/oppo/media/MediaPlayer$1;

    invoke-direct {v1, p0}, Lcom/oppo/media/MediaPlayer$1;-><init>(Lcom/oppo/media/MediaPlayer;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 97
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/oppo/media/MediaPlayer$2;

    invoke-direct {v1, p0}, Lcom/oppo/media/MediaPlayer$2;-><init>(Lcom/oppo/media/MediaPlayer;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    .line 107
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/oppo/media/MediaPlayer$3;

    invoke-direct {v1, p0}, Lcom/oppo/media/MediaPlayer$3;-><init>(Lcom/oppo/media/MediaPlayer;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 116
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/oppo/media/MediaPlayer$4;

    invoke-direct {v1, p0}, Lcom/oppo/media/MediaPlayer$4;-><init>(Lcom/oppo/media/MediaPlayer;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 133
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/oppo/media/MediaPlayer$5;

    invoke-direct {v1, p0}, Lcom/oppo/media/MediaPlayer$5;-><init>(Lcom/oppo/media/MediaPlayer;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 143
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/oppo/media/MediaPlayer$6;

    invoke-direct {v1, p0}, Lcom/oppo/media/MediaPlayer$6;-><init>(Lcom/oppo/media/MediaPlayer;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 155
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/oppo/media/MediaPlayer$7;

    invoke-direct {v1, p0}, Lcom/oppo/media/MediaPlayer$7;-><init>(Lcom/oppo/media/MediaPlayer;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 169
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .parameter "context"

    .prologue
    .line 172
    invoke-direct {p0}, Lcom/oppo/media/MediaPlayer;-><init>()V

    .line 173
    iput-object p1, p0, Lcom/oppo/media/MediaPlayer;->mContext:Landroid/content/Context;

    .line 174
    sget-object v0, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "com_oppo_media_MediaPlayer is running:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    return-void
.end method

.method static synthetic access$000(Lcom/oppo/media/MediaPlayer;)Lcom/oppo/media/MediaPlayer$OnPreparedListener;
    .locals 1
    .parameter "x0"

    .prologue
    .line 33
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOnPreparedListener:Lcom/oppo/media/MediaPlayer$OnPreparedListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/oppo/media/MediaPlayer;)Lcom/oppo/media/MediaPlayer$OnBufferingUpdateListener;
    .locals 1
    .parameter "x0"

    .prologue
    .line 33
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOnBufferingUpdateListener:Lcom/oppo/media/MediaPlayer$OnBufferingUpdateListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/oppo/media/MediaPlayer;)Lcom/oppo/media/MediaPlayer$OnCompletionListener;
    .locals 1
    .parameter "x0"

    .prologue
    .line 33
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOnCompletionListener:Lcom/oppo/media/MediaPlayer$OnCompletionListener;

    return-object v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/oppo/media/MediaPlayer;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 33
    iget v0, p0, Lcom/oppo/media/MediaPlayer;->mCurrentState:I

    return v0
.end method

.method static synthetic access$500(Lcom/oppo/media/MediaPlayer;)Lcom/oppo/media/MediaPlayer$OnErrorListener;
    .locals 1
    .parameter "x0"

    .prologue
    .line 33
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOnErrorListener:Lcom/oppo/media/MediaPlayer$OnErrorListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/oppo/media/MediaPlayer;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/oppo/media/MediaPlayer;->handleMediaPlayerError()V

    return-void
.end method

.method static synthetic access$700(Lcom/oppo/media/MediaPlayer;)Lcom/oppo/media/MediaPlayer$OnSeekCompleteListener;
    .locals 1
    .parameter "x0"

    .prologue
    .line 33
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOnSeekCompleteListener:Lcom/oppo/media/MediaPlayer$OnSeekCompleteListener;

    return-object v0
.end method

.method static synthetic access$800(Lcom/oppo/media/MediaPlayer;)Lcom/oppo/media/MediaPlayer$OnVideoSizeChangedListener;
    .locals 1
    .parameter "x0"

    .prologue
    .line 33
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOnVideoSizeChangedListener:Lcom/oppo/media/MediaPlayer$OnVideoSizeChangedListener;

    return-object v0
.end method

.method static synthetic access$900(Lcom/oppo/media/MediaPlayer;)Lcom/oppo/media/MediaPlayer$OnInfoListener;
    .locals 1
    .parameter "x0"

    .prologue
    .line 33
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOnInfoListener:Lcom/oppo/media/MediaPlayer$OnInfoListener;

    return-object v0
.end method

.method private createOppoMediaPlayer()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 262
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 265
    :cond_0
    iput-boolean v2, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    .line 267
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    if-eqz v0, :cond_1

    .line 268
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0}, Lcom/oppo/media/OppoMediaPlayer;->reset()V

    .line 355
    :goto_0
    return-void

    .line 272
    :cond_1
    new-instance v0, Lcom/oppo/media/OppoMediaPlayer;

    iget-object v1, p0, Lcom/oppo/media/MediaPlayer;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/oppo/media/OppoMediaPlayer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    .line 274
    iget v0, p0, Lcom/oppo/media/MediaPlayer;->mAudioSessionId:I

    if-ltz v0, :cond_2

    .line 275
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    iget v1, p0, Lcom/oppo/media/MediaPlayer;->mAudioSessionId:I

    invoke-virtual {v0, v1}, Lcom/oppo/media/OppoMediaPlayer;->setAudioSessionId(I)V

    .line 277
    :cond_2
    iget v0, p0, Lcom/oppo/media/MediaPlayer;->mAudioStreamType:I

    if-ltz v0, :cond_3

    .line 278
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    iget v1, p0, Lcom/oppo/media/MediaPlayer;->mAudioStreamType:I

    invoke-virtual {v0, v1}, Lcom/oppo/media/OppoMediaPlayer;->setAudioStreamType(I)V

    .line 281
    :cond_3
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->mScreenOn:Z

    if-eqz v0, :cond_4

    .line 282
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0, v2}, Lcom/oppo/media/OppoMediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 285
    :cond_4
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    new-instance v1, Lcom/oppo/media/MediaPlayer$8;

    invoke-direct {v1, p0}, Lcom/oppo/media/MediaPlayer$8;-><init>(Lcom/oppo/media/MediaPlayer;)V

    invoke-virtual {v0, v1}, Lcom/oppo/media/OppoMediaPlayer;->setOnPreparedListener(Lcom/oppo/media/OppoMediaPlayer$OnPreparedListener;)V

    .line 295
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    new-instance v1, Lcom/oppo/media/MediaPlayer$9;

    invoke-direct {v1, p0}, Lcom/oppo/media/MediaPlayer$9;-><init>(Lcom/oppo/media/MediaPlayer;)V

    invoke-virtual {v0, v1}, Lcom/oppo/media/OppoMediaPlayer;->setOnBufferingUpdateListener(Lcom/oppo/media/OppoMediaPlayer$OnBufferingUpdateListener;)V

    .line 305
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    new-instance v1, Lcom/oppo/media/MediaPlayer$10;

    invoke-direct {v1, p0}, Lcom/oppo/media/MediaPlayer$10;-><init>(Lcom/oppo/media/MediaPlayer;)V

    invoke-virtual {v0, v1}, Lcom/oppo/media/OppoMediaPlayer;->setOnCompletionListener(Lcom/oppo/media/OppoMediaPlayer$OnCompletionListener;)V

    .line 314
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    new-instance v1, Lcom/oppo/media/MediaPlayer$11;

    invoke-direct {v1, p0}, Lcom/oppo/media/MediaPlayer$11;-><init>(Lcom/oppo/media/MediaPlayer;)V

    invoke-virtual {v0, v1}, Lcom/oppo/media/OppoMediaPlayer;->setOnErrorListener(Lcom/oppo/media/OppoMediaPlayer$OnErrorListener;)V

    .line 325
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    new-instance v1, Lcom/oppo/media/MediaPlayer$12;

    invoke-direct {v1, p0}, Lcom/oppo/media/MediaPlayer$12;-><init>(Lcom/oppo/media/MediaPlayer;)V

    invoke-virtual {v0, v1}, Lcom/oppo/media/OppoMediaPlayer;->setOnSeekCompleteListener(Lcom/oppo/media/OppoMediaPlayer$OnSeekCompleteListener;)V

    .line 334
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    new-instance v1, Lcom/oppo/media/MediaPlayer$13;

    invoke-direct {v1, p0}, Lcom/oppo/media/MediaPlayer$13;-><init>(Lcom/oppo/media/MediaPlayer;)V

    invoke-virtual {v0, v1}, Lcom/oppo/media/OppoMediaPlayer;->setOnVideoSizeChangedListener(Lcom/oppo/media/OppoMediaPlayer$OnVideoSizeChangedListener;)V

    .line 345
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    new-instance v1, Lcom/oppo/media/MediaPlayer$14;

    invoke-direct {v1, p0}, Lcom/oppo/media/MediaPlayer$14;-><init>(Lcom/oppo/media/MediaPlayer;)V

    invoke-virtual {v0, v1}, Lcom/oppo/media/OppoMediaPlayer;->setOnInfoListener(Lcom/oppo/media/OppoMediaPlayer$OnInfoListener;)V

    goto :goto_0
.end method

.method private handleMediaPlayerError()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 180
    sget-object v3, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleMediaPlayerError() mCurrentState="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/oppo/media/MediaPlayer;->mCurrentState:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    invoke-direct {p0}, Lcom/oppo/media/MediaPlayer;->createOppoMediaPlayer()V

    .line 183
    iget v2, p0, Lcom/oppo/media/MediaPlayer;->mCurrentState:I

    .line 184
    .local v2, mChangeState:I
    iput v7, p0, Lcom/oppo/media/MediaPlayer;->mCurrentState:I

    .line 186
    const/4 v3, 0x2

    if-lt v2, v3, :cond_0

    .line 188
    :try_start_0
    iget-object v3, p0, Lcom/oppo/media/MediaPlayer;->mfd:Ljava/io/FileDescriptor;

    if-eqz v3, :cond_4

    .line 189
    sget-object v3, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    const-string v4, "handleMediaPlayerError() mOppoMediaPlayer setDataSource(FileDescriptor)"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    iget-object v3, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    iget-object v4, p0, Lcom/oppo/media/MediaPlayer;->mfd:Ljava/io/FileDescriptor;

    invoke-virtual {v3, v4}, Lcom/oppo/media/OppoMediaPlayer;->setDataSource(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 215
    :goto_0
    iget-object v3, p0, Lcom/oppo/media/MediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v3, :cond_0

    .line 216
    iget-object v3, p0, Lcom/oppo/media/MediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {p0, v3}, Lcom/oppo/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 220
    :cond_0
    const/4 v3, 0x4

    if-lt v2, v3, :cond_1

    .line 221
    sget-object v3, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleMediaPlayerError() mOppoMediaPlayer prepare mPrepareAsync="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/oppo/media/MediaPlayer;->mPrepareAsync:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    iget-boolean v3, p0, Lcom/oppo/media/MediaPlayer;->mPrepareAsync:Z

    if-eqz v3, :cond_7

    .line 225
    :try_start_1
    invoke-virtual {p0}, Lcom/oppo/media/MediaPlayer;->prepareAsync()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 248
    :cond_1
    :goto_1
    iget v3, p0, Lcom/oppo/media/MediaPlayer;->mSeekMs:I

    if-ltz v3, :cond_2

    .line 249
    iput-boolean v7, p0, Lcom/oppo/media/MediaPlayer;->mNeedSeeking:Z

    .line 252
    :cond_2
    const/16 v3, 0x10

    if-lt v2, v3, :cond_3

    .line 253
    sget-object v3, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleMediaPlayerError() mOppoMediaPlayer start mPrepareAsync="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/oppo/media/MediaPlayer;->mPrepareAsync:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    iget-boolean v3, p0, Lcom/oppo/media/MediaPlayer;->mPrepareAsync:Z

    if-nez v3, :cond_3

    .line 256
    invoke-virtual {p0}, Lcom/oppo/media/MediaPlayer;->start()V

    .line 259
    :cond_3
    :goto_2
    return-void

    .line 192
    :cond_4
    :try_start_2
    iget-object v3, p0, Lcom/oppo/media/MediaPlayer;->mPath:Ljava/lang/String;

    if-eqz v3, :cond_5

    .line 193
    sget-object v3, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    const-string v4, "handleMediaPlayerError() mOppoMediaPlayer setDataSource(String)"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    iget-object v3, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    iget-object v4, p0, Lcom/oppo/media/MediaPlayer;->mPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/oppo/media/OppoMediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 206
    :catch_0
    move-exception v1

    .line 207
    .local v1, ex:Ljava/lang/Exception;
    sget-object v3, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mOppoMediaPlayer setDataSource error mPath="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/oppo/media/MediaPlayer;->mPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mUri="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/oppo/media/MediaPlayer;->mUri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 209
    iget-object v3, p0, Lcom/oppo/media/MediaPlayer;->mOnErrorListener:Lcom/oppo/media/MediaPlayer$OnErrorListener;

    if-eqz v3, :cond_3

    .line 210
    iget-object v3, p0, Lcom/oppo/media/MediaPlayer;->mOnErrorListener:Lcom/oppo/media/MediaPlayer$OnErrorListener;

    invoke-interface {v3, p0, v7, v8}, Lcom/oppo/media/MediaPlayer$OnErrorListener;->onError(Lcom/oppo/media/MediaPlayer;II)Z

    goto :goto_2

    .line 195
    .end local v1           #ex:Ljava/lang/Exception;
    :cond_5
    :try_start_3
    iget-object v3, p0, Lcom/oppo/media/MediaPlayer;->mUri:Landroid/net/Uri;

    if-eqz v3, :cond_6

    .line 196
    sget-object v3, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    const-string v4, "handleMediaPlayerError() mOppoMediaPlayer setDataSource(Context, Uri, Map)"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    iget-object v3, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    iget-object v4, p0, Lcom/oppo/media/MediaPlayer;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/oppo/media/MediaPlayer;->mUri:Landroid/net/Uri;

    iget-object v6, p0, Lcom/oppo/media/MediaPlayer;->mHeaders:Ljava/util/Map;

    invoke-virtual {v3, v4, v5, v6}, Lcom/oppo/media/OppoMediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    goto/16 :goto_0

    .line 200
    :cond_6
    sget-object v3, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    const-string v4, "handleMediaPlayerError() mOppoMediaPlayer setDataSource null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    iget-object v3, p0, Lcom/oppo/media/MediaPlayer;->mOnErrorListener:Lcom/oppo/media/MediaPlayer$OnErrorListener;

    if-eqz v3, :cond_3

    .line 202
    iget-object v3, p0, Lcom/oppo/media/MediaPlayer;->mOnErrorListener:Lcom/oppo/media/MediaPlayer$OnErrorListener;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-interface {v3, p0, v4, v5}, Lcom/oppo/media/MediaPlayer$OnErrorListener;->onError(Lcom/oppo/media/MediaPlayer;II)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    .line 226
    :catch_1
    move-exception v0

    .line 227
    .local v0, e:Ljava/lang/IllegalStateException;
    sget-object v3, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    const-string v4, "handleMediaPlayerError() prepareAsync error!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 229
    iget-object v3, p0, Lcom/oppo/media/MediaPlayer;->mOnErrorListener:Lcom/oppo/media/MediaPlayer$OnErrorListener;

    if-eqz v3, :cond_3

    .line 230
    iget-object v3, p0, Lcom/oppo/media/MediaPlayer;->mOnErrorListener:Lcom/oppo/media/MediaPlayer$OnErrorListener;

    invoke-interface {v3, p0, v7, v8}, Lcom/oppo/media/MediaPlayer$OnErrorListener;->onError(Lcom/oppo/media/MediaPlayer;II)Z

    goto/16 :goto_2

    .line 236
    .end local v0           #e:Ljava/lang/IllegalStateException;
    :cond_7
    :try_start_4
    invoke-virtual {p0}, Lcom/oppo/media/MediaPlayer;->prepare()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_1

    .line 237
    :catch_2
    move-exception v0

    .line 238
    .local v0, e:Ljava/io/IOException;
    sget-object v3, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    const-string v4, "handleMediaPlayerError() prepare error!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 240
    iget-object v3, p0, Lcom/oppo/media/MediaPlayer;->mOnErrorListener:Lcom/oppo/media/MediaPlayer$OnErrorListener;

    if-eqz v3, :cond_3

    .line 241
    iget-object v3, p0, Lcom/oppo/media/MediaPlayer;->mOnErrorListener:Lcom/oppo/media/MediaPlayer$OnErrorListener;

    invoke-interface {v3, p0, v7, v8}, Lcom/oppo/media/MediaPlayer$OnErrorListener;->onError(Lcom/oppo/media/MediaPlayer;II)Z

    goto/16 :goto_2
.end method


# virtual methods
.method public deselectTrack(I)V
    .locals 1
    .parameter "index"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 545
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v0, :cond_0

    .line 546
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0, p1}, Lcom/oppo/media/OppoMediaPlayer;->deselectTrack(I)V

    .line 550
    :goto_0
    return-void

    .line 548
    :cond_0
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->deselectTrack(I)V

    goto :goto_0
.end method

.method public getAudioSessionId()I
    .locals 3

    .prologue
    .line 382
    sget-object v0, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAudioSessionId() isOppoCreat="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0}, Lcom/oppo/media/OppoMediaPlayer;->getAudioSessionId()I

    move-result v0

    iput v0, p0, Lcom/oppo/media/MediaPlayer;->mAudioSessionId:I

    .line 390
    :goto_0
    iget v0, p0, Lcom/oppo/media/MediaPlayer;->mAudioSessionId:I

    return v0

    .line 387
    :cond_0
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getAudioSessionId()I

    move-result v0

    iput v0, p0, Lcom/oppo/media/MediaPlayer;->mAudioSessionId:I

    goto :goto_0
.end method

.method public getCurrentPosition()I
    .locals 2

    .prologue
    .line 874
    iget v0, p0, Lcom/oppo/media/MediaPlayer;->mCurrentState:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 875
    const/4 v0, 0x0

    .line 881
    :goto_0
    return v0

    .line 878
    :cond_0
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v0, :cond_1

    .line 879
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0}, Lcom/oppo/media/OppoMediaPlayer;->getCurrentPosition()I

    move-result v0

    goto :goto_0

    .line 881
    :cond_1
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    goto :goto_0
.end method

.method public getDuration()I
    .locals 2

    .prologue
    .line 860
    iget v0, p0, Lcom/oppo/media/MediaPlayer;->mCurrentState:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 861
    const/4 v0, -0x1

    .line 867
    :goto_0
    return v0

    .line 864
    :cond_0
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v0, :cond_1

    .line 865
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0}, Lcom/oppo/media/OppoMediaPlayer;->getDuration()I

    move-result v0

    goto :goto_0

    .line 867
    :cond_1
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    goto :goto_0
.end method

.method public getTrackInfo()[Lcom/oppo/media/MediaPlayer$TrackInfo;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 464
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 466
    .local v1, mParcel:Landroid/os/Parcel;
    iget-boolean v4, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v4, :cond_0

    .line 467
    iget-object v4, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v4}, Lcom/oppo/media/OppoMediaPlayer;->getTrackInfo()[Lcom/oppo/media/OppoMediaPlayer$TrackInfo;

    move-result-object v2

    .line 468
    .local v2, mtrackInfo:[Lcom/oppo/media/OppoMediaPlayer$TrackInfo;
    array-length v4, v2

    new-array v3, v4, [Lcom/oppo/media/MediaPlayer$TrackInfo;

    .line 469
    .local v3, trackInfo:[Lcom/oppo/media/MediaPlayer$TrackInfo;
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_1

    .line 470
    invoke-virtual {v1, v5}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 471
    aget-object v4, v2, v0

    invoke-virtual {v4}, Lcom/oppo/media/OppoMediaPlayer$TrackInfo;->getTrackType()I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 472
    aget-object v4, v2, v0

    invoke-virtual {v4}, Lcom/oppo/media/OppoMediaPlayer$TrackInfo;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 473
    invoke-virtual {v1, v5}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 474
    new-instance v4, Lcom/oppo/media/MediaPlayer$TrackInfo;

    invoke-direct {v4, v1}, Lcom/oppo/media/MediaPlayer$TrackInfo;-><init>(Landroid/os/Parcel;)V

    aput-object v4, v3, v0

    .line 469
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 477
    .end local v0           #i:I
    .end local v2           #mtrackInfo:[Lcom/oppo/media/OppoMediaPlayer$TrackInfo;
    .end local v3           #trackInfo:[Lcom/oppo/media/MediaPlayer$TrackInfo;
    :cond_0
    iget-object v4, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v2

    .line 478
    .local v2, mtrackInfo:[Landroid/media/MediaPlayer$TrackInfo;
    array-length v4, v2

    new-array v3, v4, [Lcom/oppo/media/MediaPlayer$TrackInfo;

    .line 479
    .restart local v3       #trackInfo:[Lcom/oppo/media/MediaPlayer$TrackInfo;
    const/4 v0, 0x0

    .restart local v0       #i:I
    :goto_1
    array-length v4, v2

    if-ge v0, v4, :cond_1

    .line 480
    invoke-virtual {v1, v5}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 481
    aget-object v4, v2, v0

    invoke-virtual {v4}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 482
    aget-object v4, v2, v0

    invoke-virtual {v4}, Landroid/media/MediaPlayer$TrackInfo;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 483
    invoke-virtual {v1, v5}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 484
    new-instance v4, Lcom/oppo/media/MediaPlayer$TrackInfo;

    invoke-direct {v4, v1}, Lcom/oppo/media/MediaPlayer$TrackInfo;-><init>(Landroid/os/Parcel;)V

    aput-object v4, v3, v0

    .line 479
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 487
    .end local v2           #mtrackInfo:[Landroid/media/MediaPlayer$TrackInfo;
    :cond_1
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 489
    return-object v3
.end method

.method public getVideoHeight()I
    .locals 3

    .prologue
    .line 806
    sget-object v0, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getVideoHeight() isOppoCreat="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 808
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v0, :cond_0

    .line 809
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0}, Lcom/oppo/media/OppoMediaPlayer;->getVideoHeight()I

    move-result v0

    .line 811
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getVideoWidth()I
    .locals 3

    .prologue
    .line 796
    sget-object v0, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getVideoWidth() isOppoCreat="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v0, :cond_0

    .line 799
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0}, Lcom/oppo/media/OppoMediaPlayer;->getVideoWidth()I

    move-result v0

    .line 801
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v0

    goto :goto_0
.end method

.method public isLooping()Z
    .locals 1

    .prologue
    .line 743
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v0, :cond_0

    .line 744
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0}, Lcom/oppo/media/OppoMediaPlayer;->isLooping()Z

    move-result v0

    .line 746
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isLooping()Z

    move-result v0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 818
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v0, :cond_0

    .line 819
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0}, Lcom/oppo/media/OppoMediaPlayer;->isPlaying()Z

    move-result v0

    .line 821
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    goto :goto_0
.end method

.method public pause()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 846
    sget-object v0, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pause() isOppoCreat="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 848
    const/16 v0, 0x20

    iput v0, p0, Lcom/oppo/media/MediaPlayer;->mCurrentState:I

    .line 850
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v0, :cond_0

    .line 851
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0}, Lcom/oppo/media/OppoMediaPlayer;->pause()V

    .line 855
    :goto_0
    return-void

    .line 853
    :cond_0
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    goto :goto_0
.end method

.method public prepare()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 778
    sget-object v1, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "prepare() isOppoCreat="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 780
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/oppo/media/MediaPlayer;->mPrepareAsync:Z

    .line 781
    const/4 v1, 0x4

    iput v1, p0, Lcom/oppo/media/MediaPlayer;->mCurrentState:I

    .line 783
    iget-boolean v1, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v1, :cond_0

    .line 784
    iget-object v1, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v1}, Lcom/oppo/media/OppoMediaPlayer;->prepare()V

    .line 793
    :goto_0
    return-void

    .line 787
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 788
    :catch_0
    move-exception v0

    .line 789
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 790
    invoke-direct {p0}, Lcom/oppo/media/MediaPlayer;->handleMediaPlayerError()V

    goto :goto_0
.end method

.method public prepareAsync()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 765
    sget-object v0, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "prepareAsync() isOppoCreat="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 767
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->mPrepareAsync:Z

    .line 768
    const/4 v0, 0x4

    iput v0, p0, Lcom/oppo/media/MediaPlayer;->mCurrentState:I

    .line 770
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v0, :cond_0

    .line 771
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0}, Lcom/oppo/media/OppoMediaPlayer;->prepareAsync()V

    .line 775
    :goto_0
    return-void

    .line 773
    :cond_0
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    goto :goto_0
.end method

.method public release()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 584
    sget-object v0, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "release() isOppoCreat="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 586
    const/4 v0, 0x1

    iput v0, p0, Lcom/oppo/media/MediaPlayer;->mCurrentState:I

    .line 588
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    if-eqz v0, :cond_0

    .line 589
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0}, Lcom/oppo/media/OppoMediaPlayer;->release()V

    .line 590
    iput-object v5, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    .line 592
    :cond_0
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 593
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 594
    iput-object v5, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 596
    :cond_1
    iput-boolean v4, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    .line 597
    iput v3, p0, Lcom/oppo/media/MediaPlayer;->mAudioSessionId:I

    .line 598
    iput v3, p0, Lcom/oppo/media/MediaPlayer;->mAudioStreamType:I

    .line 599
    iput-boolean v4, p0, Lcom/oppo/media/MediaPlayer;->mScreenOn:Z

    .line 600
    return-void
.end method

.method public reset()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 553
    sget-object v0, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reset() isOppoCreat="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    const/4 v0, -0x1

    iput v0, p0, Lcom/oppo/media/MediaPlayer;->mSeekMs:I

    .line 556
    const/4 v0, 0x1

    iput v0, p0, Lcom/oppo/media/MediaPlayer;->mCurrentState:I

    .line 558
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v0, :cond_0

    .line 559
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0}, Lcom/oppo/media/OppoMediaPlayer;->reset()V

    .line 560
    iput-boolean v3, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    .line 564
    :goto_0
    iput-boolean v3, p0, Lcom/oppo/media/MediaPlayer;->mScreenOn:Z

    .line 565
    return-void

    .line 562
    :cond_0
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    goto :goto_0
.end method

.method public seekTo(I)V
    .locals 3
    .parameter "msec"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 886
    sget-object v0, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "seekTo() isOppoCreat="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " msec="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 888
    iput p1, p0, Lcom/oppo/media/MediaPlayer;->mSeekMs:I

    .line 890
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v0, :cond_0

    .line 891
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0, p1}, Lcom/oppo/media/OppoMediaPlayer;->seekTo(I)V

    .line 895
    :goto_0
    return-void

    .line 893
    :cond_0
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    goto :goto_0
.end method

.method public selectTrack(I)V
    .locals 1
    .parameter "index"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 522
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v0, :cond_0

    .line 523
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0, p1}, Lcom/oppo/media/OppoMediaPlayer;->selectTrack(I)V

    .line 527
    :goto_0
    return-void

    .line 525
    :cond_0
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->selectTrack(I)V

    goto :goto_0
.end method

.method public setAudioSessionId(I)V
    .locals 3
    .parameter "sessionId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 370
    sget-object v0, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAudioSessionId() isOppoCreat="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sessionId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    iput p1, p0, Lcom/oppo/media/MediaPlayer;->mAudioSessionId:I

    .line 374
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0, p1}, Lcom/oppo/media/OppoMediaPlayer;->setAudioSessionId(I)V

    .line 379
    :goto_0
    return-void

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setAudioSessionId(I)V

    goto :goto_0
.end method

.method public setAudioStreamType(I)V
    .locals 1
    .parameter "streamtype"

    .prologue
    .line 716
    iput p1, p0, Lcom/oppo/media/MediaPlayer;->mAudioStreamType:I

    .line 717
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v0, :cond_0

    .line 718
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0, p1}, Lcom/oppo/media/OppoMediaPlayer;->setAudioStreamType(I)V

    .line 722
    :goto_0
    return-void

    .line 720
    :cond_0
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    goto :goto_0
.end method

.method public setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 4
    .parameter "context"
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 604
    sget-object v1, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDataSource(Context, Uri) isOppoCreat="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 606
    iput-object p1, p0, Lcom/oppo/media/MediaPlayer;->mContext:Landroid/content/Context;

    .line 607
    iput-object p2, p0, Lcom/oppo/media/MediaPlayer;->mUri:Landroid/net/Uri;

    .line 608
    const/4 v1, 0x2

    iput v1, p0, Lcom/oppo/media/MediaPlayer;->mCurrentState:I

    .line 611
    :try_start_0
    iget-object v1, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p1, p2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 619
    :goto_0
    return-void

    .line 612
    :catch_0
    move-exception v0

    .line 613
    .local v0, e:Ljava/io/IOException;
    invoke-direct {p0}, Lcom/oppo/media/MediaPlayer;->createOppoMediaPlayer()V

    .line 614
    iget-object v1, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v1, p1, p2}, Lcom/oppo/media/OppoMediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0

    .line 615
    .end local v0           #e:Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 616
    .local v0, e:Ljava/lang/IllegalArgumentException;
    invoke-direct {p0}, Lcom/oppo/media/MediaPlayer;->createOppoMediaPlayer()V

    .line 617
    iget-object v1, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v1, p1, p2}, Lcom/oppo/media/OppoMediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    .locals 4
    .parameter "context"
    .parameter "uri"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 623
    .local p3, headers:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v1, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDataSource(Context, Uri, Map) isOppoCreat="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 625
    iput-object p1, p0, Lcom/oppo/media/MediaPlayer;->mContext:Landroid/content/Context;

    .line 626
    iput-object p2, p0, Lcom/oppo/media/MediaPlayer;->mUri:Landroid/net/Uri;

    .line 627
    iput-object p3, p0, Lcom/oppo/media/MediaPlayer;->mHeaders:Ljava/util/Map;

    .line 628
    const/4 v1, 0x2

    iput v1, p0, Lcom/oppo/media/MediaPlayer;->mCurrentState:I

    .line 631
    :try_start_0
    iget-object v1, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p1, p2, p3}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 639
    :goto_0
    return-void

    .line 632
    :catch_0
    move-exception v0

    .line 633
    .local v0, e:Ljava/io/IOException;
    invoke-direct {p0}, Lcom/oppo/media/MediaPlayer;->createOppoMediaPlayer()V

    .line 634
    iget-object v1, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v1, p1, p2, p3}, Lcom/oppo/media/OppoMediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    goto :goto_0

    .line 635
    .end local v0           #e:Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 636
    .local v0, e:Ljava/lang/IllegalArgumentException;
    invoke-direct {p0}, Lcom/oppo/media/MediaPlayer;->createOppoMediaPlayer()V

    .line 637
    iget-object v1, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v1, p1, p2, p3}, Lcom/oppo/media/OppoMediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public setDataSource(Ljava/io/FileDescriptor;)V
    .locals 4
    .parameter "fd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 661
    sget-object v1, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDataSource(FileDescriptor) isOppoCreat="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 663
    iput-object p1, p0, Lcom/oppo/media/MediaPlayer;->mfd:Ljava/io/FileDescriptor;

    .line 664
    const/4 v1, 0x2

    iput v1, p0, Lcom/oppo/media/MediaPlayer;->mCurrentState:I

    .line 667
    :try_start_0
    iget-object v1, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 675
    :goto_0
    return-void

    .line 668
    :catch_0
    move-exception v0

    .line 669
    .local v0, e:Ljava/io/IOException;
    invoke-direct {p0}, Lcom/oppo/media/MediaPlayer;->createOppoMediaPlayer()V

    .line 670
    iget-object v1, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v1, p1}, Lcom/oppo/media/OppoMediaPlayer;->setDataSource(Ljava/io/FileDescriptor;)V

    goto :goto_0

    .line 671
    .end local v0           #e:Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 672
    .local v0, e:Ljava/lang/IllegalArgumentException;
    invoke-direct {p0}, Lcom/oppo/media/MediaPlayer;->createOppoMediaPlayer()V

    .line 673
    iget-object v1, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v1, p1}, Lcom/oppo/media/OppoMediaPlayer;->setDataSource(Ljava/io/FileDescriptor;)V

    goto :goto_0
.end method

.method public setDataSource(Ljava/io/FileDescriptor;JJ)V
    .locals 7
    .parameter "fd"
    .parameter "offset"
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 679
    sget-object v0, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setDataSource(FileDescriptor, long, long) isOppoCreat="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    iput-object p1, p0, Lcom/oppo/media/MediaPlayer;->mfd:Ljava/io/FileDescriptor;

    .line 682
    const/4 v0, 0x2

    iput v0, p0, Lcom/oppo/media/MediaPlayer;->mCurrentState:I

    .line 685
    :try_start_0
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 693
    :goto_0
    return-void

    .line 686
    :catch_0
    move-exception v6

    .line 687
    .local v6, e:Ljava/io/IOException;
    invoke-direct {p0}, Lcom/oppo/media/MediaPlayer;->createOppoMediaPlayer()V

    .line 688
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/oppo/media/OppoMediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    goto :goto_0

    .line 689
    .end local v6           #e:Ljava/io/IOException;
    :catch_1
    move-exception v6

    .line 690
    .local v6, e:Ljava/lang/IllegalArgumentException;
    invoke-direct {p0}, Lcom/oppo/media/MediaPlayer;->createOppoMediaPlayer()V

    .line 691
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/oppo/media/OppoMediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    goto :goto_0
.end method

.method public setDataSource(Ljava/lang/String;)V
    .locals 4
    .parameter "path"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 643
    sget-object v1, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDataSource(String) isOppoCreat="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    iput-object p1, p0, Lcom/oppo/media/MediaPlayer;->mPath:Ljava/lang/String;

    .line 646
    const/4 v1, 0x2

    iput v1, p0, Lcom/oppo/media/MediaPlayer;->mCurrentState:I

    .line 649
    :try_start_0
    iget-object v1, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 657
    :goto_0
    return-void

    .line 650
    :catch_0
    move-exception v0

    .line 651
    .local v0, e:Ljava/io/IOException;
    invoke-direct {p0}, Lcom/oppo/media/MediaPlayer;->createOppoMediaPlayer()V

    .line 652
    iget-object v1, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v1, p1}, Lcom/oppo/media/OppoMediaPlayer;->setDataSource(Ljava/lang/String;)V

    goto :goto_0

    .line 653
    .end local v0           #e:Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 654
    .local v0, e:Ljava/lang/IllegalArgumentException;
    invoke-direct {p0}, Lcom/oppo/media/MediaPlayer;->createOppoMediaPlayer()V

    .line 655
    iget-object v1, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v1, p1}, Lcom/oppo/media/OppoMediaPlayer;->setDataSource(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setDisplay(Landroid/view/SurfaceHolder;)V
    .locals 3
    .parameter "sh"

    .prologue
    .line 696
    sget-object v0, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setDisplay() isOppoCreat="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 698
    iput-object p1, p0, Lcom/oppo/media/MediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 700
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v0, :cond_0

    .line 701
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0, p1}, Lcom/oppo/media/OppoMediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 705
    :goto_0
    return-void

    .line 703
    :cond_0
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    goto :goto_0
.end method

.method public setLooping(Z)V
    .locals 1
    .parameter "looping"

    .prologue
    .line 730
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v0, :cond_0

    .line 731
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0, p1}, Lcom/oppo/media/OppoMediaPlayer;->setLooping(Z)V

    .line 735
    :goto_0
    return-void

    .line 733
    :cond_0
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    goto :goto_0
.end method

.method public setOnBufferingUpdateListener(Lcom/oppo/media/MediaPlayer$OnBufferingUpdateListener;)V
    .locals 0
    .parameter "listener"

    .prologue
    .line 917
    iput-object p1, p0, Lcom/oppo/media/MediaPlayer;->mOnBufferingUpdateListener:Lcom/oppo/media/MediaPlayer$OnBufferingUpdateListener;

    .line 918
    return-void
.end method

.method public setOnCompletionListener(Lcom/oppo/media/MediaPlayer$OnCompletionListener;)V
    .locals 0
    .parameter "listener"

    .prologue
    .line 913
    iput-object p1, p0, Lcom/oppo/media/MediaPlayer;->mOnCompletionListener:Lcom/oppo/media/MediaPlayer$OnCompletionListener;

    .line 914
    return-void
.end method

.method public setOnErrorListener(Lcom/oppo/media/MediaPlayer$OnErrorListener;)V
    .locals 0
    .parameter "listener"

    .prologue
    .line 929
    iput-object p1, p0, Lcom/oppo/media/MediaPlayer;->mOnErrorListener:Lcom/oppo/media/MediaPlayer$OnErrorListener;

    .line 930
    return-void
.end method

.method public setOnInfoListener(Lcom/oppo/media/MediaPlayer$OnInfoListener;)V
    .locals 0
    .parameter "listener"

    .prologue
    .line 933
    iput-object p1, p0, Lcom/oppo/media/MediaPlayer;->mOnInfoListener:Lcom/oppo/media/MediaPlayer$OnInfoListener;

    .line 934
    return-void
.end method

.method public setOnPreparedListener(Lcom/oppo/media/MediaPlayer$OnPreparedListener;)V
    .locals 0
    .parameter "listener"

    .prologue
    .line 909
    iput-object p1, p0, Lcom/oppo/media/MediaPlayer;->mOnPreparedListener:Lcom/oppo/media/MediaPlayer$OnPreparedListener;

    .line 910
    return-void
.end method

.method public setOnSeekCompleteListener(Lcom/oppo/media/MediaPlayer$OnSeekCompleteListener;)V
    .locals 0
    .parameter "listener"

    .prologue
    .line 921
    iput-object p1, p0, Lcom/oppo/media/MediaPlayer;->mOnSeekCompleteListener:Lcom/oppo/media/MediaPlayer$OnSeekCompleteListener;

    .line 922
    return-void
.end method

.method public setOnVideoSizeChangedListener(Lcom/oppo/media/MediaPlayer$OnVideoSizeChangedListener;)V
    .locals 0
    .parameter "listener"

    .prologue
    .line 925
    iput-object p1, p0, Lcom/oppo/media/MediaPlayer;->mOnVideoSizeChangedListener:Lcom/oppo/media/MediaPlayer$OnVideoSizeChangedListener;

    .line 926
    return-void
.end method

.method public setScreenOnWhilePlaying(Z)V
    .locals 3
    .parameter "screenOn"

    .prologue
    .line 751
    sget-object v0, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setScreenOnWhilePlaying() isOppoCreat="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " screenOn="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 753
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v0, :cond_1

    .line 754
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0, p1}, Lcom/oppo/media/OppoMediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 759
    :goto_0
    if-eqz p1, :cond_0

    .line 760
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->mScreenOn:Z

    .line 762
    :cond_0
    return-void

    .line 756
    :cond_1
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    goto :goto_0
.end method

.method public setVolume(FF)V
    .locals 3
    .parameter "leftVolume"
    .parameter "rightVolume"

    .prologue
    .line 358
    sget-object v0, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVolume() isOppoCreat="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " leftVolume="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " rightVolume"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0, p1, p2}, Lcom/oppo/media/OppoMediaPlayer;->setVolume(FF)V

    .line 366
    :goto_0
    return-void

    .line 364
    :cond_0
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1, p2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto :goto_0
.end method

.method public setWakeMode(Landroid/content/Context;I)V
    .locals 3
    .parameter "context"
    .parameter "mode"

    .prologue
    .line 898
    sget-object v0, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setWakeMode() isOppoCreat="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 900
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v0, :cond_0

    .line 901
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0, p1, p2}, Lcom/oppo/media/OppoMediaPlayer;->setWakeMode(Landroid/content/Context;I)V

    .line 905
    :goto_0
    return-void

    .line 903
    :cond_0
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1, p2}, Landroid/media/MediaPlayer;->setWakeMode(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public start()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 826
    sget-object v0, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start() isOppoCreat="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 828
    const/16 v0, 0x10

    iput v0, p0, Lcom/oppo/media/MediaPlayer;->mCurrentState:I

    .line 830
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v0, :cond_1

    .line 831
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->mNeedSeeking:Z

    if-eqz v0, :cond_0

    .line 832
    sget-object v0, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mOppoMediaPlayer start seekTo: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/oppo/media/MediaPlayer;->mSeekMs:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 834
    iget v0, p0, Lcom/oppo/media/MediaPlayer;->mSeekMs:I

    invoke-virtual {p0, v0}, Lcom/oppo/media/MediaPlayer;->seekTo(I)V

    .line 835
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->mNeedSeeking:Z

    .line 836
    const/4 v0, -0x1

    iput v0, p0, Lcom/oppo/media/MediaPlayer;->mSeekMs:I

    .line 839
    :cond_0
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0}, Lcom/oppo/media/OppoMediaPlayer;->start()V

    .line 843
    :goto_0
    return-void

    .line 841
    :cond_1
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    goto :goto_0
.end method

.method public stop()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 571
    sget-object v0, Lcom/oppo/media/MediaPlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stop() isOppoCreat="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    const/16 v0, 0x40

    iput v0, p0, Lcom/oppo/media/MediaPlayer;->mCurrentState:I

    .line 575
    iget-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->isOppoCreat:Z

    if-eqz v0, :cond_0

    .line 576
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mOppoMediaPlayer:Lcom/oppo/media/OppoMediaPlayer;

    invoke-virtual {v0}, Lcom/oppo/media/OppoMediaPlayer;->stop()V

    .line 580
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/oppo/media/MediaPlayer;->mScreenOn:Z

    .line 581
    return-void

    .line 578
    :cond_0
    iget-object v0, p0, Lcom/oppo/media/MediaPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    goto :goto_0
.end method
