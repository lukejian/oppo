.class public Lcom/oppo/theme/OppoApkChangedInfo;
.super Ljava/lang/Object;
.source "OppoApkChangedInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/oppo/theme/OppoApkChangedInfo$IconXmlHandler;
    }
.end annotation


# static fields
.field public static final APK_CHANGED:Ljava/lang/String; = "ApkChanged.xml"

.field private static mAllPackageNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mCurrentTag:Ljava/lang/String;

.field private static mParseError:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    sput-object v0, Lcom/oppo/theme/OppoApkChangedInfo;->mCurrentTag:Ljava/lang/String;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/oppo/theme/OppoApkChangedInfo;->mAllPackageNames:Ljava/util/ArrayList;

    .line 122
    const/4 v0, 0x0

    sput-boolean v0, Lcom/oppo/theme/OppoApkChangedInfo;->mParseError:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/oppo/theme/OppoApkChangedInfo;->mCurrentTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"

    .prologue
    .line 36
    sput-object p0, Lcom/oppo/theme/OppoApkChangedInfo;->mCurrentTag:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/oppo/theme/OppoApkChangedInfo;->mAllPackageNames:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static getApksNumbers()I
    .locals 1

    .prologue
    .line 124
    sget-object v0, Lcom/oppo/theme/OppoApkChangedInfo;->mAllPackageNames:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public static parseIconXml()Z
    .locals 4

    .prologue
    .line 72
    const/4 v1, 0x0

    .line 73
    .local v1, input:Ljava/io/InputStream;
    sget-object v3, Lcom/oppo/theme/OppoApkChangedInfo;->mAllPackageNames:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 76
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    const-string v3, "/system/media/theme/default/ApkChanged.xml"

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    .end local v1           #input:Ljava/io/InputStream;
    .local v2, input:Ljava/io/InputStream;
    :try_start_1
    invoke-static {v2}, Lcom/oppo/theme/OppoApkChangedInfo;->parseXml(Ljava/io/InputStream;)V

    .line 79
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 81
    const/4 v3, 0x1

    .line 85
    .end local v2           #input:Ljava/io/InputStream;
    :goto_0
    return v3

    .line 82
    .restart local v1       #input:Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 85
    .local v0, ex:Ljava/lang/Exception;
    :goto_1
    const/4 v3, 0x0

    goto :goto_0

    .line 82
    .end local v0           #ex:Ljava/lang/Exception;
    .end local v1           #input:Ljava/io/InputStream;
    .restart local v2       #input:Ljava/io/InputStream;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2           #input:Ljava/io/InputStream;
    .restart local v1       #input:Ljava/io/InputStream;
    goto :goto_1
.end method

.method private static parseXml(Ljava/io/InputStream;)V
    .locals 3
    .parameter "inStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 63
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v1

    .line 64
    .local v1, spf:Ljavax/xml/parsers/SAXParserFactory;
    invoke-virtual {v1}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v0

    .line 65
    .local v0, saxParser:Ljavax/xml/parsers/SAXParser;
    new-instance v2, Lcom/oppo/theme/OppoApkChangedInfo$IconXmlHandler;

    invoke-direct {v2}, Lcom/oppo/theme/OppoApkChangedInfo$IconXmlHandler;-><init>()V

    invoke-virtual {v0, p0, v2}, Ljavax/xml/parsers/SAXParser;->parse(Ljava/io/InputStream;Lorg/xml/sax/helpers/DefaultHandler;)V

    .line 66
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 68
    return-void
.end method

.method public static themeChangeEnable(Ljava/lang/String;)Z
    .locals 1
    .parameter "packageName"

    .prologue
    .line 91
    sget-object v0, Lcom/oppo/theme/OppoApkChangedInfo;->mAllPackageNames:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    const/4 v0, 0x1

    .line 94
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static themeChangeEnable(ZLjava/lang/String;)Z
    .locals 3
    .parameter "hasValue"
    .parameter "packageName"

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 103
    if-eqz p1, :cond_1

    const-string v2, "com.android.systemui"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 119
    :cond_0
    :goto_0
    return v0

    .line 106
    :cond_1
    if-eqz p0, :cond_2

    sget-boolean v2, Lcom/oppo/theme/OppoApkChangedInfo;->mParseError:Z

    if-eqz v2, :cond_3

    :cond_2
    move v0, v1

    .line 107
    goto :goto_0

    .line 109
    :cond_3
    invoke-static {}, Lcom/oppo/theme/OppoApkChangedInfo;->getApksNumbers()I

    move-result v2

    if-gtz v2, :cond_4

    .line 110
    invoke-static {}, Lcom/oppo/theme/OppoApkChangedInfo;->parseIconXml()Z

    move-result v2

    if-nez v2, :cond_4

    .line 111
    sput-boolean v0, Lcom/oppo/theme/OppoApkChangedInfo;->mParseError:Z

    move v0, v1

    .line 112
    goto :goto_0

    .line 116
    :cond_4
    sget-object v2, Lcom/oppo/theme/OppoApkChangedInfo;->mAllPackageNames:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 119
    goto :goto_0
.end method
