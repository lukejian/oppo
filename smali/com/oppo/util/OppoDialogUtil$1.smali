.class Lcom/oppo/util/OppoDialogUtil$1;
.super Ljava/lang/Object;
.source "OppoDialogUtil.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/oppo/util/OppoDialogUtil;->setDialogDrag(Lcom/android/internal/app/AlertController;Landroid/view/Window;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private fousedView:Landroid/view/View;

.field lp:Landroid/view/WindowManager$LayoutParams;

.field mDecor:Landroid/widget/FrameLayout;

.field private mDownParamsY:I

.field private mFirst:Z

.field private mFirstParamsY:I

.field private mLastY:F

.field private mLasterY:F

.field private mOffsetY:I

.field private mStartY:F

.field private mViewHeight:I

.field private mViewWidth:I

.field parentPanel:Landroid/widget/LinearLayout;

.field tempDrawable:Landroid/graphics/drawable/Drawable;

.field final synthetic this$0:Lcom/oppo/util/OppoDialogUtil;

.field final synthetic val$ac:Lcom/android/internal/app/AlertController;

.field final synthetic val$screenHeight:I

.field final synthetic val$statusBarHeight:I

.field final synthetic val$window:Landroid/view/Window;


# direct methods
.method constructor <init>(Lcom/oppo/util/OppoDialogUtil;Landroid/view/Window;IILcom/android/internal/app/AlertController;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 281
    iput-object p1, p0, Lcom/oppo/util/OppoDialogUtil$1;->this$0:Lcom/oppo/util/OppoDialogUtil;

    iput-object p2, p0, Lcom/oppo/util/OppoDialogUtil$1;->val$window:Landroid/view/Window;

    iput p3, p0, Lcom/oppo/util/OppoDialogUtil$1;->val$screenHeight:I

    iput p4, p0, Lcom/oppo/util/OppoDialogUtil$1;->val$statusBarHeight:I

    iput-object p5, p0, Lcom/oppo/util/OppoDialogUtil$1;->val$ac:Lcom/android/internal/app/AlertController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 284
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/oppo/util/OppoDialogUtil$1;->mFirst:Z

    .line 288
    iput-object v1, p0, Lcom/oppo/util/OppoDialogUtil$1;->lp:Landroid/view/WindowManager$LayoutParams;

    .line 289
    iput-object v1, p0, Lcom/oppo/util/OppoDialogUtil$1;->mDecor:Landroid/widget/FrameLayout;

    .line 290
    iput-object v1, p0, Lcom/oppo/util/OppoDialogUtil$1;->parentPanel:Landroid/widget/LinearLayout;

    .line 293
    iput-object v1, p0, Lcom/oppo/util/OppoDialogUtil$1;->tempDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 13
    .parameter "v"
    .parameter "ev"

    .prologue
    .line 299
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    .line 388
    :cond_0
    :goto_0
    const/4 v10, 0x1

    return v10

    .line 301
    :pswitch_0
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->val$window:Landroid/view/Window;

    invoke-virtual {v10}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/FrameLayout;

    iput-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->mDecor:Landroid/widget/FrameLayout;

    .line 303
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->val$window:Landroid/view/Window;

    invoke-virtual {v10}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v10

    iput-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->lp:Landroid/view/WindowManager$LayoutParams;

    .line 304
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->val$window:Landroid/view/Window;

    const v11, 0xc0204a4

    invoke-virtual {v10, v11}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    iput-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->parentPanel:Landroid/widget/LinearLayout;

    .line 306
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->parentPanel:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->findFocus()Landroid/view/View;

    move-result-object v10

    iput-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->fousedView:Landroid/view/View;

    .line 307
    new-instance v10, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v11, p0, Lcom/oppo/util/OppoDialogUtil$1;->this$0:Lcom/oppo/util/OppoDialogUtil;

    iget-object v12, p0, Lcom/oppo/util/OppoDialogUtil$1;->val$window:Landroid/view/Window;

    #calls: Lcom/oppo/util/OppoDialogUtil;->takeScreenShot(Landroid/view/Window;)Landroid/graphics/Bitmap;
    invoke-static {v11, v12}, Lcom/oppo/util/OppoDialogUtil;->access$000(Lcom/oppo/util/OppoDialogUtil;Landroid/view/Window;)Landroid/graphics/Bitmap;

    move-result-object v11

    invoke-direct {v10, v11}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->tempDrawable:Landroid/graphics/drawable/Drawable;

    .line 309
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v10

    iput v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->mStartY:F

    .line 310
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->val$window:Landroid/view/Window;

    invoke-virtual {v10}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v10

    iput-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->lp:Landroid/view/WindowManager$LayoutParams;

    .line 311
    iget-boolean v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->mFirst:Z

    if-eqz v10, :cond_1

    .line 312
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->lp:Landroid/view/WindowManager$LayoutParams;

    iget v10, v10, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->mFirstParamsY:I

    .line 313
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->mFirst:Z

    .line 315
    :cond_1
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->parentPanel:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v10

    iput v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->mViewHeight:I

    .line 316
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->parentPanel:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v10

    iput v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->mViewWidth:I

    .line 317
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->lp:Landroid/view/WindowManager$LayoutParams;

    iget v10, v10, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->mDownParamsY:I

    .line 318
    iget v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->val$screenHeight:I

    iget v11, p0, Lcom/oppo/util/OppoDialogUtil$1;->val$statusBarHeight:I

    sub-int/2addr v10, v11

    iget v11, p0, Lcom/oppo/util/OppoDialogUtil$1;->mViewHeight:I

    sub-int/2addr v10, v11

    iput v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->mOffsetY:I

    .line 320
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->val$window:Landroid/view/Window;

    const v11, 0x1020256

    invoke-virtual {v10, v11}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    .line 321
    .local v7, topPanel:Landroid/widget/LinearLayout;
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->val$window:Landroid/view/Window;

    const v11, 0x102002b

    invoke-virtual {v10, v11}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    .line 322
    .local v2, custom:Landroid/widget/FrameLayout;
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->val$window:Landroid/view/Window;

    const v11, 0x102025d

    invoke-virtual {v10, v11}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 323
    .local v1, contentPanel:Landroid/widget/LinearLayout;
    if-eqz v2, :cond_2

    .line 324
    invoke-virtual {v2}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 326
    :cond_2
    if-eqz v1, :cond_3

    .line 327
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 329
    :cond_3
    if-eqz v7, :cond_4

    .line 330
    invoke-virtual {v7}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 333
    :cond_4
    new-instance v8, Landroid/view/View;

    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->this$0:Lcom/oppo/util/OppoDialogUtil;

    #getter for: Lcom/oppo/util/OppoDialogUtil;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/oppo/util/OppoDialogUtil;->access$100(Lcom/oppo/util/OppoDialogUtil;)Landroid/content/Context;

    move-result-object v10

    invoke-direct {v8, v10}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 334
    .local v8, view:Landroid/view/View;
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 336
    .local v5, linearParams:Landroid/view/ViewGroup$LayoutParams;
    new-instance v9, Landroid/view/ViewGroup$LayoutParams;

    const/4 v10, -0x1

    iget v11, p0, Lcom/oppo/util/OppoDialogUtil$1;->mViewHeight:I

    invoke-direct {v9, v10, v11}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 339
    .local v9, viewParams:Landroid/view/ViewGroup$LayoutParams;
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->tempDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8, v10}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 342
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->val$window:Landroid/view/Window;

    invoke-virtual {v10, v8, v9}, Landroid/view/Window;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 343
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->lp:Landroid/view/WindowManager$LayoutParams;

    iget v11, p0, Lcom/oppo/util/OppoDialogUtil$1;->mViewHeight:I

    iput v11, v10, Landroid/view/WindowManager$LayoutParams;->height:I

    goto/16 :goto_0

    .line 347
    .end local v1           #contentPanel:Landroid/widget/LinearLayout;
    .end local v2           #custom:Landroid/widget/FrameLayout;
    .end local v5           #linearParams:Landroid/view/ViewGroup$LayoutParams;
    .end local v7           #topPanel:Landroid/widget/LinearLayout;
    .end local v8           #view:Landroid/view/View;
    .end local v9           #viewParams:Landroid/view/ViewGroup$LayoutParams;
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    .line 349
    .local v6, mCurrentY:F
    iget v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->mDownParamsY:I

    iget v11, p0, Lcom/oppo/util/OppoDialogUtil$1;->mStartY:F

    sub-float/2addr v11, v6

    float-to-int v11, v11

    add-int v4, v10, v11

    .line 350
    .local v4, finalParamsY:I
    if-gez v4, :cond_5

    .line 351
    const/4 v4, 0x0

    .line 353
    :cond_5
    iget v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->mOffsetY:I

    if-le v4, v10, :cond_6

    .line 354
    iget v4, p0, Lcom/oppo/util/OppoDialogUtil$1;->mOffsetY:I

    .line 357
    :cond_6
    iget v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->mLastY:F

    sub-float v10, v6, v10

    iget v11, p0, Lcom/oppo/util/OppoDialogUtil$1;->mLastY:F

    iget v12, p0, Lcom/oppo/util/OppoDialogUtil$1;->mLasterY:F

    sub-float/2addr v11, v12

    mul-float/2addr v10, v11

    const/4 v11, 0x0

    cmpg-float v10, v10, v11

    if-gez v10, :cond_8

    if-eqz v4, :cond_7

    iget v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->mOffsetY:I

    if-ne v4, v10, :cond_8

    .line 359
    :cond_7
    iget v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->mLastY:F

    iput v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->mStartY:F

    .line 360
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->lp:Landroid/view/WindowManager$LayoutParams;

    iget v10, v10, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->mDownParamsY:I

    .line 361
    iget v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->mDownParamsY:I

    iget v11, p0, Lcom/oppo/util/OppoDialogUtil$1;->mStartY:F

    sub-float/2addr v11, v6

    float-to-int v11, v11

    add-int v4, v10, v11

    .line 363
    :cond_8
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->mDecor:Landroid/widget/FrameLayout;

    if-eqz v10, :cond_9

    .line 364
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->lp:Landroid/view/WindowManager$LayoutParams;

    iput v4, v10, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 365
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->lp:Landroid/view/WindowManager$LayoutParams;

    iget v11, p0, Lcom/oppo/util/OppoDialogUtil$1;->mViewHeight:I

    iput v11, v10, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 366
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->val$window:Landroid/view/Window;

    invoke-virtual {v10}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v10

    iget-object v11, p0, Lcom/oppo/util/OppoDialogUtil$1;->mDecor:Landroid/widget/FrameLayout;

    iget-object v12, p0, Lcom/oppo/util/OppoDialogUtil$1;->lp:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v10, v11, v12}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 368
    :cond_9
    iget v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->mLastY:F

    iput v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->mLasterY:F

    .line 369
    iput v6, p0, Lcom/oppo/util/OppoDialogUtil$1;->mLastY:F

    goto/16 :goto_0

    .line 374
    .end local v4           #finalParamsY:I
    .end local v6           #mCurrentY:F
    :pswitch_2
    :try_start_0
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->lp:Landroid/view/WindowManager$LayoutParams;

    iget v0, v10, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 375
    .local v0, ParamsY:I
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->this$0:Lcom/oppo/util/OppoDialogUtil;

    #getter for: Lcom/oppo/util/OppoDialogUtil;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/oppo/util/OppoDialogUtil;->access$100(Lcom/oppo/util/OppoDialogUtil;)Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "AlertParamsY"

    invoke-static {v10, v11, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 380
    .end local v0           #ParamsY:I
    :goto_1
    const/4 v10, 0x0

    iput v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->mLastY:F

    iput v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->mLasterY:F

    .line 381
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->val$ac:Lcom/android/internal/app/AlertController;

    invoke-virtual {v10}, Lcom/android/internal/app/AlertController;->initializeDialog()V

    .line 382
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->this$0:Lcom/oppo/util/OppoDialogUtil;

    iget-object v11, p0, Lcom/oppo/util/OppoDialogUtil$1;->val$ac:Lcom/android/internal/app/AlertController;

    iget-object v12, p0, Lcom/oppo/util/OppoDialogUtil$1;->val$window:Landroid/view/Window;

    invoke-virtual {v10, v11, v12}, Lcom/oppo/util/OppoDialogUtil;->setDialogDrag(Lcom/android/internal/app/AlertController;Landroid/view/Window;)V

    .line 383
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->fousedView:Landroid/view/View;

    if-eqz v10, :cond_0

    .line 384
    iget-object v10, p0, Lcom/oppo/util/OppoDialogUtil$1;->fousedView:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_0

    .line 377
    :catch_0
    move-exception v3

    .line 378
    .local v3, e:Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 299
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
