.class public Lcom/oppo/util/OppoThailandCalendarUtil$OppoTime;
.super Landroid/text/format/Time;
.source "OppoThailandCalendarUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/oppo/util/OppoThailandCalendarUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OppoTime"
.end annotation


# instance fields
.field private mTimeParent:Landroid/text/format/Time;


# direct methods
.method public constructor <init>(Landroid/text/format/Time;)V
    .locals 0
    .parameter "parent"

    .prologue
    .line 790
    invoke-direct {p0}, Landroid/text/format/Time;-><init>()V

    .line 791
    iput-object p1, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoTime;->mTimeParent:Landroid/text/format/Time;

    .line 792
    return-void
.end method


# virtual methods
.method public format(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "format"

    .prologue
    .line 796
    iget-object v1, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoTime;->mTimeParent:Landroid/text/format/Time;

    if-eqz v1, :cond_1

    .line 797
    iget-object v1, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoTime;->mTimeParent:Landroid/text/format/Time;

    invoke-virtual {v1, p1}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 802
    .local v0, result:Ljava/lang/String;
    :goto_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "zh"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 803
    invoke-virtual {p0, v0}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoTime;->replaceCnMonth(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 805
    :cond_0
    return-object v0

    .line 799
    .end local v0           #result:Ljava/lang/String;
    :cond_1
    invoke-super {p0, p1}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .restart local v0       #result:Ljava/lang/String;
    goto :goto_0
.end method

.method public replaceCnMonth(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .parameter "tempResult"

    .prologue
    .line 810
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v4

    .line 814
    .local v4, res:Landroid/content/res/Resources;
    const v6, 0xc070413

    :try_start_0
    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 815
    .local v0, chineseMonthNumber:[Ljava/lang/String;
    const v6, 0xc070414

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 819
    .local v1, digitMonthNumber:[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, i:I
    :goto_0
    array-length v6, v0

    if-ge v3, v6, :cond_1

    .line 820
    aget-object v6, v0, v3

    invoke-virtual {p1, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_0

    .line 821
    aget-object v6, v0, v3

    aget-object v7, v1, v3

    invoke-virtual {p1, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 819
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 816
    .end local v0           #chineseMonthNumber:[Ljava/lang/String;
    .end local v1           #digitMonthNumber:[Ljava/lang/String;
    .end local v3           #i:I
    :catch_0
    move-exception v2

    .local v2, e:Landroid/content/res/Resources$NotFoundException;
    move-object v5, p1

    .line 824
    .end local v2           #e:Landroid/content/res/Resources$NotFoundException;
    .end local p1
    .local v5, tempResult:Ljava/lang/String;
    :goto_1
    return-object v5

    .end local v5           #tempResult:Ljava/lang/String;
    .restart local v0       #chineseMonthNumber:[Ljava/lang/String;
    .restart local v1       #digitMonthNumber:[Ljava/lang/String;
    .restart local v3       #i:I
    .restart local p1
    :cond_1
    move-object v5, p1

    .end local p1
    .restart local v5       #tempResult:Ljava/lang/String;
    goto :goto_1
.end method
