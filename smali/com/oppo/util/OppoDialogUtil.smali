.class public Lcom/oppo/util/OppoDialogUtil;
.super Ljava/lang/Object;
.source "OppoDialogUtil.java"


# static fields
.field private static final BIT_BUTTON_NEGATIVE:I = 0x2

.field private static final BIT_BUTTON_NEUTRAL:I = 0x4

.field private static final BIT_BUTTON_POSITIVE:I = 0x1

.field public static final BIT_FOUSED_BUTTON_NEGATIVE:I = 0x10

.field public static final BIT_FOUSED_BUTTON_NEUTRAL:I = 0x20

.field public static final BIT_FOUSED_BUTTON_POSITIVE:I = 0x8

.field public static final BIT_FOUSED_DEFAULT:I = 0x0

.field private static final DBG:Z = true

.field private static final TAG:Ljava/lang/String; = "OppoDialogUtil"


# instance fields
.field private leftBg:Landroid/graphics/drawable/Drawable;

.field private leftFousedBg:Landroid/graphics/drawable/Drawable;

.field private mButtonNegative:Landroid/widget/Button;

.field private mButtonNeutral:Landroid/widget/Button;

.field private mButtonPositive:Landroid/widget/Button;

.field private mContext:Landroid/content/Context;

.field private mFousedFlag:I

.field private middleBg:Landroid/graphics/drawable/Drawable;

.field private middleFousedBg:Landroid/graphics/drawable/Drawable;

.field private rightBg:Landroid/graphics/drawable/Drawable;

.field private rightFousedBg:Landroid/graphics/drawable/Drawable;

.field private singleBg:Landroid/graphics/drawable/Drawable;

.field private singleFousedBg:Landroid/graphics/drawable/Drawable;

.field private textColor:Landroid/content/res/ColorStateList;

.field private textFousedColor:Landroid/content/res/ColorStateList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const/4 v0, 0x1

    iput v0, p0, Lcom/oppo/util/OppoDialogUtil;->mFousedFlag:I

    .line 83
    iput-object p1, p0, Lcom/oppo/util/OppoDialogUtil;->mContext:Landroid/content/Context;

    .line 84
    return-void
.end method

.method static synthetic access$000(Lcom/oppo/util/OppoDialogUtil;Landroid/view/Window;)Landroid/graphics/Bitmap;
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/oppo/util/OppoDialogUtil;->takeScreenShot(Landroid/view/Window;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/oppo/util/OppoDialogUtil;)Landroid/content/Context;
    .locals 1
    .parameter "x0"

    .prologue
    .line 47
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private changeFousedBg(I)V
    .locals 2
    .parameter "mFousedButtons"

    .prologue
    .line 130
    sparse-switch p1, :sswitch_data_0

    .line 173
    :goto_0
    return-void

    .line 132
    :sswitch_0
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonPositive:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->leftBg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 133
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonPositive:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->textColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 134
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNegative:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->rightFousedBg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 135
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNegative:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->textFousedColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 136
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNegative:Landroid/widget/Button;

    iget v1, p0, Lcom/oppo/util/OppoDialogUtil;->mFousedFlag:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 140
    :sswitch_1
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonPositive:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->leftBg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 141
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonPositive:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->textColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 142
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNeutral:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->rightFousedBg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 143
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNeutral:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->textFousedColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 144
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNeutral:Landroid/widget/Button;

    iget v1, p0, Lcom/oppo/util/OppoDialogUtil;->mFousedFlag:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 148
    :sswitch_2
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNegative:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->leftBg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 149
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonPositive:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->textColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 150
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNeutral:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->rightFousedBg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 151
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNeutral:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->textFousedColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 152
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNeutral:Landroid/widget/Button;

    iget v1, p0, Lcom/oppo/util/OppoDialogUtil;->mFousedFlag:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 156
    :sswitch_3
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonPositive:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->leftBg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 157
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonPositive:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->textColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 158
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNegative:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->rightFousedBg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 159
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNegative:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->textFousedColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 160
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNegative:Landroid/widget/Button;

    iget v1, p0, Lcom/oppo/util/OppoDialogUtil;->mFousedFlag:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 164
    :sswitch_4
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonPositive:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->leftBg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 165
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonPositive:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->textColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 166
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNeutral:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->middleFousedBg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 167
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNeutral:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->textFousedColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 168
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNeutral:Landroid/widget/Button;

    iget v1, p0, Lcom/oppo/util/OppoDialogUtil;->mFousedFlag:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 130
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x17 -> :sswitch_3
        0x25 -> :sswitch_1
        0x26 -> :sswitch_2
        0x27 -> :sswitch_4
    .end sparse-switch
.end method

.method private initialize(Landroid/view/Window;)V
    .locals 2
    .parameter "mWindow"

    .prologue
    .line 177
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0xc0804c4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->singleBg:Landroid/graphics/drawable/Drawable;

    .line 178
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0xc0804c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->leftBg:Landroid/graphics/drawable/Drawable;

    .line 179
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0xc0804c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->middleBg:Landroid/graphics/drawable/Drawable;

    .line 180
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0xc0804c7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->rightBg:Landroid/graphics/drawable/Drawable;

    .line 182
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0xc0804c8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->singleFousedBg:Landroid/graphics/drawable/Drawable;

    .line 183
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0xc0804c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->leftFousedBg:Landroid/graphics/drawable/Drawable;

    .line 184
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0xc0804ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->middleFousedBg:Landroid/graphics/drawable/Drawable;

    .line 185
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0xc0804cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->rightFousedBg:Landroid/graphics/drawable/Drawable;

    .line 187
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0xc06040c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->textColor:Landroid/content/res/ColorStateList;

    .line 188
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0xc06040d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->textFousedColor:Landroid/content/res/ColorStateList;

    .line 190
    return-void
.end method

.method private setButtonBackground(I)V
    .locals 2
    .parameter "whichButtons"

    .prologue
    .line 87
    packed-switch p1, :pswitch_data_0

    .line 126
    :goto_0
    return-void

    .line 90
    :pswitch_0
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonPositive:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->textFousedColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0

    .line 94
    :pswitch_1
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNegative:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->textFousedColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0

    .line 98
    :pswitch_2
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNeutral:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->textFousedColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0

    .line 103
    :pswitch_3
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonPositive:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->textFousedColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0

    .line 108
    :pswitch_4
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonPositive:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->textFousedColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0

    .line 113
    :pswitch_5
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNegative:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->textFousedColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0

    .line 119
    :pswitch_6
    iget-object v0, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonPositive:Landroid/widget/Button;

    iget-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->textFousedColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0

    .line 87
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private takeScreenShot(Landroid/view/Window;)Landroid/graphics/Bitmap;
    .locals 7
    .parameter "mWindow"

    .prologue
    const/4 v6, 0x0

    .line 394
    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    .line 395
    .local v3, view:Landroid/view/View;
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 396
    invoke-virtual {v3}, Landroid/view/View;->buildDrawingCache()V

    .line 397
    invoke-virtual {v3}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 398
    .local v2, temp:Landroid/graphics/Bitmap;
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v4

    .line 399
    .local v4, width:I
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 400
    .local v1, height:I
    invoke-static {v2, v6, v6, v4, v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 401
    .local v0, b:Landroid/graphics/Bitmap;
    invoke-virtual {v3}, Landroid/view/View;->destroyDrawingCache()V

    .line 402
    if-eqz v2, :cond_0

    .line 403
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 404
    const/4 v2, 0x0

    .line 406
    :cond_0
    return-object v0
.end method


# virtual methods
.method public setDialogButtonFlag(Landroid/view/Window;)V
    .locals 3
    .parameter "mWindow"

    .prologue
    .line 194
    const/4 v0, 0x0

    .line 195
    .local v0, flag:I
    const v1, 0x1020019

    invoke-virtual {p1, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonPositive:Landroid/widget/Button;

    .line 196
    const v1, 0x102001a

    invoke-virtual {p1, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNegative:Landroid/widget/Button;

    .line 197
    const v1, 0x102001b

    invoke-virtual {p1, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNeutral:Landroid/widget/Button;

    .line 198
    iget v1, p0, Lcom/oppo/util/OppoDialogUtil;->mFousedFlag:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonPositive:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getTag()Ljava/lang/Object;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 199
    const/16 v0, 0x8

    .line 201
    :cond_0
    iget v1, p0, Lcom/oppo/util/OppoDialogUtil;->mFousedFlag:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNegative:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getTag()Ljava/lang/Object;

    move-result-object v2

    if-ne v1, v2, :cond_1

    .line 202
    const/16 v0, 0x10

    .line 204
    :cond_1
    iget v1, p0, Lcom/oppo/util/OppoDialogUtil;->mFousedFlag:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNeutral:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getTag()Ljava/lang/Object;

    move-result-object v2

    if-ne v1, v2, :cond_2

    .line 205
    const/16 v0, 0x20

    .line 207
    :cond_2
    invoke-virtual {p0, p1, v0}, Lcom/oppo/util/OppoDialogUtil;->setDialogButtonFlag(Landroid/view/Window;I)V

    .line 208
    return-void
.end method

.method public setDialogButtonFlag(Landroid/view/Window;I)V
    .locals 3
    .parameter "mWindow"
    .parameter "flag"

    .prologue
    .line 212
    const v2, 0x1020019

    :try_start_0
    invoke-virtual {p1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonPositive:Landroid/widget/Button;

    .line 213
    const v2, 0x102001a

    invoke-virtual {p1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNegative:Landroid/widget/Button;

    .line 214
    const v2, 0x102001b

    invoke-virtual {p1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNeutral:Landroid/widget/Button;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 221
    iget-object v2, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonPositive:Landroid/widget/Button;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNegative:Landroid/widget/Button;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNeutral:Landroid/widget/Button;

    if-nez v2, :cond_1

    .line 243
    :cond_0
    :goto_0
    return-void

    .line 215
    :catch_0
    move-exception v0

    .line 217
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 225
    .end local v0           #e:Ljava/lang/Exception;
    :cond_1
    invoke-direct {p0, p1}, Lcom/oppo/util/OppoDialogUtil;->initialize(Landroid/view/Window;)V

    .line 226
    const/4 v1, 0x0

    .line 228
    .local v1, whichButtons:I
    iget-object v2, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonPositive:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    .line 229
    or-int/lit8 v1, v1, 0x1

    .line 231
    :cond_2
    iget-object v2, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNegative:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getVisibility()I

    move-result v2

    if-nez v2, :cond_3

    .line 232
    or-int/lit8 v1, v1, 0x2

    .line 234
    :cond_3
    iget-object v2, p0, Lcom/oppo/util/OppoDialogUtil;->mButtonNeutral:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getVisibility()I

    move-result v2

    if-nez v2, :cond_4

    .line 235
    or-int/lit8 v1, v1, 0x4

    .line 238
    :cond_4
    invoke-direct {p0, v1}, Lcom/oppo/util/OppoDialogUtil;->setButtonBackground(I)V

    .line 239
    if-nez p2, :cond_0

    goto :goto_0
.end method

.method public setDialogDrag(Lcom/android/internal/app/AlertController;Landroid/view/Window;)V
    .locals 0
    .parameter "ac"
    .parameter "window"

    .prologue
    .line 252
    invoke-virtual {p0, p2}, Lcom/oppo/util/OppoDialogUtil;->setDialogButtonFlag(Landroid/view/Window;)V

    .line 254
    return-void
.end method
