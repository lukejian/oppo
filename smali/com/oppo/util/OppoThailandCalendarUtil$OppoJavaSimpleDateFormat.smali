.class public Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;
.super Ljava/text/DateFormat;
.source "OppoThailandCalendarUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/oppo/util/OppoThailandCalendarUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OppoJavaSimpleDateFormat"
.end annotation


# static fields
.field static final PATTERN_CHARS:Ljava/lang/String; = "GyMdkHmsSEDFwWahKzZLc"

.field private static final RFC_822_TIMEZONE_FIELD:I = 0x12

.field private static final STAND_ALONE_DAY_OF_WEEK_FIELD:I = 0x14

.field private static final STAND_ALONE_MONTH_FIELD:I = 0x13

.field private static final serialPersistentFields:[Ljava/io/ObjectStreamField; = null

.field private static final serialVersionUID:J = 0x4243c9da93943590L


# instance fields
.field private transient creationYear:I

.field private defaultCenturyStart:Ljava/util/Date;

.field private formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

.field private pattern:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 2406
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/io/ObjectStreamField;

    const/4 v1, 0x0

    new-instance v2, Ljava/io/ObjectStreamField;

    const-string v3, "defaultCenturyStart"

    const-class v4, Ljava/util/Date;

    invoke-direct {v2, v3, v4}, Ljava/io/ObjectStreamField;-><init>(Ljava/lang/String;Ljava/lang/Class;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Ljava/io/ObjectStreamField;

    const-string v3, "formatData"

    const-class v4, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    invoke-direct {v2, v3, v4}, Ljava/io/ObjectStreamField;-><init>(Ljava/lang/String;Ljava/lang/Class;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Ljava/io/ObjectStreamField;

    const-string v3, "pattern"

    const-class v4, Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Ljava/io/ObjectStreamField;-><init>(Ljava/lang/String;Ljava/lang/Class;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Ljava/io/ObjectStreamField;

    const-string v3, "serialVersionOnStream"

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-direct {v2, v3, v4}, Ljava/io/ObjectStreamField;-><init>(Ljava/lang/String;Ljava/lang/Class;)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->serialPersistentFields:[Ljava/io/ObjectStreamField;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1333
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;-><init>(Ljava/util/Locale;)V

    .line 1334
    invoke-static {}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->defaultPattern()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->pattern:Ljava/lang/String;

    .line 1335
    new-instance v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;-><init>(Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    .line 1336
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .parameter "pattern"

    .prologue
    .line 1349
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1350
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;)V
    .locals 1
    .parameter "template"
    .parameter "value"

    .prologue
    .line 1433
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;-><init>(Ljava/util/Locale;)V

    .line 1434
    invoke-direct {p0, p1}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->validatePattern(Ljava/lang/String;)V

    .line 1435
    iput-object p1, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->pattern:Ljava/lang/String;

    .line 1436
    invoke-virtual {p2}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iput-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    .line 1437
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Locale;)V
    .locals 3
    .parameter "template"
    .parameter "locale"

    .prologue
    .line 1449
    invoke-direct {p0, p2}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;-><init>(Ljava/util/Locale;)V

    .line 1450
    invoke-direct {p0, p1}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->validatePattern(Ljava/lang/String;)V

    .line 1451
    iput-object p1, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->pattern:Ljava/lang/String;

    .line 1452
    new-instance v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    invoke-direct {v0, p2}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;-><init>(Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    .line 1453
    iget-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0xc07040e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->setAmPmStrings([Ljava/lang/String;)V

    .line 1455
    return-void
.end method

.method private constructor <init>(Ljava/util/Locale;)V
    .locals 3
    .parameter "locale"

    .prologue
    const/4 v2, 0x1

    .line 1457
    invoke-direct {p0}, Ljava/text/DateFormat;-><init>()V

    .line 1458
    invoke-static {p1}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->numberFormat:Ljava/text/NumberFormat;

    .line 1459
    iget-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->numberFormat:Ljava/text/NumberFormat;

    invoke-virtual {v0, v2}, Ljava/text/NumberFormat;->setParseIntegerOnly(Z)V

    .line 1460
    iget-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->numberFormat:Ljava/text/NumberFormat;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setGroupingUsed(Z)V

    .line 1461
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0, p1}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    .line 1462
    iget-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    const/16 v1, -0x50

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    .line 1463
    iget-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->creationYear:I

    .line 1464
    iget-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->defaultCenturyStart:Ljava/util/Date;

    .line 1465
    return-void
.end method

.method private append(Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Ljava/util/List;CI)V
    .locals 10
    .parameter "buffer"
    .parameter "position"
    .parameter
    .parameter "format"
    .parameter "count"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuffer;",
            "Ljava/text/FieldPosition;",
            "Ljava/util/List",
            "<",
            "Ljava/text/FieldPosition;",
            ">;CI)V"
        }
    .end annotation

    .prologue
    .line 1653
    .local p3, fields:Ljava/util/List;,"Ljava/util/List<Ljava/text/FieldPosition;>;"
    const/4 v2, -0x1

    .line 1654
    .local v2, field:I
    const-string v7, "GyMdkHmsSEDFwWahKzZLc"

    invoke-virtual {v7, p4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 1655
    .local v4, index:I
    const/4 v7, -0x1

    if-ne v4, v7, :cond_0

    .line 1656
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unknown pattern character \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1659
    :cond_0
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    .line 1660
    .local v0, beginPosition:I
    const/4 v1, 0x0

    .line 1661
    .local v1, dateFormatField:Ljava/text/DateFormat$Field;
    packed-switch v4, :pswitch_data_0

    .line 1765
    :goto_0
    const/4 v7, -0x1

    if-eq v2, v7, :cond_1

    .line 1766
    iget-object v7, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    invoke-virtual {v7, v2}, Ljava/util/Calendar;->get(I)I

    move-result v7

    invoke-direct {p0, p1, p5, v7}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->appendNumber(Ljava/lang/StringBuffer;II)V

    .line 1769
    :cond_1
    if-eqz p3, :cond_7

    .line 1770
    new-instance p2, Ljava/text/FieldPosition;

    .end local p2
    invoke-direct {p2, v1}, Ljava/text/FieldPosition;-><init>(Ljava/text/Format$Field;)V

    .line 1771
    .restart local p2
    invoke-virtual {p2, v0}, Ljava/text/FieldPosition;->setBeginIndex(I)V

    .line 1772
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v7

    invoke-virtual {p2, v7}, Ljava/text/FieldPosition;->setEndIndex(I)V

    .line 1773
    invoke-interface {p3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1783
    :cond_2
    :goto_1
    return-void

    .line 1663
    :pswitch_0
    sget-object v1, Ljava/text/DateFormat$Field;->ERA:Ljava/text/DateFormat$Field;

    .line 1664
    iget-object v7, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iget-object v7, v7, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->eras:[Ljava/lang/String;

    iget-object v8, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/Calendar;->get(I)I

    move-result v8

    aget-object v7, v7, v8

    invoke-virtual {p1, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 1667
    :pswitch_1
    sget-object v1, Ljava/text/DateFormat$Field;->YEAR:Ljava/text/DateFormat$Field;

    .line 1668
    iget-object v7, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 1675
    .local v6, year:I
    #calls: Lcom/oppo/util/OppoThailandCalendarUtil;->isThaiCalendarEnabled()Z
    invoke-static {}, Lcom/oppo/util/OppoThailandCalendarUtil;->access$000()Z

    move-result v7

    if-eqz v7, :cond_3

    const/16 v7, 0x98b

    if-ge v6, v7, :cond_3

    .line 1676
    add-int/lit16 v6, v6, 0x21f

    .line 1679
    :cond_3
    const/4 v7, 0x2

    if-ne p5, v7, :cond_4

    .line 1680
    const/4 v7, 0x2

    rem-int/lit8 v8, v6, 0x64

    invoke-direct {p0, p1, v7, v8}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->appendNumber(Ljava/lang/StringBuffer;II)V

    goto :goto_0

    .line 1682
    :cond_4
    invoke-direct {p0, p1, p5, v6}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->appendNumber(Ljava/lang/StringBuffer;II)V

    goto :goto_0

    .line 1686
    .end local v6           #year:I
    :pswitch_2
    sget-object v1, Ljava/text/DateFormat$Field;->MONTH:Ljava/text/DateFormat$Field;

    .line 1687
    const/4 v7, 0x1

    invoke-direct {p0, p1, p5, v7}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->appendMonth(Ljava/lang/StringBuffer;IZ)V

    goto :goto_0

    .line 1690
    :pswitch_3
    sget-object v1, Ljava/text/DateFormat$Field;->MONTH:Ljava/text/DateFormat$Field;

    .line 1691
    const/4 v7, 0x0

    invoke-direct {p0, p1, p5, v7}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->appendMonth(Ljava/lang/StringBuffer;IZ)V

    goto :goto_0

    .line 1694
    :pswitch_4
    sget-object v1, Ljava/text/DateFormat$Field;->DAY_OF_MONTH:Ljava/text/DateFormat$Field;

    .line 1695
    const/4 v2, 0x5

    .line 1696
    goto :goto_0

    .line 1698
    :pswitch_5
    sget-object v1, Ljava/text/DateFormat$Field;->HOUR_OF_DAY1:Ljava/text/DateFormat$Field;

    .line 1699
    iget-object v7, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    const/16 v8, 0xb

    invoke-virtual {v7, v8}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 1700
    .local v3, hour:I
    if-nez v3, :cond_5

    const/16 v3, 0x18

    .end local v3           #hour:I
    :cond_5
    invoke-direct {p0, p1, p5, v3}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->appendNumber(Ljava/lang/StringBuffer;II)V

    goto :goto_0

    .line 1703
    :pswitch_6
    sget-object v1, Ljava/text/DateFormat$Field;->HOUR_OF_DAY0:Ljava/text/DateFormat$Field;

    .line 1704
    const/16 v2, 0xb

    .line 1705
    goto :goto_0

    .line 1707
    :pswitch_7
    sget-object v1, Ljava/text/DateFormat$Field;->MINUTE:Ljava/text/DateFormat$Field;

    .line 1708
    const/16 v2, 0xc

    .line 1709
    goto/16 :goto_0

    .line 1711
    :pswitch_8
    sget-object v1, Ljava/text/DateFormat$Field;->SECOND:Ljava/text/DateFormat$Field;

    .line 1712
    const/16 v2, 0xd

    .line 1713
    goto/16 :goto_0

    .line 1715
    :pswitch_9
    sget-object v1, Ljava/text/DateFormat$Field;->MILLISECOND:Ljava/text/DateFormat$Field;

    .line 1716
    iget-object v7, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    const/16 v8, 0xe

    invoke-virtual {v7, v8}, Ljava/util/Calendar;->get(I)I

    move-result v5

    .line 1717
    .local v5, value:I
    invoke-direct {p0, p1, p5, v5}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->appendNumber(Ljava/lang/StringBuffer;II)V

    goto/16 :goto_0

    .line 1720
    .end local v5           #value:I
    :pswitch_a
    sget-object v1, Ljava/text/DateFormat$Field;->DAY_OF_WEEK:Ljava/text/DateFormat$Field;

    .line 1721
    const/4 v7, 0x1

    invoke-direct {p0, p1, p5, v7}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->appendDayOfWeek(Ljava/lang/StringBuffer;IZ)V

    goto/16 :goto_0

    .line 1724
    :pswitch_b
    sget-object v1, Ljava/text/DateFormat$Field;->DAY_OF_WEEK:Ljava/text/DateFormat$Field;

    .line 1725
    const/4 v7, 0x0

    invoke-direct {p0, p1, p5, v7}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->appendDayOfWeek(Ljava/lang/StringBuffer;IZ)V

    goto/16 :goto_0

    .line 1728
    :pswitch_c
    sget-object v1, Ljava/text/DateFormat$Field;->DAY_OF_YEAR:Ljava/text/DateFormat$Field;

    .line 1729
    const/4 v2, 0x6

    .line 1730
    goto/16 :goto_0

    .line 1732
    :pswitch_d
    sget-object v1, Ljava/text/DateFormat$Field;->DAY_OF_WEEK_IN_MONTH:Ljava/text/DateFormat$Field;

    .line 1733
    const/16 v2, 0x8

    .line 1734
    goto/16 :goto_0

    .line 1736
    :pswitch_e
    sget-object v1, Ljava/text/DateFormat$Field;->WEEK_OF_YEAR:Ljava/text/DateFormat$Field;

    .line 1737
    const/4 v2, 0x3

    .line 1738
    goto/16 :goto_0

    .line 1740
    :pswitch_f
    sget-object v1, Ljava/text/DateFormat$Field;->WEEK_OF_MONTH:Ljava/text/DateFormat$Field;

    .line 1741
    const/4 v2, 0x4

    .line 1742
    goto/16 :goto_0

    .line 1744
    :pswitch_10
    sget-object v1, Ljava/text/DateFormat$Field;->AM_PM:Ljava/text/DateFormat$Field;

    .line 1745
    iget-object v7, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iget-object v7, v7, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->ampms:[Ljava/lang/String;

    iget-object v8, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    const/16 v9, 0x9

    invoke-virtual {v8, v9}, Ljava/util/Calendar;->get(I)I

    move-result v8

    aget-object v7, v7, v8

    invoke-virtual {p1, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    .line 1748
    :pswitch_11
    sget-object v1, Ljava/text/DateFormat$Field;->HOUR1:Ljava/text/DateFormat$Field;

    .line 1749
    iget-object v7, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    const/16 v8, 0xa

    invoke-virtual {v7, v8}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 1750
    .restart local v3       #hour:I
    if-nez v3, :cond_6

    const/16 v3, 0xc

    .end local v3           #hour:I
    :cond_6
    invoke-direct {p0, p1, p5, v3}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->appendNumber(Ljava/lang/StringBuffer;II)V

    goto/16 :goto_0

    .line 1753
    :pswitch_12
    sget-object v1, Ljava/text/DateFormat$Field;->HOUR0:Ljava/text/DateFormat$Field;

    .line 1754
    const/16 v2, 0xa

    .line 1755
    goto/16 :goto_0

    .line 1757
    :pswitch_13
    sget-object v1, Ljava/text/DateFormat$Field;->TIME_ZONE:Ljava/text/DateFormat$Field;

    .line 1758
    const/4 v7, 0x1

    invoke-direct {p0, p1, p5, v7}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->appendTimeZone(Ljava/lang/StringBuffer;IZ)V

    goto/16 :goto_0

    .line 1761
    :pswitch_14
    sget-object v1, Ljava/text/DateFormat$Field;->TIME_ZONE:Ljava/text/DateFormat$Field;

    .line 1762
    const/4 v7, 0x0

    invoke-direct {p0, p1, p5, v7}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->appendNumericTimeZone(Ljava/lang/StringBuffer;IZ)V

    goto/16 :goto_0

    .line 1776
    :cond_7
    invoke-virtual {p2}, Ljava/text/FieldPosition;->getFieldAttribute()Ljava/text/Format$Field;

    move-result-object v7

    if-eq v7, v1, :cond_8

    invoke-virtual {p2}, Ljava/text/FieldPosition;->getFieldAttribute()Ljava/text/Format$Field;

    move-result-object v7

    if-nez v7, :cond_2

    invoke-virtual {p2}, Ljava/text/FieldPosition;->getField()I

    move-result v7

    if-ne v7, v4, :cond_2

    :cond_8
    invoke-virtual {p2}, Ljava/text/FieldPosition;->getEndIndex()I

    move-result v7

    if-nez v7, :cond_2

    .line 1779
    invoke-virtual {p2, v0}, Ljava/text/FieldPosition;->setBeginIndex(I)V

    .line 1780
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v7

    invoke-virtual {p2, v7}, Ljava/text/FieldPosition;->setEndIndex(I)V

    goto/16 :goto_1

    .line 1661
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_2
        :pswitch_a
    .end packed-switch
.end method

.method private appendDayOfWeek(Ljava/lang/StringBuffer;IZ)V
    .locals 4
    .parameter "buffer"
    .parameter "count"
    .parameter "standAlone"

    .prologue
    .line 1788
    iget-object v2, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iget-object v1, v2, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->localeData:Llibcore/icu/LocaleData;

    .line 1789
    .local v1, ld:Llibcore/icu/LocaleData;
    const/4 v2, 0x4

    if-ne p2, v2, :cond_1

    .line 1790
    if-eqz p3, :cond_0

    iget-object v0, v1, Llibcore/icu/LocaleData;->longStandAloneWeekdayNames:[Ljava/lang/String;

    .line 1796
    .local v0, days:[Ljava/lang/String;
    :goto_0
    iget-object v2, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    aget-object v2, v0, v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1797
    return-void

    .line 1790
    .end local v0           #days:[Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iget-object v0, v2, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->weekdays:[Ljava/lang/String;

    goto :goto_0

    .line 1791
    :cond_1
    const/4 v2, 0x5

    if-ne p2, v2, :cond_3

    .line 1792
    if-eqz p3, :cond_2

    iget-object v0, v1, Llibcore/icu/LocaleData;->tinyStandAloneWeekdayNames:[Ljava/lang/String;

    .restart local v0       #days:[Ljava/lang/String;
    :goto_1
    goto :goto_0

    .end local v0           #days:[Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iget-object v2, v2, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->localeData:Llibcore/icu/LocaleData;

    iget-object v0, v2, Llibcore/icu/LocaleData;->tinyWeekdayNames:[Ljava/lang/String;

    goto :goto_1

    .line 1794
    :cond_3
    if-eqz p3, :cond_4

    iget-object v0, v1, Llibcore/icu/LocaleData;->shortStandAloneWeekdayNames:[Ljava/lang/String;

    .restart local v0       #days:[Ljava/lang/String;
    :goto_2
    goto :goto_0

    .end local v0           #days:[Ljava/lang/String;
    :cond_4
    iget-object v2, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iget-object v0, v2, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->shortWeekdays:[Ljava/lang/String;

    goto :goto_2
.end method

.method private appendMonth(Ljava/lang/StringBuffer;IZ)V
    .locals 5
    .parameter "buffer"
    .parameter "count"
    .parameter "standAlone"

    .prologue
    const/4 v4, 0x2

    .line 1801
    iget-object v3, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 1802
    .local v1, month:I
    if-gt p2, v4, :cond_0

    .line 1803
    add-int/lit8 v3, v1, 0x1

    invoke-direct {p0, p1, p2, v3}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->appendNumber(Ljava/lang/StringBuffer;II)V

    .line 1817
    :goto_0
    return-void

    .line 1808
    :cond_0
    iget-object v3, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iget-object v0, v3, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->localeData:Llibcore/icu/LocaleData;

    .line 1809
    .local v0, ld:Llibcore/icu/LocaleData;
    const/4 v3, 0x4

    if-ne p2, v3, :cond_2

    .line 1810
    if-eqz p3, :cond_1

    iget-object v2, v0, Llibcore/icu/LocaleData;->longStandAloneMonthNames:[Ljava/lang/String;

    .line 1816
    .local v2, months:[Ljava/lang/String;
    :goto_1
    aget-object v3, v2, v1

    invoke-virtual {p1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 1810
    .end local v2           #months:[Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iget-object v2, v3, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->months:[Ljava/lang/String;

    goto :goto_1

    .line 1811
    :cond_2
    const/4 v3, 0x5

    if-ne p2, v3, :cond_4

    .line 1812
    if-eqz p3, :cond_3

    iget-object v2, v0, Llibcore/icu/LocaleData;->tinyStandAloneMonthNames:[Ljava/lang/String;

    .restart local v2       #months:[Ljava/lang/String;
    :goto_2
    goto :goto_1

    .end local v2           #months:[Ljava/lang/String;
    :cond_3
    iget-object v2, v0, Llibcore/icu/LocaleData;->tinyMonthNames:[Ljava/lang/String;

    goto :goto_2

    .line 1814
    :cond_4
    if-eqz p3, :cond_5

    iget-object v2, v0, Llibcore/icu/LocaleData;->shortStandAloneMonthNames:[Ljava/lang/String;

    .restart local v2       #months:[Ljava/lang/String;
    :goto_3
    goto :goto_1

    .end local v2           #months:[Ljava/lang/String;
    :cond_5
    iget-object v3, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iget-object v2, v3, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->shortMonths:[Ljava/lang/String;

    goto :goto_3
.end method

.method private appendNumber(Ljava/lang/StringBuffer;II)V
    .locals 5
    .parameter "buffer"
    .parameter "count"
    .parameter "value"

    .prologue
    .line 1876
    iget-object v1, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->numberFormat:Ljava/text/NumberFormat;

    invoke-virtual {v1}, Ljava/text/NumberFormat;->getMinimumIntegerDigits()I

    move-result v0

    .line 1877
    .local v0, minimumIntegerDigits:I
    iget-object v1, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->numberFormat:Ljava/text/NumberFormat;

    invoke-virtual {v1, p2}, Ljava/text/NumberFormat;->setMinimumIntegerDigits(I)V

    .line 1878
    iget-object v1, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->numberFormat:Ljava/text/NumberFormat;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Ljava/text/FieldPosition;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Ljava/text/FieldPosition;-><init>(I)V

    invoke-virtual {v1, v2, p1, v3}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    .line 1879
    iget-object v1, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->numberFormat:Ljava/text/NumberFormat;

    invoke-virtual {v1, v0}, Ljava/text/NumberFormat;->setMinimumIntegerDigits(I)V

    .line 1880
    return-void
.end method

.method private appendNumericTimeZone(Ljava/lang/StringBuffer;IZ)V
    .locals 8
    .parameter "buffer"
    .parameter "count"
    .parameter "generalTimeZone"

    .prologue
    const v7, 0x36ee80

    const/4 v6, 0x4

    const/4 v5, 0x2

    .line 1855
    iget-object v2, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    const/16 v3, 0xf

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget-object v3, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    const/16 v4, 0x10

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int v0, v2, v3

    .line 1856
    .local v0, offset:I
    const/16 v1, 0x2b

    .line 1857
    .local v1, sign:C
    if-gez v0, :cond_0

    .line 1858
    const/16 v1, 0x2d

    .line 1859
    neg-int v0, v0

    .line 1861
    :cond_0
    if-nez p3, :cond_1

    if-ne p2, v6, :cond_2

    .line 1862
    :cond_1
    const-string v2, "GMT"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1864
    :cond_2
    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1865
    div-int v2, v0, v7

    invoke-direct {p0, p1, v5, v2}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->appendNumber(Ljava/lang/StringBuffer;II)V

    .line 1866
    if-nez p3, :cond_3

    if-lt p2, v6, :cond_4

    .line 1867
    :cond_3
    const/16 v2, 0x3a

    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1869
    :cond_4
    rem-int v2, v0, v7

    const v3, 0xea60

    div-int/2addr v2, v3

    invoke-direct {p0, p1, v5, v2}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->appendNumber(Ljava/lang/StringBuffer;II)V

    .line 1870
    return-void
.end method

.method private appendTimeZone(Ljava/lang/StringBuffer;IZ)V
    .locals 7
    .parameter "buffer"
    .parameter "count"
    .parameter "generalTimeZone"

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1829
    if-eqz p3, :cond_3

    .line 1830
    iget-object v5, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v3

    .line 1831
    .local v3, tz:Ljava/util/TimeZone;
    iget-object v5, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    const/16 v6, 0x10

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->get(I)I

    move-result v5

    if-eqz v5, :cond_0

    move v1, v4

    .line 1832
    .local v1, daylight:Z
    :goto_0
    const/4 v5, 0x4

    if-ge p2, v5, :cond_1

    .line 1833
    .local v2, style:I
    :goto_1
    iget-object v4, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iget-boolean v4, v4, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->customZoneStrings:Z

    if-nez v4, :cond_2

    .line 1834
    iget-object v4, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iget-object v4, v4, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->locale:Ljava/util/Locale;

    invoke-virtual {v3, v1, v2, v4}, Ljava/util/TimeZone;->getDisplayName(ZILjava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1850
    .end local v1           #daylight:Z
    .end local v2           #style:I
    .end local v3           #tz:Ljava/util/TimeZone;
    :goto_2
    return-void

    .restart local v3       #tz:Ljava/util/TimeZone;
    :cond_0
    move v1, v2

    .line 1831
    goto :goto_0

    .restart local v1       #daylight:Z
    :cond_1
    move v2, v4

    .line 1832
    goto :goto_1

    .line 1841
    .restart local v2       #style:I
    :cond_2
    iget-object v4, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iget-object v4, v4, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->zoneStrings:[[Ljava/lang/String;

    invoke-virtual {v3}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1, v2}, Llibcore/icu/TimeZones;->getDisplayName([[Ljava/lang/String;Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v0

    .line 1842
    .local v0, custom:Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 1843
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 1849
    .end local v0           #custom:Ljava/lang/String;
    .end local v1           #daylight:Z
    .end local v2           #style:I
    .end local v3           #tz:Ljava/util/TimeZone;
    :cond_3
    invoke-direct {p0, p1, p2, p3}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->appendNumericTimeZone(Ljava/lang/StringBuffer;IZ)V

    goto :goto_2
.end method

.method private static convertPattern(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 9
    .parameter "template"
    .parameter "fromChars"
    .parameter "toChars"
    .parameter "check"

    .prologue
    .line 2369
    if-nez p3, :cond_0

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2394
    .end local p0
    :goto_0
    return-object p0

    .line 2372
    .restart local p0
    :cond_0
    const/4 v5, 0x0

    .line 2373
    .local v5, quote:Z
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 2374
    .local v4, output:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 2375
    .local v2, length:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    if-ge v0, v2, :cond_7

    .line 2377
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 2378
    .local v3, next:C
    const/16 v6, 0x27

    if-ne v3, v6, :cond_1

    .line 2379
    if-nez v5, :cond_2

    const/4 v5, 0x1

    .line 2381
    :cond_1
    :goto_2
    if-nez v5, :cond_3

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .local v1, index:I
    const/4 v6, -0x1

    if-eq v1, v6, :cond_3

    .line 2382
    invoke-virtual {p2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2375
    .end local v1           #index:I
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2379
    :cond_2
    const/4 v5, 0x0

    goto :goto_2

    .line 2383
    :cond_3
    if-eqz p3, :cond_6

    if-nez v5, :cond_6

    const/16 v6, 0x61

    if-lt v3, v6, :cond_4

    const/16 v6, 0x7a

    if-le v3, v6, :cond_5

    :cond_4
    const/16 v6, 0x41

    if-lt v3, v6, :cond_6

    const/16 v6, 0x5a

    if-gt v3, v6, :cond_6

    .line 2385
    :cond_5
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Invalid pattern character \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\' in "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 2388
    :cond_6
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 2391
    .end local v3           #next:C
    :cond_7
    if-eqz v5, :cond_8

    .line 2392
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Unterminated quote"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 2394
    :cond_8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0
.end method

.method private static defaultPattern()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 1506
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Llibcore/icu/LocaleData;->get(Ljava/util/Locale;)Llibcore/icu/LocaleData;

    move-result-object v0

    .line 1507
    .local v0, localeData:Llibcore/icu/LocaleData;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Llibcore/icu/LocaleData;->getDateFormat(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0, v3}, Llibcore/icu/LocaleData;->getTimeFormat(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private error(Ljava/text/ParsePosition;ILjava/util/TimeZone;)Ljava/util/Date;
    .locals 1
    .parameter "position"
    .parameter "offset"
    .parameter "zone"

    .prologue
    .line 1883
    invoke-virtual {p1, p2}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    .line 1884
    iget-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    invoke-virtual {v0, p3}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1885
    const/4 v0, 0x0

    return-object v0
.end method

.method private formatImpl(Ljava/util/Date;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Ljava/util/List;)Ljava/lang/StringBuffer;
    .locals 11
    .parameter "date"
    .parameter "buffer"
    .parameter "field"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            "Ljava/lang/StringBuffer;",
            "Ljava/text/FieldPosition;",
            "Ljava/util/List",
            "<",
            "Ljava/text/FieldPosition;",
            ">;)",
            "Ljava/lang/StringBuffer;"
        }
    .end annotation

    .prologue
    .line 1600
    .local p4, fields:Ljava/util/List;,"Ljava/util/List<Ljava/text/FieldPosition;>;"
    const/4 v10, 0x0

    .line 1601
    .local v10, quote:Z
    const/4 v7, -0x1

    .local v7, last:I
    const/4 v5, 0x0

    .line 1602
    .local v5, count:I
    iget-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1603
    if-eqz p3, :cond_0

    .line 1604
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Ljava/text/FieldPosition;->setBeginIndex(I)V

    .line 1605
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Ljava/text/FieldPosition;->setEndIndex(I)V

    .line 1608
    :cond_0
    iget-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->pattern:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    .line 1609
    .local v9, patternLength:I
    const/4 v6, 0x0

    .local v6, i:I
    :goto_0
    if-ge v6, v9, :cond_b

    .line 1610
    iget-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->pattern:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v8

    .line 1611
    .local v8, next:I
    const/16 v0, 0x27

    if-ne v8, v0, :cond_4

    .line 1612
    if-lez v5, :cond_1

    .line 1613
    int-to-char v4, v7

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->append(Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Ljava/util/List;CI)V

    .line 1614
    const/4 v5, 0x0

    .line 1616
    :cond_1
    if-ne v7, v8, :cond_2

    .line 1617
    const/16 v0, 0x27

    invoke-virtual {p2, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1618
    const/4 v7, -0x1

    .line 1622
    :goto_1
    if-nez v10, :cond_3

    const/4 v10, 0x1

    .line 1609
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1620
    :cond_2
    move v7, v8

    goto :goto_1

    .line 1622
    :cond_3
    const/4 v10, 0x0

    goto :goto_2

    .line 1625
    :cond_4
    if-nez v10, :cond_9

    if-eq v7, v8, :cond_6

    const/16 v0, 0x61

    if-lt v8, v0, :cond_5

    const/16 v0, 0x7a

    if-le v8, v0, :cond_6

    :cond_5
    const/16 v0, 0x41

    if-lt v8, v0, :cond_9

    const/16 v0, 0x5a

    if-gt v8, v0, :cond_9

    .line 1627
    :cond_6
    if-ne v7, v8, :cond_7

    .line 1628
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1630
    :cond_7
    if-lez v5, :cond_8

    .line 1631
    int-to-char v4, v7

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->append(Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Ljava/util/List;CI)V

    .line 1633
    :cond_8
    move v7, v8

    .line 1634
    const/4 v5, 0x1

    goto :goto_2

    .line 1637
    :cond_9
    if-lez v5, :cond_a

    .line 1638
    int-to-char v4, v7

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->append(Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Ljava/util/List;CI)V

    .line 1639
    const/4 v5, 0x0

    .line 1641
    :cond_a
    const/4 v7, -0x1

    .line 1642
    int-to-char v0, v8

    invoke-virtual {p2, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 1645
    .end local v8           #next:I
    :cond_b
    if-lez v5, :cond_c

    .line 1646
    int-to-char v4, v7

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->append(Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Ljava/util/List;CI)V

    .line 1648
    :cond_c
    return-object p2
.end method

.method private formatToCharacterIteratorImpl(Ljava/util/Date;)Ljava/text/AttributedCharacterIterator;
    .locals 8
    .parameter "date"

    .prologue
    .line 1560
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 1561
    .local v2, buffer:Ljava/lang/StringBuffer;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1564
    .local v3, fields:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/text/FieldPosition;>;"
    const/4 v6, 0x0

    invoke-direct {p0, p1, v2, v6, v3}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatImpl(Ljava/util/Date;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Ljava/util/List;)Ljava/lang/StringBuffer;

    .line 1567
    new-instance v0, Ljava/text/AttributedString;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/text/AttributedString;-><init>(Ljava/lang/String;)V

    .line 1570
    .local v0, as:Ljava/text/AttributedString;
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/text/FieldPosition;

    .line 1571
    .local v5, pos:Ljava/text/FieldPosition;
    invoke-virtual {v5}, Ljava/text/FieldPosition;->getFieldAttribute()Ljava/text/Format$Field;

    move-result-object v1

    .line 1572
    .local v1, attribute:Ljava/text/Format$Field;
    invoke-virtual {v5}, Ljava/text/FieldPosition;->getBeginIndex()I

    move-result v6

    invoke-virtual {v5}, Ljava/text/FieldPosition;->getEndIndex()I

    move-result v7

    invoke-virtual {v0, v1, v1, v6, v7}, Ljava/text/AttributedString;->addAttribute(Ljava/text/AttributedCharacterIterator$Attribute;Ljava/lang/Object;II)V

    goto :goto_0

    .line 1576
    .end local v1           #attribute:Ljava/text/Format$Field;
    .end local v5           #pos:Ljava/text/FieldPosition;
    :cond_0
    invoke-virtual {v0}, Ljava/text/AttributedString;->getIterator()Ljava/text/AttributedCharacterIterator;

    move-result-object v6

    return-object v6
.end method

.method private parse(Ljava/lang/String;ICI)I
    .locals 17
    .parameter "string"
    .parameter "offset"
    .parameter "format"
    .parameter "count"

    .prologue
    .line 1933
    const-string v3, "GyMdkHmsSEDFwWahKzZLc"

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v13

    .line 1934
    .local v13, index:I
    const/4 v3, -0x1

    if-ne v13, v3, :cond_0

    .line 1935
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown pattern character \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1937
    :cond_0
    const/4 v10, -0x1

    .line 1939
    .local v10, field:I
    const/4 v7, 0x0

    .line 1940
    .local v7, absolute:I
    if-gez p4, :cond_1

    .line 1941
    move/from16 v0, p4

    neg-int v0, v0

    move/from16 p4, v0

    .line 1942
    move/from16 v7, p4

    .line 1944
    :cond_1
    packed-switch v13, :pswitch_data_0

    .line 2037
    :goto_0
    const/4 v3, -0x1

    if-eq v10, v3, :cond_2

    .line 2038
    const/4 v11, 0x0

    move-object/from16 v6, p0

    move-object/from16 v8, p1

    move/from16 v9, p2

    invoke-direct/range {v6 .. v11}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->parseNumber(ILjava/lang/String;III)I

    move-result p2

    .line 2040
    .end local p2
    :cond_2
    :goto_1
    return p2

    .line 1946
    .restart local p2
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iget-object v3, v3, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->eras:[Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->parseText(Ljava/lang/String;I[Ljava/lang/String;I)I

    move-result p2

    goto :goto_1

    .line 1948
    :pswitch_1
    const/4 v3, 0x3

    move/from16 v0, p4

    if-lt v0, v3, :cond_3

    .line 1949
    const/4 v10, 0x1

    goto :goto_0

    .line 1951
    :cond_3
    new-instance v14, Ljava/text/ParsePosition;

    move/from16 v0, p2

    invoke-direct {v14, v0}, Ljava/text/ParsePosition;-><init>(I)V

    .line 1952
    .local v14, position:Ljava/text/ParsePosition;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v7, v1, v14}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->parseNumber(ILjava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v15

    .line 1953
    .local v15, result:Ljava/lang/Number;
    if-nez v15, :cond_4

    .line 1954
    invoke-virtual {v14}, Ljava/text/ParsePosition;->getErrorIndex()I

    move-result v3

    neg-int v3, v3

    add-int/lit8 p2, v3, -0x1

    goto :goto_1

    .line 1956
    :cond_4
    invoke-virtual {v15}, Ljava/lang/Number;->intValue()I

    move-result v16

    .line 1958
    .local v16, year:I
    invoke-virtual {v14}, Ljava/text/ParsePosition;->getIndex()I

    move-result v3

    sub-int v3, v3, p2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_5

    if-ltz v16, :cond_5

    .line 1959
    move-object/from16 v0, p0

    iget v3, v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->creationYear:I

    div-int/lit8 v3, v3, 0x64

    mul-int/lit8 v3, v3, 0x64

    add-int v16, v16, v3

    .line 1960
    move-object/from16 v0, p0

    iget v3, v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->creationYear:I

    move/from16 v0, v16

    if-ge v0, v3, :cond_5

    .line 1961
    add-int/lit8 v16, v16, 0x64

    .line 1964
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    const/4 v4, 0x1

    move/from16 v0, v16

    invoke-virtual {v3, v4, v0}, Ljava/util/Calendar;->set(II)V

    .line 1965
    invoke-virtual {v14}, Ljava/text/ParsePosition;->getIndex()I

    move-result p2

    goto :goto_1

    .line 1969
    .end local v14           #position:Ljava/text/ParsePosition;
    .end local v15           #result:Ljava/lang/Number;
    .end local v16           #year:I
    :pswitch_2
    const/4 v8, 0x1

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move/from16 v5, p2

    move/from16 v6, p4

    invoke-direct/range {v3 .. v8}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->parseMonth(Ljava/lang/String;IIIZ)I

    move-result p2

    goto :goto_1

    .line 1971
    :pswitch_3
    const/4 v8, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move/from16 v5, p2

    move/from16 v6, p4

    invoke-direct/range {v3 .. v8}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->parseMonth(Ljava/lang/String;IIIZ)I

    move-result p2

    goto :goto_1

    .line 1973
    :pswitch_4
    const/4 v10, 0x5

    .line 1974
    goto/16 :goto_0

    .line 1976
    :pswitch_5
    new-instance v14, Ljava/text/ParsePosition;

    move/from16 v0, p2

    invoke-direct {v14, v0}, Ljava/text/ParsePosition;-><init>(I)V

    .line 1977
    .restart local v14       #position:Ljava/text/ParsePosition;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v7, v1, v14}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->parseNumber(ILjava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v15

    .line 1978
    .restart local v15       #result:Ljava/lang/Number;
    if-nez v15, :cond_6

    .line 1979
    invoke-virtual {v14}, Ljava/text/ParsePosition;->getErrorIndex()I

    move-result v3

    neg-int v3, v3

    add-int/lit8 p2, v3, -0x1

    goto/16 :goto_1

    .line 1981
    :cond_6
    invoke-virtual {v15}, Ljava/lang/Number;->intValue()I

    move-result v12

    .line 1982
    .local v12, hour:I
    const/16 v3, 0x18

    if-ne v12, v3, :cond_7

    .line 1983
    const/4 v12, 0x0

    .line 1985
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    const/16 v4, 0xb

    invoke-virtual {v3, v4, v12}, Ljava/util/Calendar;->set(II)V

    .line 1986
    invoke-virtual {v14}, Ljava/text/ParsePosition;->getIndex()I

    move-result p2

    goto/16 :goto_1

    .line 1988
    .end local v12           #hour:I
    .end local v14           #position:Ljava/text/ParsePosition;
    .end local v15           #result:Ljava/lang/Number;
    :pswitch_6
    const/16 v10, 0xb

    .line 1989
    goto/16 :goto_0

    .line 1991
    :pswitch_7
    const/16 v10, 0xc

    .line 1992
    goto/16 :goto_0

    .line 1994
    :pswitch_8
    const/16 v10, 0xd

    .line 1995
    goto/16 :goto_0

    .line 1997
    :pswitch_9
    const/16 v10, 0xe

    .line 1998
    goto/16 :goto_0

    .line 2000
    :pswitch_a
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->parseDayOfWeek(Ljava/lang/String;IZ)I

    move-result p2

    goto/16 :goto_1

    .line 2002
    :pswitch_b
    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->parseDayOfWeek(Ljava/lang/String;IZ)I

    move-result p2

    goto/16 :goto_1

    .line 2004
    :pswitch_c
    const/4 v10, 0x6

    .line 2005
    goto/16 :goto_0

    .line 2007
    :pswitch_d
    const/16 v10, 0x8

    .line 2008
    goto/16 :goto_0

    .line 2010
    :pswitch_e
    const/4 v10, 0x3

    .line 2011
    goto/16 :goto_0

    .line 2013
    :pswitch_f
    const/4 v10, 0x4

    .line 2014
    goto/16 :goto_0

    .line 2016
    :pswitch_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iget-object v3, v3, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->ampms:[Ljava/lang/String;

    const/16 v4, 0x9

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->parseText(Ljava/lang/String;I[Ljava/lang/String;I)I

    move-result p2

    goto/16 :goto_1

    .line 2018
    :pswitch_11
    new-instance v14, Ljava/text/ParsePosition;

    move/from16 v0, p2

    invoke-direct {v14, v0}, Ljava/text/ParsePosition;-><init>(I)V

    .line 2019
    .restart local v14       #position:Ljava/text/ParsePosition;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v7, v1, v14}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->parseNumber(ILjava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v15

    .line 2020
    .restart local v15       #result:Ljava/lang/Number;
    if-nez v15, :cond_8

    .line 2021
    invoke-virtual {v14}, Ljava/text/ParsePosition;->getErrorIndex()I

    move-result v3

    neg-int v3, v3

    add-int/lit8 p2, v3, -0x1

    goto/16 :goto_1

    .line 2023
    :cond_8
    invoke-virtual {v15}, Ljava/lang/Number;->intValue()I

    move-result v12

    .line 2024
    .restart local v12       #hour:I
    const/16 v3, 0xc

    if-ne v12, v3, :cond_9

    .line 2025
    const/4 v12, 0x0

    .line 2027
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    const/16 v4, 0xa

    invoke-virtual {v3, v4, v12}, Ljava/util/Calendar;->set(II)V

    .line 2028
    invoke-virtual {v14}, Ljava/text/ParsePosition;->getIndex()I

    move-result p2

    goto/16 :goto_1

    .line 2030
    .end local v12           #hour:I
    .end local v14           #position:Ljava/text/ParsePosition;
    .end local v15           #result:Ljava/lang/Number;
    :pswitch_12
    const/16 v10, 0xa

    .line 2031
    goto/16 :goto_0

    .line 2033
    :pswitch_13
    invoke-direct/range {p0 .. p2}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->parseTimeZone(Ljava/lang/String;I)I

    move-result p2

    goto/16 :goto_1

    .line 2035
    :pswitch_14
    invoke-direct/range {p0 .. p2}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->parseTimeZone(Ljava/lang/String;I)I

    move-result p2

    goto/16 :goto_1

    .line 1944
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_2
        :pswitch_a
    .end packed-switch
.end method

.method private parseDayOfWeek(Ljava/lang/String;IZ)I
    .locals 4
    .parameter "string"
    .parameter "offset"
    .parameter "standAlone"

    .prologue
    const/4 v3, 0x7

    .line 2044
    iget-object v2, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iget-object v1, v2, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->localeData:Llibcore/icu/LocaleData;

    .line 2045
    .local v1, ld:Llibcore/icu/LocaleData;
    if-eqz p3, :cond_1

    iget-object v2, v1, Llibcore/icu/LocaleData;->longStandAloneWeekdayNames:[Ljava/lang/String;

    :goto_0
    invoke-direct {p0, p1, p2, v2, v3}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->parseText(Ljava/lang/String;I[Ljava/lang/String;I)I

    move-result v0

    .line 2048
    .local v0, index:I
    if-gez v0, :cond_0

    .line 2049
    if-eqz p3, :cond_2

    iget-object v2, v1, Llibcore/icu/LocaleData;->shortStandAloneWeekdayNames:[Ljava/lang/String;

    :goto_1
    invoke-direct {p0, p1, p2, v2, v3}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->parseText(Ljava/lang/String;I[Ljava/lang/String;I)I

    move-result v0

    .line 2053
    :cond_0
    return v0

    .line 2045
    .end local v0           #index:I
    :cond_1
    iget-object v2, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iget-object v2, v2, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->weekdays:[Ljava/lang/String;

    goto :goto_0

    .line 2049
    .restart local v0       #index:I
    :cond_2
    iget-object v2, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iget-object v2, v2, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->shortWeekdays:[Ljava/lang/String;

    goto :goto_1
.end method

.method private parseMonth(Ljava/lang/String;IIIZ)I
    .locals 8
    .parameter "string"
    .parameter "offset"
    .parameter "count"
    .parameter "absolute"
    .parameter "standAlone"

    .prologue
    const/4 v4, 0x2

    .line 2057
    if-gt p3, v4, :cond_1

    .line 2058
    const/4 v5, -0x1

    move-object v0, p0

    move v1, p4

    move-object v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->parseNumber(ILjava/lang/String;III)I

    move-result v6

    .line 2069
    :cond_0
    :goto_0
    return v6

    .line 2060
    :cond_1
    iget-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iget-object v7, v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->localeData:Llibcore/icu/LocaleData;

    .line 2061
    .local v7, ld:Llibcore/icu/LocaleData;
    if-eqz p5, :cond_2

    iget-object v0, v7, Llibcore/icu/LocaleData;->longStandAloneMonthNames:[Ljava/lang/String;

    :goto_1
    invoke-direct {p0, p1, p2, v0, v4}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->parseText(Ljava/lang/String;I[Ljava/lang/String;I)I

    move-result v6

    .line 2064
    .local v6, index:I
    if-gez v6, :cond_0

    .line 2065
    if-eqz p5, :cond_3

    iget-object v0, v7, Llibcore/icu/LocaleData;->shortStandAloneMonthNames:[Ljava/lang/String;

    :goto_2
    invoke-direct {p0, p1, p2, v0, v4}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->parseText(Ljava/lang/String;I[Ljava/lang/String;I)I

    move-result v6

    goto :goto_0

    .line 2061
    .end local v6           #index:I
    :cond_2
    iget-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iget-object v0, v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->months:[Ljava/lang/String;

    goto :goto_1

    .line 2065
    .restart local v6       #index:I
    :cond_3
    iget-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iget-object v0, v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->shortMonths:[Ljava/lang/String;

    goto :goto_2
.end method

.method private parseNumber(ILjava/lang/String;III)I
    .locals 4
    .parameter "max"
    .parameter "string"
    .parameter "offset"
    .parameter "field"
    .parameter "skew"

    .prologue
    .line 2217
    new-instance v0, Ljava/text/ParsePosition;

    invoke-direct {v0, p3}, Ljava/text/ParsePosition;-><init>(I)V

    .line 2218
    .local v0, position:Ljava/text/ParsePosition;
    invoke-direct {p0, p1, p2, v0}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->parseNumber(ILjava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v1

    .line 2219
    .local v1, result:Ljava/lang/Number;
    if-nez v1, :cond_0

    .line 2220
    invoke-virtual {v0}, Ljava/text/ParsePosition;->getErrorIndex()I

    move-result v2

    neg-int v2, v2

    add-int/lit8 v2, v2, -0x1

    .line 2223
    :goto_0
    return v2

    .line 2222
    :cond_0
    iget-object v2, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v3

    add-int/2addr v3, p5

    invoke-virtual {v2, p4, v3}, Ljava/util/Calendar;->set(II)V

    .line 2223
    invoke-virtual {v0}, Ljava/text/ParsePosition;->getIndex()I

    move-result v2

    goto :goto_0
.end method

.method private parseNumber(ILjava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;
    .locals 12
    .parameter "max"
    .parameter "string"
    .parameter "position"

    .prologue
    .line 2171
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    .line 2172
    .local v4, length:I
    invoke-virtual {p3}, Ljava/text/ParsePosition;->getIndex()I

    move-result v2

    .line 2173
    .local v2, index:I
    if-lez p1, :cond_0

    sub-int v8, v4, v2

    if-ge p1, v8, :cond_0

    .line 2174
    add-int v4, v2, p1

    .line 2176
    :cond_0
    :goto_0
    if-ge v2, v4, :cond_2

    invoke-virtual {p2, v2}, Ljava/lang/String;->charAt(I)C

    move-result v8

    const/16 v9, 0x20

    if-eq v8, v9, :cond_1

    invoke-virtual {p2, v2}, Ljava/lang/String;->charAt(I)C

    move-result v8

    const/16 v9, 0x9

    if-ne v8, v9, :cond_2

    .line 2177
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2179
    :cond_2
    if-nez p1, :cond_4

    .line 2180
    invoke-virtual {p3, v2}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 2181
    iget-object v8, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->numberFormat:Ljava/text/NumberFormat;

    invoke-virtual {v8, p2, p3}, Ljava/text/NumberFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v6

    .line 2188
    .local v6, n:Ljava/lang/Number;
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Ljava/lang/Number;->longValue()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-gez v8, :cond_3

    .line 2189
    iget-object v8, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->numberFormat:Ljava/text/NumberFormat;

    instance-of v8, v8, Ljava/text/DecimalFormat;

    if-eqz v8, :cond_3

    .line 2190
    iget-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->numberFormat:Ljava/text/NumberFormat;

    check-cast v0, Ljava/text/DecimalFormat;

    .line 2191
    .local v0, df:Ljava/text/DecimalFormat;
    invoke-virtual {p3}, Ljava/text/ParsePosition;->getIndex()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {p2, v8}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 2192
    .local v3, lastChar:C
    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v8

    invoke-virtual {v8}, Ljava/text/DecimalFormatSymbols;->getMinusSign()C

    move-result v5

    .line 2193
    .local v5, minusSign:C
    if-ne v3, v5, :cond_3

    .line 2194
    invoke-virtual {v6}, Ljava/lang/Number;->longValue()J

    move-result-wide v8

    neg-long v8, v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 2195
    invoke-virtual {p3}, Ljava/text/ParsePosition;->getIndex()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {p3, v8}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 2213
    .end local v0           #df:Ljava/text/DecimalFormat;
    .end local v3           #lastChar:C
    .end local v5           #minusSign:C
    .end local v6           #n:Ljava/lang/Number;
    :cond_3
    :goto_1
    return-object v6

    .line 2202
    :cond_4
    const/4 v7, 0x0

    .line 2204
    .local v7, result:I
    :goto_2
    if-ge v2, v4, :cond_5

    invoke-virtual {p2, v2}, Ljava/lang/String;->charAt(I)C

    move-result v8

    const/16 v9, 0xa

    invoke-static {v8, v9}, Ljava/lang/Character;->digit(CI)I

    move-result v1

    .local v1, digit:I
    const/4 v8, -0x1

    if-eq v1, v8, :cond_5

    .line 2205
    mul-int/lit8 v8, v7, 0xa

    add-int v7, v8, v1

    .line 2206
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 2208
    .end local v1           #digit:I
    :cond_5
    invoke-virtual {p3}, Ljava/text/ParsePosition;->getIndex()I

    move-result v8

    if-ne v2, v8, :cond_6

    .line 2209
    invoke-virtual {p3, v2}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    .line 2210
    const/4 v6, 0x0

    goto :goto_1

    .line 2212
    :cond_6
    invoke-virtual {p3, v2}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 2213
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto :goto_1
.end method

.method private parseText(Ljava/lang/String;I[Ljava/lang/String;I)I
    .locals 9
    .parameter "string"
    .parameter "offset"
    .parameter "text"
    .parameter "field"

    .prologue
    const/4 v8, -0x1

    .line 2227
    const/4 v6, -0x1

    .line 2228
    .local v6, found:I
    const/4 v7, 0x0

    .local v7, i:I
    :goto_0
    array-length v0, p3

    if-ge v7, v0, :cond_3

    .line 2229
    aget-object v0, p3, v7

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2228
    :cond_0
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 2232
    :cond_1
    const/4 v1, 0x1

    aget-object v3, p3, v7

    const/4 v4, 0x0

    aget-object v0, p3, v7

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    move-object v0, p1

    move v2, p2

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2235
    if-eq v6, v8, :cond_2

    aget-object v0, p3, v7

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    aget-object v1, p3, v6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 2236
    :cond_2
    move v6, v7

    goto :goto_1

    .line 2240
    :cond_3
    if-eq v6, v8, :cond_4

    .line 2241
    iget-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    invoke-virtual {v0, p4, v6}, Ljava/util/Calendar;->set(II)V

    .line 2242
    aget-object v0, p3, v6

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, p2

    .line 2244
    :goto_2
    return v0

    :cond_4
    neg-int v0, p2

    add-int/lit8 v0, v0, -0x1

    goto :goto_2
.end method

.method private parseTimeZone(Ljava/lang/String;I)I
    .locals 23
    .parameter "string"
    .parameter "offset"

    .prologue
    .line 2248
    const-string v2, "GMT"

    const/4 v3, 0x0

    const/4 v4, 0x3

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v0, v1, v2, v3, v4}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v10

    .line 2249
    .local v10, foundGMT:Z
    if-eqz v10, :cond_0

    .line 2250
    add-int/lit8 p2, p2, 0x3

    .line 2253
    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v2

    move/from16 v0, p2

    if-ge v0, v2, :cond_7

    invoke-virtual/range {p1 .. p2}, Ljava/lang/String;->charAt(I)C

    move-result v21

    .local v21, sign:C
    const/16 v2, 0x2b

    move/from16 v0, v21

    if-eq v0, v2, :cond_1

    const/16 v2, 0x2d

    move/from16 v0, v21

    if-ne v0, v2, :cond_7

    .line 2254
    :cond_1
    new-instance v17, Ljava/text/ParsePosition;

    add-int/lit8 v2, p2, 0x1

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Ljava/text/ParsePosition;-><init>(I)V

    .line 2255
    .local v17, position:Ljava/text/ParsePosition;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->numberFormat:Ljava/text/NumberFormat;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v2, v0, v1}, Ljava/text/NumberFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v19

    .line 2256
    .local v19, result:Ljava/lang/Number;
    if-nez v19, :cond_2

    .line 2257
    invoke-virtual/range {v17 .. v17}, Ljava/text/ParsePosition;->getErrorIndex()I

    move-result v2

    neg-int v2, v2

    add-int/lit8 p2, v2, -0x1

    .line 2316
    .end local v17           #position:Ljava/text/ParsePosition;
    .end local v19           #result:Ljava/lang/Number;
    .end local v21           #sign:C
    .end local p2
    :goto_0
    return p2

    .line 2259
    .restart local v17       #position:Ljava/text/ParsePosition;
    .restart local v19       #result:Ljava/lang/Number;
    .restart local v21       #sign:C
    .restart local p2
    :cond_2
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Number;->intValue()I

    move-result v11

    .line 2260
    .local v11, hour:I
    const v2, 0x36ee80

    mul-int v18, v11, v2

    .line 2261
    .local v18, raw:I
    invoke-virtual/range {v17 .. v17}, Ljava/text/ParsePosition;->getIndex()I

    move-result v14

    .line 2262
    .local v14, index:I
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v14, v2, :cond_6

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x3a

    if-ne v2, v3, :cond_6

    .line 2263
    add-int/lit8 v2, v14, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 2264
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->numberFormat:Ljava/text/NumberFormat;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v2, v0, v1}, Ljava/text/NumberFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v19

    .line 2265
    if-nez v19, :cond_3

    .line 2266
    invoke-virtual/range {v17 .. v17}, Ljava/text/ParsePosition;->getErrorIndex()I

    move-result v2

    neg-int v2, v2

    add-int/lit8 p2, v2, -0x1

    goto :goto_0

    .line 2268
    :cond_3
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Number;->intValue()I

    move-result v16

    .line 2269
    .local v16, minute:I
    const v2, 0xea60

    mul-int v2, v2, v16

    add-int v18, v18, v2

    .line 2273
    .end local v16           #minute:I
    :cond_4
    :goto_1
    const/16 v2, 0x2d

    move/from16 v0, v21

    if-ne v0, v2, :cond_5

    .line 2274
    move/from16 v0, v18

    neg-int v0, v0

    move/from16 v18, v0

    .line 2276
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    new-instance v3, Ljava/util/SimpleTimeZone;

    const-string v4, ""

    move/from16 v0, v18

    invoke-direct {v3, v0, v4}, Ljava/util/SimpleTimeZone;-><init>(ILjava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 2277
    invoke-virtual/range {v17 .. v17}, Ljava/text/ParsePosition;->getIndex()I

    move-result p2

    goto :goto_0

    .line 2270
    :cond_6
    const/16 v2, 0x18

    if-lt v11, v2, :cond_4

    .line 2271
    div-int/lit8 v2, v11, 0x64

    const v3, 0x36ee80

    mul-int/2addr v2, v3

    rem-int/lit8 v3, v11, 0x64

    const v4, 0xea60

    mul-int/2addr v3, v4

    add-int v18, v2, v3

    goto :goto_1

    .line 2279
    .end local v11           #hour:I
    .end local v14           #index:I
    .end local v17           #position:Ljava/text/ParsePosition;
    .end local v18           #raw:I
    .end local v19           #result:Ljava/lang/Number;
    .end local v21           #sign:C
    :cond_7
    if-eqz v10, :cond_8

    .line 2280
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    const-string v3, "GMT"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    goto/16 :goto_0

    .line 2283
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    invoke-virtual {v2}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->internalZoneStrings()[[Ljava/lang/String;

    move-result-object v8

    .local v8, arr$:[[Ljava/lang/String;
    array-length v15, v8

    .local v15, len$:I
    const/4 v13, 0x0

    .local v13, i$:I
    :goto_2
    if-ge v13, v15, :cond_10

    aget-object v20, v8, v13

    .line 2284
    .local v20, row:[Ljava/lang/String;
    const/4 v12, 0x1

    .local v12, i:I
    :goto_3
    const/4 v2, 0x5

    if-ge v12, v2, :cond_f

    .line 2285
    aget-object v2, v20, v12

    if-nez v2, :cond_a

    .line 2284
    :cond_9
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    .line 2291
    :cond_a
    const/4 v3, 0x1

    aget-object v5, v20, v12

    const/4 v6, 0x0

    aget-object v2, v20, v12

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    move-object/from16 v2, p1

    move/from16 v4, p2

    invoke-virtual/range {v2 .. v7}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 2292
    const/4 v2, 0x0

    aget-object v2, v20, v2

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v22

    .line 2293
    .local v22, zone:Ljava/util/TimeZone;
    if-nez v22, :cond_b

    .line 2294
    move/from16 v0, p2

    neg-int v2, v0

    add-int/lit8 p2, v2, -0x1

    goto/16 :goto_0

    .line 2296
    :cond_b
    invoke-virtual/range {v22 .. v22}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v18

    .line 2297
    .restart local v18       #raw:I
    const/4 v2, 0x3

    if-eq v12, v2, :cond_c

    const/4 v2, 0x4

    if-ne v12, v2, :cond_e

    .line 2300
    :cond_c
    invoke-virtual/range {v22 .. v22}, Ljava/util/TimeZone;->getDSTSavings()I

    move-result v9

    .line 2304
    .local v9, dstSavings:I
    if-nez v9, :cond_d

    .line 2307
    const v9, 0x36ee80

    .line 2309
    :cond_d
    add-int v18, v18, v9

    .line 2311
    .end local v9           #dstSavings:I
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    new-instance v3, Ljava/util/SimpleTimeZone;

    const-string v4, ""

    move/from16 v0, v18

    invoke-direct {v3, v0, v4}, Ljava/util/SimpleTimeZone;-><init>(ILjava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 2312
    aget-object v2, v20, v12

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int p2, p2, v2

    goto/16 :goto_0

    .line 2283
    .end local v18           #raw:I
    .end local v22           #zone:Ljava/util/TimeZone;
    :cond_f
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 2316
    .end local v12           #i:I
    .end local v20           #row:[Ljava/lang/String;
    :cond_10
    move/from16 v0, p2

    neg-int v2, v0

    add-int/lit8 p2, v2, -0x1

    goto/16 :goto_0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 5
    .parameter "stream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 2424
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readFields()Ljava/io/ObjectInputStream$GetField;

    move-result-object v1

    .line 2425
    .local v1, fields:Ljava/io/ObjectInputStream$GetField;
    const-string v3, "serialVersionOnStream"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Ljava/io/ObjectInputStream$GetField;->get(Ljava/lang/String;I)I

    move-result v2

    .line 2427
    .local v2, version:I
    if-lez v2, :cond_0

    .line 2428
    const-string v3, "defaultCenturyStart"

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v3, v4}, Ljava/io/ObjectInputStream$GetField;->get(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    .line 2432
    .local v0, date:Ljava/util/Date;
    :goto_0
    invoke-virtual {p0, v0}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->set2DigitYearStart(Ljava/util/Date;)V

    .line 2433
    const-string v3, "formatData"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Ljava/io/ObjectInputStream$GetField;->get(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iput-object v3, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    .line 2434
    const-string v3, "pattern"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Ljava/io/ObjectInputStream$GetField;->get(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->pattern:Ljava/lang/String;

    .line 2435
    return-void

    .line 2430
    .end local v0           #date:Ljava/util/Date;
    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .restart local v0       #date:Ljava/util/Date;
    goto :goto_0
.end method

.method private validateFormat(C)V
    .locals 4
    .parameter "format"

    .prologue
    .line 1360
    const-string v1, "GyMdkHmsSEDFwWahKzZLc"

    invoke-virtual {v1, p1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 1361
    .local v0, index:I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1362
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown pattern character \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1364
    :cond_0
    return-void
.end method

.method private validatePattern(Ljava/lang/String;)V
    .locals 8
    .parameter "template"

    .prologue
    .line 1375
    const/4 v5, 0x0

    .line 1376
    .local v5, quote:Z
    const/4 v2, -0x1

    .local v2, last:I
    const/4 v0, 0x0

    .line 1378
    .local v0, count:I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    .line 1379
    .local v4, patternLength:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    if-ge v1, v4, :cond_a

    .line 1380
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 1381
    .local v3, next:I
    const/16 v6, 0x27

    if-ne v3, v6, :cond_3

    .line 1382
    if-lez v0, :cond_0

    .line 1383
    int-to-char v6, v2

    invoke-direct {p0, v6}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->validateFormat(C)V

    .line 1384
    const/4 v0, 0x0

    .line 1386
    :cond_0
    if-ne v2, v3, :cond_1

    .line 1387
    const/4 v2, -0x1

    .line 1391
    :goto_1
    if-nez v5, :cond_2

    const/4 v5, 0x1

    .line 1379
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1389
    :cond_1
    move v2, v3

    goto :goto_1

    .line 1391
    :cond_2
    const/4 v5, 0x0

    goto :goto_2

    .line 1394
    :cond_3
    if-nez v5, :cond_8

    if-eq v2, v3, :cond_5

    const/16 v6, 0x61

    if-lt v3, v6, :cond_4

    const/16 v6, 0x7a

    if-le v3, v6, :cond_5

    :cond_4
    const/16 v6, 0x41

    if-lt v3, v6, :cond_8

    const/16 v6, 0x5a

    if-gt v3, v6, :cond_8

    .line 1396
    :cond_5
    if-ne v2, v3, :cond_6

    .line 1397
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1399
    :cond_6
    if-lez v0, :cond_7

    .line 1400
    int-to-char v6, v2

    invoke-direct {p0, v6}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->validateFormat(C)V

    .line 1402
    :cond_7
    move v2, v3

    .line 1403
    const/4 v0, 0x1

    goto :goto_2

    .line 1406
    :cond_8
    if-lez v0, :cond_9

    .line 1407
    int-to-char v6, v2

    invoke-direct {p0, v6}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->validateFormat(C)V

    .line 1408
    const/4 v0, 0x0

    .line 1410
    :cond_9
    const/4 v2, -0x1

    goto :goto_2

    .line 1413
    .end local v3           #next:I
    :cond_a
    if-lez v0, :cond_b

    .line 1414
    int-to-char v6, v2

    invoke-direct {p0, v6}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->validateFormat(C)V

    .line 1417
    :cond_b
    if-eqz v5, :cond_c

    .line 1418
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Unterminated quote"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 1420
    :cond_c
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 3
    .parameter "stream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2414
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->putFields()Ljava/io/ObjectOutputStream$PutField;

    move-result-object v0

    .line 2415
    .local v0, fields:Ljava/io/ObjectOutputStream$PutField;
    const-string v1, "defaultCenturyStart"

    iget-object v2, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->defaultCenturyStart:Ljava/util/Date;

    invoke-virtual {v0, v1, v2}, Ljava/io/ObjectOutputStream$PutField;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2416
    const-string v1, "formatData"

    iget-object v2, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    invoke-virtual {v0, v1, v2}, Ljava/io/ObjectOutputStream$PutField;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2417
    const-string v1, "pattern"

    iget-object v2, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->pattern:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/io/ObjectOutputStream$PutField;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2418
    const-string v1, "serialVersionOnStream"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/io/ObjectOutputStream$PutField;->put(Ljava/lang/String;I)V

    .line 2419
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->writeFields()V

    .line 2420
    return-void
.end method


# virtual methods
.method public applyLocalizedPattern(Ljava/lang/String;)V
    .locals 3
    .parameter "template"

    .prologue
    .line 1474
    iget-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    invoke-virtual {v0}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->getLocalPatternChars()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GyMdkHmsSEDFwWahKzZLc"

    const/4 v2, 0x1

    invoke-static {p1, v0, v1, v2}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->convertPattern(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->pattern:Ljava/lang/String;

    .line 1475
    return-void
.end method

.method public applyPattern(Ljava/lang/String;)V
    .locals 0
    .parameter "template"

    .prologue
    .line 1486
    invoke-direct {p0, p1}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->validatePattern(Ljava/lang/String;)V

    .line 1487
    iput-object p1, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->pattern:Ljava/lang/String;

    .line 1488
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1499
    invoke-super {p0}, Ljava/text/DateFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;

    .line 1500
    .local v0, clone:Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;
    iget-object v1, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    invoke-virtual {v1}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iput-object v1, v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    .line 1501
    new-instance v1, Ljava/util/Date;

    iget-object v2, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->defaultCenturyStart:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v1, v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->defaultCenturyStart:Ljava/util/Date;

    .line 1502
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .parameter "object"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1523
    if-ne p0, p1, :cond_1

    .line 1530
    :cond_0
    :goto_0
    return v1

    .line 1526
    :cond_1
    instance-of v3, p1, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;

    if-nez v3, :cond_2

    move v1, v2

    .line 1527
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 1529
    check-cast v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;

    .line 1530
    .local v0, simple:Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;
    invoke-super {p0, p1}, Ljava/text/DateFormat;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->pattern:Ljava/lang/String;

    iget-object v4, v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->pattern:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iget-object v4, v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    invoke-virtual {v3, v4}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public format(Ljava/util/Date;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 1
    .parameter "date"
    .parameter "buffer"
    .parameter "fieldPos"

    .prologue
    .line 1907
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatImpl(Ljava/util/Date;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Ljava/util/List;)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public formatToCharacterIterator(Ljava/lang/Object;)Ljava/text/AttributedCharacterIterator;
    .locals 3
    .parameter "object"

    .prologue
    .line 1546
    if-nez p1, :cond_0

    .line 1547
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "object == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1549
    :cond_0
    instance-of v0, p1, Ljava/util/Date;

    if-eqz v0, :cond_1

    .line 1550
    check-cast p1, Ljava/util/Date;

    .end local p1
    invoke-direct {p0, p1}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatToCharacterIteratorImpl(Ljava/util/Date;)Ljava/text/AttributedCharacterIterator;

    move-result-object v0

    .line 1553
    :goto_0
    return-object v0

    .line 1552
    .restart local p1
    :cond_1
    instance-of v0, p1, Ljava/lang/Number;

    if-eqz v0, :cond_2

    .line 1553
    new-instance v0, Ljava/util/Date;

    check-cast p1, Ljava/lang/Number;

    .end local p1
    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-direct {p0, v0}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatToCharacterIteratorImpl(Ljava/util/Date;)Ljava/text/AttributedCharacterIterator;

    move-result-object v0

    goto :goto_0

    .line 1555
    .restart local p1
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad class: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public get2DigitYearStart()Ljava/util/Date;
    .locals 1

    .prologue
    .line 1915
    iget-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->defaultCenturyStart:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    return-object v0
.end method

.method public getDateFormatSymbols()Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;
    .locals 1

    .prologue
    .line 1924
    iget-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    invoke-virtual {v0}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 1929
    invoke-super {p0}, Ljava/text/DateFormat;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->pattern:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    invoke-virtual {v1}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->creationYear:I

    add-int/2addr v0, v1

    return v0
.end method

.method public parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Date;
    .locals 15
    .parameter "string"
    .parameter "position"

    .prologue
    .line 2091
    const/4 v10, 0x0

    .line 2092
    .local v10, quote:Z
    const/4 v5, -0x1

    .local v5, last:I
    const/4 v1, 0x0

    .local v1, count:I
    invoke-virtual/range {p2 .. p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v8

    .line 2093
    .local v8, offset:I
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v6

    .line 2094
    .local v6, length:I
    iget-object v13, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    invoke-virtual {v13}, Ljava/util/Calendar;->clear()V

    .line 2095
    iget-object v13, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    invoke-virtual {v13}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v12

    .line 2096
    .local v12, zone:Ljava/util/TimeZone;
    iget-object v13, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->pattern:Ljava/lang/String;

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v9

    .line 2097
    .local v9, patternLength:I
    const/4 v4, 0x0

    .local v4, i:I
    :goto_0
    if-ge v4, v9, :cond_10

    .line 2098
    iget-object v13, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->pattern:Ljava/lang/String;

    invoke-virtual {v13, v4}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 2099
    .local v7, next:I
    const/16 v13, 0x27

    if-ne v7, v13, :cond_6

    .line 2100
    if-lez v1, :cond_1

    .line 2101
    int-to-char v13, v5

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v8, v13, v1}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->parse(Ljava/lang/String;ICI)I

    move-result v8

    if-gez v8, :cond_0

    .line 2102
    neg-int v13, v8

    add-int/lit8 v13, v13, -0x1

    move-object/from16 v0, p2

    invoke-direct {p0, v0, v13, v12}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->error(Ljava/text/ParsePosition;ILjava/util/TimeZone;)Ljava/util/Date;

    move-result-object v2

    .line 2167
    .end local v7           #next:I
    :goto_1
    return-object v2

    .line 2104
    .restart local v7       #next:I
    :cond_0
    const/4 v1, 0x0

    .line 2106
    :cond_1
    if-ne v5, v7, :cond_4

    .line 2107
    if-ge v8, v6, :cond_2

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Ljava/lang/String;->charAt(I)C

    move-result v13

    const/16 v14, 0x27

    if-eq v13, v14, :cond_3

    .line 2108
    :cond_2
    move-object/from16 v0, p2

    invoke-direct {p0, v0, v8, v12}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->error(Ljava/text/ParsePosition;ILjava/util/TimeZone;)Ljava/util/Date;

    move-result-object v2

    goto :goto_1

    .line 2110
    :cond_3
    add-int/lit8 v8, v8, 0x1

    .line 2111
    const/4 v5, -0x1

    .line 2115
    :goto_2
    if-nez v10, :cond_5

    const/4 v10, 0x1

    .line 2097
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2113
    :cond_4
    move v5, v7

    goto :goto_2

    .line 2115
    :cond_5
    const/4 v10, 0x0

    goto :goto_3

    .line 2118
    :cond_6
    if-nez v10, :cond_b

    if-eq v5, v7, :cond_8

    const/16 v13, 0x61

    if-lt v7, v13, :cond_7

    const/16 v13, 0x7a

    if-le v7, v13, :cond_8

    :cond_7
    const/16 v13, 0x41

    if-lt v7, v13, :cond_b

    const/16 v13, 0x5a

    if-gt v7, v13, :cond_b

    .line 2120
    :cond_8
    if-ne v5, v7, :cond_9

    .line 2121
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2123
    :cond_9
    if-lez v1, :cond_a

    .line 2124
    int-to-char v13, v5

    neg-int v14, v1

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v8, v13, v14}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->parse(Ljava/lang/String;ICI)I

    move-result v8

    if-gez v8, :cond_a

    .line 2125
    neg-int v13, v8

    add-int/lit8 v13, v13, -0x1

    move-object/from16 v0, p2

    invoke-direct {p0, v0, v13, v12}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->error(Ljava/text/ParsePosition;ILjava/util/TimeZone;)Ljava/util/Date;

    move-result-object v2

    goto :goto_1

    .line 2128
    :cond_a
    move v5, v7

    .line 2129
    const/4 v1, 0x1

    goto :goto_3

    .line 2132
    :cond_b
    if-lez v1, :cond_d

    .line 2133
    int-to-char v13, v5

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v8, v13, v1}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->parse(Ljava/lang/String;ICI)I

    move-result v8

    if-gez v8, :cond_c

    .line 2134
    neg-int v13, v8

    add-int/lit8 v13, v13, -0x1

    move-object/from16 v0, p2

    invoke-direct {p0, v0, v13, v12}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->error(Ljava/text/ParsePosition;ILjava/util/TimeZone;)Ljava/util/Date;

    move-result-object v2

    goto :goto_1

    .line 2136
    :cond_c
    const/4 v1, 0x0

    .line 2138
    :cond_d
    const/4 v5, -0x1

    .line 2139
    if-ge v8, v6, :cond_e

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Ljava/lang/String;->charAt(I)C

    move-result v13

    if-eq v13, v7, :cond_f

    .line 2140
    :cond_e
    move-object/from16 v0, p2

    invoke-direct {p0, v0, v8, v12}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->error(Ljava/text/ParsePosition;ILjava/util/TimeZone;)Ljava/util/Date;

    move-result-object v2

    goto :goto_1

    .line 2142
    :cond_f
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 2145
    .end local v7           #next:I
    :cond_10
    if-lez v1, :cond_11

    .line 2146
    int-to-char v13, v5

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v8, v13, v1}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->parse(Ljava/lang/String;ICI)I

    move-result v8

    if-gez v8, :cond_11

    .line 2147
    neg-int v13, v8

    add-int/lit8 v13, v13, -0x1

    move-object/from16 v0, p2

    invoke-direct {p0, v0, v13, v12}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->error(Ljava/text/ParsePosition;ILjava/util/TimeZone;)Ljava/util/Date;

    move-result-object v2

    goto/16 :goto_1

    .line 2154
    :cond_11
    :try_start_0
    iget-object v13, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Ljava/util/Calendar;->get(I)I

    move-result v11

    .line 2155
    .local v11, year:I
    const/16 v13, 0x98b

    if-le v11, v13, :cond_12

    .line 2156
    add-int/lit16 v11, v11, -0x21f

    .line 2157
    iget-object v13, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    const/4 v14, 0x1

    invoke-virtual {v13, v14, v11}, Ljava/util/Calendar;->set(II)V

    .line 2161
    :cond_12
    iget-object v13, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    invoke-virtual {v13}, Ljava/util/Calendar;->getTime()Ljava/util/Date;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 2165
    .local v2, date:Ljava/util/Date;
    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 2166
    iget-object v13, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->calendar:Ljava/util/Calendar;

    invoke-virtual {v13, v12}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    goto/16 :goto_1

    .line 2162
    .end local v2           #date:Ljava/util/Date;
    .end local v11           #year:I
    :catch_0
    move-exception v3

    .line 2163
    .local v3, e:Ljava/lang/IllegalArgumentException;
    move-object/from16 v0, p2

    invoke-direct {p0, v0, v8, v12}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->error(Ljava/text/ParsePosition;ILjava/util/TimeZone;)Ljava/util/Date;

    move-result-object v2

    goto/16 :goto_1
.end method

.method public set2DigitYearStart(Ljava/util/Date;)V
    .locals 2
    .parameter "date"

    .prologue
    .line 2343
    invoke-virtual {p1}, Ljava/util/Date;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Date;

    iput-object v1, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->defaultCenturyStart:Ljava/util/Date;

    .line 2344
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 2345
    .local v0, cal:Ljava/util/Calendar;
    iget-object v1, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->defaultCenturyStart:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 2346
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->creationYear:I

    .line 2347
    return-void
.end method

.method public setDateFormatSymbols(Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;)V
    .locals 1
    .parameter "value"

    .prologue
    .line 2355
    invoke-virtual {p1}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    iput-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    .line 2356
    return-void
.end method

.method public toLocalizedPattern()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2364
    iget-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->pattern:Ljava/lang/String;

    const-string v1, "GyMdkHmsSEDFwWahKzZLc"

    iget-object v2, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->formatData:Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;

    invoke-virtual {v2}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoDateFormatSymbols;->getLocalPatternChars()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->convertPattern(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toPattern()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2403
    iget-object v0, p0, Lcom/oppo/util/OppoThailandCalendarUtil$OppoJavaSimpleDateFormat;->pattern:Ljava/lang/String;

    return-object v0
.end method
