.class public Lcom/oppo/util/OppoListBackToTopUtils;
.super Ljava/lang/Object;
.source "OppoListBackToTopUtils.java"


# instance fields
.field private final BUTTON_MARGIN_X:I

.field private final BUTTON_MARGIN_Y:I

.field private final FAR_DURATION:I

.field private final GAP_DURATION:I

.field private final NEAR_DURATION:I

.field private final SCREEN_COUNT:I

.field backToTop:Ljava/lang/Runnable;

.field handler:Landroid/os/Handler;

.field private mBackToTop:Landroid/widget/Button;

.field private mList:Landroid/widget/AbsListView;

.field private mPopupWindow:Landroid/widget/PopupWindow;


# direct methods
.method public constructor <init>(Landroid/widget/AbsListView;)V
    .locals 1
    .parameter "list"

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/oppo/util/OppoListBackToTopUtils;->handler:Landroid/os/Handler;

    .line 45
    const/16 v0, 0x96

    iput v0, p0, Lcom/oppo/util/OppoListBackToTopUtils;->BUTTON_MARGIN_X:I

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/oppo/util/OppoListBackToTopUtils;->BUTTON_MARGIN_Y:I

    .line 49
    const/16 v0, 0xc8

    iput v0, p0, Lcom/oppo/util/OppoListBackToTopUtils;->NEAR_DURATION:I

    .line 51
    const/16 v0, 0x190

    iput v0, p0, Lcom/oppo/util/OppoListBackToTopUtils;->FAR_DURATION:I

    .line 53
    const/16 v0, 0x32

    iput v0, p0, Lcom/oppo/util/OppoListBackToTopUtils;->GAP_DURATION:I

    .line 55
    const/4 v0, 0x2

    iput v0, p0, Lcom/oppo/util/OppoListBackToTopUtils;->SCREEN_COUNT:I

    .line 93
    new-instance v0, Lcom/oppo/util/OppoListBackToTopUtils$2;

    invoke-direct {v0, p0}, Lcom/oppo/util/OppoListBackToTopUtils$2;-><init>(Lcom/oppo/util/OppoListBackToTopUtils;)V

    iput-object v0, p0, Lcom/oppo/util/OppoListBackToTopUtils;->backToTop:Ljava/lang/Runnable;

    .line 61
    iput-object p1, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mList:Landroid/widget/AbsListView;

    .line 62
    return-void
.end method

.method private StartBackToTopAnimation()V
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 117
    iget-object v6, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mList:Landroid/widget/AbsListView;

    invoke-virtual {v6, v8}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 118
    .local v2, itemHeight:I
    iget-object v6, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mList:Landroid/widget/AbsListView;

    invoke-virtual {v6}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v3

    .line 119
    .local v3, itemNear:I
    iget-object v6, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mList:Landroid/widget/AbsListView;

    invoke-virtual {v6}, Landroid/widget/AbsListView;->getChildCount()I

    move-result v6

    mul-int/lit8 v1, v6, 0x2

    .line 120
    .local v1, itemFar:I
    add-int/lit8 v6, v3, 0x1

    mul-int v0, v6, v2

    .line 121
    .local v0, distance:I
    iget-object v6, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mList:Landroid/widget/AbsListView;

    invoke-virtual {v6}, Landroid/widget/AbsListView;->getHeight()I

    move-result v5

    .line 122
    .local v5, oneScreenHeight:I
    mul-int/lit8 v4, v5, 0x2

    .line 123
    .local v4, maxDistance:I
    if-ge v0, v4, :cond_0

    .line 124
    iget-object v6, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mList:Landroid/widget/AbsListView;

    const/16 v7, 0xc8

    invoke-virtual {v6, v8, v8, v7}, Landroid/widget/AbsListView;->smoothScrollToPositionFromTop(III)V

    .line 125
    iget-object v6, p0, Lcom/oppo/util/OppoListBackToTopUtils;->handler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/oppo/util/OppoListBackToTopUtils;->backToTop:Ljava/lang/Runnable;

    const-wide/16 v8, 0xfa

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 131
    :goto_0
    return-void

    .line 127
    :cond_0
    iget-object v6, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mList:Landroid/widget/AbsListView;

    invoke-virtual {v6, v1}, Landroid/widget/AbsListView;->setSelection(I)V

    .line 128
    iget-object v6, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mList:Landroid/widget/AbsListView;

    const/16 v7, 0x190

    invoke-virtual {v6, v8, v8, v7}, Landroid/widget/AbsListView;->smoothScrollToPositionFromTop(III)V

    .line 129
    iget-object v6, p0, Lcom/oppo/util/OppoListBackToTopUtils;->handler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/oppo/util/OppoListBackToTopUtils;->backToTop:Ljava/lang/Runnable;

    const-wide/16 v8, 0x1c2

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/oppo/util/OppoListBackToTopUtils;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/oppo/util/OppoListBackToTopUtils;->StartBackToTopAnimation()V

    return-void
.end method

.method static synthetic access$100(Lcom/oppo/util/OppoListBackToTopUtils;)Landroid/widget/AbsListView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 37
    iget-object v0, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mList:Landroid/widget/AbsListView;

    return-object v0
.end method

.method private initBackToTopWindow()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x2

    .line 66
    iget-object v2, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mList:Landroid/widget/AbsListView;

    invoke-virtual {v2}, Landroid/widget/AbsListView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 68
    .local v0, inflate:Landroid/view/LayoutInflater;
    const v2, 0xc09044d

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 71
    .local v1, mPopupContent:Landroid/view/ViewGroup;
    const v2, 0xc0204a5

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mBackToTop:Landroid/widget/Button;

    .line 73
    iget-object v2, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mBackToTop:Landroid/widget/Button;

    new-instance v3, Lcom/oppo/util/OppoListBackToTopUtils$1;

    invoke-direct {v3, p0}, Lcom/oppo/util/OppoListBackToTopUtils$1;-><init>(Lcom/oppo/util/OppoListBackToTopUtils;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    new-instance v2, Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mList:Landroid/widget/AbsListView;

    invoke-virtual {v3}, Landroid/widget/AbsListView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mPopupWindow:Landroid/widget/PopupWindow;

    .line 83
    iget-object v2, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v2, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 84
    iget-object v2, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mPopupWindow:Landroid/widget/PopupWindow;

    const v3, 0xc030404

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 86
    iget-object v2, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v2, v5}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 87
    iget-object v2, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v2, v4, v4}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    .line 89
    iget-object v2, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mPopupWindow:Landroid/widget/PopupWindow;

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v3, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 91
    return-void
.end method


# virtual methods
.method public showBackToTop(Z)V
    .locals 5
    .parameter "show"

    .prologue
    .line 102
    if-eqz p1, :cond_2

    .line 103
    iget-object v0, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mPopupWindow:Landroid/widget/PopupWindow;

    if-nez v0, :cond_0

    .line 104
    invoke-direct {p0}, Lcom/oppo/util/OppoListBackToTopUtils;->initBackToTopWindow()V

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 107
    iget-object v0, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mList:Landroid/widget/AbsListView;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mList:Landroid/widget/AbsListView;

    invoke-virtual {v3}, Landroid/widget/AbsListView;->getRight()I

    move-result v3

    add-int/lit16 v3, v3, -0x96

    iget-object v4, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mList:Landroid/widget/AbsListView;

    invoke-virtual {v4}, Landroid/widget/AbsListView;->getBottom()I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 114
    :cond_1
    :goto_0
    return-void

    .line 111
    :cond_2
    iget-object v0, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/oppo/util/OppoListBackToTopUtils;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    goto :goto_0
.end method
