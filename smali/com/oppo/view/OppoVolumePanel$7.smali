.class Lcom/oppo/view/OppoVolumePanel$7;
.super Landroid/content/BroadcastReceiver;
.source "OppoVolumePanel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/oppo/view/OppoVolumePanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/oppo/view/OppoVolumePanel;


# direct methods
.method constructor <init>(Lcom/oppo/view/OppoVolumePanel;)V
    .locals 0
    .parameter

    .prologue
    .line 1125
    iput-object p1, p0, Lcom/oppo/view/OppoVolumePanel$7;->this$0:Lcom/oppo/view/OppoVolumePanel;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .parameter "context"
    .parameter "intent"

    .prologue
    .line 1128
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1129
    .local v0, action:Ljava/lang/String;
    invoke-static {}, Lcom/oppo/view/OppoVolumePanel;->access$800()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1130
    const-string v1, "OppoVolumePanel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The action is:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " !"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1132
    :cond_0
    const-string v1, "android.intent.action.SKIN_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1133
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel$7;->this$0:Lcom/oppo/view/OppoVolumePanel;

    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel$7;->this$0:Lcom/oppo/view/OppoVolumePanel;

    iget-object v2, v2, Lcom/oppo/view/OppoVolumePanel;->mContext:Landroid/content/Context;

    #calls: Lcom/oppo/view/OppoVolumePanel;->inflaterBaseUI(Landroid/content/Context;)V
    invoke-static {v1, v2}, Lcom/oppo/view/OppoVolumePanel;->access$900(Lcom/oppo/view/OppoVolumePanel;Landroid/content/Context;)V

    .line 1134
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel$7;->this$0:Lcom/oppo/view/OppoVolumePanel;

    #calls: Lcom/oppo/view/OppoVolumePanel;->createSliders()V
    invoke-static {v1}, Lcom/oppo/view/OppoVolumePanel;->access$1000(Lcom/oppo/view/OppoVolumePanel;)V

    .line 1139
    :cond_1
    :goto_0
    return-void

    .line 1135
    :cond_2
    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1136
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel$7;->this$0:Lcom/oppo/view/OppoVolumePanel;

    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel$7;->this$0:Lcom/oppo/view/OppoVolumePanel;

    iget-object v2, v2, Lcom/oppo/view/OppoVolumePanel;->mContext:Landroid/content/Context;

    #calls: Lcom/oppo/view/OppoVolumePanel;->inflaterBaseUI(Landroid/content/Context;)V
    invoke-static {v1, v2}, Lcom/oppo/view/OppoVolumePanel;->access$900(Lcom/oppo/view/OppoVolumePanel;Landroid/content/Context;)V

    .line 1137
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel$7;->this$0:Lcom/oppo/view/OppoVolumePanel;

    #calls: Lcom/oppo/view/OppoVolumePanel;->createSliders()V
    invoke-static {v1}, Lcom/oppo/view/OppoVolumePanel;->access$1000(Lcom/oppo/view/OppoVolumePanel;)V

    goto :goto_0
.end method
