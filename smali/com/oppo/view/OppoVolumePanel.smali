.class public Lcom/oppo/view/OppoVolumePanel;
.super Landroid/os/Handler;
.source "OppoVolumePanel.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/OppoSeekBar$OnOppoSeekBarFromUserChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/oppo/view/OppoVolumePanel$WarningDialogReceiver;,
        Lcom/oppo/view/OppoVolumePanel$StreamControl;,
        Lcom/oppo/view/OppoVolumePanel$StreamResources;
    }
.end annotation


# static fields
.field private static final ACTION_MEDIA_VOLUME_MODE_CHANGED:Ljava/lang/String; = "action_media_volume_mode_changed"

.field private static final ACTION_SKIN_CHANGED:Ljava/lang/String; = "android.intent.action.SKIN_CHANGED"

.field private static final ACTION_SYSTEM_VOLUME_MODE_CHANGED:Ljava/lang/String; = "action_system_volume_mode_changed"

.field private static final BEEP_DURATION:I = 0x96

.field private static final FREE_DELAY:I = 0x2710

.field private static LOGD:Z = false

.field private static final MAX_VOLUME:I = 0x64

.field private static final MSG_DISPLAY_SAFE_VOLUME_WARNING:I = 0xb

.field private static final MSG_FREE_RESOURCES:I = 0x1

.field private static final MSG_MUTE_CHANGED:I = 0x7

.field private static final MSG_PLAY_SOUND:I = 0x2

.field private static final MSG_REMOTE_VOLUME_CHANGED:I = 0x8

.field private static final MSG_REMOTE_VOLUME_UPDATE_IF_SHOWN:I = 0x9

.field private static final MSG_RINGER_MODE_CHANGED:I = 0x6

.field private static final MSG_SLIDER_VISIBILITY_CHANGED:I = 0xa

.field private static final MSG_STOP_SOUNDS:I = 0x3

.field private static final MSG_TIMEOUT:I = 0x5

.field private static final MSG_VIBRATE:I = 0x4

.field private static final MSG_VOLUME_CHANGED:I = 0x0

.field public static final PLAY_SOUND_DELAY:I = 0x12c

.field private static final STREAMS:[Lcom/oppo/view/OppoVolumePanel$StreamResources; = null

.field private static final STREAM_MASTER:I = -0x64

.field private static final TAG:Ljava/lang/String; = "OppoVolumePanel"

.field private static final TIMEOUT_DELAY:I = 0xbb8

.field public static final VIBRATE_DELAY:I = 0x12c

.field private static final VIBRATE_DURATION:I = 0x12c

.field private static final WAIT_TIME_RELEASE_LOCK:I = 0x64

.field private static sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

.field private static sConfirmSafeVolumeLock:Ljava/lang/Object;


# instance fields
.field private mActiveStreamType:I

.field private mAudioManager:Landroid/media/AudioManager;

.field protected mAudioService:Landroid/media/AudioService;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field protected mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/Dialog;

.field private mPanel:Landroid/view/ViewGroup;

.field private final mPlayMasterStreamTones:Z

.field private mPowerManager:Landroid/os/PowerManager;

.field private mRingIsSilent:Z

.field private mSafeVolumeWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mShowCombinedVolumes:Z

.field private mSliderGroup:Landroid/view/ViewGroup;

.field private mStreamControls:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/oppo/view/OppoVolumePanel$StreamControl;",
            ">;"
        }
    .end annotation
.end field

.field private mToneGenerators:[Landroid/media/ToneGenerator;

.field private mVibrator:Landroid/os/Vibrator;

.field private mView:Landroid/view/View;

.field private titleStreamControl:Lcom/oppo/view/OppoVolumePanel$StreamControl;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 84
    sput-boolean v2, Lcom/oppo/view/OppoVolumePanel;->LOGD:Z

    .line 228
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/oppo/view/OppoVolumePanel$StreamResources;

    sget-object v1, Lcom/oppo/view/OppoVolumePanel$StreamResources;->BluetoothSCOStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aput-object v1, v0, v2

    const/4 v1, 0x1

    sget-object v2, Lcom/oppo/view/OppoVolumePanel$StreamResources;->RingerStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/oppo/view/OppoVolumePanel$StreamResources;->VoiceStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/oppo/view/OppoVolumePanel$StreamResources;->MediaStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/oppo/view/OppoVolumePanel$StreamResources;->NotificationStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/oppo/view/OppoVolumePanel$StreamResources;->AlarmStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/oppo/view/OppoVolumePanel$StreamResources;->MasterStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/oppo/view/OppoVolumePanel$StreamResources;->RemoteStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/oppo/view/OppoVolumePanel$StreamResources;->SystemStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aput-object v2, v0, v1

    sput-object v0, Lcom/oppo/view/OppoVolumePanel;->STREAMS:[Lcom/oppo/view/OppoVolumePanel$StreamResources;

    .line 270
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/oppo/view/OppoVolumePanel;->sConfirmSafeVolumeLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/media/AudioService;)V
    .locals 11
    .parameter "context"
    .parameter "volumeService"

    .prologue
    .line 326
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 146
    const/4 v8, -0x1

    iput v8, p0, Lcom/oppo/view/OppoVolumePanel;->mActiveStreamType:I

    .line 1125
    new-instance v8, Lcom/oppo/view/OppoVolumePanel$7;

    invoke-direct {v8, p0}, Lcom/oppo/view/OppoVolumePanel$7;-><init>(Lcom/oppo/view/OppoVolumePanel;)V

    iput-object v8, p0, Lcom/oppo/view/OppoVolumePanel;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 327
    iput-object p1, p0, Lcom/oppo/view/OppoVolumePanel;->mContext:Landroid/content/Context;

    .line 328
    const-string v8, "audio"

    invoke-virtual {p1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/media/AudioManager;

    iput-object v8, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioManager:Landroid/media/AudioManager;

    .line 329
    iput-object p2, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioService:Landroid/media/AudioService;

    .line 332
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0xc0c0406

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v6

    .line 334
    .local v6, useMasterVolume:Z
    if-eqz v6, :cond_1

    .line 335
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    sget-object v8, Lcom/oppo/view/OppoVolumePanel;->STREAMS:[Lcom/oppo/view/OppoVolumePanel$StreamResources;

    array-length v8, v8

    if-ge v1, v8, :cond_1

    .line 336
    sget-object v8, Lcom/oppo/view/OppoVolumePanel;->STREAMS:[Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aget-object v5, v8, v1

    .line 337
    .local v5, streamRes:Lcom/oppo/view/OppoVolumePanel$StreamResources;
    iget v8, v5, Lcom/oppo/view/OppoVolumePanel$StreamResources;->streamType:I

    const/16 v9, -0x64

    if-ne v8, v9, :cond_0

    const/4 v8, 0x1

    :goto_1
    iput-boolean v8, v5, Lcom/oppo/view/OppoVolumePanel$StreamResources;->show:Z

    .line 335
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 337
    :cond_0
    const/4 v8, 0x0

    goto :goto_1

    .line 341
    .end local v1           #i:I
    .end local v5           #streamRes:Lcom/oppo/view/OppoVolumePanel$StreamResources;
    :cond_1
    invoke-direct {p0, p1}, Lcom/oppo/view/OppoVolumePanel;->inflaterBaseUI(Landroid/content/Context;)V

    .line 343
    new-instance v8, Lcom/oppo/view/OppoVolumePanel$1;

    const v9, 0xc030407

    invoke-direct {v8, p0, p1, v9}, Lcom/oppo/view/OppoVolumePanel$1;-><init>(Lcom/oppo/view/OppoVolumePanel;Landroid/content/Context;I)V

    iput-object v8, p0, Lcom/oppo/view/OppoVolumePanel;->mDialog:Landroid/app/Dialog;

    .line 354
    iget-object v8, p0, Lcom/oppo/view/OppoVolumePanel;->mDialog:Landroid/app/Dialog;

    const-string v9, "Volume control"

    invoke-virtual {v8, v9}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 355
    iget-object v8, p0, Lcom/oppo/view/OppoVolumePanel;->mDialog:Landroid/app/Dialog;

    iget-object v9, p0, Lcom/oppo/view/OppoVolumePanel;->mView:Landroid/view/View;

    invoke-virtual {v8, v9}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 356
    iget-object v8, p0, Lcom/oppo/view/OppoVolumePanel;->mDialog:Landroid/app/Dialog;

    new-instance v9, Lcom/oppo/view/OppoVolumePanel$2;

    invoke-direct {v9, p0}, Lcom/oppo/view/OppoVolumePanel$2;-><init>(Lcom/oppo/view/OppoVolumePanel;)V

    invoke-virtual {v8, v9}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 363
    iget-object v8, p0, Lcom/oppo/view/OppoVolumePanel;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v8}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v7

    .line 364
    .local v7, window:Landroid/view/Window;
    const/16 v8, 0x30

    invoke-virtual {v7, v8}, Landroid/view/Window;->setGravity(I)V

    .line 365
    invoke-virtual {v7}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 366
    .local v2, lp:Landroid/view/WindowManager$LayoutParams;
    const/4 v8, 0x0

    iput-object v8, v2, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 368
    iget-object v8, p0, Lcom/oppo/view/OppoVolumePanel;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0xc050432

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v8

    iput v8, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 370
    const/16 v8, 0x7e4

    iput v8, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 371
    const/4 v8, -0x2

    iput v8, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 372
    const/4 v8, -0x2

    iput v8, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 373
    invoke-virtual {v7, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 374
    const v8, 0x40028

    invoke-virtual {v7, v8}, Landroid/view/Window;->addFlags(I)V

    .line 377
    invoke-static {}, Landroid/media/AudioSystem;->getNumStreamTypes()I

    move-result v8

    new-array v8, v8, [Landroid/media/ToneGenerator;

    iput-object v8, p0, Lcom/oppo/view/OppoVolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    .line 378
    const-string v8, "vibrator"

    invoke-virtual {p1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/Vibrator;

    iput-object v8, p0, Lcom/oppo/view/OppoVolumePanel;->mVibrator:Landroid/os/Vibrator;

    .line 382
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/oppo/view/OppoVolumePanel;->mShowCombinedVolumes:Z

    .line 384
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0xc0c0406

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    .line 386
    .local v4, masterVolumeOnly:Z
    iget-object v8, p0, Lcom/oppo/view/OppoVolumePanel;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0xc0c0407

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    .line 388
    .local v3, masterVolumeKeySounds:Z
    if-eqz v4, :cond_2

    if-eqz v3, :cond_2

    const/4 v8, 0x1

    :goto_2
    iput-boolean v8, p0, Lcom/oppo/view/OppoVolumePanel;->mPlayMasterStreamTones:Z

    .line 390
    invoke-direct {p0}, Lcom/oppo/view/OppoVolumePanel;->listenToRingerMode()V

    .line 391
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 392
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v8, "android.intent.action.SKIN_CHANGED"

    invoke-virtual {v0, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 393
    const-string v8, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v0, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 394
    iget-object v8, p0, Lcom/oppo/view/OppoVolumePanel;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/oppo/view/OppoVolumePanel;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v8, v9, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 398
    const-string v8, "power"

    invoke-virtual {p1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/PowerManager;

    iput-object v8, p0, Lcom/oppo/view/OppoVolumePanel;->mPowerManager:Landroid/os/PowerManager;

    .line 399
    iget-object v8, p0, Lcom/oppo/view/OppoVolumePanel;->mPowerManager:Landroid/os/PowerManager;

    const v9, 0x1000000a

    const-string v10, "OppoVolumePanel.mSafeVolumeWakeLock"

    invoke-virtual {v8, v9, v10}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v8

    iput-object v8, p0, Lcom/oppo/view/OppoVolumePanel;->mSafeVolumeWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 403
    return-void

    .line 388
    .end local v0           #filter:Landroid/content/IntentFilter;
    :cond_2
    const/4 v8, 0x0

    goto :goto_2
.end method

.method static synthetic access$000()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lcom/oppo/view/OppoVolumePanel;->sConfirmSafeVolumeLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/oppo/view/OppoVolumePanel;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/oppo/view/OppoVolumePanel;->createSliders()V

    return-void
.end method

.method static synthetic access$102(Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .parameter "x0"

    .prologue
    .line 80
    sput-object p0, Lcom/oppo/view/OppoVolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    return-object p0
.end method

.method static synthetic access$200(Lcom/oppo/view/OppoVolumePanel;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/oppo/view/OppoVolumePanel;->forceTimeout()V

    return-void
.end method

.method static synthetic access$300(Lcom/oppo/view/OppoVolumePanel;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 80
    iget v0, p0, Lcom/oppo/view/OppoVolumePanel;->mActiveStreamType:I

    return v0
.end method

.method static synthetic access$302(Lcom/oppo/view/OppoVolumePanel;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 80
    iput p1, p0, Lcom/oppo/view/OppoVolumePanel;->mActiveStreamType:I

    return p1
.end method

.method static synthetic access$400(Lcom/oppo/view/OppoVolumePanel;)Landroid/media/AudioManager;
    .locals 1
    .parameter "x0"

    .prologue
    .line 80
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/oppo/view/OppoVolumePanel;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/oppo/view/OppoVolumePanel;->resetTimeout()V

    return-void
.end method

.method static synthetic access$700(Lcom/oppo/view/OppoVolumePanel;)Ljava/util/HashMap;
    .locals 1
    .parameter "x0"

    .prologue
    .line 80
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mStreamControls:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$800()Z
    .locals 1

    .prologue
    .line 80
    sget-boolean v0, Lcom/oppo/view/OppoVolumePanel;->LOGD:Z

    return v0
.end method

.method static synthetic access$900(Lcom/oppo/view/OppoVolumePanel;Landroid/content/Context;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/oppo/view/OppoVolumePanel;->inflaterBaseUI(Landroid/content/Context;)V

    return-void
.end method

.method private addOtherVolumes()V
    .locals 5

    .prologue
    .line 562
    iget-boolean v3, p0, Lcom/oppo/view/OppoVolumePanel;->mShowCombinedVolumes:Z

    if-nez v3, :cond_1

    .line 575
    :cond_0
    return-void

    .line 565
    :cond_1
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    sget-object v3, Lcom/oppo/view/OppoVolumePanel;->STREAMS:[Lcom/oppo/view/OppoVolumePanel$StreamResources;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 567
    sget-object v3, Lcom/oppo/view/OppoVolumePanel;->STREAMS:[Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aget-object v3, v3, v0

    iget v2, v3, Lcom/oppo/view/OppoVolumePanel$StreamResources;->streamType:I

    .line 568
    .local v2, streamType:I
    sget-object v3, Lcom/oppo/view/OppoVolumePanel;->STREAMS:[Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aget-object v3, v3, v0

    iget-boolean v3, v3, Lcom/oppo/view/OppoVolumePanel$StreamResources;->show:Z

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/oppo/view/OppoVolumePanel;->mActiveStreamType:I

    if-ne v2, v3, :cond_3

    .line 565
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 571
    :cond_3
    iget-object v3, p0, Lcom/oppo/view/OppoVolumePanel;->mStreamControls:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/oppo/view/OppoVolumePanel$StreamControl;

    .line 572
    .local v1, sc:Lcom/oppo/view/OppoVolumePanel$StreamControl;
    iget-object v3, p0, Lcom/oppo/view/OppoVolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    iget-object v4, v1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 573
    invoke-direct {p0, v1}, Lcom/oppo/view/OppoVolumePanel;->updateSlider(Lcom/oppo/view/OppoVolumePanel$StreamControl;)V

    goto :goto_1
.end method

.method private collapse()V
    .locals 4

    .prologue
    .line 623
    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 624
    .local v0, count:I
    const/4 v1, 0x1

    .local v1, i:I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 625
    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 624
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 627
    :cond_0
    return-void
.end method

.method private createSliders()V
    .locals 11

    .prologue
    const/4 v8, 0x0

    const/4 v10, 0x0

    .line 484
    iget-object v7, p0, Lcom/oppo/view/OppoVolumePanel;->mStreamControls:Ljava/util/HashMap;

    if-eqz v7, :cond_0

    .line 485
    iget-object v7, p0, Lcom/oppo/view/OppoVolumePanel;->mStreamControls:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->clear()V

    .line 486
    iput-object v10, p0, Lcom/oppo/view/OppoVolumePanel;->mStreamControls:Ljava/util/HashMap;

    .line 488
    :cond_0
    iget-object v7, p0, Lcom/oppo/view/OppoVolumePanel;->mContext:Landroid/content/Context;

    const-string v9, "layout_inflater"

    invoke-virtual {v7, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 490
    .local v1, inflater:Landroid/view/LayoutInflater;
    new-instance v7, Ljava/util/HashMap;

    sget-object v9, Lcom/oppo/view/OppoVolumePanel;->STREAMS:[Lcom/oppo/view/OppoVolumePanel$StreamResources;

    array-length v9, v9

    invoke-direct {v7, v9}, Ljava/util/HashMap;-><init>(I)V

    iput-object v7, p0, Lcom/oppo/view/OppoVolumePanel;->mStreamControls:Ljava/util/HashMap;

    .line 491
    iget-object v7, p0, Lcom/oppo/view/OppoVolumePanel;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 492
    .local v3, res:Landroid/content/res/Resources;
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    sget-object v7, Lcom/oppo/view/OppoVolumePanel;->STREAMS:[Lcom/oppo/view/OppoVolumePanel$StreamResources;

    array-length v7, v7

    if-ge v0, v7, :cond_4

    .line 493
    sget-object v7, Lcom/oppo/view/OppoVolumePanel;->STREAMS:[Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aget-object v5, v7, v0

    .line 494
    .local v5, streamRes:Lcom/oppo/view/OppoVolumePanel$StreamResources;
    iget v6, v5, Lcom/oppo/view/OppoVolumePanel$StreamResources;->streamType:I

    .line 500
    .local v6, streamType:I
    new-instance v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;

    invoke-direct {v4, p0, v10}, Lcom/oppo/view/OppoVolumePanel$StreamControl;-><init>(Lcom/oppo/view/OppoVolumePanel;Lcom/oppo/view/OppoVolumePanel$1;)V

    .line 501
    .local v4, sc:Lcom/oppo/view/OppoVolumePanel$StreamControl;
    iput v6, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamType:I

    .line 502
    const v7, 0xc090434

    invoke-virtual {v1, v7, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    iput-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    .line 503
    iget-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    invoke-virtual {v7, v4}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    .line 504
    iget-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    const v9, 0xc02045c

    invoke-virtual {v7, v9}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamTitle:Landroid/widget/TextView;

    .line 505
    iget-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamTitle:Landroid/widget/TextView;

    iget v9, v5, Lcom/oppo/view/OppoVolumePanel$StreamResources;->descRes:I

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(I)V

    .line 506
    iget-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    const v9, 0xc02045d

    invoke-virtual {v7, v9}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    .line 507
    iget-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 508
    iget-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    iget v9, v5, Lcom/oppo/view/OppoVolumePanel$StreamResources;->descRes:I

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 509
    iget-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    invoke-virtual {v7, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 510
    iget v7, v5, Lcom/oppo/view/OppoVolumePanel$StreamResources;->iconRes:I

    iput v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->iconRes:I

    .line 511
    iget v7, v5, Lcom/oppo/view/OppoVolumePanel$StreamResources;->iconMuteRes:I

    iput v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->iconMuteRes:I

    .line 512
    iget-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    iget v9, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->iconRes:I

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 517
    iget-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    const v9, 0xc02045e

    invoke-virtual {v7, v9}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/OppoSeekBar;

    iput-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    .line 518
    const/4 v7, 0x3

    if-ne v7, v6, :cond_1

    .line 519
    iget-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-virtual {v7, v8}, Landroid/widget/OppoSeekBar;->setSafeMediaVolumeEnabled(Z)V

    .line 520
    iget-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-virtual {v7, p0}, Landroid/widget/OppoSeekBar;->setOppoSeekBarFromUserChangeListener(Landroid/widget/OppoSeekBar$OnOppoSeekBarFromUserChangeListener;)V

    .line 533
    :cond_1
    const/4 v7, 0x6

    if-eq v6, v7, :cond_2

    if-nez v6, :cond_3

    :cond_2
    const/4 v2, 0x1

    .line 535
    .local v2, plusOne:I
    :goto_1
    iget-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-direct {p0, v6}, Lcom/oppo/view/OppoVolumePanel;->getStreamMaxVolume(I)I

    move-result v9

    add-int/2addr v9, v2

    invoke-virtual {v7, v9}, Landroid/widget/OppoSeekBar;->setMax(I)V

    .line 536
    iget-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-virtual {v7, p0}, Landroid/widget/OppoSeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 537
    iget-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-virtual {v7, v4}, Landroid/widget/OppoSeekBar;->setTag(Ljava/lang/Object;)V

    .line 538
    iget-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    const v9, 0xc020423

    invoke-virtual {v7, v9}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->divider:Landroid/widget/ImageView;

    .line 539
    iget-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    const v9, 0xc020424

    invoke-virtual {v7, v9}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->mMoreButton:Landroid/widget/ImageView;

    .line 540
    iget-object v7, v4, Lcom/oppo/view/OppoVolumePanel$StreamControl;->mMoreButton:Landroid/widget/ImageView;

    invoke-virtual {v7, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 541
    iget-object v7, p0, Lcom/oppo/view/OppoVolumePanel;->mStreamControls:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v9, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 492
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .end local v2           #plusOne:I
    :cond_3
    move v2, v8

    .line 533
    goto :goto_1

    .line 543
    .end local v4           #sc:Lcom/oppo/view/OppoVolumePanel$StreamControl;
    .end local v5           #streamRes:Lcom/oppo/view/OppoVolumePanel$StreamResources;
    .end local v6           #streamType:I
    :cond_4
    return-void
.end method

.method private expand()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 611
    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 612
    .local v0, count:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 613
    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 614
    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/oppo/view/OppoVolumePanel$StreamControl;

    iget-object v2, v2, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 615
    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/oppo/view/OppoVolumePanel$StreamControl;

    iget-object v2, v2, Lcom/oppo/view/OppoVolumePanel$StreamControl;->mMoreButton:Landroid/widget/ImageView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 616
    add-int/lit8 v2, v0, -0x1

    if-eq v1, v2, :cond_0

    .line 617
    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/oppo/view/OppoVolumePanel$StreamControl;

    iget-object v2, v2, Lcom/oppo/view/OppoVolumePanel$StreamControl;->divider:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 612
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 620
    :cond_1
    return-void
.end method

.method private forceTimeout()V
    .locals 1

    .prologue
    const/4 v0, 0x5

    .line 1216
    invoke-virtual {p0, v0}, Lcom/oppo/view/OppoVolumePanel;->removeMessages(I)V

    .line 1217
    invoke-virtual {p0, v0}, Lcom/oppo/view/OppoVolumePanel;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/oppo/view/OppoVolumePanel;->sendMessage(Landroid/os/Message;)Z

    .line 1218
    return-void
.end method

.method private getOrCreateToneGenerator(I)Landroid/media/ToneGenerator;
    .locals 4
    .parameter "streamType"

    .prologue
    .line 1075
    const/16 v1, -0x64

    if-ne p1, v1, :cond_0

    .line 1079
    iget-boolean v1, p0, Lcom/oppo/view/OppoVolumePanel;->mPlayMasterStreamTones:Z

    if-eqz v1, :cond_2

    .line 1080
    const/4 p1, 0x1

    .line 1085
    :cond_0
    monitor-enter p0

    .line 1086
    :try_start_0
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    aget-object v1, v1, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 1088
    :try_start_1
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    new-instance v2, Landroid/media/ToneGenerator;

    const/16 v3, 0x64

    invoke-direct {v2, p1, v3}, Landroid/media/ToneGenerator;-><init>(II)V

    aput-object v2, v1, p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1096
    :cond_1
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    aget-object v1, v1, p1

    monitor-exit p0

    :goto_1
    return-object v1

    .line 1082
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 1089
    :catch_0
    move-exception v0

    .line 1090
    .local v0, e:Ljava/lang/RuntimeException;
    sget-boolean v1, Lcom/oppo/view/OppoVolumePanel;->LOGD:Z

    if-eqz v1, :cond_1

    .line 1091
    const-string v1, "OppoVolumePanel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ToneGenerator constructor failed with RuntimeException: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1097
    .end local v0           #e:Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method private getStreamMaxVolume(I)I
    .locals 1
    .parameter "streamType"

    .prologue
    .line 454
    const/16 v0, -0x64

    if-ne p1, v0, :cond_0

    .line 455
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getMasterMaxVolume()I

    move-result v0

    .line 459
    :goto_0
    return v0

    .line 456
    :cond_0
    const/16 v0, -0xc8

    if-ne p1, v0, :cond_1

    .line 457
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioService:Landroid/media/AudioService;

    invoke-virtual {v0}, Landroid/media/AudioService;->getRemoteStreamMaxVolume()I

    move-result v0

    goto :goto_0

    .line 459
    :cond_1
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    goto :goto_0
.end method

.method private getStreamVolume(I)I
    .locals 1
    .parameter "streamType"

    .prologue
    .line 464
    const/16 v0, -0x64

    if-ne p1, v0, :cond_0

    .line 465
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getMasterVolume()I

    move-result v0

    .line 469
    :goto_0
    return v0

    .line 466
    :cond_0
    const/16 v0, -0xc8

    if-ne p1, v0, :cond_1

    .line 467
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioService:Landroid/media/AudioService;

    invoke-virtual {v0}, Landroid/media/AudioService;->getRemoteStreamVolume()I

    move-result v0

    goto :goto_0

    .line 469
    :cond_1
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    goto :goto_0
.end method

.method private inflaterBaseUI(Landroid/content/Context;)V
    .locals 3
    .parameter "context"

    .prologue
    .line 406
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 408
    .local v0, inflater:Landroid/view/LayoutInflater;
    const v1, 0xc090433

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mView:Landroid/view/View;

    .line 409
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mView:Landroid/view/View;

    new-instance v2, Lcom/oppo/view/OppoVolumePanel$3;

    invoke-direct {v2, p0}, Lcom/oppo/view/OppoVolumePanel$3;-><init>(Lcom/oppo/view/OppoVolumePanel;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 416
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mView:Landroid/view/View;

    const v2, 0xc02045f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mPanel:Landroid/view/ViewGroup;

    .line 417
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mView:Landroid/view/View;

    const v2, 0xc020425

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    .line 418
    return-void
.end method

.method private isMuted(I)Z
    .locals 2
    .parameter "streamType"

    .prologue
    const/4 v0, 0x0

    .line 442
    const/16 v1, -0x64

    if-ne p1, v1, :cond_1

    .line 443
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMasterMute()Z

    move-result v0

    .line 449
    :cond_0
    :goto_0
    return v0

    .line 444
    :cond_1
    const/16 v1, -0xc8

    if-ne p1, v1, :cond_2

    .line 445
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioService:Landroid/media/AudioService;

    invoke-virtual {v1}, Landroid/media/AudioService;->getRemoteStreamVolume()I

    move-result v1

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 446
    :cond_2
    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    .line 449
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->isStreamMute(I)Z

    move-result v0

    goto :goto_0
.end method

.method private listenToRingerMode()V
    .locals 3

    .prologue
    .line 426
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 427
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 428
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/oppo/view/OppoVolumePanel$4;

    invoke-direct {v2, p0}, Lcom/oppo/view/OppoVolumePanel$4;-><init>(Lcom/oppo/view/OppoVolumePanel;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 439
    return-void
.end method

.method private reorderSliders(I)V
    .locals 3
    .parameter "activeStreamType"

    .prologue
    .line 546
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 548
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mStreamControls:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;

    .line 549
    .local v0, active:Lcom/oppo/view/OppoVolumePanel$StreamControl;
    if-nez v0, :cond_0

    .line 550
    const/4 v1, -0x1

    iput v1, p0, Lcom/oppo/view/OppoVolumePanel;->mActiveStreamType:I

    .line 558
    :goto_0
    invoke-direct {p0}, Lcom/oppo/view/OppoVolumePanel;->addOtherVolumes()V

    .line 559
    return-void

    .line 552
    :cond_0
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    iget-object v2, v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 553
    iput p1, p0, Lcom/oppo/view/OppoVolumePanel;->mActiveStreamType:I

    .line 554
    iget-object v1, v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 555
    invoke-direct {p0, v0}, Lcom/oppo/view/OppoVolumePanel;->updateSlider(Lcom/oppo/view/OppoVolumePanel$StreamControl;)V

    goto :goto_0
.end method

.method private resetTimeout()V
    .locals 3

    .prologue
    const/4 v0, 0x5

    .line 1211
    invoke-virtual {p0, v0}, Lcom/oppo/view/OppoVolumePanel;->removeMessages(I)V

    .line 1212
    invoke-virtual {p0, v0}, Lcom/oppo/view/OppoVolumePanel;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0xbb8

    invoke-virtual {p0, v0, v1, v2}, Lcom/oppo/view/OppoVolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1213
    return-void
.end method

.method private ringImageClick()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1261
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 1262
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 1268
    :goto_0
    return-void

    .line 1263
    :cond_0
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    if-nez v0, :cond_1

    .line 1264
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setRingerMode(I)V

    goto :goto_0

    .line 1266
    :cond_1
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setRingerMode(I)V

    goto :goto_0
.end method

.method private setMusicIcon(II)V
    .locals 4
    .parameter "resId"
    .parameter "resMuteId"

    .prologue
    const/4 v3, 0x3

    .line 1106
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mStreamControls:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;

    .line 1107
    .local v0, sc:Lcom/oppo/view/OppoVolumePanel$StreamControl;
    if-eqz v0, :cond_0

    .line 1108
    iput p1, v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;->iconRes:I

    .line 1109
    iput p2, v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;->iconMuteRes:I

    .line 1110
    iget-object v2, v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    invoke-direct {p0, v3}, Lcom/oppo/view/OppoVolumePanel;->getStreamVolume(I)I

    move-result v1

    if-nez v1, :cond_1

    iget v1, v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;->iconMuteRes:I

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1112
    :cond_0
    return-void

    .line 1110
    :cond_1
    iget v1, v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;->iconRes:I

    goto :goto_0
.end method

.method private setStreamVolume(III)V
    .locals 1
    .parameter "streamType"
    .parameter "index"
    .parameter "flags"

    .prologue
    .line 474
    const/16 v0, -0x64

    if-ne p1, v0, :cond_0

    .line 475
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p2, p3}, Landroid/media/AudioManager;->setMasterVolume(II)V

    .line 481
    :goto_0
    return-void

    .line 476
    :cond_0
    const/16 v0, -0xc8

    if-ne p1, v0, :cond_1

    .line 477
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioService:Landroid/media/AudioService;

    invoke-virtual {v0, p2}, Landroid/media/AudioService;->setRemoteStreamVolume(I)V

    goto :goto_0

    .line 479
    :cond_1
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1, p2, p3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_0
.end method

.method private updateSlider(Lcom/oppo/view/OppoVolumePanel$StreamControl;)V
    .locals 5
    .parameter "sc"

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 579
    iget v1, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamType:I

    invoke-direct {p0, v1}, Lcom/oppo/view/OppoVolumePanel;->isMuted(I)Z

    move-result v0

    .line 580
    .local v0, muted:Z
    iget-object v1, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    iget v2, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamType:I

    invoke-direct {p0, v2}, Lcom/oppo/view/OppoVolumePanel;->getStreamVolume(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/OppoSeekBar;->setProgress(I)V

    .line 581
    iget v1, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamType:I

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 590
    iget-object v1, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    const v2, 0xc080499

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 598
    :goto_0
    iget v1, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamType:I

    const/16 v2, -0xc8

    if-ne v1, v2, :cond_3

    .line 601
    iget-object v1, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-virtual {v1, v3}, Landroid/widget/OppoSeekBar;->setEnabled(Z)V

    .line 607
    :goto_1
    return-void

    .line 593
    :cond_0
    iget v1, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamType:I

    if-ne v1, v4, :cond_2

    .line 594
    iget-object v2, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    invoke-direct {p0, v4}, Lcom/oppo/view/OppoVolumePanel;->getStreamVolume(I)I

    move-result v1

    if-nez v1, :cond_1

    iget v1, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->iconMuteRes:I

    :goto_2
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_1
    iget v1, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->iconRes:I

    goto :goto_2

    .line 596
    :cond_2
    invoke-virtual {p0, p1}, Lcom/oppo/view/OppoVolumePanel;->changeStreamIcon(Lcom/oppo/view/OppoVolumePanel$StreamControl;)V

    goto :goto_0

    .line 602
    :cond_3
    iget v1, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamType:I

    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2}, Landroid/media/AudioManager;->getMasterStreamType()I

    move-result v2

    if-eq v1, v2, :cond_4

    if-eqz v0, :cond_4

    .line 603
    iget-object v1, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/OppoSeekBar;->setEnabled(Z)V

    goto :goto_1

    .line 605
    :cond_4
    iget-object v1, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-virtual {v1, v3}, Landroid/widget/OppoSeekBar;->setEnabled(Z)V

    goto :goto_1
.end method

.method private updateStates()V
    .locals 4

    .prologue
    .line 630
    iget-object v3, p0, Lcom/oppo/view/OppoVolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 631
    .local v0, count:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 632
    iget-object v3, p0, Lcom/oppo/view/OppoVolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/oppo/view/OppoVolumePanel$StreamControl;

    .line 633
    .local v2, sc:Lcom/oppo/view/OppoVolumePanel$StreamControl;
    invoke-direct {p0, v2}, Lcom/oppo/view/OppoVolumePanel;->updateSlider(Lcom/oppo/view/OppoVolumePanel$StreamControl;)V

    .line 631
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 635
    .end local v2           #sc:Lcom/oppo/view/OppoVolumePanel$StreamControl;
    :cond_0
    return-void
.end method

.method private volumeImageClick(I)V
    .locals 3
    .parameter "streamType"

    .prologue
    .line 1271
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 1272
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "action_media_volume_mode_changed"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1278
    :cond_0
    :goto_0
    return-void

    .line 1274
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1275
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "action_system_volume_mode_changed"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public changeStreamIcon(Lcom/oppo/view/OppoVolumePanel$StreamControl;)V
    .locals 3
    .parameter "sc"

    .prologue
    .line 1283
    if-eqz p1, :cond_1

    .line 1284
    iget v1, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamType:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    iget v1, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamType:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    iget v1, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamType:I

    const/16 v2, 0xa

    if-eq v1, v2, :cond_0

    iget v1, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamType:I

    const/4 v2, 0x6

    if-eq v1, v2, :cond_0

    iget v1, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamType:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    iget v1, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamType:I

    const/16 v2, -0x64

    if-eq v1, v2, :cond_0

    iget v1, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1291
    :cond_0
    iget v1, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamType:I

    invoke-direct {p0, v1}, Lcom/oppo/view/OppoVolumePanel;->getStreamVolume(I)I

    move-result v0

    .line 1292
    .local v0, volume:I
    iget-object v2, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    if-nez v0, :cond_2

    iget v1, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->iconMuteRes:I

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1295
    .end local v0           #volume:I
    :cond_1
    return-void

    .line 1292
    .restart local v0       #volume:I
    :cond_2
    iget v1, p1, Lcom/oppo/view/OppoVolumePanel$StreamControl;->iconRes:I

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "msg"

    .prologue
    .line 1144
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1208
    :cond_0
    :goto_0
    return-void

    .line 1147
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v1, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {p0, v0, v1}, Lcom/oppo/view/OppoVolumePanel;->onVolumeChanged(II)V

    goto :goto_0

    .line 1152
    :pswitch_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v1, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {p0, v0, v1}, Lcom/oppo/view/OppoVolumePanel;->onMuteChanged(II)V

    goto :goto_0

    .line 1157
    :pswitch_2
    invoke-virtual {p0}, Lcom/oppo/view/OppoVolumePanel;->onFreeResources()V

    goto :goto_0

    .line 1162
    :pswitch_3
    invoke-virtual {p0}, Lcom/oppo/view/OppoVolumePanel;->onStopSounds()V

    goto :goto_0

    .line 1167
    :pswitch_4
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v1, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {p0, v0, v1}, Lcom/oppo/view/OppoVolumePanel;->onPlaySound(II)V

    goto :goto_0

    .line 1172
    :pswitch_5
    invoke-virtual {p0}, Lcom/oppo/view/OppoVolumePanel;->onVibrate()V

    goto :goto_0

    .line 1177
    :pswitch_6
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1178
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 1179
    const/4 v0, -0x1

    iput v0, p0, Lcom/oppo/view/OppoVolumePanel;->mActiveStreamType:I

    goto :goto_0

    .line 1184
    :pswitch_7
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1185
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/oppo/view/OppoVolumePanel;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0x12c

    invoke-virtual {p0, v0, v1, v2}, Lcom/oppo/view/OppoVolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1186
    invoke-direct {p0}, Lcom/oppo/view/OppoVolumePanel;->updateStates()V

    goto :goto_0

    .line 1192
    :pswitch_8
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v1, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {p0, v0, v1}, Lcom/oppo/view/OppoVolumePanel;->onRemoteVolumeChanged(II)V

    goto :goto_0

    .line 1197
    :pswitch_9
    invoke-virtual {p0}, Lcom/oppo/view/OppoVolumePanel;->onRemoteVolumeUpdateIfShown()V

    goto :goto_0

    .line 1201
    :pswitch_a
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v1, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {p0, v0, v1}, Lcom/oppo/view/OppoVolumePanel;->onSliderVisibilityChanged(II)V

    goto :goto_0

    .line 1205
    :pswitch_b
    invoke-virtual {p0}, Lcom/oppo/view/OppoVolumePanel;->onDisplaySafeVolumeWarning()V

    goto :goto_0

    .line 1144
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_1
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 1248
    sget-object v2, Lcom/oppo/view/OppoVolumePanel;->STREAMS:[Lcom/oppo/view/OppoVolumePanel$StreamResources;

    array-length v2, v2

    add-int/lit8 v0, v2, -0x1

    .local v0, i:I
    :goto_0
    if-ltz v0, :cond_3

    .line 1249
    sget-object v2, Lcom/oppo/view/OppoVolumePanel;->STREAMS:[Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aget-object v1, v2, v0

    .line 1250
    .local v1, streamRes:Lcom/oppo/view/OppoVolumePanel$StreamResources;
    iget v2, v1, Lcom/oppo/view/OppoVolumePanel$StreamResources;->streamType:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mStreamControls:Ljava/util/HashMap;

    iget v3, v1, Lcom/oppo/view/OppoVolumePanel$StreamResources;->streamType:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/oppo/view/OppoVolumePanel$StreamControl;

    iget-object v2, v2, Lcom/oppo/view/OppoVolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    if-ne p1, v2, :cond_1

    .line 1251
    invoke-direct {p0}, Lcom/oppo/view/OppoVolumePanel;->ringImageClick()V

    .line 1248
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1252
    :cond_1
    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mStreamControls:Ljava/util/HashMap;

    iget v3, v1, Lcom/oppo/view/OppoVolumePanel$StreamResources;->streamType:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/oppo/view/OppoVolumePanel$StreamControl;

    iget-object v2, v2, Lcom/oppo/view/OppoVolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    if-ne p1, v2, :cond_2

    iget v2, v1, Lcom/oppo/view/OppoVolumePanel$StreamResources;->streamType:I

    invoke-direct {p0, v2}, Lcom/oppo/view/OppoVolumePanel;->isMuted(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1253
    iget v2, v1, Lcom/oppo/view/OppoVolumePanel$StreamResources;->streamType:I

    invoke-direct {p0, v2}, Lcom/oppo/view/OppoVolumePanel;->volumeImageClick(I)V

    goto :goto_1

    .line 1254
    :cond_2
    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mStreamControls:Ljava/util/HashMap;

    iget v3, v1, Lcom/oppo/view/OppoVolumePanel$StreamResources;->streamType:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/oppo/view/OppoVolumePanel$StreamControl;

    iget-object v2, v2, Lcom/oppo/view/OppoVolumePanel$StreamControl;->mMoreButton:Landroid/widget/ImageView;

    if-ne p1, v2, :cond_0

    .line 1255
    invoke-direct {p0}, Lcom/oppo/view/OppoVolumePanel;->expand()V

    goto :goto_1

    .line 1258
    .end local v1           #streamRes:Lcom/oppo/view/OppoVolumePanel$StreamResources;
    :cond_3
    invoke-direct {p0}, Lcom/oppo/view/OppoVolumePanel;->resetTimeout()V

    .line 1259
    return-void
.end method

.method protected onDisplaySafeVolumeWarning()V
    .locals 6

    .prologue
    .line 1003
    sget-object v3, Lcom/oppo/view/OppoVolumePanel;->sConfirmSafeVolumeLock:Ljava/lang/Object;

    monitor-enter v3

    .line 1004
    :try_start_0
    sget-object v2, Lcom/oppo/view/OppoVolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    if-eqz v2, :cond_0

    .line 1005
    monitor-exit v3

    .line 1069
    :goto_0
    return-void

    .line 1007
    :cond_0
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/oppo/view/OppoVolumePanel;->mContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0xc0404c7

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v4, 0x1040013

    new-instance v5, Lcom/oppo/view/OppoVolumePanel$6;

    invoke-direct {v5, p0}, Lcom/oppo/view/OppoVolumePanel$6;-><init>(Lcom/oppo/view/OppoVolumePanel;)V

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v4, 0x1040009

    new-instance v5, Lcom/oppo/view/OppoVolumePanel$5;

    invoke-direct {v5, p0}, Lcom/oppo/view/OppoVolumePanel$5;-><init>(Lcom/oppo/view/OppoVolumePanel;)V

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v4, 0x1010355

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    sput-object v2, Lcom/oppo/view/OppoVolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    .line 1047
    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mStreamControls:Ljava/util/HashMap;

    if-eqz v2, :cond_4

    .line 1048
    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mStreamControls:Ljava/util/HashMap;

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;

    .line 1049
    .local v0, sc:Lcom/oppo/view/OppoVolumePanel$StreamControl;
    if-eqz v0, :cond_3

    .line 1050
    new-instance v1, Lcom/oppo/view/OppoVolumePanel$WarningDialogReceiver;

    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/oppo/view/OppoVolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    iget-object v5, v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-direct {v1, v2, v4, v5}, Lcom/oppo/view/OppoVolumePanel$WarningDialogReceiver;-><init>(Landroid/content/Context;Landroid/app/Dialog;Landroid/widget/OppoSeekBar;)V

    .line 1057
    .end local v0           #sc:Lcom/oppo/view/OppoVolumePanel$StreamControl;
    .local v1, warning:Lcom/oppo/view/OppoVolumePanel$WarningDialogReceiver;
    :goto_1
    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mSafeVolumeWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mPowerManager:Landroid/os/PowerManager;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mVibrator:Landroid/os/Vibrator;

    if-eqz v2, :cond_2

    .line 1058
    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mSafeVolumeWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1059
    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mSafeVolumeWakeLock:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v4, 0x64

    invoke-virtual {v2, v4, v5}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 1061
    :cond_1
    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mVibrator:Landroid/os/Vibrator;

    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v4, v5}, Landroid/os/Vibrator;->vibrate(J)V

    .line 1064
    :cond_2
    sget-object v2, Lcom/oppo/view/OppoVolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1065
    sget-object v2, Lcom/oppo/view/OppoVolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v4, 0x7d9

    invoke-virtual {v2, v4}, Landroid/view/Window;->setType(I)V

    .line 1067
    sget-object v2, Lcom/oppo/view/OppoVolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 1068
    monitor-exit v3

    goto/16 :goto_0

    .end local v1           #warning:Lcom/oppo/view/OppoVolumePanel$WarningDialogReceiver;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 1052
    .restart local v0       #sc:Lcom/oppo/view/OppoVolumePanel$StreamControl;
    :cond_3
    :try_start_1
    new-instance v1, Lcom/oppo/view/OppoVolumePanel$WarningDialogReceiver;

    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/oppo/view/OppoVolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    invoke-direct {v1, v2, v4}, Lcom/oppo/view/OppoVolumePanel$WarningDialogReceiver;-><init>(Landroid/content/Context;Landroid/app/Dialog;)V

    .restart local v1       #warning:Lcom/oppo/view/OppoVolumePanel$WarningDialogReceiver;
    goto :goto_1

    .line 1055
    .end local v0           #sc:Lcom/oppo/view/OppoVolumePanel$StreamControl;
    .end local v1           #warning:Lcom/oppo/view/OppoVolumePanel$WarningDialogReceiver;
    :cond_4
    new-instance v1, Lcom/oppo/view/OppoVolumePanel$WarningDialogReceiver;

    iget-object v2, p0, Lcom/oppo/view/OppoVolumePanel;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/oppo/view/OppoVolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    invoke-direct {v1, v2, v4}, Lcom/oppo/view/OppoVolumePanel$WarningDialogReceiver;-><init>(Landroid/content/Context;Landroid/app/Dialog;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .restart local v1       #warning:Lcom/oppo/view/OppoVolumePanel$WarningDialogReceiver;
    goto :goto_1
.end method

.method protected onFreeResources()V
    .locals 3

    .prologue
    .line 1115
    monitor-enter p0

    .line 1116
    :try_start_0
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    .local v0, i:I
    :goto_0
    if-ltz v0, :cond_1

    .line 1117
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 1118
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/media/ToneGenerator;->release()V

    .line 1120
    :cond_0
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 1116
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1122
    :cond_1
    monitor-exit p0

    .line 1123
    return-void

    .line 1122
    .end local v0           #i:I
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected onMuteChanged(II)V
    .locals 4
    .parameter "streamType"
    .parameter "flags"

    .prologue
    .line 755
    sget-boolean v1, Lcom/oppo/view/OppoVolumePanel;->LOGD:Z

    if-eqz v1, :cond_0

    .line 756
    const-string v1, "OppoVolumePanel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onMuteChanged(streamType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", flags: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 758
    :cond_0
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mStreamControls:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;

    .line 759
    .local v0, sc:Lcom/oppo/view/OppoVolumePanel$StreamControl;
    if-eqz v0, :cond_1

    .line 760
    iget-object v2, v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    iget v1, v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamType:I

    invoke-direct {p0, v1}, Lcom/oppo/view/OppoVolumePanel;->isMuted(I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;->iconMuteRes:I

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 763
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/oppo/view/OppoVolumePanel;->onVolumeChanged(II)V

    .line 764
    return-void

    .line 760
    :cond_2
    iget v1, v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;->iconRes:I

    goto :goto_0
.end method

.method public onOppoSeekBarProgressrFromUserChanged(Landroid/widget/SeekBar;IZ)V
    .locals 4
    .parameter "seekBar"
    .parameter "progress"
    .parameter "fromUser"

    .prologue
    .line 1299
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 1300
    .local v1, tag:Ljava/lang/Object;
    if-eqz p3, :cond_0

    instance-of v2, v1, Lcom/oppo/view/OppoVolumePanel$StreamControl;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 1301
    check-cast v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;

    .line 1302
    .local v0, sc:Lcom/oppo/view/OppoVolumePanel$StreamControl;
    iget v2, v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamType:I

    invoke-direct {p0, v2}, Lcom/oppo/view/OppoVolumePanel;->getStreamVolume(I)I

    move-result v2

    if-eq v2, p2, :cond_0

    const/4 v2, 0x3

    iget v3, v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamType:I

    if-ne v2, v3, :cond_0

    .line 1304
    iget v2, v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamType:I

    const/4 v3, 0x0

    invoke-direct {p0, v2, p2, v3}, Lcom/oppo/view/OppoVolumePanel;->setStreamVolume(III)V

    .line 1307
    .end local v0           #sc:Lcom/oppo/view/OppoVolumePanel$StreamControl;
    :cond_0
    invoke-direct {p0}, Lcom/oppo/view/OppoVolumePanel;->resetTimeout()V

    .line 1308
    return-void
.end method

.method protected onPlaySound(II)V
    .locals 4
    .parameter "streamType"
    .parameter "flags"

    .prologue
    const/4 v2, 0x3

    .line 899
    invoke-virtual {p0, v2}, Lcom/oppo/view/OppoVolumePanel;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 900
    invoke-virtual {p0, v2}, Lcom/oppo/view/OppoVolumePanel;->removeMessages(I)V

    .line 902
    invoke-virtual {p0}, Lcom/oppo/view/OppoVolumePanel;->onStopSounds()V

    .line 905
    :cond_0
    monitor-enter p0

    .line 906
    :try_start_0
    invoke-direct {p0, p1}, Lcom/oppo/view/OppoVolumePanel;->getOrCreateToneGenerator(I)Landroid/media/ToneGenerator;

    move-result-object v0

    .line 907
    .local v0, toneGen:Landroid/media/ToneGenerator;
    if-eqz v0, :cond_1

    .line 908
    const/16 v1, 0x21

    const/16 v2, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/media/ToneGenerator;->startTone(II)Z

    .line 909
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/oppo/view/OppoVolumePanel;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x96

    invoke-virtual {p0, v1, v2, v3}, Lcom/oppo/view/OppoVolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 911
    :cond_1
    monitor-exit p0

    .line 912
    return-void

    .line 911
    .end local v0           #toneGen:Landroid/media/ToneGenerator;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 4
    .parameter "seekBar"
    .parameter "progress"
    .parameter "fromUser"

    .prologue
    .line 1221
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 1222
    .local v1, tag:Ljava/lang/Object;
    if-eqz p3, :cond_0

    instance-of v2, v1, Lcom/oppo/view/OppoVolumePanel$StreamControl;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 1223
    check-cast v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;

    .line 1224
    .local v0, sc:Lcom/oppo/view/OppoVolumePanel$StreamControl;
    iget v2, v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamType:I

    invoke-direct {p0, v2}, Lcom/oppo/view/OppoVolumePanel;->getStreamVolume(I)I

    move-result v2

    if-eq v2, p2, :cond_0

    .line 1225
    iget v2, v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamType:I

    const/4 v3, 0x1

    invoke-direct {p0, v2, p2, v3}, Lcom/oppo/view/OppoVolumePanel;->setStreamVolume(III)V

    .line 1228
    .end local v0           #sc:Lcom/oppo/view/OppoVolumePanel$StreamControl;
    :cond_0
    invoke-direct {p0}, Lcom/oppo/view/OppoVolumePanel;->resetTimeout()V

    .line 1229
    return-void
.end method

.method protected onRemoteVolumeChanged(II)V
    .locals 5
    .parameter "streamType"
    .parameter "flags"

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    const/16 v1, -0xc8

    .line 942
    and-int/lit8 v0, p2, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 943
    :cond_0
    monitor-enter p0

    .line 944
    :try_start_0
    iget v0, p0, Lcom/oppo/view/OppoVolumePanel;->mActiveStreamType:I

    if-eq v0, v1, :cond_1

    .line 945
    const/16 v0, -0xc8

    invoke-direct {p0, v0}, Lcom/oppo/view/OppoVolumePanel;->reorderSliders(I)V

    .line 947
    :cond_1
    const/16 v0, -0xc8

    invoke-virtual {p0, v0, p2}, Lcom/oppo/view/OppoVolumePanel;->onShowVolumeChanged(II)V

    .line 948
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 954
    :cond_2
    :goto_0
    and-int/lit8 v0, p2, 0x4

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/oppo/view/OppoVolumePanel;->mRingIsSilent:Z

    if-nez v0, :cond_3

    .line 955
    invoke-virtual {p0, v3}, Lcom/oppo/view/OppoVolumePanel;->removeMessages(I)V

    .line 956
    invoke-virtual {p0, v3, p1, p2}, Lcom/oppo/view/OppoVolumePanel;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0x12c

    invoke-virtual {p0, v0, v1, v2}, Lcom/oppo/view/OppoVolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 959
    :cond_3
    and-int/lit8 v0, p2, 0x8

    if-eqz v0, :cond_4

    .line 960
    invoke-virtual {p0, v3}, Lcom/oppo/view/OppoVolumePanel;->removeMessages(I)V

    .line 961
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/oppo/view/OppoVolumePanel;->removeMessages(I)V

    .line 962
    invoke-virtual {p0}, Lcom/oppo/view/OppoVolumePanel;->onStopSounds()V

    .line 965
    :cond_4
    invoke-virtual {p0, v4}, Lcom/oppo/view/OppoVolumePanel;->removeMessages(I)V

    .line 966
    invoke-virtual {p0, v4}, Lcom/oppo/view/OppoVolumePanel;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0x2710

    invoke-virtual {p0, v0, v1, v2}, Lcom/oppo/view/OppoVolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 967
    invoke-direct {p0}, Lcom/oppo/view/OppoVolumePanel;->resetTimeout()V

    .line 968
    return-void

    .line 948
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 950
    :cond_5
    sget-boolean v0, Lcom/oppo/view/OppoVolumePanel;->LOGD:Z

    if-eqz v0, :cond_2

    .line 951
    const-string v0, "OppoVolumePanel"

    const-string v1, "not calling onShowVolumeChanged(), no FLAG_SHOW_UI or no UI"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onRemoteVolumeUpdateIfShown()V
    .locals 3

    .prologue
    const/16 v2, -0xc8

    .line 971
    sget-boolean v0, Lcom/oppo/view/OppoVolumePanel;->LOGD:Z

    if-eqz v0, :cond_0

    .line 972
    const-string v0, "OppoVolumePanel"

    const-string v1, "onRemoteVolumeUpdateIfShown()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 973
    :cond_0
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/oppo/view/OppoVolumePanel;->mActiveStreamType:I

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mStreamControls:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 975
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/oppo/view/OppoVolumePanel;->onShowVolumeChanged(II)V

    .line 977
    :cond_1
    return-void
.end method

.method protected onShowVolumeChanged(II)V
    .locals 13
    .parameter "streamType"
    .parameter "flags"

    .prologue
    const v12, 0xc080493

    const/4 v11, 0x4

    const/4 v10, 0x0

    const/16 v9, -0xc8

    const/4 v8, 0x1

    .line 767
    invoke-direct {p0, p1}, Lcom/oppo/view/OppoVolumePanel;->getStreamVolume(I)I

    move-result v0

    .line 768
    .local v0, index:I
    iget-object v5, p0, Lcom/oppo/view/OppoVolumePanel;->mStreamControls:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/oppo/view/OppoVolumePanel$StreamControl;

    .line 769
    .local v3, sc:Lcom/oppo/view/OppoVolumePanel$StreamControl;
    iput-boolean v10, p0, Lcom/oppo/view/OppoVolumePanel;->mRingIsSilent:Z

    .line 771
    sget-boolean v5, Lcom/oppo/view/OppoVolumePanel;->LOGD:Z

    if-eqz v5, :cond_0

    .line 772
    const-string v5, "OppoVolumePanel"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onShowVolumeChanged(streamType: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", flags: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "), index: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 778
    :cond_0
    invoke-direct {p0, p1}, Lcom/oppo/view/OppoVolumePanel;->getStreamMaxVolume(I)I

    move-result v1

    .line 780
    .local v1, max:I
    sparse-switch p1, :sswitch_data_0

    .line 851
    :cond_1
    :goto_0
    :sswitch_0
    if-eqz v3, :cond_3

    .line 852
    iget-object v5, v3, Lcom/oppo/view/OppoVolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-virtual {v5}, Landroid/widget/OppoSeekBar;->getMax()I

    move-result v5

    if-eq v5, v1, :cond_2

    .line 853
    iget-object v5, v3, Lcom/oppo/view/OppoVolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-virtual {v5, v1}, Landroid/widget/OppoSeekBar;->setMax(I)V

    .line 856
    :cond_2
    iget-object v5, v3, Lcom/oppo/view/OppoVolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-virtual {v5, v0}, Landroid/widget/OppoSeekBar;->setProgress(I)V

    .line 857
    iget-object v5, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v5}, Landroid/media/AudioManager;->getMasterStreamType()I

    move-result v5

    if-eq p1, v5, :cond_9

    if-eq p1, v9, :cond_9

    invoke-direct {p0, p1}, Lcom/oppo/view/OppoVolumePanel;->isMuted(I)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 859
    iget-object v5, v3, Lcom/oppo/view/OppoVolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-virtual {v5, v10}, Landroid/widget/OppoSeekBar;->setEnabled(Z)V

    .line 865
    :goto_1
    invoke-virtual {p0, v3}, Lcom/oppo/view/OppoVolumePanel;->changeStreamIcon(Lcom/oppo/view/OppoVolumePanel$StreamControl;)V

    .line 869
    :cond_3
    iget-object v5, p0, Lcom/oppo/view/OppoVolumePanel;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->isShowing()Z

    move-result v5

    if-nez v5, :cond_6

    .line 870
    if-eqz v3, :cond_4

    .line 871
    iget-object v5, v3, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamTitle:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 872
    iget-object v5, v3, Lcom/oppo/view/OppoVolumePanel$StreamControl;->mMoreButton:Landroid/widget/ImageView;

    invoke-virtual {v5, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 873
    iget-object v5, v3, Lcom/oppo/view/OppoVolumePanel$StreamControl;->divider:Landroid/widget/ImageView;

    invoke-virtual {v5, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 875
    :cond_4
    iput-object v3, p0, Lcom/oppo/view/OppoVolumePanel;->titleStreamControl:Lcom/oppo/view/OppoVolumePanel$StreamControl;

    .line 876
    if-ne p1, v9, :cond_a

    const/4 v4, -0x1

    .line 878
    .local v4, stream:I
    :goto_2
    iget-object v5, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v5, v4}, Landroid/media/AudioManager;->forceVolumeControlStream(I)V

    .line 879
    iget-object v5, p0, Lcom/oppo/view/OppoVolumePanel;->mDialog:Landroid/app/Dialog;

    iget-object v6, p0, Lcom/oppo/view/OppoVolumePanel;->mView:Landroid/view/View;

    invoke-virtual {v5, v6}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 881
    iget-boolean v5, p0, Lcom/oppo/view/OppoVolumePanel;->mShowCombinedVolumes:Z

    if-eqz v5, :cond_5

    .line 882
    invoke-direct {p0}, Lcom/oppo/view/OppoVolumePanel;->collapse()V

    .line 884
    :cond_5
    iget-object v5, p0, Lcom/oppo/view/OppoVolumePanel;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->show()V

    .line 888
    .end local v4           #stream:I
    :cond_6
    if-eq p1, v9, :cond_7

    and-int/lit8 v5, p2, 0x10

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioService:Landroid/media/AudioService;

    invoke-virtual {v5, p1}, Landroid/media/AudioService;->isStreamAffectedByRingerMode(I)Z

    move-result v5

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v5}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v5

    if-ne v5, v8, :cond_7

    .line 893
    invoke-virtual {p0, v11}, Lcom/oppo/view/OppoVolumePanel;->obtainMessage(I)Landroid/os/Message;

    move-result-object v5

    const-wide/16 v6, 0x12c

    invoke-virtual {p0, v5, v6, v7}, Lcom/oppo/view/OppoVolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 895
    :cond_7
    return-void

    .line 783
    :sswitch_1
    iget-object v5, p0, Lcom/oppo/view/OppoVolumePanel;->mContext:Landroid/content/Context;

    invoke-static {v5, v8}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v2

    .line 785
    .local v2, ringuri:Landroid/net/Uri;
    if-nez v2, :cond_1

    .line 786
    iput-boolean v8, p0, Lcom/oppo/view/OppoVolumePanel;->mRingIsSilent:Z

    goto/16 :goto_0

    .line 793
    .end local v2           #ringuri:Landroid/net/Uri;
    :sswitch_2
    iget-object v5, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioManager:Landroid/media/AudioManager;

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Landroid/media/AudioManager;->getDevicesForStream(I)I

    move-result v5

    and-int/lit16 v5, v5, 0x380

    if-eqz v5, :cond_8

    .line 795
    const v5, 0xc08045f

    const v6, 0xc080461

    invoke-direct {p0, v5, v6}, Lcom/oppo/view/OppoVolumePanel;->setMusicIcon(II)V

    goto/16 :goto_0

    .line 798
    :cond_8
    const v5, 0xc08049b

    invoke-direct {p0, v5, v12}, Lcom/oppo/view/OppoVolumePanel;->setMusicIcon(II)V

    goto/16 :goto_0

    .line 804
    :sswitch_3
    const v5, 0xc08049b

    invoke-direct {p0, v5, v12}, Lcom/oppo/view/OppoVolumePanel;->setMusicIcon(II)V

    goto/16 :goto_0

    .line 815
    :sswitch_4
    add-int/lit8 v0, v0, 0x1

    .line 816
    add-int/lit8 v1, v1, 0x1

    .line 817
    goto/16 :goto_0

    .line 825
    :sswitch_5
    iget-object v5, p0, Lcom/oppo/view/OppoVolumePanel;->mContext:Landroid/content/Context;

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v2

    .line 827
    .restart local v2       #ringuri:Landroid/net/Uri;
    if-nez v2, :cond_1

    .line 828
    iput-boolean v8, p0, Lcom/oppo/view/OppoVolumePanel;->mRingIsSilent:Z

    goto/16 :goto_0

    .line 839
    .end local v2           #ringuri:Landroid/net/Uri;
    :sswitch_6
    add-int/lit8 v0, v0, 0x1

    .line 840
    add-int/lit8 v1, v1, 0x1

    .line 841
    goto/16 :goto_0

    .line 845
    :sswitch_7
    sget-boolean v5, Lcom/oppo/view/OppoVolumePanel;->LOGD:Z

    if-eqz v5, :cond_1

    .line 846
    const-string v5, "OppoVolumePanel"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "showing remote volume "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " over "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 861
    :cond_9
    iget-object v5, v3, Lcom/oppo/view/OppoVolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-virtual {v5, v8}, Landroid/widget/OppoSeekBar;->setEnabled(Z)V

    goto/16 :goto_1

    :cond_a
    move v4, p1

    .line 876
    goto/16 :goto_2

    .line 780
    :sswitch_data_0
    .sparse-switch
        -0xc8 -> :sswitch_7
        0x0 -> :sswitch_4
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_0
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0xa -> :sswitch_3
    .end sparse-switch
.end method

.method protected declared-synchronized onSliderVisibilityChanged(II)V
    .locals 6
    .parameter "streamType"
    .parameter "visible"

    .prologue
    const/4 v1, 0x1

    .line 988
    monitor-enter p0

    :try_start_0
    sget-boolean v3, Lcom/oppo/view/OppoVolumePanel;->LOGD:Z

    if-eqz v3, :cond_0

    const-string v3, "OppoVolumePanel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onSliderVisibilityChanged(stream="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", visi="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 989
    :cond_0
    if-ne p2, v1, :cond_2

    .line 990
    .local v1, isVisible:Z
    :goto_0
    sget-object v3, Lcom/oppo/view/OppoVolumePanel;->STREAMS:[Lcom/oppo/view/OppoVolumePanel$StreamResources;

    array-length v3, v3

    add-int/lit8 v0, v3, -0x1

    .local v0, i:I
    :goto_1
    if-ltz v0, :cond_1

    .line 991
    sget-object v3, Lcom/oppo/view/OppoVolumePanel;->STREAMS:[Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aget-object v2, v3, v0

    .line 992
    .local v2, streamRes:Lcom/oppo/view/OppoVolumePanel$StreamResources;
    iget v3, v2, Lcom/oppo/view/OppoVolumePanel$StreamResources;->streamType:I

    if-ne v3, p1, :cond_3

    .line 993
    iput-boolean v1, v2, Lcom/oppo/view/OppoVolumePanel$StreamResources;->show:Z

    .line 994
    if-nez v1, :cond_1

    iget v3, p0, Lcom/oppo/view/OppoVolumePanel;->mActiveStreamType:I

    if-ne v3, p1, :cond_1

    .line 995
    const/4 v3, -0x1

    iput v3, p0, Lcom/oppo/view/OppoVolumePanel;->mActiveStreamType:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1000
    .end local v2           #streamRes:Lcom/oppo/view/OppoVolumePanel$StreamResources;
    :cond_1
    monitor-exit p0

    return-void

    .line 989
    .end local v0           #i:I
    .end local v1           #isVisible:Z
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 990
    .restart local v0       #i:I
    .restart local v1       #isVisible:Z
    .restart local v2       #streamRes:Lcom/oppo/view/OppoVolumePanel$StreamResources;
    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 988
    .end local v0           #i:I
    .end local v1           #isVisible:Z
    .end local v2           #streamRes:Lcom/oppo/view/OppoVolumePanel$StreamResources;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .parameter "seekBar"

    .prologue
    .line 1231
    return-void
.end method

.method protected onStopSounds()V
    .locals 4

    .prologue
    .line 916
    monitor-enter p0

    .line 917
    :try_start_0
    invoke-static {}, Landroid/media/AudioSystem;->getNumStreamTypes()I

    move-result v1

    .line 918
    .local v1, numStreamTypes:I
    add-int/lit8 v0, v1, -0x1

    .local v0, i:I
    :goto_0
    if-ltz v0, :cond_1

    .line 919
    iget-object v3, p0, Lcom/oppo/view/OppoVolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    aget-object v2, v3, v0

    .line 920
    .local v2, toneGen:Landroid/media/ToneGenerator;
    if-eqz v2, :cond_0

    .line 921
    invoke-virtual {v2}, Landroid/media/ToneGenerator;->stopTone()V

    .line 918
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 924
    .end local v2           #toneGen:Landroid/media/ToneGenerator;
    :cond_1
    monitor-exit p0

    .line 925
    return-void

    .line 924
    .end local v0           #i:I
    .end local v1           #numStreamTypes:I
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 4
    .parameter "seekBar"

    .prologue
    const/16 v3, -0xc8

    .line 1234
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 1235
    .local v1, tag:Ljava/lang/Object;
    instance-of v2, v1, Lcom/oppo/view/OppoVolumePanel$StreamControl;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 1236
    check-cast v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;

    .line 1241
    .local v0, sc:Lcom/oppo/view/OppoVolumePanel$StreamControl;
    iget v2, v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;->streamType:I

    if-ne v2, v3, :cond_0

    .line 1242
    invoke-direct {p0, v3}, Lcom/oppo/view/OppoVolumePanel;->getStreamVolume(I)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1245
    .end local v0           #sc:Lcom/oppo/view/OppoVolumePanel$StreamControl;
    :cond_0
    return-void
.end method

.method protected onVibrate()V
    .locals 3

    .prologue
    .line 930
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 935
    :goto_0
    return-void

    .line 934
    :cond_0
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mVibrator:Landroid/os/Vibrator;

    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate(J)V

    goto :goto_0
.end method

.method protected onVolumeChanged(II)V
    .locals 5
    .parameter "streamType"
    .parameter "flags"

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    .line 723
    sget-boolean v0, Lcom/oppo/view/OppoVolumePanel;->LOGD:Z

    if-eqz v0, :cond_0

    .line 724
    const-string v0, "OppoVolumePanel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onVolumeChanged(streamType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", flags: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 725
    :cond_0
    monitor-enter p0

    .line 726
    :try_start_0
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 727
    invoke-virtual {p0, p1, p2}, Lcom/oppo/view/OppoVolumePanel;->onShowVolumeChanged(II)V

    .line 734
    :cond_1
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 736
    and-int/lit8 v0, p2, 0x4

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/oppo/view/OppoVolumePanel;->mRingIsSilent:Z

    if-nez v0, :cond_2

    .line 737
    invoke-virtual {p0, v3}, Lcom/oppo/view/OppoVolumePanel;->removeMessages(I)V

    .line 738
    invoke-virtual {p0, v3, p1, p2}, Lcom/oppo/view/OppoVolumePanel;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0x12c

    invoke-virtual {p0, v0, v1, v2}, Lcom/oppo/view/OppoVolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 741
    :cond_2
    and-int/lit8 v0, p2, 0x8

    if-eqz v0, :cond_3

    .line 742
    invoke-virtual {p0, v3}, Lcom/oppo/view/OppoVolumePanel;->removeMessages(I)V

    .line 743
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/oppo/view/OppoVolumePanel;->removeMessages(I)V

    .line 744
    const-string v0, "OppoVolumePanel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onVolumeChanged() ---  removemessage --- MSG_VIBRATE flags= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 745
    invoke-virtual {p0}, Lcom/oppo/view/OppoVolumePanel;->onStopSounds()V

    .line 748
    :cond_3
    invoke-virtual {p0, v4}, Lcom/oppo/view/OppoVolumePanel;->removeMessages(I)V

    .line 749
    invoke-virtual {p0, v4}, Lcom/oppo/view/OppoVolumePanel;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0x2710

    invoke-virtual {p0, v0, v1, v2}, Lcom/oppo/view/OppoVolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 750
    invoke-direct {p0}, Lcom/oppo/view/OppoVolumePanel;->resetTimeout()V

    .line 751
    return-void

    .line 728
    :cond_4
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_1

    .line 729
    :try_start_1
    iget v0, p0, Lcom/oppo/view/OppoVolumePanel;->mActiveStreamType:I

    if-eq v0, p1, :cond_5

    .line 730
    invoke-direct {p0, p1}, Lcom/oppo/view/OppoVolumePanel;->reorderSliders(I)V

    .line 732
    :cond_5
    invoke-virtual {p0, p1, p2}, Lcom/oppo/view/OppoVolumePanel;->onShowVolumeChanged(II)V

    goto :goto_0

    .line 734
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public postDisplaySafeVolumeWarning(I)V
    .locals 3
    .parameter "flags"

    .prologue
    const/16 v2, 0xb

    .line 703
    invoke-virtual {p0, v2}, Lcom/oppo/view/OppoVolumePanel;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 714
    :cond_0
    :goto_0
    return-void

    .line 704
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v2, p1, v1}, Lcom/oppo/view/OppoVolumePanel;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 707
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mStreamControls:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    .line 708
    iget-object v1, p0, Lcom/oppo/view/OppoVolumePanel;->mStreamControls:Ljava/util/HashMap;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;

    .line 709
    .local v0, sc:Lcom/oppo/view/OppoVolumePanel$StreamControl;
    if-eqz v0, :cond_0

    .line 710
    iget-object v1, v0, Lcom/oppo/view/OppoVolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/OppoSeekBar;->setSafeMediaVolumeEnabled(Z)V

    goto :goto_0
.end method

.method public postHasNewRemotePlaybackInfo()V
    .locals 2

    .prologue
    const/16 v1, 0x9

    .line 675
    invoke-virtual {p0, v1}, Lcom/oppo/view/OppoVolumePanel;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 680
    :goto_0
    return-void

    .line 679
    :cond_0
    invoke-virtual {p0, v1}, Lcom/oppo/view/OppoVolumePanel;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method public postMasterMuteChanged(I)V
    .locals 1
    .parameter "flags"

    .prologue
    .line 699
    const/16 v0, -0x64

    invoke-virtual {p0, v0, p1}, Lcom/oppo/view/OppoVolumePanel;->postMuteChanged(II)V

    .line 700
    return-void
.end method

.method public postMasterVolumeChanged(I)V
    .locals 1
    .parameter "flags"

    .prologue
    .line 683
    const/16 v0, -0x64

    invoke-virtual {p0, v0, p1}, Lcom/oppo/view/OppoVolumePanel;->postVolumeChanged(II)V

    .line 684
    return-void
.end method

.method public postMuteChanged(II)V
    .locals 1
    .parameter "streamType"
    .parameter "flags"

    .prologue
    .line 687
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/oppo/view/OppoVolumePanel;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 696
    :goto_0
    return-void

    .line 689
    :cond_0
    monitor-enter p0

    .line 690
    :try_start_0
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mStreamControls:Ljava/util/HashMap;

    if-nez v0, :cond_1

    .line 691
    invoke-direct {p0}, Lcom/oppo/view/OppoVolumePanel;->createSliders()V

    .line 693
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 694
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/oppo/view/OppoVolumePanel;->removeMessages(I)V

    .line 695
    const/4 v0, 0x7

    invoke-virtual {p0, v0, p1, p2}, Lcom/oppo/view/OppoVolumePanel;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 693
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public postRemoteSliderVisibility(Z)V
    .locals 3
    .parameter "visible"

    .prologue
    .line 660
    const/16 v1, 0xa

    const/16 v2, -0xc8

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v1, v2, v0}, Lcom/oppo/view/OppoVolumePanel;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 662
    return-void

    .line 660
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public postRemoteVolumeChanged(II)V
    .locals 2
    .parameter "streamType"
    .parameter "flags"

    .prologue
    const/16 v1, 0x8

    .line 648
    invoke-virtual {p0, v1}, Lcom/oppo/view/OppoVolumePanel;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 657
    :goto_0
    return-void

    .line 650
    :cond_0
    monitor-enter p0

    .line 651
    :try_start_0
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mStreamControls:Ljava/util/HashMap;

    if-nez v0, :cond_1

    .line 652
    invoke-direct {p0}, Lcom/oppo/view/OppoVolumePanel;->createSliders()V

    .line 654
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 655
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/oppo/view/OppoVolumePanel;->removeMessages(I)V

    .line 656
    invoke-virtual {p0, v1, p1, p2}, Lcom/oppo/view/OppoVolumePanel;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 654
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public postVolumeChanged(II)V
    .locals 1
    .parameter "streamType"
    .parameter "flags"

    .prologue
    .line 638
    monitor-enter p0

    .line 639
    :try_start_0
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mStreamControls:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 640
    invoke-direct {p0}, Lcom/oppo/view/OppoVolumePanel;->createSliders()V

    .line 642
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 643
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/oppo/view/OppoVolumePanel;->removeMessages(I)V

    .line 644
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lcom/oppo/view/OppoVolumePanel;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 645
    return-void

    .line 642
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setLayoutDirection(I)V
    .locals 1
    .parameter "layoutDirection"

    .prologue
    .line 421
    iget-object v0, p0, Lcom/oppo/view/OppoVolumePanel;->mPanel:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setLayoutDirection(I)V

    .line 422
    invoke-direct {p0}, Lcom/oppo/view/OppoVolumePanel;->updateStates()V

    .line 423
    return-void
.end method
