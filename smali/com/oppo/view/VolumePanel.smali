.class public Lcom/oppo/view/VolumePanel;
.super Landroid/os/Handler;
.source "VolumePanel.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/OppoSeekBar$OnOppoSeekBarFromUserChangeListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/oppo/view/VolumePanel$WarningDialogReceiver;,
        Lcom/oppo/view/VolumePanel$StreamControl;,
        Lcom/oppo/view/VolumePanel$StreamResources;
    }
.end annotation


# static fields
.field private static final ACTION_MEDIA_VOLUME_MODE_CHANGED:Ljava/lang/String; = "action_media_volume_mode_changed"

.field private static final ACTION_SKIN_CHANGED:Ljava/lang/String; = "android.intent.action.SKIN_CHANGED"

.field private static final ACTION_SYSTEM_VOLUME_MODE_CHANGED:Ljava/lang/String; = "action_system_volume_mode_changed"

.field private static final BEEP_DURATION:I = 0x96

.field private static final FREE_DELAY:I = 0x2710

.field private static LOGD:Z = false

.field private static final MAX_VOLUME:I = 0x64

.field private static final MSG_DISPLAY_SAFE_VOLUME_WARNING:I = 0xb

.field private static final MSG_FREE_RESOURCES:I = 0x1

.field private static final MSG_MUTE_CHANGED:I = 0x7

.field private static final MSG_PLAY_SOUND:I = 0x2

.field private static final MSG_REMOTE_VOLUME_CHANGED:I = 0x8

.field private static final MSG_REMOTE_VOLUME_UPDATE_IF_SHOWN:I = 0x9

.field private static final MSG_RINGER_MODE_CHANGED:I = 0x6

.field private static final MSG_SLIDER_VISIBILITY_CHANGED:I = 0xa

.field private static final MSG_STOP_SOUNDS:I = 0x3

.field private static final MSG_TIMEOUT:I = 0x5

.field private static final MSG_VIBRATE:I = 0x4

.field private static final MSG_VOLUME_CHANGED:I = 0x0

.field private static final OPPO_UNIFORM_VOLUME_SETTINGS_ENABLED:Ljava/lang/String; = "oppo_uniform_volume_settings_enabled"

.field public static final PLAY_SOUND_DELAY:I = 0x12c

.field private static final STREAMS:[Lcom/oppo/view/VolumePanel$StreamResources; = null

.field private static final STREAM_MASTER:I = -0x64

.field private static final TAG:Ljava/lang/String; = "jinpeng/VolumePanel"

.field private static final TIMEOUT_DELAY:I = 0xbb8

.field public static final VIBRATE_DELAY:I = 0x12c

.field private static final VIBRATE_DURATION:I = 0x12c

.field private static final WAIT_TIME_RELEASE_LOCK:I = 0x64

.field private static sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

.field private static sConfirmSafeVolumeLock:Ljava/lang/Object;


# instance fields
.field private lp:Landroid/view/ViewGroup$LayoutParams;

.field private mActiveStreamType:I

.field private mAudioManager:Landroid/media/AudioManager;

.field protected mAudioService:Landroid/media/AudioService;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field protected mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/Dialog;

.field private mIsTouch:Z

.field private mOppoVolumeDisk:Landroid/view/ViewGroup;

.field private mOppoVolumeIcon:Landroid/widget/ImageView;

.field private mOppoVolumeProgress:Lcom/oppo/widget/OppoVolumeProgress;

.field private mOppoVolumeTitle:Landroid/widget/TextView;

.field private mPanel:Landroid/view/ViewGroup;

.field private final mPlayMasterStreamTones:Z

.field private mPowerManager:Landroid/os/PowerManager;

.field private mRingIsSilent:Z

.field private mSafeVolumeWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mShowCombinedVolumes:Z

.field private mSliderGroup:Landroid/view/ViewGroup;

.field private mStreamControls:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/oppo/view/VolumePanel$StreamControl;",
            ">;"
        }
    .end annotation
.end field

.field private mToneGenerators:[Landroid/media/ToneGenerator;

.field private mVibrator:Landroid/os/Vibrator;

.field private mView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 88
    sput-boolean v2, Lcom/oppo/view/VolumePanel;->LOGD:Z

    .line 268
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/oppo/view/VolumePanel$StreamResources;

    sget-object v1, Lcom/oppo/view/VolumePanel$StreamResources;->BluetoothSCOStream:Lcom/oppo/view/VolumePanel$StreamResources;

    aput-object v1, v0, v2

    const/4 v1, 0x1

    sget-object v2, Lcom/oppo/view/VolumePanel$StreamResources;->RingerStream:Lcom/oppo/view/VolumePanel$StreamResources;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/oppo/view/VolumePanel$StreamResources;->VoiceStream:Lcom/oppo/view/VolumePanel$StreamResources;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/oppo/view/VolumePanel$StreamResources;->MediaStream:Lcom/oppo/view/VolumePanel$StreamResources;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/oppo/view/VolumePanel$StreamResources;->NotificationStream:Lcom/oppo/view/VolumePanel$StreamResources;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/oppo/view/VolumePanel$StreamResources;->AlarmStream:Lcom/oppo/view/VolumePanel$StreamResources;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/oppo/view/VolumePanel$StreamResources;->FMStream:Lcom/oppo/view/VolumePanel$StreamResources;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/oppo/view/VolumePanel$StreamResources;->MasterStream:Lcom/oppo/view/VolumePanel$StreamResources;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/oppo/view/VolumePanel$StreamResources;->RemoteStream:Lcom/oppo/view/VolumePanel$StreamResources;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/oppo/view/VolumePanel$StreamResources;->SystemStream:Lcom/oppo/view/VolumePanel$StreamResources;

    aput-object v2, v0, v1

    sput-object v0, Lcom/oppo/view/VolumePanel;->STREAMS:[Lcom/oppo/view/VolumePanel$StreamResources;

    .line 306
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/oppo/view/VolumePanel;->sConfirmSafeVolumeLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/media/AudioService;)V
    .locals 11
    .parameter "context"
    .parameter "volumeService"

    .prologue
    .line 364
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 154
    const/4 v8, -0x1

    iput v8, p0, Lcom/oppo/view/VolumePanel;->mActiveStreamType:I

    .line 167
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/oppo/view/VolumePanel;->mIsTouch:Z

    .line 1210
    new-instance v8, Lcom/oppo/view/VolumePanel$7;

    invoke-direct {v8, p0}, Lcom/oppo/view/VolumePanel$7;-><init>(Lcom/oppo/view/VolumePanel;)V

    iput-object v8, p0, Lcom/oppo/view/VolumePanel;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 365
    iput-object p1, p0, Lcom/oppo/view/VolumePanel;->mContext:Landroid/content/Context;

    .line 366
    const-string v8, "audio"

    invoke-virtual {p1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/media/AudioManager;

    iput-object v8, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    .line 367
    iput-object p2, p0, Lcom/oppo/view/VolumePanel;->mAudioService:Landroid/media/AudioService;

    .line 370
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0xc0c0406

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v6

    .line 372
    .local v6, useMasterVolume:Z
    if-eqz v6, :cond_1

    .line 373
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    sget-object v8, Lcom/oppo/view/VolumePanel;->STREAMS:[Lcom/oppo/view/VolumePanel$StreamResources;

    array-length v8, v8

    if-ge v1, v8, :cond_1

    .line 374
    sget-object v8, Lcom/oppo/view/VolumePanel;->STREAMS:[Lcom/oppo/view/VolumePanel$StreamResources;

    aget-object v5, v8, v1

    .line 375
    .local v5, streamRes:Lcom/oppo/view/VolumePanel$StreamResources;
    iget v8, v5, Lcom/oppo/view/VolumePanel$StreamResources;->streamType:I

    const/16 v9, -0x64

    if-ne v8, v9, :cond_0

    const/4 v8, 0x1

    :goto_1
    iput-boolean v8, v5, Lcom/oppo/view/VolumePanel$StreamResources;->show:Z

    .line 373
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 375
    :cond_0
    const/4 v8, 0x0

    goto :goto_1

    .line 379
    .end local v1           #i:I
    .end local v5           #streamRes:Lcom/oppo/view/VolumePanel$StreamResources;
    :cond_1
    invoke-direct {p0, p1}, Lcom/oppo/view/VolumePanel;->inflaterBaseUI(Landroid/content/Context;)V

    .line 381
    new-instance v8, Lcom/oppo/view/VolumePanel$1;

    const v9, 0xc030407

    invoke-direct {v8, p0, p1, v9}, Lcom/oppo/view/VolumePanel$1;-><init>(Lcom/oppo/view/VolumePanel;Landroid/content/Context;I)V

    iput-object v8, p0, Lcom/oppo/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    .line 392
    iget-object v8, p0, Lcom/oppo/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    const-string v9, "Volume control"

    invoke-virtual {v8, v9}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 393
    new-instance v8, Landroid/view/ViewGroup$LayoutParams;

    const/4 v9, -0x2

    const/4 v10, -0x2

    invoke-direct {v8, v9, v10}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iput-object v8, p0, Lcom/oppo/view/VolumePanel;->lp:Landroid/view/ViewGroup$LayoutParams;

    .line 394
    iget-object v8, p0, Lcom/oppo/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    iget-object v9, p0, Lcom/oppo/view/VolumePanel;->mView:Landroid/view/View;

    iget-object v10, p0, Lcom/oppo/view/VolumePanel;->lp:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v8, v9, v10}, Landroid/app/Dialog;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 395
    iget-object v8, p0, Lcom/oppo/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    new-instance v9, Lcom/oppo/view/VolumePanel$2;

    invoke-direct {v9, p0}, Lcom/oppo/view/VolumePanel$2;-><init>(Lcom/oppo/view/VolumePanel;)V

    invoke-virtual {v8, v9}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 402
    iget-object v8, p0, Lcom/oppo/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v8}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v7

    .line 403
    .local v7, window:Landroid/view/Window;
    const/16 v8, 0x30

    invoke-virtual {v7, v8}, Landroid/view/Window;->setGravity(I)V

    .line 404
    invoke-virtual {v7}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 405
    .local v2, lp:Landroid/view/WindowManager$LayoutParams;
    const/4 v8, 0x0

    iput-object v8, v2, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 407
    iget-object v8, p0, Lcom/oppo/view/VolumePanel;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0xc05044a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v8

    iput v8, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 409
    const/16 v8, 0x7e4

    iput v8, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 410
    const/4 v8, -0x2

    iput v8, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 411
    const/4 v8, -0x2

    iput v8, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 412
    invoke-virtual {v7, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 413
    const v8, 0x40528

    invoke-virtual {v7, v8}, Landroid/view/Window;->addFlags(I)V

    .line 417
    invoke-static {}, Landroid/media/AudioSystem;->getNumStreamTypes()I

    move-result v8

    new-array v8, v8, [Landroid/media/ToneGenerator;

    iput-object v8, p0, Lcom/oppo/view/VolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    .line 418
    const-string v8, "vibrator"

    invoke-virtual {p1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/Vibrator;

    iput-object v8, p0, Lcom/oppo/view/VolumePanel;->mVibrator:Landroid/os/Vibrator;

    .line 422
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/oppo/view/VolumePanel;->mShowCombinedVolumes:Z

    .line 433
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0xc0c0406

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    .line 435
    .local v4, masterVolumeOnly:Z
    iget-object v8, p0, Lcom/oppo/view/VolumePanel;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0xc0c0407

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    .line 438
    .local v3, masterVolumeKeySounds:Z
    if-eqz v4, :cond_2

    if-eqz v3, :cond_2

    const/4 v8, 0x1

    :goto_2
    iput-boolean v8, p0, Lcom/oppo/view/VolumePanel;->mPlayMasterStreamTones:Z

    .line 439
    invoke-direct {p0}, Lcom/oppo/view/VolumePanel;->listenToRingerMode()V

    .line 441
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 442
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v8, "android.intent.action.SKIN_CHANGED"

    invoke-virtual {v0, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 443
    const-string v8, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v0, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 444
    const-string v8, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v0, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 445
    iget-object v8, p0, Lcom/oppo/view/VolumePanel;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/oppo/view/VolumePanel;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v8, v9, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 449
    const-string v8, "power"

    invoke-virtual {p1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/PowerManager;

    iput-object v8, p0, Lcom/oppo/view/VolumePanel;->mPowerManager:Landroid/os/PowerManager;

    .line 450
    iget-object v8, p0, Lcom/oppo/view/VolumePanel;->mPowerManager:Landroid/os/PowerManager;

    const v9, 0x1000000a

    const-string v10, "VolumePanel.mSafeVolumeWakeLock"

    invoke-virtual {v8, v9, v10}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v8

    iput-object v8, p0, Lcom/oppo/view/VolumePanel;->mSafeVolumeWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 453
    return-void

    .line 438
    .end local v0           #filter:Landroid/content/IntentFilter;
    :cond_2
    const/4 v8, 0x0

    goto :goto_2
.end method

.method static synthetic access$000()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/oppo/view/VolumePanel;->sConfirmSafeVolumeLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/oppo/view/VolumePanel;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/oppo/view/VolumePanel;->createSliders()V

    return-void
.end method

.method static synthetic access$102(Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .parameter "x0"

    .prologue
    .line 83
    sput-object p0, Lcom/oppo/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/oppo/view/VolumePanel;)Landroid/app/Dialog;
    .locals 1
    .parameter "x0"

    .prologue
    .line 83
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$200(Lcom/oppo/view/VolumePanel;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/oppo/view/VolumePanel;->forceTimeout()V

    return-void
.end method

.method static synthetic access$300(Lcom/oppo/view/VolumePanel;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 83
    iget v0, p0, Lcom/oppo/view/VolumePanel;->mActiveStreamType:I

    return v0
.end method

.method static synthetic access$302(Lcom/oppo/view/VolumePanel;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 83
    iput p1, p0, Lcom/oppo/view/VolumePanel;->mActiveStreamType:I

    return p1
.end method

.method static synthetic access$400(Lcom/oppo/view/VolumePanel;)Landroid/media/AudioManager;
    .locals 1
    .parameter "x0"

    .prologue
    .line 83
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/oppo/view/VolumePanel;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/oppo/view/VolumePanel;->resetTimeout()V

    return-void
.end method

.method static synthetic access$700(Lcom/oppo/view/VolumePanel;)Ljava/util/HashMap;
    .locals 1
    .parameter "x0"

    .prologue
    .line 83
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$800()Z
    .locals 1

    .prologue
    .line 83
    sget-boolean v0, Lcom/oppo/view/VolumePanel;->LOGD:Z

    return v0
.end method

.method static synthetic access$900(Lcom/oppo/view/VolumePanel;Landroid/content/Context;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/oppo/view/VolumePanel;->inflaterBaseUI(Landroid/content/Context;)V

    return-void
.end method

.method private addOtherVolumes()V
    .locals 5

    .prologue
    .line 617
    iget-boolean v3, p0, Lcom/oppo/view/VolumePanel;->mShowCombinedVolumes:Z

    if-nez v3, :cond_1

    .line 629
    :cond_0
    return-void

    .line 619
    :cond_1
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    sget-object v3, Lcom/oppo/view/VolumePanel;->STREAMS:[Lcom/oppo/view/VolumePanel$StreamResources;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 621
    sget-object v3, Lcom/oppo/view/VolumePanel;->STREAMS:[Lcom/oppo/view/VolumePanel$StreamResources;

    aget-object v3, v3, v0

    iget v2, v3, Lcom/oppo/view/VolumePanel$StreamResources;->streamType:I

    .line 622
    .local v2, streamType:I
    sget-object v3, Lcom/oppo/view/VolumePanel;->STREAMS:[Lcom/oppo/view/VolumePanel$StreamResources;

    aget-object v3, v3, v0

    iget-boolean v3, v3, Lcom/oppo/view/VolumePanel$StreamResources;->show:Z

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/oppo/view/VolumePanel;->mActiveStreamType:I

    if-ne v2, v3, :cond_3

    .line 619
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 625
    :cond_3
    iget-object v3, p0, Lcom/oppo/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/oppo/view/VolumePanel$StreamControl;

    .line 626
    .local v1, sc:Lcom/oppo/view/VolumePanel$StreamControl;
    iget-object v3, p0, Lcom/oppo/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    iget-object v4, v1, Lcom/oppo/view/VolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 627
    invoke-direct {p0, v1}, Lcom/oppo/view/VolumePanel;->updateSlider(Lcom/oppo/view/VolumePanel$StreamControl;)V

    goto :goto_1
.end method

.method private addUnifiedControl()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1474
    iget-object v6, p0, Lcom/oppo/view/VolumePanel;->mContext:Landroid/content/Context;

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 1476
    .local v1, inflater:Landroid/view/LayoutInflater;
    const v6, 0xc09044b

    invoke-virtual {v1, v6, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 1477
    .local v2, layout:Landroid/view/View;
    iget-object v6, p0, Lcom/oppo/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    invoke-virtual {v6, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1478
    const v6, 0xc0204a3

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CompoundButton;

    .line 1479
    .local v3, uc:Landroid/widget/CompoundButton;
    if-eqz v3, :cond_0

    .line 1480
    iget-object v6, p0, Lcom/oppo/view/VolumePanel;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "oppo_uniform_volume_settings_enabled"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 1482
    .local v0, enabled:I
    invoke-virtual {v3, v8}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1483
    if-ne v0, v4, :cond_1

    :goto_0
    invoke-virtual {v3, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 1484
    invoke-virtual {v3, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1486
    .end local v0           #enabled:I
    :cond_0
    return-void

    .restart local v0       #enabled:I
    :cond_1
    move v4, v5

    .line 1483
    goto :goto_0
.end method

.method private collapse()V
    .locals 4

    .prologue
    .line 679
    iget-object v2, p0, Lcom/oppo/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 680
    .local v0, count:I
    const/4 v1, 0x1

    .local v1, i:I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 681
    iget-object v2, p0, Lcom/oppo/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 680
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 683
    :cond_0
    return-void
.end method

.method private createSliders()V
    .locals 11

    .prologue
    const/4 v8, 0x0

    const/4 v10, 0x0

    .line 538
    sget-boolean v7, Lcom/oppo/view/VolumePanel;->LOGD:Z

    if-eqz v7, :cond_0

    const-string v7, "jinpeng/VolumePanel"

    const-string v9, "createSliders()"

    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    :cond_0
    iget-object v7, p0, Lcom/oppo/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    if-eqz v7, :cond_1

    .line 540
    iget-object v7, p0, Lcom/oppo/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->clear()V

    .line 541
    iput-object v10, p0, Lcom/oppo/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    .line 543
    :cond_1
    iget-object v7, p0, Lcom/oppo/view/VolumePanel;->mContext:Landroid/content/Context;

    const-string v9, "layout_inflater"

    invoke-virtual {v7, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 545
    .local v1, inflater:Landroid/view/LayoutInflater;
    new-instance v7, Ljava/util/HashMap;

    sget-object v9, Lcom/oppo/view/VolumePanel;->STREAMS:[Lcom/oppo/view/VolumePanel$StreamResources;

    array-length v9, v9

    invoke-direct {v7, v9}, Ljava/util/HashMap;-><init>(I)V

    iput-object v7, p0, Lcom/oppo/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    .line 546
    iget-object v7, p0, Lcom/oppo/view/VolumePanel;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 547
    .local v3, res:Landroid/content/res/Resources;
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    sget-object v7, Lcom/oppo/view/VolumePanel;->STREAMS:[Lcom/oppo/view/VolumePanel$StreamResources;

    array-length v7, v7

    if-ge v0, v7, :cond_6

    .line 548
    sget-object v7, Lcom/oppo/view/VolumePanel;->STREAMS:[Lcom/oppo/view/VolumePanel$StreamResources;

    aget-object v5, v7, v0

    .line 549
    .local v5, streamRes:Lcom/oppo/view/VolumePanel$StreamResources;
    iget v6, v5, Lcom/oppo/view/VolumePanel$StreamResources;->streamType:I

    .line 555
    .local v6, streamType:I
    new-instance v4, Lcom/oppo/view/VolumePanel$StreamControl;

    invoke-direct {v4, p0, v10}, Lcom/oppo/view/VolumePanel$StreamControl;-><init>(Lcom/oppo/view/VolumePanel;Lcom/oppo/view/VolumePanel$1;)V

    .line 556
    .local v4, sc:Lcom/oppo/view/VolumePanel$StreamControl;
    iput v6, v4, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    .line 557
    const v7, 0xc090434

    invoke-virtual {v1, v7, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    iput-object v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    .line 558
    iget-object v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    invoke-virtual {v7, v4}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    .line 559
    iget-object v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    const v9, 0xc02045c

    invoke-virtual {v7, v9}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->streamTitle:Landroid/widget/TextView;

    .line 560
    iget-object v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->streamTitle:Landroid/widget/TextView;

    iget v9, v5, Lcom/oppo/view/VolumePanel$StreamResources;->descRes:I

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(I)V

    .line 561
    iget-object v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    const v9, 0xc02045d

    invoke-virtual {v7, v9}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    .line 562
    iget-object v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 563
    iget-object v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    iget v9, v5, Lcom/oppo/view/VolumePanel$StreamResources;->descRes:I

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 564
    iget-object v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    invoke-virtual {v7, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 565
    iget v7, v5, Lcom/oppo/view/VolumePanel$StreamResources;->iconRes:I

    iput v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->iconRes:I

    .line 566
    iget v7, v5, Lcom/oppo/view/VolumePanel$StreamResources;->iconMuteRes:I

    iput v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->iconMuteRes:I

    .line 567
    iget v7, v5, Lcom/oppo/view/VolumePanel$StreamResources;->circleIconRes:I

    iput v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->circleIconRes:I

    .line 568
    iget v7, v5, Lcom/oppo/view/VolumePanel$StreamResources;->circleIconMuteRes:I

    iput v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->circleIconMuteRes:I

    .line 569
    iget-object v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    iget v9, v4, Lcom/oppo/view/VolumePanel$StreamControl;->iconRes:I

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 574
    iget-object v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    const v9, 0xc02045e

    invoke-virtual {v7, v9}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/OppoSeekBar;

    iput-object v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    .line 575
    const/4 v7, 0x3

    if-ne v7, v6, :cond_2

    .line 576
    iget-object v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-virtual {v7, v8}, Landroid/widget/OppoSeekBar;->setSafeMediaVolumeEnabled(Z)V

    .line 577
    iget-object v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-virtual {v7, p0}, Landroid/widget/OppoSeekBar;->setOppoSeekBarFromUserChangeListener(Landroid/widget/OppoSeekBar$OnOppoSeekBarFromUserChangeListener;)V

    .line 580
    :cond_2
    iget-boolean v7, v5, Lcom/oppo/view/VolumePanel$StreamResources;->show:Z

    iput-boolean v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->show:Z

    .line 581
    const/4 v7, 0x6

    if-eq v6, v7, :cond_3

    if-nez v6, :cond_4

    :cond_3
    const/4 v2, 0x1

    .line 583
    .local v2, plusOne:I
    :goto_1
    invoke-static {}, Landroid/media/AudioSystem;->getNumStreamTypes()I

    move-result v7

    if-lt v6, v7, :cond_5

    .line 547
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .end local v2           #plusOne:I
    :cond_4
    move v2, v8

    .line 581
    goto :goto_1

    .line 586
    .restart local v2       #plusOne:I
    :cond_5
    iget-object v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-direct {p0, v6}, Lcom/oppo/view/VolumePanel;->getStreamMaxVolume(I)I

    move-result v9

    add-int/2addr v9, v2

    invoke-virtual {v7, v9}, Landroid/widget/OppoSeekBar;->setMax(I)V

    .line 587
    iget-object v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-virtual {v7, p0}, Landroid/widget/OppoSeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 588
    iget-object v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-virtual {v7, v4}, Landroid/widget/OppoSeekBar;->setTag(Ljava/lang/Object;)V

    .line 589
    iget-object v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    const v9, 0xc020423

    invoke-virtual {v7, v9}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, v4, Lcom/oppo/view/VolumePanel$StreamControl;->divider:Landroid/widget/ImageView;

    .line 590
    iget-object v7, p0, Lcom/oppo/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v9, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 592
    .end local v2           #plusOne:I
    .end local v4           #sc:Lcom/oppo/view/VolumePanel$StreamControl;
    .end local v5           #streamRes:Lcom/oppo/view/VolumePanel$StreamResources;
    .end local v6           #streamType:I
    :cond_6
    return-void
.end method

.method private expand()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    .line 666
    sget-boolean v3, Lcom/oppo/view/VolumePanel;->LOGD:Z

    if-eqz v3, :cond_0

    const-string v3, "jinpeng/VolumePanel"

    const-string v4, "expand()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    :cond_0
    iget-object v3, p0, Lcom/oppo/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 668
    .local v0, count:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    if-ge v1, v0, :cond_2

    .line 669
    iget-object v3, p0, Lcom/oppo/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/oppo/view/VolumePanel$StreamControl;

    .line 670
    .local v2, sc:Lcom/oppo/view/VolumePanel$StreamControl;
    if-eqz v2, :cond_1

    .line 671
    iget-object v3, v2, Lcom/oppo/view/VolumePanel$StreamControl;->divider:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 668
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 674
    .end local v2           #sc:Lcom/oppo/view/VolumePanel$StreamControl;
    :cond_2
    iget-object v3, p0, Lcom/oppo/view/VolumePanel;->mOppoVolumeDisk:Landroid/view/ViewGroup;

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 675
    iget-object v3, p0, Lcom/oppo/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 676
    return-void
.end method

.method private forceTimeout()V
    .locals 1

    .prologue
    const/4 v0, 0x5

    .line 1308
    invoke-virtual {p0, v0}, Lcom/oppo/view/VolumePanel;->removeMessages(I)V

    .line 1309
    invoke-virtual {p0, v0}, Lcom/oppo/view/VolumePanel;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/oppo/view/VolumePanel;->sendMessage(Landroid/os/Message;)Z

    .line 1310
    return-void
.end method

.method private getOrCreateToneGenerator(I)Landroid/media/ToneGenerator;
    .locals 4
    .parameter "streamType"

    .prologue
    .line 1160
    const/16 v1, -0x64

    if-ne p1, v1, :cond_0

    .line 1164
    iget-boolean v1, p0, Lcom/oppo/view/VolumePanel;->mPlayMasterStreamTones:Z

    if-eqz v1, :cond_2

    .line 1165
    const/4 p1, 0x1

    .line 1170
    :cond_0
    monitor-enter p0

    .line 1171
    :try_start_0
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    aget-object v1, v1, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 1173
    :try_start_1
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    new-instance v2, Landroid/media/ToneGenerator;

    const/16 v3, 0x64

    invoke-direct {v2, p1, v3}, Landroid/media/ToneGenerator;-><init>(II)V

    aput-object v2, v1, p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1181
    :cond_1
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    aget-object v1, v1, p1

    monitor-exit p0

    :goto_1
    return-object v1

    .line 1167
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 1174
    :catch_0
    move-exception v0

    .line 1175
    .local v0, e:Ljava/lang/RuntimeException;
    sget-boolean v1, Lcom/oppo/view/VolumePanel;->LOGD:Z

    if-eqz v1, :cond_1

    .line 1176
    const-string v1, "jinpeng/VolumePanel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ToneGenerator constructor failed with RuntimeException: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1182
    .end local v0           #e:Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method private getStreamMaxVolume(I)I
    .locals 1
    .parameter "streamType"

    .prologue
    .line 508
    const/16 v0, -0x64

    if-ne p1, v0, :cond_0

    .line 509
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getMasterMaxVolume()I

    move-result v0

    .line 513
    :goto_0
    return v0

    .line 510
    :cond_0
    const/16 v0, -0xc8

    if-ne p1, v0, :cond_1

    .line 511
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mAudioService:Landroid/media/AudioService;

    invoke-virtual {v0}, Landroid/media/AudioService;->getRemoteStreamMaxVolume()I

    move-result v0

    goto :goto_0

    .line 513
    :cond_1
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    goto :goto_0
.end method

.method private getStreamVolume(I)I
    .locals 1
    .parameter "streamType"

    .prologue
    .line 518
    const/16 v0, -0x64

    if-ne p1, v0, :cond_0

    .line 519
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getMasterVolume()I

    move-result v0

    .line 523
    :goto_0
    return v0

    .line 520
    :cond_0
    const/16 v0, -0xc8

    if-ne p1, v0, :cond_1

    .line 521
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mAudioService:Landroid/media/AudioService;

    invoke-virtual {v0}, Landroid/media/AudioService;->getRemoteStreamVolume()I

    move-result v0

    goto :goto_0

    .line 523
    :cond_1
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    goto :goto_0
.end method

.method private inflaterBaseUI(Landroid/content/Context;)V
    .locals 3
    .parameter "context"

    .prologue
    .line 456
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 458
    .local v0, inflater:Landroid/view/LayoutInflater;
    const v1, 0xc090433

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/oppo/view/VolumePanel;->mView:Landroid/view/View;

    .line 459
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mView:Landroid/view/View;

    new-instance v2, Lcom/oppo/view/VolumePanel$3;

    invoke-direct {v2, p0}, Lcom/oppo/view/VolumePanel$3;-><init>(Lcom/oppo/view/VolumePanel;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 465
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mView:Landroid/view/View;

    const v2, 0xc02045f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/oppo/view/VolumePanel;->mPanel:Landroid/view/ViewGroup;

    .line 466
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mView:Landroid/view/View;

    const v2, 0xc020425

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/oppo/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    .line 467
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mView:Landroid/view/View;

    const v2, 0xc02049f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/oppo/view/VolumePanel;->mOppoVolumeDisk:Landroid/view/ViewGroup;

    .line 468
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mOppoVolumeDisk:Landroid/view/ViewGroup;

    invoke-virtual {v1, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 469
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mView:Landroid/view/View;

    const v2, 0xc0204a0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/oppo/widget/OppoVolumeProgress;

    iput-object v1, p0, Lcom/oppo/view/VolumePanel;->mOppoVolumeProgress:Lcom/oppo/widget/OppoVolumeProgress;

    .line 470
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mView:Landroid/view/View;

    const v2, 0xc0204a1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/oppo/view/VolumePanel;->mOppoVolumeIcon:Landroid/widget/ImageView;

    .line 471
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mView:Landroid/view/View;

    const v2, 0xc0204a2

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/oppo/view/VolumePanel;->mOppoVolumeTitle:Landroid/widget/TextView;

    .line 472
    return-void
.end method

.method private isMuted(I)Z
    .locals 2
    .parameter "streamType"

    .prologue
    const/4 v0, 0x0

    .line 496
    const/16 v1, -0x64

    if-ne p1, v1, :cond_1

    .line 497
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMasterMute()Z

    move-result v0

    .line 503
    :cond_0
    :goto_0
    return v0

    .line 498
    :cond_1
    const/16 v1, -0xc8

    if-ne p1, v1, :cond_2

    .line 499
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mAudioService:Landroid/media/AudioService;

    invoke-virtual {v1}, Landroid/media/AudioService;->getRemoteStreamVolume()I

    move-result v1

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 500
    :cond_2
    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    .line 503
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->isStreamMute(I)Z

    move-result v0

    goto :goto_0
.end method

.method private listenToRingerMode()V
    .locals 3

    .prologue
    .line 480
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 481
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 482
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/oppo/view/VolumePanel$4;

    invoke-direct {v2, p0}, Lcom/oppo/view/VolumePanel$4;-><init>(Lcom/oppo/view/VolumePanel;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 493
    return-void
.end method

.method private reorderSliders(I)V
    .locals 4
    .parameter "activeStreamType"

    .prologue
    .line 595
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 596
    sget-boolean v1, Lcom/oppo/view/VolumePanel;->LOGD:Z

    if-eqz v1, :cond_0

    const-string v1, "jinpeng/VolumePanel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reorderSliders() activeStreamType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    :cond_0
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/oppo/view/VolumePanel$StreamControl;

    .line 598
    .local v0, active:Lcom/oppo/view/VolumePanel$StreamControl;
    if-nez v0, :cond_1

    .line 599
    const-string v1, "VolumePanel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Missing stream type! - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    const/4 v1, -0x1

    iput v1, p0, Lcom/oppo/view/VolumePanel;->mActiveStreamType:I

    .line 609
    :goto_0
    invoke-direct {p0}, Lcom/oppo/view/VolumePanel;->addOtherVolumes()V

    .line 614
    return-void

    .line 602
    :cond_1
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    iget-object v2, v0, Lcom/oppo/view/VolumePanel$StreamControl;->group:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 603
    iput p1, p0, Lcom/oppo/view/VolumePanel;->mActiveStreamType:I

    .line 605
    invoke-direct {p0, v0}, Lcom/oppo/view/VolumePanel;->updateSlider(Lcom/oppo/view/VolumePanel$StreamControl;)V

    .line 606
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mOppoVolumeTitle:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/oppo/view/VolumePanel$StreamControl;->streamTitle:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private resetTimeout()V
    .locals 3

    .prologue
    const/4 v0, 0x5

    .line 1303
    invoke-virtual {p0, v0}, Lcom/oppo/view/VolumePanel;->removeMessages(I)V

    .line 1304
    invoke-virtual {p0, v0}, Lcom/oppo/view/VolumePanel;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0xbb8

    invoke-virtual {p0, v0, v1, v2}, Lcom/oppo/view/VolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1305
    return-void
.end method

.method private ringImageClick()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1424
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 1425
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 1431
    :goto_0
    return-void

    .line 1426
    :cond_0
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    if-nez v0, :cond_1

    .line 1427
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setRingerMode(I)V

    goto :goto_0

    .line 1429
    :cond_1
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setRingerMode(I)V

    goto :goto_0
.end method

.method private setMusicIcon(II)V
    .locals 4
    .parameter "resId"
    .parameter "resMuteId"

    .prologue
    const/4 v3, 0x3

    .line 1191
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/oppo/view/VolumePanel$StreamControl;

    .line 1192
    .local v0, sc:Lcom/oppo/view/VolumePanel$StreamControl;
    if-eqz v0, :cond_0

    .line 1193
    iput p1, v0, Lcom/oppo/view/VolumePanel$StreamControl;->iconRes:I

    .line 1194
    iput p2, v0, Lcom/oppo/view/VolumePanel$StreamControl;->iconMuteRes:I

    .line 1195
    iget-object v2, v0, Lcom/oppo/view/VolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    invoke-direct {p0, v3}, Lcom/oppo/view/VolumePanel;->getStreamVolume(I)I

    move-result v1

    if-nez v1, :cond_1

    iget v1, v0, Lcom/oppo/view/VolumePanel$StreamControl;->iconMuteRes:I

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1197
    :cond_0
    return-void

    .line 1195
    :cond_1
    iget v1, v0, Lcom/oppo/view/VolumePanel$StreamControl;->iconRes:I

    goto :goto_0
.end method

.method private setStreamVolume(III)V
    .locals 1
    .parameter "streamType"
    .parameter "index"
    .parameter "flags"

    .prologue
    .line 528
    const/16 v0, -0x64

    if-ne p1, v0, :cond_0

    .line 529
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p2, p3}, Landroid/media/AudioManager;->setMasterVolume(II)V

    .line 535
    :goto_0
    return-void

    .line 530
    :cond_0
    const/16 v0, -0xc8

    if-ne p1, v0, :cond_1

    .line 531
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mAudioService:Landroid/media/AudioService;

    invoke-virtual {v0, p2}, Landroid/media/AudioService;->setRemoteStreamVolume(I)V

    goto :goto_0

    .line 533
    :cond_1
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1, p2, p3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_0
.end method

.method private updateSlider(Lcom/oppo/view/VolumePanel$StreamControl;)V
    .locals 6
    .parameter "sc"

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 633
    if-nez p1, :cond_0

    .line 663
    :goto_0
    return-void

    .line 637
    :cond_0
    sget-boolean v1, Lcom/oppo/view/VolumePanel;->LOGD:Z

    if-eqz v1, :cond_1

    const-string v1, "jinpeng/VolumePanel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateSlider() type="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mActiveStreamType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/oppo/view/VolumePanel;->mActiveStreamType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 639
    :cond_1
    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    invoke-direct {p0, v1}, Lcom/oppo/view/VolumePanel;->isMuted(I)Z

    move-result v0

    .line 640
    .local v0, muted:Z
    iget-object v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    iget v2, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    invoke-direct {p0, v2}, Lcom/oppo/view/VolumePanel;->getStreamVolume(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/OppoSeekBar;->setProgress(I)V

    .line 641
    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    iget v2, p0, Lcom/oppo/view/VolumePanel;->mActiveStreamType:I

    if-ne v1, v2, :cond_2

    .line 642
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mOppoVolumeProgress:Lcom/oppo/widget/OppoVolumeProgress;

    iget v2, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    invoke-direct {p0, v2}, Lcom/oppo/view/VolumePanel;->getStreamVolume(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x64

    iget v3, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    invoke-direct {p0, v3}, Lcom/oppo/view/VolumePanel;->getStreamMaxVolume(I)I

    move-result v3

    div-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/oppo/widget/OppoVolumeProgress;->updateProgress(I)V

    .line 645
    :cond_2
    invoke-direct {p0, p1}, Lcom/oppo/view/VolumePanel;->updateVolumeIcon(Lcom/oppo/view/VolumePanel$StreamControl;)V

    .line 646
    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    if-ne v1, v5, :cond_3

    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    if-ne v1, v4, :cond_3

    .line 648
    iget-object v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    const v2, 0xc080499

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 654
    :goto_1
    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    const/16 v2, -0xc8

    if-ne v1, v2, :cond_6

    .line 657
    iget-object v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-virtual {v1, v4}, Landroid/widget/OppoSeekBar;->setEnabled(Z)V

    goto :goto_0

    .line 649
    :cond_3
    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    if-ne v1, v5, :cond_5

    .line 650
    iget-object v2, p1, Lcom/oppo/view/VolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    invoke-direct {p0, v5}, Lcom/oppo/view/VolumePanel;->getStreamVolume(I)I

    move-result v1

    if-nez v1, :cond_4

    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->iconMuteRes:I

    :goto_2
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_4
    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->iconRes:I

    goto :goto_2

    .line 652
    :cond_5
    invoke-virtual {p0, p1}, Lcom/oppo/view/VolumePanel;->changeStreamIcon(Lcom/oppo/view/VolumePanel$StreamControl;)V

    goto :goto_1

    .line 658
    :cond_6
    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    iget-object v2, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2}, Landroid/media/AudioManager;->getMasterStreamType()I

    move-result v2

    if-eq v1, v2, :cond_7

    if-eqz v0, :cond_7

    .line 659
    iget-object v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/OppoSeekBar;->setEnabled(Z)V

    goto/16 :goto_0

    .line 661
    :cond_7
    iget-object v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-virtual {v1, v4}, Landroid/widget/OppoSeekBar;->setEnabled(Z)V

    goto/16 :goto_0
.end method

.method private updateStates()V
    .locals 5

    .prologue
    .line 686
    sget-boolean v3, Lcom/oppo/view/VolumePanel;->LOGD:Z

    if-eqz v3, :cond_0

    const-string v3, "jinpeng/VolumePanel"

    const-string v4, "updateStates()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    :cond_0
    iget-object v3, p0, Lcom/oppo/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 688
    .local v0, count:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    if-ge v1, v0, :cond_2

    .line 689
    iget-object v3, p0, Lcom/oppo/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/oppo/view/VolumePanel$StreamControl;

    .line 696
    .local v2, sc:Lcom/oppo/view/VolumePanel$StreamControl;
    if-eqz v2, :cond_1

    .line 697
    invoke-direct {p0, v2}, Lcom/oppo/view/VolumePanel;->updateSlider(Lcom/oppo/view/VolumePanel$StreamControl;)V

    .line 688
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 701
    .end local v2           #sc:Lcom/oppo/view/VolumePanel$StreamControl;
    :cond_2
    return-void
.end method

.method private updateVolumeIcon(Lcom/oppo/view/VolumePanel$StreamControl;)V
    .locals 7
    .parameter "sc"

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    const/4 v4, 0x3

    .line 1387
    sget-boolean v1, Lcom/oppo/view/VolumePanel;->LOGD:Z

    if-eqz v1, :cond_0

    const-string v1, "jinpeng/VolumePanel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateVolumeIcon() streamType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mActiveStreamType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/oppo/view/VolumePanel;->mActiveStreamType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1389
    :cond_0
    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    iget v2, p0, Lcom/oppo/view/VolumePanel;->mActiveStreamType:I

    if-ne v1, v2, :cond_1

    if-nez p1, :cond_2

    .line 1421
    :cond_1
    :goto_0
    return-void

    .line 1392
    :cond_2
    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    if-ne v1, v5, :cond_3

    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    if-ne v1, v6, :cond_3

    .line 1394
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mOppoVolumeIcon:Landroid/widget/ImageView;

    const v2, 0xc0804b9

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 1395
    :cond_3
    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    if-ne v1, v5, :cond_5

    .line 1396
    iget-object v2, p0, Lcom/oppo/view/VolumePanel;->mOppoVolumeIcon:Landroid/widget/ImageView;

    invoke-direct {p0, v5}, Lcom/oppo/view/VolumePanel;->getStreamVolume(I)I

    move-result v1

    if-nez v1, :cond_4

    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->circleIconMuteRes:I

    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_4
    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->circleIconRes:I

    goto :goto_1

    .line 1399
    :cond_5
    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    if-ne v1, v4, :cond_7

    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v4}, Landroid/media/AudioManager;->getDevicesForStream(I)I

    move-result v1

    and-int/lit16 v1, v1, 0x380

    if-eqz v1, :cond_7

    .line 1404
    iget-object v2, p0, Lcom/oppo/view/VolumePanel;->mOppoVolumeIcon:Landroid/widget/ImageView;

    invoke-direct {p0, v4}, Lcom/oppo/view/VolumePanel;->getStreamVolume(I)I

    move-result v1

    if-nez v1, :cond_6

    const v1, 0xc0804bd

    :goto_2
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_6
    const v1, 0xc0804bc

    goto :goto_2

    .line 1407
    :cond_7
    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    if-eq v1, v4, :cond_8

    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_8

    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    const/16 v2, 0xa

    if-eq v1, v2, :cond_8

    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_8

    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    const/16 v2, -0x64

    if-eq v1, v2, :cond_8

    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    if-eq v1, v6, :cond_8

    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    if-nez v1, :cond_a

    .line 1414
    :cond_8
    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    invoke-direct {p0, v1}, Lcom/oppo/view/VolumePanel;->getStreamVolume(I)I

    move-result v0

    .line 1415
    .local v0, volume:I
    iget-object v2, p0, Lcom/oppo/view/VolumePanel;->mOppoVolumeIcon:Landroid/widget/ImageView;

    if-nez v0, :cond_9

    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->circleIconMuteRes:I

    :goto_3
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_9
    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->circleIconRes:I

    goto :goto_3

    .line 1416
    .end local v0           #volume:I
    :cond_a
    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_1

    .line 1417
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mOppoVolumeIcon:Landroid/widget/ImageView;

    iget v2, p1, Lcom/oppo/view/VolumePanel$StreamControl;->circleIconRes:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0
.end method

.method private volumeImageClick(I)V
    .locals 3
    .parameter "streamType"

    .prologue
    .line 1434
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 1435
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "action_media_volume_mode_changed"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1439
    :cond_0
    :goto_0
    return-void

    .line 1436
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1437
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "action_system_volume_mode_changed"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public changeStreamIcon(Lcom/oppo/view/VolumePanel$StreamControl;)V
    .locals 3
    .parameter "sc"

    .prologue
    .line 1444
    if-eqz p1, :cond_1

    .line 1445
    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    const/16 v2, 0xa

    if-eq v1, v2, :cond_0

    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    const/4 v2, 0x6

    if-eq v1, v2, :cond_0

    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    const/16 v2, -0x64

    if-eq v1, v2, :cond_0

    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1452
    :cond_0
    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    invoke-direct {p0, v1}, Lcom/oppo/view/VolumePanel;->getStreamVolume(I)I

    move-result v0

    .line 1453
    .local v0, volume:I
    iget-object v2, p1, Lcom/oppo/view/VolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    if-nez v0, :cond_2

    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->iconMuteRes:I

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1454
    invoke-direct {p0, p1}, Lcom/oppo/view/VolumePanel;->updateVolumeIcon(Lcom/oppo/view/VolumePanel$StreamControl;)V

    .line 1457
    .end local v0           #volume:I
    :cond_1
    return-void

    .line 1453
    .restart local v0       #volume:I
    :cond_2
    iget v1, p1, Lcom/oppo/view/VolumePanel$StreamControl;->iconRes:I

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "msg"

    .prologue
    .line 1236
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1300
    :cond_0
    :goto_0
    return-void

    .line 1239
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v1, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {p0, v0, v1}, Lcom/oppo/view/VolumePanel;->onVolumeChanged(II)V

    goto :goto_0

    .line 1244
    :pswitch_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v1, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {p0, v0, v1}, Lcom/oppo/view/VolumePanel;->onMuteChanged(II)V

    goto :goto_0

    .line 1249
    :pswitch_2
    invoke-virtual {p0}, Lcom/oppo/view/VolumePanel;->onFreeResources()V

    goto :goto_0

    .line 1254
    :pswitch_3
    invoke-virtual {p0}, Lcom/oppo/view/VolumePanel;->onStopSounds()V

    goto :goto_0

    .line 1259
    :pswitch_4
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v1, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {p0, v0, v1}, Lcom/oppo/view/VolumePanel;->onPlaySound(II)V

    goto :goto_0

    .line 1264
    :pswitch_5
    invoke-virtual {p0}, Lcom/oppo/view/VolumePanel;->onVibrate()V

    goto :goto_0

    .line 1269
    :pswitch_6
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1270
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 1271
    const/4 v0, -0x1

    iput v0, p0, Lcom/oppo/view/VolumePanel;->mActiveStreamType:I

    goto :goto_0

    .line 1276
    :pswitch_7
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1277
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/oppo/view/VolumePanel;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0x12c

    invoke-virtual {p0, v0, v1, v2}, Lcom/oppo/view/VolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1278
    invoke-direct {p0}, Lcom/oppo/view/VolumePanel;->updateStates()V

    goto :goto_0

    .line 1284
    :pswitch_8
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v1, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {p0, v0, v1}, Lcom/oppo/view/VolumePanel;->onRemoteVolumeChanged(II)V

    goto :goto_0

    .line 1289
    :pswitch_9
    invoke-virtual {p0}, Lcom/oppo/view/VolumePanel;->onRemoteVolumeUpdateIfShown()V

    goto :goto_0

    .line 1293
    :pswitch_a
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v1, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {p0, v0, v1}, Lcom/oppo/view/VolumePanel;->onSliderVisibilityChanged(II)V

    goto :goto_0

    .line 1297
    :pswitch_b
    invoke-virtual {p0}, Lcom/oppo/view/VolumePanel;->onDisplaySafeVolumeWarning()V

    goto :goto_0

    .line 1236
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_1
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 6
    .parameter "buttonView"
    .parameter "isChecked"

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x0

    .line 1490
    if-eqz p2, :cond_2

    .line 1491
    const/4 v0, 0x2

    .line 1492
    .local v0, baseStreamType:I
    invoke-direct {p0, v5}, Lcom/oppo/view/VolumePanel;->getStreamVolume(I)I

    move-result v2

    .line 1493
    .local v2, progress:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    sget-object v4, Lcom/oppo/view/VolumePanel;->STREAMS:[Lcom/oppo/view/VolumePanel$StreamResources;

    array-length v4, v4

    if-ge v1, v4, :cond_2

    .line 1495
    sget-object v4, Lcom/oppo/view/VolumePanel;->STREAMS:[Lcom/oppo/view/VolumePanel$StreamResources;

    aget-object v4, v4, v1

    iget-boolean v4, v4, Lcom/oppo/view/VolumePanel$StreamResources;->show:Z

    if-eqz v4, :cond_0

    sget-object v4, Lcom/oppo/view/VolumePanel;->STREAMS:[Lcom/oppo/view/VolumePanel$StreamResources;

    aget-object v4, v4, v1

    iget v4, v4, Lcom/oppo/view/VolumePanel$StreamResources;->streamType:I

    if-ne v4, v5, :cond_1

    .line 1493
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1498
    :cond_1
    sget-object v4, Lcom/oppo/view/VolumePanel;->STREAMS:[Lcom/oppo/view/VolumePanel$StreamResources;

    aget-object v4, v4, v1

    iget v4, v4, Lcom/oppo/view/VolumePanel$StreamResources;->streamType:I

    invoke-direct {p0, v4, v2, v3}, Lcom/oppo/view/VolumePanel;->setStreamVolume(III)V

    goto :goto_1

    .line 1501
    .end local v0           #baseStreamType:I
    .end local v1           #i:I
    .end local v2           #progress:I
    :cond_2
    iget-object v4, p0, Lcom/oppo/view/VolumePanel;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "oppo_uniform_volume_settings_enabled"

    if-eqz p2, :cond_3

    const/4 v3, 0x1

    :cond_3
    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1503
    invoke-direct {p0}, Lcom/oppo/view/VolumePanel;->resetTimeout()V

    .line 1504
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7
    .parameter "v"

    .prologue
    .line 1355
    iget-object v4, p0, Lcom/oppo/view/VolumePanel;->mOppoVolumeDisk:Landroid/view/ViewGroup;

    if-ne p1, v4, :cond_1

    .line 1356
    invoke-direct {p0}, Lcom/oppo/view/VolumePanel;->expand()V

    .line 1383
    :cond_0
    invoke-direct {p0}, Lcom/oppo/view/VolumePanel;->resetTimeout()V

    .line 1384
    return-void

    .line 1358
    :cond_1
    sget-object v4, Lcom/oppo/view/VolumePanel;->STREAMS:[Lcom/oppo/view/VolumePanel$StreamResources;

    array-length v4, v4

    add-int/lit8 v1, v4, -0x1

    .local v1, i:I
    :goto_0
    if-ltz v1, :cond_0

    .line 1359
    sget-object v4, Lcom/oppo/view/VolumePanel;->STREAMS:[Lcom/oppo/view/VolumePanel$StreamResources;

    aget-object v3, v4, v1

    .line 1360
    .local v3, streamRes:Lcom/oppo/view/VolumePanel$StreamResources;
    iget-object v4, p0, Lcom/oppo/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    iget v5, v3, Lcom/oppo/view/VolumePanel$StreamResources;->streamType:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/oppo/view/VolumePanel$StreamControl;

    .line 1361
    .local v2, sc:Lcom/oppo/view/VolumePanel$StreamControl;
    if-eqz v2, :cond_2

    .line 1362
    iget v4, v3, Lcom/oppo/view/VolumePanel$StreamResources;->streamType:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    iget-object v4, v2, Lcom/oppo/view/VolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    if-ne p1, v4, :cond_3

    .line 1363
    invoke-direct {p0}, Lcom/oppo/view/VolumePanel;->ringImageClick()V

    .line 1358
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 1364
    :cond_3
    iget-object v4, v2, Lcom/oppo/view/VolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    if-ne p1, v4, :cond_2

    iget v4, v3, Lcom/oppo/view/VolumePanel$StreamResources;->streamType:I

    invoke-direct {p0, v4}, Lcom/oppo/view/VolumePanel;->isMuted(I)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1371
    iget-object v4, p0, Lcom/oppo/view/VolumePanel;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "oppo_uniform_volume_settings_enabled"

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 1373
    .local v0, enabled:I
    const/4 v4, 0x1

    if-ne v0, v4, :cond_4

    .line 1374
    invoke-direct {p0}, Lcom/oppo/view/VolumePanel;->ringImageClick()V

    goto :goto_1

    .line 1376
    :cond_4
    iget v4, v3, Lcom/oppo/view/VolumePanel$StreamResources;->streamType:I

    invoke-direct {p0, v4}, Lcom/oppo/view/VolumePanel;->volumeImageClick(I)V

    goto :goto_1
.end method

.method protected onDisplaySafeVolumeWarning()V
    .locals 6

    .prologue
    .line 1081
    sget-object v3, Lcom/oppo/view/VolumePanel;->sConfirmSafeVolumeLock:Ljava/lang/Object;

    monitor-enter v3

    .line 1082
    :try_start_0
    sget-object v2, Lcom/oppo/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    if-eqz v2, :cond_0

    .line 1083
    monitor-exit v3

    .line 1154
    :goto_0
    return-void

    .line 1085
    :cond_0
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/oppo/view/VolumePanel;->mContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0xc0404c7

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v4, 0x1040013

    new-instance v5, Lcom/oppo/view/VolumePanel$6;

    invoke-direct {v5, p0}, Lcom/oppo/view/VolumePanel$6;-><init>(Lcom/oppo/view/VolumePanel;)V

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v4, 0x1040009

    new-instance v5, Lcom/oppo/view/VolumePanel$5;

    invoke-direct {v5, p0}, Lcom/oppo/view/VolumePanel$5;-><init>(Lcom/oppo/view/VolumePanel;)V

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v4, 0x1010355

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    sput-object v2, Lcom/oppo/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    .line 1125
    iget-object v2, p0, Lcom/oppo/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    if-eqz v2, :cond_5

    .line 1126
    iget-object v2, p0, Lcom/oppo/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/oppo/view/VolumePanel$StreamControl;

    .line 1127
    .local v0, sc:Lcom/oppo/view/VolumePanel$StreamControl;
    if-eqz v0, :cond_4

    .line 1128
    new-instance v1, Lcom/oppo/view/VolumePanel$WarningDialogReceiver;

    iget-object v2, p0, Lcom/oppo/view/VolumePanel;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/oppo/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    iget-object v5, v0, Lcom/oppo/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-direct {v1, v2, v4, v5}, Lcom/oppo/view/VolumePanel$WarningDialogReceiver;-><init>(Landroid/content/Context;Landroid/app/Dialog;Landroid/widget/OppoSeekBar;)V

    .line 1135
    .end local v0           #sc:Lcom/oppo/view/VolumePanel$StreamControl;
    .local v1, warning:Lcom/oppo/view/VolumePanel$WarningDialogReceiver;
    :goto_1
    iget-object v2, p0, Lcom/oppo/view/VolumePanel;->mSafeVolumeWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/oppo/view/VolumePanel;->mPowerManager:Landroid/os/PowerManager;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/oppo/view/VolumePanel;->mVibrator:Landroid/os/Vibrator;

    if-eqz v2, :cond_2

    .line 1136
    iget-object v2, p0, Lcom/oppo/view/VolumePanel;->mSafeVolumeWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/oppo/view/VolumePanel;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1137
    iget-object v2, p0, Lcom/oppo/view/VolumePanel;->mSafeVolumeWakeLock:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v4, 0x64

    invoke-virtual {v2, v4, v5}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 1139
    :cond_1
    iget-object v2, p0, Lcom/oppo/view/VolumePanel;->mVibrator:Landroid/os/Vibrator;

    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v4, v5}, Landroid/os/Vibrator;->vibrate(J)V

    .line 1141
    :cond_2
    iget-object v2, p0, Lcom/oppo/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    if-eqz v2, :cond_3

    .line 1142
    iget-object v2, p0, Lcom/oppo/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/oppo/view/VolumePanel$StreamControl;

    .line 1143
    .restart local v0       #sc:Lcom/oppo/view/VolumePanel$StreamControl;
    if-eqz v0, :cond_3

    .line 1144
    const-string v2, "jinpeng/VolumePanel"

    const-string v4, "post_setEnabled_true"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1145
    iget-object v2, v0, Lcom/oppo/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/widget/OppoSeekBar;->setSafeMediaVolumeEnabled(Z)V

    .line 1149
    .end local v0           #sc:Lcom/oppo/view/VolumePanel$StreamControl;
    :cond_3
    sget-object v2, Lcom/oppo/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1150
    sget-object v2, Lcom/oppo/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v4, 0x7d9

    invoke-virtual {v2, v4}, Landroid/view/Window;->setType(I)V

    .line 1152
    sget-object v2, Lcom/oppo/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 1153
    monitor-exit v3

    goto/16 :goto_0

    .end local v1           #warning:Lcom/oppo/view/VolumePanel$WarningDialogReceiver;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 1130
    .restart local v0       #sc:Lcom/oppo/view/VolumePanel$StreamControl;
    :cond_4
    :try_start_1
    new-instance v1, Lcom/oppo/view/VolumePanel$WarningDialogReceiver;

    iget-object v2, p0, Lcom/oppo/view/VolumePanel;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/oppo/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    invoke-direct {v1, v2, v4}, Lcom/oppo/view/VolumePanel$WarningDialogReceiver;-><init>(Landroid/content/Context;Landroid/app/Dialog;)V

    .restart local v1       #warning:Lcom/oppo/view/VolumePanel$WarningDialogReceiver;
    goto :goto_1

    .line 1133
    .end local v0           #sc:Lcom/oppo/view/VolumePanel$StreamControl;
    .end local v1           #warning:Lcom/oppo/view/VolumePanel$WarningDialogReceiver;
    :cond_5
    new-instance v1, Lcom/oppo/view/VolumePanel$WarningDialogReceiver;

    iget-object v2, p0, Lcom/oppo/view/VolumePanel;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/oppo/view/VolumePanel;->sConfirmSafeVolumeDialog:Landroid/app/AlertDialog;

    invoke-direct {v1, v2, v4}, Lcom/oppo/view/VolumePanel$WarningDialogReceiver;-><init>(Landroid/content/Context;Landroid/app/Dialog;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .restart local v1       #warning:Lcom/oppo/view/VolumePanel$WarningDialogReceiver;
    goto :goto_1
.end method

.method protected onFreeResources()V
    .locals 3

    .prologue
    .line 1200
    monitor-enter p0

    .line 1201
    :try_start_0
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    .local v0, i:I
    :goto_0
    if-ltz v0, :cond_1

    .line 1202
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 1203
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/media/ToneGenerator;->release()V

    .line 1205
    :cond_0
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 1201
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1207
    :cond_1
    monitor-exit p0

    .line 1208
    return-void

    .line 1207
    .end local v0           #i:I
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected onMuteChanged(II)V
    .locals 4
    .parameter "streamType"
    .parameter "flags"

    .prologue
    .line 829
    sget-boolean v1, Lcom/oppo/view/VolumePanel;->LOGD:Z

    if-eqz v1, :cond_0

    const-string v1, "jinpeng/VolumePanel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onMuteChanged(streamType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", flags: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 831
    :cond_0
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/oppo/view/VolumePanel$StreamControl;

    .line 832
    .local v0, sc:Lcom/oppo/view/VolumePanel$StreamControl;
    if-eqz v0, :cond_1

    .line 833
    iget-object v2, v0, Lcom/oppo/view/VolumePanel$StreamControl;->icon:Landroid/widget/ImageView;

    iget v1, v0, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    invoke-direct {p0, v1}, Lcom/oppo/view/VolumePanel;->isMuted(I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, v0, Lcom/oppo/view/VolumePanel$StreamControl;->iconMuteRes:I

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 836
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/oppo/view/VolumePanel;->onVolumeChanged(II)V

    .line 837
    return-void

    .line 833
    :cond_2
    iget v1, v0, Lcom/oppo/view/VolumePanel$StreamControl;->iconRes:I

    goto :goto_0
.end method

.method public onOppoSeekBarProgressrFromUserChanged(Landroid/widget/SeekBar;IZ)V
    .locals 4
    .parameter "seekBar"
    .parameter "progress"
    .parameter "fromUser"

    .prologue
    .line 1461
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 1462
    .local v1, tag:Ljava/lang/Object;
    if-eqz p3, :cond_0

    instance-of v2, v1, Lcom/oppo/view/VolumePanel$StreamControl;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 1463
    check-cast v0, Lcom/oppo/view/VolumePanel$StreamControl;

    .line 1464
    .local v0, sc:Lcom/oppo/view/VolumePanel$StreamControl;
    iget v2, v0, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    invoke-direct {p0, v2}, Lcom/oppo/view/VolumePanel;->getStreamVolume(I)I

    move-result v2

    if-eq v2, p2, :cond_0

    const/4 v2, 0x3

    iget v3, v0, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    if-ne v2, v3, :cond_0

    .line 1466
    iget v2, v0, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    const/4 v3, 0x0

    invoke-direct {p0, v2, p2, v3}, Lcom/oppo/view/VolumePanel;->setStreamVolume(III)V

    .line 1469
    .end local v0           #sc:Lcom/oppo/view/VolumePanel$StreamControl;
    :cond_0
    invoke-direct {p0}, Lcom/oppo/view/VolumePanel;->resetTimeout()V

    .line 1470
    return-void
.end method

.method protected onPlaySound(II)V
    .locals 4
    .parameter "streamType"
    .parameter "flags"

    .prologue
    const/4 v2, 0x3

    .line 976
    invoke-virtual {p0, v2}, Lcom/oppo/view/VolumePanel;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 977
    invoke-virtual {p0, v2}, Lcom/oppo/view/VolumePanel;->removeMessages(I)V

    .line 979
    invoke-virtual {p0}, Lcom/oppo/view/VolumePanel;->onStopSounds()V

    .line 982
    :cond_0
    monitor-enter p0

    .line 983
    :try_start_0
    invoke-direct {p0, p1}, Lcom/oppo/view/VolumePanel;->getOrCreateToneGenerator(I)Landroid/media/ToneGenerator;

    move-result-object v0

    .line 984
    .local v0, toneGen:Landroid/media/ToneGenerator;
    if-eqz v0, :cond_1

    .line 985
    const/16 v1, 0x21

    const/16 v2, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/media/ToneGenerator;->startTone(II)Z

    .line 986
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/oppo/view/VolumePanel;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x96

    invoke-virtual {p0, v1, v2, v3}, Lcom/oppo/view/VolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 988
    :cond_1
    monitor-exit p0

    .line 989
    return-void

    .line 988
    .end local v0           #toneGen:Landroid/media/ToneGenerator;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 5
    .parameter "seekBar"
    .parameter "progress"
    .parameter "fromUser"

    .prologue
    .line 1314
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 1315
    .local v1, tag:Ljava/lang/Object;
    if-eqz p3, :cond_1

    instance-of v2, v1, Lcom/oppo/view/VolumePanel$StreamControl;

    if-eqz v2, :cond_1

    .line 1316
    sget-boolean v2, Lcom/oppo/view/VolumePanel;->LOGD:Z

    if-eqz v2, :cond_0

    const-string v2, "jinpeng/VolumePanel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onProgressChanged() progress="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object v0, v1

    .line 1317
    check-cast v0, Lcom/oppo/view/VolumePanel$StreamControl;

    .line 1318
    .local v0, sc:Lcom/oppo/view/VolumePanel$StreamControl;
    iget v2, v0, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    invoke-direct {p0, v2}, Lcom/oppo/view/VolumePanel;->getStreamVolume(I)I

    move-result v2

    if-eq v2, p2, :cond_1

    .line 1319
    iget v2, v0, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    const/4 v3, 0x0

    invoke-direct {p0, v2, p2, v3}, Lcom/oppo/view/VolumePanel;->setStreamVolume(III)V

    .line 1322
    .end local v0           #sc:Lcom/oppo/view/VolumePanel$StreamControl;
    :cond_1
    invoke-direct {p0}, Lcom/oppo/view/VolumePanel;->resetTimeout()V

    .line 1323
    return-void
.end method

.method protected onRemoteVolumeChanged(II)V
    .locals 6
    .parameter "streamType"
    .parameter "flags"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x2

    const/16 v3, -0xc8

    .line 1018
    sget-boolean v0, Lcom/oppo/view/VolumePanel;->LOGD:Z

    if-eqz v0, :cond_0

    const-string v0, "jinpeng/VolumePanel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRemoteVolumeChanged(stream:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", flags: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1020
    :cond_0
    and-int/lit8 v0, p2, 0x1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1021
    :cond_1
    monitor-enter p0

    .line 1022
    :try_start_0
    iget v0, p0, Lcom/oppo/view/VolumePanel;->mActiveStreamType:I

    if-eq v0, v3, :cond_2

    .line 1023
    const/16 v0, -0xc8

    invoke-direct {p0, v0}, Lcom/oppo/view/VolumePanel;->reorderSliders(I)V

    .line 1025
    :cond_2
    const/16 v0, -0xc8

    invoke-virtual {p0, v0, p2}, Lcom/oppo/view/VolumePanel;->onShowVolumeChanged(II)V

    .line 1026
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1031
    :cond_3
    :goto_0
    and-int/lit8 v0, p2, 0x4

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/oppo/view/VolumePanel;->mRingIsSilent:Z

    if-nez v0, :cond_4

    .line 1032
    invoke-virtual {p0, v4}, Lcom/oppo/view/VolumePanel;->removeMessages(I)V

    .line 1033
    invoke-virtual {p0, v4, p1, p2}, Lcom/oppo/view/VolumePanel;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0x12c

    invoke-virtual {p0, v0, v1, v2}, Lcom/oppo/view/VolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1036
    :cond_4
    and-int/lit8 v0, p2, 0x8

    if-eqz v0, :cond_5

    .line 1037
    invoke-virtual {p0, v4}, Lcom/oppo/view/VolumePanel;->removeMessages(I)V

    .line 1038
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/oppo/view/VolumePanel;->removeMessages(I)V

    .line 1039
    invoke-virtual {p0}, Lcom/oppo/view/VolumePanel;->onStopSounds()V

    .line 1042
    :cond_5
    invoke-virtual {p0, v5}, Lcom/oppo/view/VolumePanel;->removeMessages(I)V

    .line 1043
    invoke-virtual {p0, v5}, Lcom/oppo/view/VolumePanel;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0x2710

    invoke-virtual {p0, v0, v1, v2}, Lcom/oppo/view/VolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1045
    invoke-direct {p0}, Lcom/oppo/view/VolumePanel;->resetTimeout()V

    .line 1046
    return-void

    .line 1026
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1028
    :cond_6
    sget-boolean v0, Lcom/oppo/view/VolumePanel;->LOGD:Z

    if-eqz v0, :cond_3

    const-string v0, "jinpeng/VolumePanel"

    const-string v1, "not calling onShowVolumeChanged(), no FLAG_SHOW_UI or no UI"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onRemoteVolumeUpdateIfShown()V
    .locals 3

    .prologue
    const/16 v2, -0xc8

    .line 1049
    sget-boolean v0, Lcom/oppo/view/VolumePanel;->LOGD:Z

    if-eqz v0, :cond_0

    const-string v0, "jinpeng/VolumePanel"

    const-string v1, "onRemoteVolumeUpdateIfShown()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1050
    :cond_0
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/oppo/view/VolumePanel;->mActiveStreamType:I

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 1053
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/oppo/view/VolumePanel;->onShowVolumeChanged(II)V

    .line 1055
    :cond_1
    return-void
.end method

.method protected onShowVolumeChanged(II)V
    .locals 13
    .parameter "streamType"
    .parameter "flags"

    .prologue
    const v12, 0xc08049b

    const v11, 0xc080493

    const/4 v10, 0x0

    const/16 v9, -0xc8

    const/4 v8, 0x1

    .line 840
    invoke-direct {p0, p1}, Lcom/oppo/view/VolumePanel;->getStreamVolume(I)I

    move-result v0

    .line 842
    .local v0, index:I
    iput-boolean v10, p0, Lcom/oppo/view/VolumePanel;->mRingIsSilent:Z

    .line 844
    sget-boolean v5, Lcom/oppo/view/VolumePanel;->LOGD:Z

    if-eqz v5, :cond_0

    .line 845
    const-string v5, "jinpeng/VolumePanel"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onShowVolumeChanged(streamType: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", flags: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "), index: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 851
    :cond_0
    invoke-direct {p0, p1}, Lcom/oppo/view/VolumePanel;->getStreamMaxVolume(I)I

    move-result v1

    .line 853
    .local v1, max:I
    sparse-switch p1, :sswitch_data_0

    .line 924
    :cond_1
    :goto_0
    :sswitch_0
    iget-object v5, p0, Lcom/oppo/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/oppo/view/VolumePanel$StreamControl;

    .line 925
    .local v3, sc:Lcom/oppo/view/VolumePanel$StreamControl;
    if-eqz v3, :cond_5

    .line 926
    iget-object v5, v3, Lcom/oppo/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-virtual {v5}, Landroid/widget/OppoSeekBar;->getMax()I

    move-result v5

    if-eq v5, v1, :cond_2

    .line 927
    iget-object v5, v3, Lcom/oppo/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-virtual {v5, v1}, Landroid/widget/OppoSeekBar;->setMax(I)V

    .line 930
    :cond_2
    iget-object v5, v3, Lcom/oppo/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-virtual {v5, v0}, Landroid/widget/OppoSeekBar;->setProgress(I)V

    .line 931
    sget-boolean v5, Lcom/oppo/view/VolumePanel;->LOGD:Z

    if-eqz v5, :cond_3

    const-string v5, "jinpeng/VolumePanel"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onShowVolumeChanged mActiveStreamType="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/oppo/view/VolumePanel;->mActiveStreamType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", streamType="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 933
    :cond_3
    iget v5, p0, Lcom/oppo/view/VolumePanel;->mActiveStreamType:I

    if-ne p1, v5, :cond_4

    .line 934
    iget-object v5, p0, Lcom/oppo/view/VolumePanel;->mOppoVolumeProgress:Lcom/oppo/widget/OppoVolumeProgress;

    mul-int/lit8 v6, v0, 0x64

    div-int/2addr v6, v1

    invoke-virtual {v5, v6}, Lcom/oppo/widget/OppoVolumeProgress;->updateProgress(I)V

    .line 936
    :cond_4
    iget-object v5, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v5}, Landroid/media/AudioManager;->getMasterStreamType()I

    move-result v5

    if-eq p1, v5, :cond_9

    if-eq p1, v9, :cond_9

    invoke-direct {p0, p1}, Lcom/oppo/view/VolumePanel;->isMuted(I)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 939
    iget-object v5, v3, Lcom/oppo/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-virtual {v5, v10}, Landroid/widget/OppoSeekBar;->setEnabled(Z)V

    .line 945
    :goto_1
    invoke-virtual {p0, v3}, Lcom/oppo/view/VolumePanel;->changeStreamIcon(Lcom/oppo/view/VolumePanel$StreamControl;)V

    .line 949
    :cond_5
    iget-object v5, p0, Lcom/oppo/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->isShowing()Z

    move-result v5

    if-nez v5, :cond_6

    .line 950
    if-ne p1, v9, :cond_a

    const/4 v4, -0x1

    .line 952
    .local v4, stream:I
    :goto_2
    iget-object v5, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v5, v4}, Landroid/media/AudioManager;->forceVolumeControlStream(I)V

    .line 953
    iget-object v5, p0, Lcom/oppo/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    iget-object v6, p0, Lcom/oppo/view/VolumePanel;->mView:Landroid/view/View;

    iget-object v7, p0, Lcom/oppo/view/VolumePanel;->lp:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v5, v6, v7}, Landroid/app/Dialog;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 960
    iget-object v5, p0, Lcom/oppo/view/VolumePanel;->mSliderGroup:Landroid/view/ViewGroup;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 961
    iget-object v5, p0, Lcom/oppo/view/VolumePanel;->mOppoVolumeDisk:Landroid/view/ViewGroup;

    invoke-virtual {v5, v10}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 962
    iget-object v5, p0, Lcom/oppo/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->show()V

    .line 966
    .end local v4           #stream:I
    :cond_6
    if-eq p1, v9, :cond_7

    and-int/lit8 v5, p2, 0x10

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/oppo/view/VolumePanel;->mAudioService:Landroid/media/AudioService;

    invoke-virtual {v5, p1}, Landroid/media/AudioService;->isStreamAffectedByRingerMode(I)Z

    move-result v5

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v5}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v5

    if-ne v5, v8, :cond_7

    .line 970
    const/4 v5, 0x4

    invoke-virtual {p0, v5}, Lcom/oppo/view/VolumePanel;->obtainMessage(I)Landroid/os/Message;

    move-result-object v5

    const-wide/16 v6, 0x12c

    invoke-virtual {p0, v5, v6, v7}, Lcom/oppo/view/VolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 972
    :cond_7
    return-void

    .line 857
    .end local v3           #sc:Lcom/oppo/view/VolumePanel$StreamControl;
    :sswitch_1
    iget-object v5, p0, Lcom/oppo/view/VolumePanel;->mContext:Landroid/content/Context;

    invoke-static {v5, v8}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v2

    .line 859
    .local v2, ringuri:Landroid/net/Uri;
    if-nez v2, :cond_1

    .line 860
    iput-boolean v8, p0, Lcom/oppo/view/VolumePanel;->mRingIsSilent:Z

    goto/16 :goto_0

    .line 867
    .end local v2           #ringuri:Landroid/net/Uri;
    :sswitch_2
    iget-object v5, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Landroid/media/AudioManager;->getDevicesForStream(I)I

    move-result v5

    and-int/lit16 v5, v5, 0x380

    if-eqz v5, :cond_8

    .line 871
    const v5, 0xc08045f

    const v6, 0xc080461

    invoke-direct {p0, v5, v6}, Lcom/oppo/view/VolumePanel;->setMusicIcon(II)V

    goto/16 :goto_0

    .line 873
    :cond_8
    invoke-direct {p0, v12, v11}, Lcom/oppo/view/VolumePanel;->setMusicIcon(II)V

    goto/16 :goto_0

    .line 879
    :sswitch_3
    invoke-direct {p0, v12, v11}, Lcom/oppo/view/VolumePanel;->setMusicIcon(II)V

    goto/16 :goto_0

    .line 889
    :sswitch_4
    add-int/lit8 v0, v0, 0x1

    .line 890
    add-int/lit8 v1, v1, 0x1

    .line 891
    goto/16 :goto_0

    .line 899
    :sswitch_5
    iget-object v5, p0, Lcom/oppo/view/VolumePanel;->mContext:Landroid/content/Context;

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v2

    .line 901
    .restart local v2       #ringuri:Landroid/net/Uri;
    if-nez v2, :cond_1

    .line 902
    iput-boolean v8, p0, Lcom/oppo/view/VolumePanel;->mRingIsSilent:Z

    goto/16 :goto_0

    .line 913
    .end local v2           #ringuri:Landroid/net/Uri;
    :sswitch_6
    add-int/lit8 v0, v0, 0x1

    .line 914
    add-int/lit8 v1, v1, 0x1

    .line 915
    goto/16 :goto_0

    .line 919
    :sswitch_7
    sget-boolean v5, Lcom/oppo/view/VolumePanel;->LOGD:Z

    if-eqz v5, :cond_1

    const-string v5, "jinpeng/VolumePanel"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "showing remote volume "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " over "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 941
    .restart local v3       #sc:Lcom/oppo/view/VolumePanel$StreamControl;
    :cond_9
    iget-object v5, v3, Lcom/oppo/view/VolumePanel$StreamControl;->seekbarView:Landroid/widget/OppoSeekBar;

    invoke-virtual {v5, v8}, Landroid/widget/OppoSeekBar;->setEnabled(Z)V

    goto/16 :goto_1

    :cond_a
    move v4, p1

    .line 950
    goto/16 :goto_2

    .line 853
    nop

    :sswitch_data_0
    .sparse-switch
        -0xc8 -> :sswitch_7
        0x0 -> :sswitch_4
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_0
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0xa -> :sswitch_3
    .end sparse-switch
.end method

.method protected declared-synchronized onSliderVisibilityChanged(II)V
    .locals 6
    .parameter "streamType"
    .parameter "visible"

    .prologue
    const/4 v1, 0x1

    .line 1066
    monitor-enter p0

    :try_start_0
    sget-boolean v3, Lcom/oppo/view/VolumePanel;->LOGD:Z

    if-eqz v3, :cond_0

    const-string v3, "jinpeng/VolumePanel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onSliderVisibilityChanged(stream="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", visi="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1067
    :cond_0
    if-ne p2, v1, :cond_2

    .line 1068
    .local v1, isVisible:Z
    :goto_0
    sget-object v3, Lcom/oppo/view/VolumePanel;->STREAMS:[Lcom/oppo/view/VolumePanel$StreamResources;

    array-length v3, v3

    add-int/lit8 v0, v3, -0x1

    .local v0, i:I
    :goto_1
    if-ltz v0, :cond_1

    .line 1069
    sget-object v3, Lcom/oppo/view/VolumePanel;->STREAMS:[Lcom/oppo/view/VolumePanel$StreamResources;

    aget-object v2, v3, v0

    .line 1070
    .local v2, streamRes:Lcom/oppo/view/VolumePanel$StreamResources;
    iget v3, v2, Lcom/oppo/view/VolumePanel$StreamResources;->streamType:I

    if-ne v3, p1, :cond_3

    .line 1071
    iput-boolean v1, v2, Lcom/oppo/view/VolumePanel$StreamResources;->show:Z

    .line 1072
    if-nez v1, :cond_1

    iget v3, p0, Lcom/oppo/view/VolumePanel;->mActiveStreamType:I

    if-ne v3, p1, :cond_1

    .line 1073
    const/4 v3, -0x1

    iput v3, p0, Lcom/oppo/view/VolumePanel;->mActiveStreamType:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1078
    .end local v2           #streamRes:Lcom/oppo/view/VolumePanel$StreamResources;
    :cond_1
    monitor-exit p0

    return-void

    .line 1067
    .end local v0           #i:I
    .end local v1           #isVisible:Z
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 1068
    .restart local v0       #i:I
    .restart local v1       #isVisible:Z
    .restart local v2       #streamRes:Lcom/oppo/view/VolumePanel$StreamResources;
    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 1066
    .end local v0           #i:I
    .end local v1           #isVisible:Z
    .end local v2           #streamRes:Lcom/oppo/view/VolumePanel$StreamResources;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 4
    .parameter "seekBar"

    .prologue
    .line 1326
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 1327
    .local v1, tag:Ljava/lang/Object;
    instance-of v2, v1, Lcom/oppo/view/VolumePanel$StreamControl;

    if-eqz v2, :cond_1

    move-object v0, v1

    .line 1328
    check-cast v0, Lcom/oppo/view/VolumePanel$StreamControl;

    .line 1329
    .local v0, sc:Lcom/oppo/view/VolumePanel$StreamControl;
    iget v2, v0, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    if-eqz v2, :cond_0

    iget v2, v0, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_1

    .line 1331
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/oppo/view/VolumePanel;->mIsTouch:Z

    .line 1334
    .end local v0           #sc:Lcom/oppo/view/VolumePanel$StreamControl;
    :cond_1
    return-void
.end method

.method protected onStopSounds()V
    .locals 4

    .prologue
    .line 993
    monitor-enter p0

    .line 994
    :try_start_0
    invoke-static {}, Landroid/media/AudioSystem;->getNumStreamTypes()I

    move-result v1

    .line 995
    .local v1, numStreamTypes:I
    add-int/lit8 v0, v1, -0x1

    .local v0, i:I
    :goto_0
    if-ltz v0, :cond_1

    .line 996
    iget-object v3, p0, Lcom/oppo/view/VolumePanel;->mToneGenerators:[Landroid/media/ToneGenerator;

    aget-object v2, v3, v0

    .line 997
    .local v2, toneGen:Landroid/media/ToneGenerator;
    if-eqz v2, :cond_0

    .line 998
    invoke-virtual {v2}, Landroid/media/ToneGenerator;->stopTone()V

    .line 995
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1001
    .end local v2           #toneGen:Landroid/media/ToneGenerator;
    :cond_1
    monitor-exit p0

    .line 1002
    return-void

    .line 1001
    .end local v0           #i:I
    .end local v1           #numStreamTypes:I
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 4
    .parameter "seekBar"

    .prologue
    const/16 v3, -0xc8

    .line 1337
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 1338
    .local v1, tag:Ljava/lang/Object;
    instance-of v2, v1, Lcom/oppo/view/VolumePanel$StreamControl;

    if-eqz v2, :cond_2

    move-object v0, v1

    .line 1339
    check-cast v0, Lcom/oppo/view/VolumePanel$StreamControl;

    .line 1344
    .local v0, sc:Lcom/oppo/view/VolumePanel$StreamControl;
    iget v2, v0, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    if-ne v2, v3, :cond_0

    .line 1345
    invoke-direct {p0, v3}, Lcom/oppo/view/VolumePanel;->getStreamVolume(I)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1347
    :cond_0
    iget v2, v0, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    if-eqz v2, :cond_1

    iget v2, v0, Lcom/oppo/view/VolumePanel$StreamControl;->streamType:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_2

    .line 1349
    :cond_1
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/oppo/view/VolumePanel;->mIsTouch:Z

    .line 1352
    .end local v0           #sc:Lcom/oppo/view/VolumePanel$StreamControl;
    :cond_2
    return-void
.end method

.method protected onVibrate()V
    .locals 3

    .prologue
    .line 1007
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 1012
    :goto_0
    return-void

    .line 1011
    :cond_0
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mVibrator:Landroid/os/Vibrator;

    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate(J)V

    goto :goto_0
.end method

.method protected onVolumeChanged(II)V
    .locals 6
    .parameter "streamType"
    .parameter "flags"

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 789
    sget-boolean v1, Lcom/oppo/view/VolumePanel;->LOGD:Z

    if-eqz v1, :cond_0

    const-string v1, "jinpeng/VolumePanel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onVolumeChanged(streamType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", flags: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    :cond_0
    const/4 v0, 0x1

    .line 792
    .local v0, update:Z
    iget-boolean v1, p0, Lcom/oppo/view/VolumePanel;->mIsTouch:Z

    if-ne v1, v4, :cond_2

    if-eqz p1, :cond_1

    const/4 v1, 0x6

    if-ne p1, v1, :cond_2

    .line 794
    :cond_1
    const/4 v0, 0x0

    .line 797
    :cond_2
    if-eqz v0, :cond_4

    .line 798
    monitor-enter p0

    .line 799
    :try_start_0
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 800
    invoke-virtual {p0, p1, p2}, Lcom/oppo/view/VolumePanel;->onShowVolumeChanged(II)V

    .line 807
    :cond_3
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 810
    :cond_4
    and-int/lit8 v1, p2, 0x4

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/oppo/view/VolumePanel;->mRingIsSilent:Z

    if-nez v1, :cond_5

    .line 811
    invoke-virtual {p0, v5}, Lcom/oppo/view/VolumePanel;->removeMessages(I)V

    .line 812
    invoke-virtual {p0, v5, p1, p2}, Lcom/oppo/view/VolumePanel;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x12c

    invoke-virtual {p0, v1, v2, v3}, Lcom/oppo/view/VolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 815
    :cond_5
    and-int/lit8 v1, p2, 0x8

    if-eqz v1, :cond_6

    .line 816
    invoke-virtual {p0, v5}, Lcom/oppo/view/VolumePanel;->removeMessages(I)V

    .line 817
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/oppo/view/VolumePanel;->removeMessages(I)V

    .line 818
    invoke-virtual {p0}, Lcom/oppo/view/VolumePanel;->onStopSounds()V

    .line 821
    :cond_6
    invoke-virtual {p0, v4}, Lcom/oppo/view/VolumePanel;->removeMessages(I)V

    .line 822
    invoke-virtual {p0, v4}, Lcom/oppo/view/VolumePanel;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x2710

    invoke-virtual {p0, v1, v2, v3}, Lcom/oppo/view/VolumePanel;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 824
    invoke-direct {p0}, Lcom/oppo/view/VolumePanel;->resetTimeout()V

    .line 825
    return-void

    .line 801
    :cond_7
    and-int/lit8 v1, p2, 0x1

    if-eqz v1, :cond_3

    .line 802
    :try_start_1
    iget v1, p0, Lcom/oppo/view/VolumePanel;->mActiveStreamType:I

    if-eq v1, p1, :cond_8

    .line 803
    invoke-direct {p0, p1}, Lcom/oppo/view/VolumePanel;->reorderSliders(I)V

    .line 805
    :cond_8
    invoke-virtual {p0, p1, p2}, Lcom/oppo/view/VolumePanel;->onShowVolumeChanged(II)V

    goto :goto_0

    .line 807
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public postDisplaySafeVolumeWarning()V
    .locals 3

    .prologue
    const/16 v2, 0xb

    const/4 v1, 0x0

    .line 778
    invoke-virtual {p0, v2}, Lcom/oppo/view/VolumePanel;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 780
    :goto_0
    return-void

    .line 779
    :cond_0
    invoke-virtual {p0, v2, v1, v1}, Lcom/oppo/view/VolumePanel;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method public postHasNewRemotePlaybackInfo()V
    .locals 2

    .prologue
    const/16 v1, 0x9

    .line 752
    invoke-virtual {p0, v1}, Lcom/oppo/view/VolumePanel;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 756
    :goto_0
    return-void

    .line 755
    :cond_0
    invoke-virtual {p0, v1}, Lcom/oppo/view/VolumePanel;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method public postMasterMuteChanged(I)V
    .locals 1
    .parameter "flags"

    .prologue
    .line 774
    const/16 v0, -0x64

    invoke-virtual {p0, v0, p1}, Lcom/oppo/view/VolumePanel;->postMuteChanged(II)V

    .line 775
    return-void
.end method

.method public postMasterVolumeChanged(I)V
    .locals 1
    .parameter "flags"

    .prologue
    .line 759
    const/16 v0, -0x64

    invoke-virtual {p0, v0, p1}, Lcom/oppo/view/VolumePanel;->postVolumeChanged(II)V

    .line 760
    return-void
.end method

.method public postMuteChanged(II)V
    .locals 1
    .parameter "streamType"
    .parameter "flags"

    .prologue
    .line 763
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/oppo/view/VolumePanel;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 771
    :goto_0
    return-void

    .line 764
    :cond_0
    monitor-enter p0

    .line 765
    :try_start_0
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    if-nez v0, :cond_1

    .line 766
    invoke-direct {p0}, Lcom/oppo/view/VolumePanel;->createSliders()V

    .line 768
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 769
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/oppo/view/VolumePanel;->removeMessages(I)V

    .line 770
    const/4 v0, 0x7

    invoke-virtual {p0, v0, p1, p2}, Lcom/oppo/view/VolumePanel;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 768
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public postRemoteSliderVisibility(Z)V
    .locals 3
    .parameter "visible"

    .prologue
    .line 736
    const/16 v1, 0xa

    const/16 v2, -0xc8

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v1, v2, v0}, Lcom/oppo/view/VolumePanel;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 738
    return-void

    .line 736
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public postRemoteVolumeChanged(II)V
    .locals 2
    .parameter "streamType"
    .parameter "flags"

    .prologue
    const/16 v1, 0x8

    .line 725
    invoke-virtual {p0, v1}, Lcom/oppo/view/VolumePanel;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 733
    :goto_0
    return-void

    .line 726
    :cond_0
    monitor-enter p0

    .line 727
    :try_start_0
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    if-nez v0, :cond_1

    .line 728
    invoke-direct {p0}, Lcom/oppo/view/VolumePanel;->createSliders()V

    .line 730
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 731
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/oppo/view/VolumePanel;->removeMessages(I)V

    .line 732
    invoke-virtual {p0, v1, p1, p2}, Lcom/oppo/view/VolumePanel;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 730
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public postVolumeChanged(II)V
    .locals 4
    .parameter "streamType"
    .parameter "flags"

    .prologue
    .line 704
    sget-boolean v1, Lcom/oppo/view/VolumePanel;->LOGD:Z

    if-eqz v1, :cond_0

    const-string v1, "jinpeng/VolumePanel"

    const-string v2, "postVolumeChanged() ======================="

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 705
    :cond_0
    sget-boolean v1, Lcom/oppo/view/VolumePanel;->LOGD:Z

    if-eqz v1, :cond_1

    const-string v1, "jinpeng/VolumePanel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "postVolumeChanged() streamType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", flags="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 708
    :cond_1
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/oppo/view/VolumePanel;->mActiveStreamType:I

    if-eq p1, v1, :cond_3

    .line 709
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/oppo/view/VolumePanel$StreamControl;

    .line 710
    .local v0, sc:Lcom/oppo/view/VolumePanel$StreamControl;
    if-eqz v0, :cond_2

    iget-boolean v1, v0, Lcom/oppo/view/VolumePanel$StreamControl;->show:Z

    if-nez v1, :cond_3

    .line 722
    .end local v0           #sc:Lcom/oppo/view/VolumePanel$StreamControl;
    :cond_2
    :goto_0
    return-void

    .line 715
    :cond_3
    monitor-enter p0

    .line 716
    :try_start_0
    iget-object v1, p0, Lcom/oppo/view/VolumePanel;->mStreamControls:Ljava/util/HashMap;

    if-nez v1, :cond_4

    .line 717
    invoke-direct {p0}, Lcom/oppo/view/VolumePanel;->createSliders()V

    .line 719
    :cond_4
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 720
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/oppo/view/VolumePanel;->removeMessages(I)V

    .line 721
    const/4 v1, 0x0

    invoke-virtual {p0, v1, p1, p2}, Lcom/oppo/view/VolumePanel;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 719
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public setLayoutDirection(I)V
    .locals 1
    .parameter "layoutDirection"

    .prologue
    .line 475
    iget-object v0, p0, Lcom/oppo/view/VolumePanel;->mPanel:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setLayoutDirection(I)V

    .line 476
    invoke-direct {p0}, Lcom/oppo/view/VolumePanel;->updateStates()V

    .line 477
    return-void
.end method
