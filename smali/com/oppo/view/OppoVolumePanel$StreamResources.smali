.class final enum Lcom/oppo/view/OppoVolumePanel$StreamResources;
.super Ljava/lang/Enum;
.source "OppoVolumePanel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/oppo/view/OppoVolumePanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "StreamResources"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/oppo/view/OppoVolumePanel$StreamResources;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/oppo/view/OppoVolumePanel$StreamResources;

.field public static final enum AlarmStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

.field public static final enum BluetoothSCOStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

.field public static final enum MasterStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

.field public static final enum MediaStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

.field public static final enum NotificationStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

.field public static final enum RemoteStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

.field public static final enum RingerStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

.field public static final enum SystemStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

.field public static final enum VoiceStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;


# instance fields
.field descRes:I

.field iconMuteRes:I

.field iconRes:I

.field show:Z

.field streamType:I


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .prologue
    const/4 v14, 0x4

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 157
    new-instance v0, Lcom/oppo/view/OppoVolumePanel$StreamResources;

    const-string v1, "BluetoothSCOStream"

    const/4 v3, 0x6

    const v4, 0xc04048a

    const v5, 0xc08045f

    const v6, 0xc080461

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/oppo/view/OppoVolumePanel$StreamResources;-><init>(Ljava/lang/String;IIIIIZ)V

    sput-object v0, Lcom/oppo/view/OppoVolumePanel$StreamResources;->BluetoothSCOStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    .line 162
    new-instance v3, Lcom/oppo/view/OppoVolumePanel$StreamResources;

    const-string v4, "RingerStream"

    const v7, 0xc04048b

    const v8, 0xc080494

    const v9, 0xc080495

    move v5, v11

    move v6, v12

    move v10, v11

    invoke-direct/range {v3 .. v10}, Lcom/oppo/view/OppoVolumePanel$StreamResources;-><init>(Ljava/lang/String;IIIIIZ)V

    sput-object v3, Lcom/oppo/view/OppoVolumePanel$StreamResources;->RingerStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    .line 167
    new-instance v3, Lcom/oppo/view/OppoVolumePanel$StreamResources;

    const-string v4, "VoiceStream"

    const v7, 0xc04048c

    const v8, 0xc080460

    const v9, 0xc080460

    move v5, v12

    move v6, v2

    move v10, v2

    invoke-direct/range {v3 .. v10}, Lcom/oppo/view/OppoVolumePanel$StreamResources;-><init>(Ljava/lang/String;IIIIIZ)V

    sput-object v3, Lcom/oppo/view/OppoVolumePanel$StreamResources;->VoiceStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    .line 172
    new-instance v3, Lcom/oppo/view/OppoVolumePanel$StreamResources;

    const-string v4, "AlarmStream"

    const v7, 0xc04048d

    const v8, 0xc080036

    const v9, 0xc080035

    move v5, v13

    move v6, v14

    move v10, v2

    invoke-direct/range {v3 .. v10}, Lcom/oppo/view/OppoVolumePanel$StreamResources;-><init>(Ljava/lang/String;IIIIIZ)V

    sput-object v3, Lcom/oppo/view/OppoVolumePanel$StreamResources;->AlarmStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    .line 177
    new-instance v3, Lcom/oppo/view/OppoVolumePanel$StreamResources;

    const-string v4, "MediaStream"

    const v7, 0xc04048e

    const v8, 0xc08049b

    const v9, 0xc080493

    move v5, v14

    move v6, v13

    move v10, v11

    invoke-direct/range {v3 .. v10}, Lcom/oppo/view/OppoVolumePanel$StreamResources;-><init>(Ljava/lang/String;IIIIIZ)V

    sput-object v3, Lcom/oppo/view/OppoVolumePanel$StreamResources;->MediaStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    .line 182
    new-instance v3, Lcom/oppo/view/OppoVolumePanel$StreamResources;

    const-string v4, "SystemStream"

    const/4 v5, 0x5

    const v7, 0xc040527

    const v8, 0xc080497

    const v9, 0xc080498

    move v6, v11

    move v10, v11

    invoke-direct/range {v3 .. v10}, Lcom/oppo/view/OppoVolumePanel$StreamResources;-><init>(Ljava/lang/String;IIIIIZ)V

    sput-object v3, Lcom/oppo/view/OppoVolumePanel$StreamResources;->SystemStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    .line 187
    new-instance v3, Lcom/oppo/view/OppoVolumePanel$StreamResources;

    const-string v4, "NotificationStream"

    const/4 v5, 0x6

    const/4 v6, 0x5

    const v7, 0xc04048f

    const v8, 0xc080496

    const v9, 0xc08049a

    move v10, v2

    invoke-direct/range {v3 .. v10}, Lcom/oppo/view/OppoVolumePanel$StreamResources;-><init>(Ljava/lang/String;IIIIIZ)V

    sput-object v3, Lcom/oppo/view/OppoVolumePanel$StreamResources;->NotificationStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    .line 200
    new-instance v3, Lcom/oppo/view/OppoVolumePanel$StreamResources;

    const-string v4, "MasterStream"

    const/4 v5, 0x7

    const/16 v6, -0x64

    const v7, 0xc04048e

    const v8, 0xc080497

    const v9, 0xc080498

    move v10, v2

    invoke-direct/range {v3 .. v10}, Lcom/oppo/view/OppoVolumePanel$StreamResources;-><init>(Ljava/lang/String;IIIIIZ)V

    sput-object v3, Lcom/oppo/view/OppoVolumePanel$StreamResources;->MasterStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    .line 205
    new-instance v3, Lcom/oppo/view/OppoVolumePanel$StreamResources;

    const-string v4, "RemoteStream"

    const/16 v5, 0x8

    const/16 v6, -0xc8

    const v7, 0xc04048e

    const v8, 0xc080481

    const v9, 0xc080482

    move v10, v2

    invoke-direct/range {v3 .. v10}, Lcom/oppo/view/OppoVolumePanel$StreamResources;-><init>(Ljava/lang/String;IIIIIZ)V

    sput-object v3, Lcom/oppo/view/OppoVolumePanel$StreamResources;->RemoteStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    .line 156
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/oppo/view/OppoVolumePanel$StreamResources;

    sget-object v1, Lcom/oppo/view/OppoVolumePanel$StreamResources;->BluetoothSCOStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aput-object v1, v0, v2

    sget-object v1, Lcom/oppo/view/OppoVolumePanel$StreamResources;->RingerStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aput-object v1, v0, v11

    sget-object v1, Lcom/oppo/view/OppoVolumePanel$StreamResources;->VoiceStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aput-object v1, v0, v12

    sget-object v1, Lcom/oppo/view/OppoVolumePanel$StreamResources;->AlarmStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aput-object v1, v0, v13

    sget-object v1, Lcom/oppo/view/OppoVolumePanel$StreamResources;->MediaStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aput-object v1, v0, v14

    const/4 v1, 0x5

    sget-object v2, Lcom/oppo/view/OppoVolumePanel$StreamResources;->SystemStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/oppo/view/OppoVolumePanel$StreamResources;->NotificationStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/oppo/view/OppoVolumePanel$StreamResources;->MasterStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/oppo/view/OppoVolumePanel$StreamResources;->RemoteStream:Lcom/oppo/view/OppoVolumePanel$StreamResources;

    aput-object v2, v0, v1

    sput-object v0, Lcom/oppo/view/OppoVolumePanel$StreamResources;->$VALUES:[Lcom/oppo/view/OppoVolumePanel$StreamResources;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIIZ)V
    .locals 0
    .parameter
    .parameter
    .parameter "streamType"
    .parameter "descRes"
    .parameter "iconRes"
    .parameter "iconMuteRes"
    .parameter "show"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIIZ)V"
        }
    .end annotation

    .prologue
    .line 218
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 219
    iput p3, p0, Lcom/oppo/view/OppoVolumePanel$StreamResources;->streamType:I

    .line 220
    iput p4, p0, Lcom/oppo/view/OppoVolumePanel$StreamResources;->descRes:I

    .line 221
    iput p5, p0, Lcom/oppo/view/OppoVolumePanel$StreamResources;->iconRes:I

    .line 222
    iput p6, p0, Lcom/oppo/view/OppoVolumePanel$StreamResources;->iconMuteRes:I

    .line 223
    iput-boolean p7, p0, Lcom/oppo/view/OppoVolumePanel$StreamResources;->show:Z

    .line 224
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/oppo/view/OppoVolumePanel$StreamResources;
    .locals 1
    .parameter "name"

    .prologue
    .line 156
    const-class v0, Lcom/oppo/view/OppoVolumePanel$StreamResources;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/oppo/view/OppoVolumePanel$StreamResources;

    return-object v0
.end method

.method public static values()[Lcom/oppo/view/OppoVolumePanel$StreamResources;
    .locals 1

    .prologue
    .line 156
    sget-object v0, Lcom/oppo/view/OppoVolumePanel$StreamResources;->$VALUES:[Lcom/oppo/view/OppoVolumePanel$StreamResources;

    invoke-virtual {v0}, [Lcom/oppo/view/OppoVolumePanel$StreamResources;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/oppo/view/OppoVolumePanel$StreamResources;

    return-object v0
.end method
