.class public Lcom/oppo/widget/OppoVolumeProgress;
.super Landroid/widget/ImageView;
.source "OppoVolumeProgress.java"


# static fields
.field private static final DBG:Z = false

.field private static final TAG:Ljava/lang/String; = "VolumePanel/OppoVolumeProgress"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFilter:Landroid/graphics/PaintFlagsDrawFilter;

.field private mHeight:I

.field private mLeftProgress:Landroid/graphics/Bitmap;

.field private mPaint:Landroid/graphics/Paint;

.field private mProgress:I

.field private mRightProgress:Landroid/graphics/Bitmap;

.field private mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .parameter "context"

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 46
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 37
    iput-object v0, p0, Lcom/oppo/widget/OppoVolumeProgress;->mLeftProgress:Landroid/graphics/Bitmap;

    .line 38
    iput-object v0, p0, Lcom/oppo/widget/OppoVolumeProgress;->mRightProgress:Landroid/graphics/Bitmap;

    .line 39
    iput-object v0, p0, Lcom/oppo/widget/OppoVolumeProgress;->mPaint:Landroid/graphics/Paint;

    .line 40
    iput-object v0, p0, Lcom/oppo/widget/OppoVolumeProgress;->mFilter:Landroid/graphics/PaintFlagsDrawFilter;

    .line 41
    iput v1, p0, Lcom/oppo/widget/OppoVolumeProgress;->mProgress:I

    .line 42
    iput v1, p0, Lcom/oppo/widget/OppoVolumeProgress;->mWidth:I

    .line 43
    iput v1, p0, Lcom/oppo/widget/OppoVolumeProgress;->mHeight:I

    .line 47
    iput-object p1, p0, Lcom/oppo/widget/OppoVolumeProgress;->mContext:Landroid/content/Context;

    .line 48
    invoke-direct {p0}, Lcom/oppo/widget/OppoVolumeProgress;->init()V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/oppo/widget/OppoVolumeProgress;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 56
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    iput-object v0, p0, Lcom/oppo/widget/OppoVolumeProgress;->mLeftProgress:Landroid/graphics/Bitmap;

    .line 38
    iput-object v0, p0, Lcom/oppo/widget/OppoVolumeProgress;->mRightProgress:Landroid/graphics/Bitmap;

    .line 39
    iput-object v0, p0, Lcom/oppo/widget/OppoVolumeProgress;->mPaint:Landroid/graphics/Paint;

    .line 40
    iput-object v0, p0, Lcom/oppo/widget/OppoVolumeProgress;->mFilter:Landroid/graphics/PaintFlagsDrawFilter;

    .line 41
    iput v1, p0, Lcom/oppo/widget/OppoVolumeProgress;->mProgress:I

    .line 42
    iput v1, p0, Lcom/oppo/widget/OppoVolumeProgress;->mWidth:I

    .line 43
    iput v1, p0, Lcom/oppo/widget/OppoVolumeProgress;->mHeight:I

    .line 57
    iput-object p1, p0, Lcom/oppo/widget/OppoVolumeProgress;->mContext:Landroid/content/Context;

    .line 58
    invoke-direct {p0}, Lcom/oppo/widget/OppoVolumeProgress;->init()V

    .line 59
    return-void
.end method

.method private init()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 62
    iget-object v0, p0, Lcom/oppo/widget/OppoVolumeProgress;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0xc0804b1

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/oppo/widget/OppoVolumeProgress;->mLeftProgress:Landroid/graphics/Bitmap;

    .line 64
    iget-object v0, p0, Lcom/oppo/widget/OppoVolumeProgress;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0xc0804b2

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/oppo/widget/OppoVolumeProgress;->mRightProgress:Landroid/graphics/Bitmap;

    .line 66
    iget-object v0, p0, Lcom/oppo/widget/OppoVolumeProgress;->mLeftProgress:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/oppo/widget/OppoVolumeProgress;->mWidth:I

    .line 67
    iget-object v0, p0, Lcom/oppo/widget/OppoVolumeProgress;->mLeftProgress:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/oppo/widget/OppoVolumeProgress;->mHeight:I

    .line 68
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/oppo/widget/OppoVolumeProgress;->mPaint:Landroid/graphics/Paint;

    .line 69
    new-instance v0, Landroid/graphics/PaintFlagsDrawFilter;

    const/4 v1, 0x3

    invoke-direct {v0, v2, v1}, Landroid/graphics/PaintFlagsDrawFilter;-><init>(II)V

    iput-object v0, p0, Lcom/oppo/widget/OppoVolumeProgress;->mFilter:Landroid/graphics/PaintFlagsDrawFilter;

    .line 72
    iput v2, p0, Lcom/oppo/widget/OppoVolumeProgress;->mProgress:I

    .line 73
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .parameter "canvas"

    .prologue
    const/high16 v7, 0x43b4

    const/4 v6, 0x0

    const/high16 v5, 0x42c8

    const/4 v4, 0x0

    .line 91
    iget v1, p0, Lcom/oppo/widget/OppoVolumeProgress;->mProgress:I

    int-to-float v0, v1

    .line 93
    .local v0, p:F
    const/high16 v1, 0x4248

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_1

    .line 94
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 95
    iget v1, p0, Lcom/oppo/widget/OppoVolumeProgress;->mWidth:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/oppo/widget/OppoVolumeProgress;->mWidth:I

    iget v3, p0, Lcom/oppo/widget/OppoVolumeProgress;->mHeight:I

    invoke-virtual {p1, v1, v6, v2, v3}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 97
    iget-object v1, p0, Lcom/oppo/widget/OppoVolumeProgress;->mFilter:Landroid/graphics/PaintFlagsDrawFilter;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->setDrawFilter(Landroid/graphics/DrawFilter;)V

    .line 98
    iget v1, p0, Lcom/oppo/widget/OppoVolumeProgress;->mWidth:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/oppo/widget/OppoVolumeProgress;->mHeight:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 99
    div-float v1, v0, v5

    mul-float/2addr v1, v7

    const/high16 v2, 0x4334

    add-float/2addr v1, v2

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->rotate(F)V

    .line 100
    iget v1, p0, Lcom/oppo/widget/OppoVolumeProgress;->mWidth:I

    neg-int v1, v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/oppo/widget/OppoVolumeProgress;->mHeight:I

    neg-int v2, v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 101
    iget-object v1, p0, Lcom/oppo/widget/OppoVolumeProgress;->mRightProgress:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/oppo/widget/OppoVolumeProgress;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v4, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 102
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 116
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 117
    return-void

    .line 103
    :cond_1
    cmpg-float v1, v0, v5

    if-gtz v1, :cond_0

    .line 104
    iget-object v1, p0, Lcom/oppo/widget/OppoVolumeProgress;->mRightProgress:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/oppo/widget/OppoVolumeProgress;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v4, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 106
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 107
    iget v1, p0, Lcom/oppo/widget/OppoVolumeProgress;->mWidth:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/oppo/widget/OppoVolumeProgress;->mHeight:I

    invoke-virtual {p1, v6, v6, v1, v2}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 108
    iget-object v1, p0, Lcom/oppo/widget/OppoVolumeProgress;->mFilter:Landroid/graphics/PaintFlagsDrawFilter;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->setDrawFilter(Landroid/graphics/DrawFilter;)V

    .line 109
    iget v1, p0, Lcom/oppo/widget/OppoVolumeProgress;->mWidth:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/oppo/widget/OppoVolumeProgress;->mHeight:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 110
    div-float v1, v0, v5

    mul-float/2addr v1, v7

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->rotate(F)V

    .line 111
    iget v1, p0, Lcom/oppo/widget/OppoVolumeProgress;->mWidth:I

    neg-int v1, v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/oppo/widget/OppoVolumeProgress;->mHeight:I

    neg-int v2, v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 112
    iget-object v1, p0, Lcom/oppo/widget/OppoVolumeProgress;->mLeftProgress:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/oppo/widget/OppoVolumeProgress;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v4, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 113
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 86
    iget v0, p0, Lcom/oppo/widget/OppoVolumeProgress;->mWidth:I

    iget v1, p0, Lcom/oppo/widget/OppoVolumeProgress;->mHeight:I

    invoke-virtual {p0, v0, v1}, Lcom/oppo/widget/OppoVolumeProgress;->setMeasuredDimension(II)V

    .line 87
    return-void
.end method

.method public updateProgress(I)V
    .locals 1
    .parameter "progress"

    .prologue
    .line 77
    iget v0, p0, Lcom/oppo/widget/OppoVolumeProgress;->mProgress:I

    if-eq p1, v0, :cond_0

    .line 78
    iput p1, p0, Lcom/oppo/widget/OppoVolumeProgress;->mProgress:I

    .line 80
    invoke-virtual {p0}, Lcom/oppo/widget/OppoVolumeProgress;->postInvalidate()V

    .line 82
    :cond_0
    return-void
.end method
