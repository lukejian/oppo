.class Lcom/oppo/widget/OppoOptionMenuBar$9;
.super Ljava/lang/Object;
.source "OppoOptionMenuBar.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/oppo/widget/OppoOptionMenuBar;->setElementItemAnimationUp(Landroid/widget/TextView;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/oppo/widget/OppoOptionMenuBar;

.field final synthetic val$elementItem:Landroid/widget/TextView;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/oppo/widget/OppoOptionMenuBar;ILandroid/widget/TextView;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2339
    iput-object p1, p0, Lcom/oppo/widget/OppoOptionMenuBar$9;->this$0:Lcom/oppo/widget/OppoOptionMenuBar;

    iput p2, p0, Lcom/oppo/widget/OppoOptionMenuBar$9;->val$position:I

    iput-object p3, p0, Lcom/oppo/widget/OppoOptionMenuBar$9;->val$elementItem:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 3
    .parameter "animation"

    .prologue
    .line 2360
    iget-object v0, p0, Lcom/oppo/widget/OppoOptionMenuBar$9;->this$0:Lcom/oppo/widget/OppoOptionMenuBar;

    #getter for: Lcom/oppo/widget/OppoOptionMenuBar;->isEnterAminEnd:[Z
    invoke-static {v0}, Lcom/oppo/widget/OppoOptionMenuBar;->access$1700(Lcom/oppo/widget/OppoOptionMenuBar;)[Z

    move-result-object v0

    iget v1, p0, Lcom/oppo/widget/OppoOptionMenuBar$9;->val$position:I

    const/4 v2, 0x1

    aput-boolean v2, v0, v1

    .line 2361
    iget-object v0, p0, Lcom/oppo/widget/OppoOptionMenuBar$9;->this$0:Lcom/oppo/widget/OppoOptionMenuBar;

    #getter for: Lcom/oppo/widget/OppoOptionMenuBar;->mCurrentShowAnim:[Landroid/animation/Animator;
    invoke-static {v0}, Lcom/oppo/widget/OppoOptionMenuBar;->access$1800(Lcom/oppo/widget/OppoOptionMenuBar;)[Landroid/animation/Animator;

    move-result-object v0

    iget v1, p0, Lcom/oppo/widget/OppoOptionMenuBar$9;->val$position:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 2362
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .parameter "animation"

    .prologue
    .line 2354
    iget-object v0, p0, Lcom/oppo/widget/OppoOptionMenuBar$9;->this$0:Lcom/oppo/widget/OppoOptionMenuBar;

    #getter for: Lcom/oppo/widget/OppoOptionMenuBar;->isEnterAminEnd:[Z
    invoke-static {v0}, Lcom/oppo/widget/OppoOptionMenuBar;->access$1700(Lcom/oppo/widget/OppoOptionMenuBar;)[Z

    move-result-object v0

    iget v1, p0, Lcom/oppo/widget/OppoOptionMenuBar$9;->val$position:I

    const/4 v2, 0x1

    aput-boolean v2, v0, v1

    .line 2355
    iget-object v0, p0, Lcom/oppo/widget/OppoOptionMenuBar$9;->this$0:Lcom/oppo/widget/OppoOptionMenuBar;

    #getter for: Lcom/oppo/widget/OppoOptionMenuBar;->mCurrentShowAnim:[Landroid/animation/Animator;
    invoke-static {v0}, Lcom/oppo/widget/OppoOptionMenuBar;->access$1800(Lcom/oppo/widget/OppoOptionMenuBar;)[Landroid/animation/Animator;

    move-result-object v0

    iget v1, p0, Lcom/oppo/widget/OppoOptionMenuBar$9;->val$position:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 2356
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 3
    .parameter "animation"

    .prologue
    .line 2349
    iget-object v0, p0, Lcom/oppo/widget/OppoOptionMenuBar$9;->this$0:Lcom/oppo/widget/OppoOptionMenuBar;

    #getter for: Lcom/oppo/widget/OppoOptionMenuBar;->isEnterAminEnd:[Z
    invoke-static {v0}, Lcom/oppo/widget/OppoOptionMenuBar;->access$1700(Lcom/oppo/widget/OppoOptionMenuBar;)[Z

    move-result-object v0

    iget v1, p0, Lcom/oppo/widget/OppoOptionMenuBar$9;->val$position:I

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    .line 2350
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 4
    .parameter "animation"

    .prologue
    const/high16 v3, 0x3f80

    .line 2342
    iget-object v0, p0, Lcom/oppo/widget/OppoOptionMenuBar$9;->this$0:Lcom/oppo/widget/OppoOptionMenuBar;

    #getter for: Lcom/oppo/widget/OppoOptionMenuBar;->isEnterAminEnd:[Z
    invoke-static {v0}, Lcom/oppo/widget/OppoOptionMenuBar;->access$1700(Lcom/oppo/widget/OppoOptionMenuBar;)[Z

    move-result-object v0

    iget v1, p0, Lcom/oppo/widget/OppoOptionMenuBar$9;->val$position:I

    const/4 v2, 0x0

    aput-boolean v2, v0, v1

    .line 2343
    iget-object v0, p0, Lcom/oppo/widget/OppoOptionMenuBar$9;->val$elementItem:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setScaleX(F)V

    .line 2344
    iget-object v0, p0, Lcom/oppo/widget/OppoOptionMenuBar$9;->val$elementItem:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setScaleY(F)V

    .line 2345
    return-void
.end method
