.class final Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;
.super Landroid/widget/BaseAdapter;
.source "OppoResolverActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/app/OppoResolverActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ResolveListAdapter"
.end annotation


# instance fields
.field private final mBaseResolveList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentResolveList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mInitialIntents:[Landroid/content/Intent;

.field private final mIntent:Landroid/content/Intent;

.field private final mLaunchedFromUid:I

.field private mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/internal/app/OppoResolverActivity;


# direct methods
.method public constructor <init>(Lcom/android/internal/app/OppoResolverActivity;Landroid/content/Context;Landroid/content/Intent;[Landroid/content/Intent;Ljava/util/List;I)V
    .locals 2
    .parameter
    .parameter "context"
    .parameter "intent"
    .parameter "initialIntents"
    .parameter
    .parameter "launchedFromUid"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/Intent;",
            "[",
            "Landroid/content/Intent;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 480
    .local p5, rList:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    iput-object p1, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/OppoResolverActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 481
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p3}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mIntent:Landroid/content/Intent;

    .line 482
    iget-object v0, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mIntent:Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 483
    iput-object p4, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mInitialIntents:[Landroid/content/Intent;

    .line 484
    iput-object p5, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mBaseResolveList:Ljava/util/List;

    .line 485
    iput p6, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mLaunchedFromUid:I

    .line 486
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 487
    invoke-direct {p0}, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->rebuildList()V

    .line 488
    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;)Ljava/util/List;
    .locals 1
    .parameter "x0"

    .prologue
    .line 469
    iget-object v0, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    return-object v0
.end method

.method private final bindView(Landroid/view/View;Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;)V
    .locals 5
    .parameter "view"
    .parameter "info"
    .annotation build Landroid/annotation/OppoHook;
        level = .enum Landroid/annotation/OppoHook$OppoHookType;->CHANGE_CODE:Landroid/annotation/OppoHook$OppoHookType;
        note = "Xiaokang.Feng@Plf.SDK,2013-04-12,Modify for ConvertIcon"
        property = .enum Landroid/annotation/OppoHook$OppoRomType;->ROM:Landroid/annotation/OppoHook$OppoRomType;
    .end annotation

    .prologue
    .line 754
    const v3, 0xc0204a9

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 755
    .local v1, text:Landroid/widget/TextView;
    const v3, 0xc0204aa

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 756
    .local v2, text2:Landroid/widget/TextView;
    const v3, 0xc020434

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 757
    .local v0, icon:Landroid/widget/ImageView;
    iget-object v3, p2, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;->displayLabel:Ljava/lang/CharSequence;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 758
    iget-object v3, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/OppoResolverActivity;

    #getter for: Lcom/android/internal/app/OppoResolverActivity;->mShowExtended:Z
    invoke-static {v3}, Lcom/android/internal/app/OppoResolverActivity;->access$400(Lcom/android/internal/app/OppoResolverActivity;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 759
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 760
    iget-object v3, p2, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;->extendedInfo:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 764
    :goto_0
    iget-object v3, p2, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;->displayIcon:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_0

    .line 771
    iget-object v3, p2, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;->ri:Landroid/content/pm/ResolveInfo;

    iget-object v4, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/OppoResolverActivity;

    #getter for: Lcom/android/internal/app/OppoResolverActivity;->mPm:Landroid/content/pm/PackageManager;
    invoke-static {v4}, Lcom/android/internal/app/OppoResolverActivity;->access$300(Lcom/android/internal/app/OppoResolverActivity;)Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/internal/app/OppoResolverActivity$Injector;->oppoLoadIconForResolveInfo(Landroid/content/pm/ResolveInfo;Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p2, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;->displayIcon:Landroid/graphics/drawable/Drawable;

    .line 774
    :cond_0
    iget-object v3, p2, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;->displayIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 775
    return-void

    .line 762
    :cond_1
    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private processGroup(Ljava/util/List;IILandroid/content/pm/ResolveInfo;Ljava/lang/CharSequence;)V
    .locals 16
    .parameter
    .parameter "start"
    .parameter "end"
    .parameter "ro"
    .parameter "roLabel"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;II",
            "Landroid/content/pm/ResolveInfo;",
            "Ljava/lang/CharSequence;",
            ")V"
        }
    .end annotation

    .prologue
    .line 651
    .local p1, rList:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    sub-int v1, p3, p2

    add-int/lit8 v12, v1, 0x1

    .line 652
    .local v12, num:I
    const/4 v1, 0x1

    if-ne v12, v1, :cond_1

    .line 654
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    new-instance v1, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/OppoResolverActivity;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    invoke-direct/range {v1 .. v6}, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;-><init>(Lcom/android/internal/app/OppoResolverActivity;Landroid/content/pm/ResolveInfo;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    invoke-interface {v15, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 693
    :cond_0
    return-void

    .line 656
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/OppoResolverActivity;

    const/4 v2, 0x1

    #setter for: Lcom/android/internal/app/OppoResolverActivity;->mShowExtended:Z
    invoke-static {v1, v2}, Lcom/android/internal/app/OppoResolverActivity;->access$402(Lcom/android/internal/app/OppoResolverActivity;Z)Z

    .line 657
    const/4 v14, 0x0

    .line 658
    .local v14, usePkg:Z
    move-object/from16 v0, p4

    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/OppoResolverActivity;

    #getter for: Lcom/android/internal/app/OppoResolverActivity;->mPm:Landroid/content/pm/PackageManager;
    invoke-static {v2}, Lcom/android/internal/app/OppoResolverActivity;->access$300(Lcom/android/internal/app/OppoResolverActivity;)Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v13

    .line 659
    .local v13, startApp:Ljava/lang/CharSequence;
    if-nez v13, :cond_2

    .line 660
    const/4 v14, 0x1

    .line 662
    :cond_2
    if-nez v14, :cond_5

    .line 664
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 666
    .local v7, duplicates:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/CharSequence;>;"
    invoke-virtual {v7, v13}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 667
    add-int/lit8 v8, p2, 0x1

    .local v8, j:I
    :goto_0
    move/from16 v0, p3

    if-gt v8, v0, :cond_4

    .line 668
    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/pm/ResolveInfo;

    .line 669
    .local v10, jRi:Landroid/content/pm/ResolveInfo;
    iget-object v1, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/OppoResolverActivity;

    #getter for: Lcom/android/internal/app/OppoResolverActivity;->mPm:Landroid/content/pm/PackageManager;
    invoke-static {v2}, Lcom/android/internal/app/OppoResolverActivity;->access$300(Lcom/android/internal/app/OppoResolverActivity;)Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v9

    .line 670
    .local v9, jApp:Ljava/lang/CharSequence;
    if-eqz v9, :cond_3

    invoke-virtual {v7, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 671
    :cond_3
    const/4 v14, 0x1

    .line 678
    .end local v9           #jApp:Ljava/lang/CharSequence;
    .end local v10           #jRi:Landroid/content/pm/ResolveInfo;
    :cond_4
    invoke-virtual {v7}, Ljava/util/HashSet;->clear()V

    .line 680
    .end local v7           #duplicates:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/CharSequence;>;"
    .end local v8           #j:I
    :cond_5
    move/from16 v11, p2

    .local v11, k:I
    :goto_1
    move/from16 v0, p3

    if-gt v11, v0, :cond_0

    .line 681
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 682
    .local v3, add:Landroid/content/pm/ResolveInfo;
    if-eqz v14, :cond_7

    .line 684
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    new-instance v1, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/OppoResolverActivity;

    iget-object v4, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v4, p5

    invoke-direct/range {v1 .. v6}, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;-><init>(Lcom/android/internal/app/OppoResolverActivity;Landroid/content/pm/ResolveInfo;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    invoke-interface {v15, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 680
    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 674
    .end local v3           #add:Landroid/content/pm/ResolveInfo;
    .end local v11           #k:I
    .restart local v7       #duplicates:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/CharSequence;>;"
    .restart local v8       #j:I
    .restart local v9       #jApp:Ljava/lang/CharSequence;
    .restart local v10       #jRi:Landroid/content/pm/ResolveInfo;
    :cond_6
    invoke-virtual {v7, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 667
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 688
    .end local v7           #duplicates:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/CharSequence;>;"
    .end local v8           #j:I
    .end local v9           #jApp:Ljava/lang/CharSequence;
    .end local v10           #jRi:Landroid/content/pm/ResolveInfo;
    .restart local v3       #add:Landroid/content/pm/ResolveInfo;
    .restart local v11       #k:I
    :cond_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    new-instance v1, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/OppoResolverActivity;

    iget-object v4, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/OppoResolverActivity;

    #getter for: Lcom/android/internal/app/OppoResolverActivity;->mPm:Landroid/content/pm/PackageManager;
    invoke-static {v5}, Lcom/android/internal/app/OppoResolverActivity;->access$300(Lcom/android/internal/app/OppoResolverActivity;)Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v4, p5

    invoke-direct/range {v1 .. v6}, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;-><init>(Lcom/android/internal/app/OppoResolverActivity;Landroid/content/pm/ResolveInfo;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    invoke-interface {v15, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method private rebuildList()V
    .locals 23
    .annotation build Landroid/annotation/OppoHook;
        level = .enum Landroid/annotation/OppoHook$OppoHookType;->CHANGE_CODE:Landroid/annotation/OppoHook$OppoHookType;
        note = "JiFeng.Tan@PEXP.Midware.Midware, Add for Mexico requirement"
        property = .enum Landroid/annotation/OppoHook$OppoRomType;->ROM:Landroid/annotation/OppoHook$OppoRomType;
    .end annotation

    .prologue
    .line 512
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mBaseResolveList:Ljava/util/List;

    if-eqz v1, :cond_2

    .line 513
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mBaseResolveList:Ljava/util/List;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    .line 537
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    if-eqz v1, :cond_13

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v13

    .local v13, N:I
    if-lez v13, :cond_13

    .line 540
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/pm/ResolveInfo;

    .line 541
    .local v11, r0:Landroid/content/pm/ResolveInfo;
    const/16 v18, 0x1

    .local v18, i:I
    :goto_0
    move/from16 v0, v18

    if-ge v0, v13, :cond_6

    .line 542
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    move/from16 v0, v18

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 549
    .local v3, ri:Landroid/content/pm/ResolveInfo;
    iget v1, v11, Landroid/content/pm/ResolveInfo;->priority:I

    iget v2, v3, Landroid/content/pm/ResolveInfo;->priority:I

    if-ne v1, v2, :cond_1

    iget-boolean v1, v11, Landroid/content/pm/ResolveInfo;->isDefault:Z

    iget-boolean v2, v3, Landroid/content/pm/ResolveInfo;->isDefault:Z

    if-eq v1, v2, :cond_5

    .line 551
    :cond_1
    :goto_1
    move/from16 v0, v18

    if-ge v0, v13, :cond_5

    .line 552
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    move/from16 v0, v18

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 553
    add-int/lit8 v13, v13, -0x1

    goto :goto_1

    .line 515
    .end local v3           #ri:Landroid/content/pm/ResolveInfo;
    .end local v11           #r0:Landroid/content/pm/ResolveInfo;
    .end local v13           #N:I
    .end local v18           #i:I
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/OppoResolverActivity;

    #getter for: Lcom/android/internal/app/OppoResolverActivity;->mPm:Landroid/content/pm/PackageManager;
    invoke-static {v1}, Lcom/android/internal/app/OppoResolverActivity;->access$300(Lcom/android/internal/app/OppoResolverActivity;)Landroid/content/pm/PackageManager;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mIntent:Landroid/content/Intent;

    const/high16 v5, 0x1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/OppoResolverActivity;

    #getter for: Lcom/android/internal/app/OppoResolverActivity;->mAlwaysUseOption:Z
    invoke-static {v1}, Lcom/android/internal/app/OppoResolverActivity;->access$200(Lcom/android/internal/app/OppoResolverActivity;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/16 v1, 0x40

    :goto_2
    or-int/2addr v1, v5

    invoke-virtual {v2, v4, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    .line 523
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 524
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v18, v1, -0x1

    .restart local v18       #i:I
    :goto_3
    if-ltz v18, :cond_0

    .line 525
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    move/from16 v0, v18

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    iget-object v14, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 526
    .local v14, ai:Landroid/content/pm/ActivityInfo;
    iget-object v1, v14, Landroid/content/pm/ActivityInfo;->permission:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mLaunchedFromUid:I

    iget-object v4, v14, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    iget-boolean v5, v14, Landroid/content/pm/ActivityInfo;->exported:Z

    invoke-static {v1, v2, v4, v5}, Landroid/app/ActivityManager;->checkComponentPermission(Ljava/lang/String;IIZ)I

    move-result v17

    .line 529
    .local v17, granted:I
    if-eqz v17, :cond_3

    .line 531
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    move/from16 v0, v18

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 524
    :cond_3
    add-int/lit8 v18, v18, -0x1

    goto :goto_3

    .line 515
    .end local v14           #ai:Landroid/content/pm/ActivityInfo;
    .end local v17           #granted:I
    .end local v18           #i:I
    :cond_4
    const/4 v1, 0x0

    goto :goto_2

    .line 541
    .restart local v3       #ri:Landroid/content/pm/ResolveInfo;
    .restart local v11       #r0:Landroid/content/pm/ResolveInfo;
    .restart local v13       #N:I
    .restart local v18       #i:I
    :cond_5
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_0

    .line 557
    .end local v3           #ri:Landroid/content/pm/ResolveInfo;
    :cond_6
    const/4 v1, 0x1

    if-le v13, v1, :cond_7

    .line 558
    new-instance v20, Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/OppoResolverActivity;

    #getter for: Lcom/android/internal/app/OppoResolverActivity;->mPm:Landroid/content/pm/PackageManager;
    invoke-static {v1}, Lcom/android/internal/app/OppoResolverActivity;->access$300(Lcom/android/internal/app/OppoResolverActivity;)Landroid/content/pm/PackageManager;

    move-result-object v1

    move-object/from16 v0, v20

    invoke-direct {v0, v1}, Landroid/content/pm/ResolveInfo$DisplayNameComparator;-><init>(Landroid/content/pm/PackageManager;)V

    .line 560
    .local v20, rComparator:Landroid/content/pm/ResolveInfo$DisplayNameComparator;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    move-object/from16 v0, v20

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 563
    .end local v20           #rComparator:Landroid/content/pm/ResolveInfo$DisplayNameComparator;
    :cond_7
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    .line 566
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mInitialIntents:[Landroid/content/Intent;

    if-eqz v1, :cond_b

    .line 567
    const/16 v18, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mInitialIntents:[Landroid/content/Intent;

    array-length v1, v1

    move/from16 v0, v18

    if-ge v0, v1, :cond_b

    .line 568
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mInitialIntents:[Landroid/content/Intent;

    aget-object v6, v1, v18

    .line 569
    .local v6, ii:Landroid/content/Intent;
    if-nez v6, :cond_8

    .line 567
    :goto_5
    add-int/lit8 v18, v18, 0x1

    goto :goto_4

    .line 572
    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/OppoResolverActivity;

    invoke-virtual {v1}, Lcom/android/internal/app/OppoResolverActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v6, v1, v2}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    move-result-object v14

    .line 574
    .restart local v14       #ai:Landroid/content/pm/ActivityInfo;
    if-nez v14, :cond_9

    .line 575
    const-string v1, "ResolverActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No activity found for "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 579
    :cond_9
    new-instance v3, Landroid/content/pm/ResolveInfo;

    invoke-direct {v3}, Landroid/content/pm/ResolveInfo;-><init>()V

    .line 580
    .restart local v3       #ri:Landroid/content/pm/ResolveInfo;
    iput-object v14, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 581
    instance-of v1, v6, Landroid/content/pm/LabeledIntent;

    if-eqz v1, :cond_a

    move-object/from16 v19, v6

    .line 582
    check-cast v19, Landroid/content/pm/LabeledIntent;

    .line 583
    .local v19, li:Landroid/content/pm/LabeledIntent;
    invoke-virtual/range {v19 .. v19}, Landroid/content/pm/LabeledIntent;->getSourcePackage()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Landroid/content/pm/ResolveInfo;->resolvePackageName:Ljava/lang/String;

    .line 584
    invoke-virtual/range {v19 .. v19}, Landroid/content/pm/LabeledIntent;->getLabelResource()I

    move-result v1

    iput v1, v3, Landroid/content/pm/ResolveInfo;->labelRes:I

    .line 585
    invoke-virtual/range {v19 .. v19}, Landroid/content/pm/LabeledIntent;->getNonLocalizedLabel()Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v3, Landroid/content/pm/ResolveInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    .line 586
    invoke-virtual/range {v19 .. v19}, Landroid/content/pm/LabeledIntent;->getIconResource()I

    move-result v1

    iput v1, v3, Landroid/content/pm/ResolveInfo;->icon:I

    .line 588
    .end local v19           #li:Landroid/content/pm/LabeledIntent;
    :cond_a
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    new-instance v1, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/OppoResolverActivity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/OppoResolverActivity;

    invoke-virtual {v4}, Lcom/android/internal/app/OppoResolverActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;-><init>(Lcom/android/internal/app/OppoResolverActivity;Landroid/content/pm/ResolveInfo;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 595
    .end local v3           #ri:Landroid/content/pm/ResolveInfo;
    .end local v6           #ii:Landroid/content/Intent;
    .end local v14           #ai:Landroid/content/pm/ActivityInfo;
    :cond_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    .end local v11           #r0:Landroid/content/pm/ResolveInfo;
    check-cast v11, Landroid/content/pm/ResolveInfo;

    .line 596
    .restart local v11       #r0:Landroid/content/pm/ResolveInfo;
    const/4 v9, 0x0

    .line 597
    .local v9, start:I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/OppoResolverActivity;

    #getter for: Lcom/android/internal/app/OppoResolverActivity;->mPm:Landroid/content/pm/PackageManager;
    invoke-static {v1}, Lcom/android/internal/app/OppoResolverActivity;->access$300(Lcom/android/internal/app/OppoResolverActivity;)Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v11, v1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v12

    .line 598
    .local v12, r0Label:Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/OppoResolverActivity;

    const/4 v2, 0x0

    #setter for: Lcom/android/internal/app/OppoResolverActivity;->mShowExtended:Z
    invoke-static {v1, v2}, Lcom/android/internal/app/OppoResolverActivity;->access$402(Lcom/android/internal/app/OppoResolverActivity;Z)Z

    .line 599
    const/16 v18, 0x1

    :goto_6
    move/from16 v0, v18

    if-ge v0, v13, :cond_f

    .line 600
    if-nez v12, :cond_c

    .line 601
    iget-object v1, v11, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v12, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 603
    :cond_c
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    move/from16 v0, v18

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 604
    .restart local v3       #ri:Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/OppoResolverActivity;

    #getter for: Lcom/android/internal/app/OppoResolverActivity;->mPm:Landroid/content/pm/PackageManager;
    invoke-static {v1}, Lcom/android/internal/app/OppoResolverActivity;->access$300(Lcom/android/internal/app/OppoResolverActivity;)Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v22

    .line 605
    .local v22, riLabel:Ljava/lang/CharSequence;
    if-nez v22, :cond_d

    .line 606
    iget-object v1, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v22, v0

    .line 608
    :cond_d
    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 599
    :goto_7
    add-int/lit8 v18, v18, 0x1

    goto :goto_6

    .line 611
    :cond_e
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    add-int/lit8 v10, v18, -0x1

    move-object/from16 v7, p0

    invoke-direct/range {v7 .. v12}, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->processGroup(Ljava/util/List;IILandroid/content/pm/ResolveInfo;Ljava/lang/CharSequence;)V

    .line 612
    move-object v11, v3

    .line 613
    move-object/from16 v12, v22

    .line 614
    move/from16 v9, v18

    goto :goto_7

    .line 617
    .end local v3           #ri:Landroid/content/pm/ResolveInfo;
    .end local v22           #riLabel:Ljava/lang/CharSequence;
    :cond_f
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mCurrentResolveList:Ljava/util/List;

    add-int/lit8 v10, v13, -0x1

    move-object/from16 v7, p0

    invoke-direct/range {v7 .. v12}, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->processGroup(Ljava/util/List;IILandroid/content/pm/ResolveInfo;Ljava/lang/CharSequence;)V

    .line 620
    const-string v1, "persist.sys.oppo.region"

    const-string v2, "CN"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 621
    .local v21, region:Ljava/lang/String;
    if-eqz v21, :cond_13

    const-string v1, "MX"

    move-object/from16 v0, v21

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mIntent:Landroid/content/Intent;

    if-eqz v1, :cond_13

    const-string v1, "android.intent.action.SEND"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mIntent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 622
    const/16 v18, 0x0

    :goto_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    move/from16 v0, v18

    if-ge v0, v1, :cond_13

    .line 623
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    move/from16 v0, v18

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;

    .line 624
    .local v15, appInfo:Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;
    if-eqz v15, :cond_10

    iget-object v1, v15, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;->ri:Landroid/content/pm/ResolveInfo;

    if-eqz v1, :cond_10

    iget-object v1, v15, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;->ri:Landroid/content/pm/ResolveInfo;

    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v1, :cond_10

    .line 625
    const-string v1, "com.oppo.plugger"

    iget-object v2, v15, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;->ri:Landroid/content/pm/ResolveInfo;

    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 626
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    move/from16 v0, v18

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v15

    .end local v15           #appInfo:Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;
    check-cast v15, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;

    .line 627
    .restart local v15       #appInfo:Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2, v15}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 622
    :cond_10
    :goto_9
    add-int/lit8 v18, v18, 0x1

    goto :goto_8

    .line 628
    :cond_11
    const-string v1, "com.android.mms"

    iget-object v2, v15, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;->ri:Landroid/content/pm/ResolveInfo;

    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 629
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_10

    .line 630
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;

    .line 631
    .local v16, appInfo0:Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;
    const-string v1, "com.oppo.plugger"

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;->ri:Landroid/content/pm/ResolveInfo;

    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 632
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    move/from16 v0, v18

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v15

    .end local v15           #appInfo:Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;
    check-cast v15, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;

    .line 633
    .restart local v15       #appInfo:Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    const/4 v2, 0x1

    invoke-interface {v1, v2, v15}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_9

    .line 635
    :cond_12
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    move/from16 v0, v18

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v15

    .end local v15           #appInfo:Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;
    check-cast v15, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;

    .line 636
    .restart local v15       #appInfo:Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2, v15}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_9

    .line 646
    .end local v9           #start:I
    .end local v11           #r0:Landroid/content/pm/ResolveInfo;
    .end local v12           #r0Label:Ljava/lang/CharSequence;
    .end local v13           #N:I
    .end local v15           #appInfo:Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;
    .end local v16           #appInfo0:Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;
    .end local v18           #i:I
    .end local v21           #region:Ljava/lang/String;
    :cond_13
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 721
    iget-object v0, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter "position"

    .prologue
    .line 725
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 729
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 734
    if-nez p2, :cond_0

    .line 735
    iget-object v3, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v4, 0xc09044f

    const/4 v5, 0x0

    invoke-virtual {v3, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 739
    .local v2, view:Landroid/view/View;
    const v3, 0xc020434

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 740
    .local v0, icon:Landroid/widget/ImageView;
    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 741
    .local v1, lp:Landroid/view/ViewGroup$LayoutParams;
    iget-object v3, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/OppoResolverActivity;

    #getter for: Lcom/android/internal/app/OppoResolverActivity;->mIconSize:I
    invoke-static {v3}, Lcom/android/internal/app/OppoResolverActivity;->access$500(Lcom/android/internal/app/OppoResolverActivity;)I

    move-result v3

    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 745
    .end local v0           #icon:Landroid/widget/ImageView;
    .end local v1           #lp:Landroid/view/ViewGroup$LayoutParams;
    :goto_0
    iget-object v3, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;

    invoke-direct {p0, v2, v3}, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->bindView(Landroid/view/View;Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;)V

    .line 746
    return-object v2

    .line 743
    .end local v2           #view:Landroid/view/View;
    :cond_0
    move-object v2, p2

    .restart local v2       #view:Landroid/view/View;
    goto :goto_0
.end method

.method public handlePackagesChanged()V
    .locals 3

    .prologue
    .line 491
    invoke-virtual {p0}, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->getCount()I

    move-result v1

    .line 492
    .local v1, oldItemCount:I
    invoke-direct {p0}, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->rebuildList()V

    .line 493
    invoke-virtual {p0}, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->notifyDataSetChanged()V

    .line 494
    iget-object v2, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_0

    .line 496
    iget-object v2, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/OppoResolverActivity;

    invoke-virtual {v2}, Lcom/android/internal/app/OppoResolverActivity;->finish()V

    .line 499
    :cond_0
    invoke-virtual {p0}, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->getCount()I

    move-result v0

    .line 500
    .local v0, newItemCount:I
    if-eq v0, v1, :cond_1

    .line 501
    iget-object v2, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->this$0:Lcom/android/internal/app/OppoResolverActivity;

    invoke-virtual {v2}, Lcom/android/internal/app/OppoResolverActivity;->resizeGrid()V

    .line 503
    :cond_1
    return-void
.end method

.method public intentForPosition(I)Landroid/content/Intent;
    .locals 6
    .parameter "position"

    .prologue
    .line 704
    iget-object v3, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    if-nez v3, :cond_0

    .line 705
    const/4 v2, 0x0

    .line 717
    :goto_0
    return-object v2

    .line 708
    :cond_0
    iget-object v3, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;

    .line 710
    .local v1, dri:Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;
    new-instance v2, Landroid/content/Intent;

    iget-object v3, v1, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;->origIntent:Landroid/content/Intent;

    if-eqz v3, :cond_1

    iget-object v3, v1, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;->origIntent:Landroid/content/Intent;

    :goto_1
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 712
    .local v2, intent:Landroid/content/Intent;
    const/high16 v3, 0x300

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 714
    iget-object v3, v1, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;->ri:Landroid/content/pm/ResolveInfo;

    iget-object v0, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 715
    .local v0, ai:Landroid/content/pm/ActivityInfo;
    new-instance v3, Landroid/content/ComponentName;

    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v5, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto :goto_0

    .line 710
    .end local v0           #ai:Landroid/content/pm/ActivityInfo;
    .end local v2           #intent:Landroid/content/Intent;
    :cond_1
    iget-object v3, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mIntent:Landroid/content/Intent;

    goto :goto_1
.end method

.method public resolveInfoForPosition(I)Landroid/content/pm/ResolveInfo;
    .locals 1
    .parameter "position"

    .prologue
    .line 696
    iget-object v0, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 697
    const/4 v0, 0x0

    .line 700
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/internal/app/OppoResolverActivity$ResolveListAdapter;->mList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;

    iget-object v0, v0, Lcom/android/internal/app/OppoResolverActivity$DisplayResolveInfo;->ri:Landroid/content/pm/ResolveInfo;

    goto :goto_0
.end method
