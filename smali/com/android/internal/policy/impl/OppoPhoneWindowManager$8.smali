.class Lcom/android/internal/policy/impl/OppoPhoneWindowManager$8;
.super Landroid/os/Handler;
.source "OppoPhoneWindowManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/OppoPhoneWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/OppoPhoneWindowManager;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;)V
    .locals 0
    .parameter

    .prologue
    .line 1511
    iput-object p1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$8;->this$0:Lcom/android/internal/policy/impl/OppoPhoneWindowManager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .parameter "msg"

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1514
    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    .line 1526
    :goto_0
    return-void

    .line 1516
    :pswitch_0
    iget v5, p1, Landroid/os/Message;->arg1:I

    if-ne v5, v3, :cond_0

    move v1, v3

    .line 1517
    .local v1, isHomeOrLock:Z
    :goto_1
    iget v5, p1, Landroid/os/Message;->arg2:I

    if-ne v5, v3, :cond_1

    move v2, v3

    .line 1519
    .local v2, isSemipermeable:Z
    :goto_2
    iget-object v3, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$8;->this$0:Lcom/android/internal/policy/impl/OppoPhoneWindowManager;

    invoke-virtual {v3, v1, v2}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->sendIsHomeModeIntent(ZZ)V

    goto :goto_0

    .end local v1           #isHomeOrLock:Z
    .end local v2           #isSemipermeable:Z
    :cond_0
    move v1, v4

    .line 1516
    goto :goto_1

    .restart local v1       #isHomeOrLock:Z
    :cond_1
    move v2, v4

    .line 1517
    goto :goto_2

    .line 1522
    .end local v1           #isHomeOrLock:Z
    :pswitch_1
    iget v5, p1, Landroid/os/Message;->arg1:I

    if-ne v5, v3, :cond_2

    move v0, v3

    .line 1523
    .local v0, isCameraShow:Z
    :goto_3
    iget-object v3, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$8;->this$0:Lcom/android/internal/policy/impl/OppoPhoneWindowManager;

    invoke-virtual {v3, v0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->sendIsCameraModeIntent(Z)V

    goto :goto_0

    .end local v0           #isCameraShow:Z
    :cond_2
    move v0, v4

    .line 1522
    goto :goto_3

    .line 1514
    nop

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
