.class Lcom/android/internal/policy/impl/OppoPhoneWindowManager$9;
.super Ljava/lang/Object;
.source "OppoPhoneWindowManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->showBootMessage(Ljava/lang/CharSequence;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/OppoPhoneWindowManager;

.field final synthetic val$msg:Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;Ljava/lang/CharSequence;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1630
    iput-object p1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$9;->this$0:Lcom/android/internal/policy/impl/OppoPhoneWindowManager;

    iput-object p2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$9;->val$msg:Ljava/lang/CharSequence;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1632
    iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$9;->this$0:Lcom/android/internal/policy/impl/OppoPhoneWindowManager;

    iget-object v2, v2, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    if-nez v2, :cond_0

    .line 1633
    iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$9;->this$0:Lcom/android/internal/policy/impl/OppoPhoneWindowManager;

    new-instance v3, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$9$1;

    iget-object v4, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$9;->this$0:Lcom/android/internal/policy/impl/OppoPhoneWindowManager;

    #getter for: Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->access$500(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$9;->this$0:Lcom/android/internal/policy/impl/OppoPhoneWindowManager;

    #getter for: Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->access$500(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getThemeResId()I

    move-result v5

    invoke-direct {v3, p0, v4, v5}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$9$1;-><init>(Lcom/android/internal/policy/impl/OppoPhoneWindowManager$9;Landroid/content/Context;I)V

    iput-object v3, v2, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    .line 1667
    iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$9;->this$0:Lcom/android/internal/policy/impl/OppoPhoneWindowManager;

    iget-object v2, v2, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v6}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 1668
    iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$9;->this$0:Lcom/android/internal/policy/impl/OppoPhoneWindowManager;

    iget-object v2, v2, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x7e5

    invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V

    .line 1670
    iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$9;->this$0:Lcom/android/internal/policy/impl/OppoPhoneWindowManager;

    iget-object v2, v2, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 1671
    .local v0, lp:Landroid/view/WindowManager$LayoutParams;
    const/4 v2, 0x5

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    .line 1672
    iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$9;->this$0:Lcom/android/internal/policy/impl/OppoPhoneWindowManager;

    iget-object v2, v2, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1673
    iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$9;->this$0:Lcom/android/internal/policy/impl/OppoPhoneWindowManager;

    iget-object v2, v2, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v6}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 1674
    iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$9;->this$0:Lcom/android/internal/policy/impl/OppoPhoneWindowManager;

    iget-object v2, v2, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V

    .line 1676
    .end local v0           #lp:Landroid/view/WindowManager$LayoutParams;
    :cond_0
    iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$9;->this$0:Lcom/android/internal/policy/impl/OppoPhoneWindowManager;

    iget-object v2, v2, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    const v3, 0x102000b

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1677
    .local v1, messageView:Landroid/widget/TextView;
    iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$9;->val$msg:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1678
    return-void
.end method
