.class public Lcom/android/internal/policy/impl/OppoPhoneWindowManager;
.super Lcom/android/internal/policy/impl/PhoneWindowManager;
.source "OppoPhoneWindowManager.java"

# interfaces
.implements Landroid/view/OppoWindowManagerPolicy;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/OppoPhoneWindowManager$KeyLockModeReceiver;
    }
.end annotation


# static fields
.field private static final ACTION_END_CALL:Ljava/lang/String; = "android.intent.action.END_CALL"

.field private static final ACTION_KEY_LOCK:Ljava/lang/String; = "com.oppo.intent.action.KEY_LOCK_MODE"

.field private static final ACTION_SCREEN_SHOT:Ljava/lang/String; = "oppo.intent.action.SCREEN_SHOT"

.field private static final CAMERA_PKG:Ljava/lang/String; = "com.oppo.camera"

.field private static final DISMISS_KEYGUARD_NONE:I = 0x0
    .annotation build Landroid/annotation/OppoHook;
        level = .enum Landroid/annotation/OppoHook$OppoHookType;->NEW_FIELD:Landroid/annotation/OppoHook$OppoHookType;
        note = "zhangkai@Plf.DesktopApp.Keyguard add for apklock"
        property = .enum Landroid/annotation/OppoHook$OppoRomType;->OPPO:Landroid/annotation/OppoHook$OppoRomType;
    .end annotation
.end field

.field private static final HOME_KEY_DOUBLE_CLICK_DETECT_TIME:I = 0xfa

.field private static final HOME_KEY_DOUBLE_CLICK_DETECT_TIME_SLEEP:I = 0xc8

.field private static final KEY_LOCK_MODE_HOME:I = 0x3

.field private static final KEY_LOCK_MODE_NORMAL:I = 0x0

.field private static final KEY_LOCK_MODE_POWER:I = 0x1

.field private static final KEY_LOCK_MODE_POWER_HOME:I = 0x2

.field static final KEY_OFFSET_VALUE:I = 0x320

.field private static final LAYER_WALLPAPER:Ljava/lang/String; = "LAYER_WALLPAPER"

.field private static final MAX_WAIT_TIME:I = 0x3e8

.field private static final MSG_ISCAMERAMODE:I = 0x11

.field private static final MSG_ISHOMEMODE:I = 0x10

.field private static final OPPO_IGNORE_INCALL_SCREEN:Ljava/lang/String; = "com.android.phone/com.android.phone.OppoIgnoreInComingCallScreen"

.field private static final OPPO_IGNORE_SPEECH_ASSIST:Ljava/lang/String; = "com.oppo.speechassist"

.field private static final OPPO_INCALL_SCREEN:Ljava/lang/String; = "com.android.phone/com.android.phone.OppoInCallScreen"

.field private static final OPPO_SCREENSHOT_CHORD_DEBOUNCE_DELAY_MILLIS:J = 0x96L

.field private static final PROXIMITY_THRESHOLD:F = 2.0f

.field public static final START_SPEECH_DISABLE:Ljava/lang/String; = "com.oppo.intent.action.START_SPEECH_DISABLE"

.field public static final START_SPEECH_ENABLE:Ljava/lang/String; = "com.oppo.intent.action.START_SPEECH_ENABLE"

.field private static TAG:Ljava/lang/String; = null

.field private static final WAIT_FOR_START_TIME:J = 0x7d0L


# instance fields
.field private FORCE_RESUME_FOR_CHANGING_THEME:J

.field private final LongHomePressedEscaped:Ljava/lang/Runnable;

.field private flashlights:Z

.field private isMute:Z

.field private lasDoubletHomeKeyTime:J

.field private lastHomeKeyTime:J
    .annotation build Landroid/annotation/OppoHook;
        level = .enum Landroid/annotation/OppoHook$OppoHookType;->NEW_FIELD:Landroid/annotation/OppoHook$OppoHookType;
        note = "only for export machine"
        property = .enum Landroid/annotation/OppoHook$OppoRomType;->OPPO:Landroid/annotation/OppoHook$OppoRomType;
    .end annotation
.end field

.field listener:Landroid/telephony/PhoneStateListener;

.field private longPressMenuEnable:Z

.field private mApkLockScreenSyncObj:Ljava/lang/Object;
    .annotation build Landroid/annotation/OppoHook;
        level = .enum Landroid/annotation/OppoHook$OppoHookType;->NEW_FIELD:Landroid/annotation/OppoHook$OppoHookType;
        note = "zhangkai@Plf.DesktopApp.Keyguard add for apklock"
        property = .enum Landroid/annotation/OppoHook$OppoRomType;->OPPO:Landroid/annotation/OppoHook$OppoRomType;
    .end annotation
.end field

.field mApkLockScreens:Ljava/util/ArrayList;
    .annotation build Landroid/annotation/OppoHook;
        level = .enum Landroid/annotation/OppoHook$OppoHookType;->NEW_FIELD:Landroid/annotation/OppoHook$OppoHookType;
        note = "zhangkai@Plf.DesktopApp.Keyguard add for apklock"
        property = .enum Landroid/annotation/OppoHook$OppoRomType;->OPPO:Landroid/annotation/OppoHook$OppoRomType;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/WindowManagerPolicy$WindowState;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurrentKeyMode:I

.field private final mHomeKeyTap:Ljava/lang/Runnable;
    .annotation build Landroid/annotation/OppoHook;
        level = .enum Landroid/annotation/OppoHook$OppoHookType;->CHANGE_CODE:Landroid/annotation/OppoHook$OppoHookType;
        note = "only for export machine"
        property = .enum Landroid/annotation/OppoHook$OppoRomType;->OPPO:Landroid/annotation/OppoHook$OppoRomType;
    .end annotation
.end field

.field private mIsCameraShow:Z

.field private mIsGaussFlag:Z

.field private mIsShowGaussFlag:Z

.field private mIsStatusBarSemipermeable:Z

.field private mIsStatusBarTans:Z

.field private final mKeyLockIntentProcess:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mKeyLockModeReceiver:Lcom/android/internal/policy/impl/OppoPhoneWindowManager$KeyLockModeReceiver;

.field private mLauncherWin:Landroid/view/WindowManagerPolicy$WindowState;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mMyHandler:Landroid/os/Handler;

.field mOppoBaseReceiver:Landroid/content/BroadcastReceiver;

.field private mOppoPowerDownKeyConsumedByScreenshotChord:Z

.field private mOppoPowerKeyTime:J

.field private mOppoPowerKeyTriggered:Z

.field private mOppoScreenshotChordEnabled:Z

.field private mOppoVolumeDownKeyConsumedByScreenshotChord:Z

.field private mOppoVolumeDownKeyTime:J

.field private mOppoVolumeDownKeyTriggered:Z

.field private mOppoVolumeUpKeyTriggered:Z

.field private mPauseForChangingTheme:Z

.field mProximityListener:Landroid/hardware/SensorEventListener;

.field private mProximitySensor:Landroid/hardware/Sensor;

.field private mProximitySensorActive:Z

.field private mProximitySensorEnabled:Z

.field mResumeForChangingTheme:Ljava/lang/Runnable;

.field private mRingingTime:J

#.field public mSensorManager:Landroid/hardware/SensorManager;

.field mShotScreenHelper:Lcom/android/internal/policy/impl/OppoShotScreenHelper;

.field private mTopAppWin:Landroid/view/WindowManagerPolicy$WindowState;

.field private mVolumeKeyPokeUserActivity:Z

.field private mWallpaperLayer:I

.field private mWallpaperWin:Landroid/view/WindowManagerPolicy$WindowState;

.field private object:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 103
    const-string v0, "OppoPhoneWindowManager"

    sput-object v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 101
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;-><init>()V

    .line 111
    iput v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mCurrentKeyMode:I

    .line 114
    new-instance v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$KeyLockModeReceiver;

    invoke-direct {v0, p0, v3}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$KeyLockModeReceiver;-><init>(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;Lcom/android/internal/policy/impl/OppoPhoneWindowManager$1;)V

    iput-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mKeyLockModeReceiver:Lcom/android/internal/policy/impl/OppoPhoneWindowManager$KeyLockModeReceiver;

    .line 120
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->flashlights:Z

    .line 122
    iput-wide v4, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->lasDoubletHomeKeyTime:J

    .line 126
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mKeyLockIntentProcess:Ljava/util/ArrayList;

    .line 138
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoScreenshotChordEnabled:Z

    .line 150
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->isMute:Z

    .line 152
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mPauseForChangingTheme:Z

    .line 153
    const-wide/16 v0, 0x7530

    iput-wide v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->FORCE_RESUME_FOR_CHANGING_THEME:J

    .line 157
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->longPressMenuEnable:Z

    .line 165
    iput-wide v4, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->lastHomeKeyTime:J

    .line 173
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mVolumeKeyPokeUserActivity:Z

    .line 176
    new-instance v0, Lcom/android/internal/policy/impl/OppoShotScreenHelper;

    invoke-direct {v0}, Lcom/android/internal/policy/impl/OppoShotScreenHelper;-><init>()V

    iput-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mShotScreenHelper:Lcom/android/internal/policy/impl/OppoShotScreenHelper;

    .line 179
    iput-wide v4, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mRingingTime:J

    .line 192
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mApkLockScreens:Ljava/util/ArrayList;

    .line 197
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mApkLockScreenSyncObj:Ljava/lang/Object;

    .line 242
    new-instance v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$1;

    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$1;-><init>(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;)V

    iput-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mResumeForChangingTheme:Ljava/lang/Runnable;

    .line 261
    new-instance v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$2;

    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$2;-><init>(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;)V

    iput-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoBaseReceiver:Landroid/content/BroadcastReceiver;

    .line 751
    new-instance v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$3;

    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$3;-><init>(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;)V

    iput-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->LongHomePressedEscaped:Ljava/lang/Runnable;

    .line 1225
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mProximitySensorEnabled:Z

    .line 1226
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mProximitySensorActive:Z

    .line 1227
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->object:Ljava/lang/Object;

    .line 1232
    new-instance v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$5;

    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$5;-><init>(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;)V

    iput-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mProximityListener:Landroid/hardware/SensorEventListener;

    .line 1291
    new-instance v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$6;

    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$6;-><init>(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;)V

    iput-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHomeKeyTap:Ljava/lang/Runnable;

    .line 1453
    new-instance v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$7;

    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$7;-><init>(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;)V

    iput-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->listener:Landroid/telephony/PhoneStateListener;

    .line 1497
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mIsGaussFlag:Z

    .line 1498
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mIsShowGaussFlag:Z

    .line 1499
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mIsStatusBarTans:Z

    .line 1500
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mIsStatusBarSemipermeable:Z

    .line 1501
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mIsCameraShow:Z

    .line 1502
    iput-object v3, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mWallpaperWin:Landroid/view/WindowManagerPolicy$WindowState;

    .line 1503
    iput-object v3, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mLauncherWin:Landroid/view/WindowManagerPolicy$WindowState;

    .line 1504
    iput-object v3, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mTopAppWin:Landroid/view/WindowManagerPolicy$WindowState;

    .line 1507
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mWallpaperLayer:I

    .line 1511
    new-instance v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$8;

    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$8;-><init>(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;)V

    iput-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mMyHandler:Landroid/os/Handler;

    return-void
.end method

.method private RemoveHomeLongPressedR()V
    .locals 2

    .prologue
    .line 776
    iget-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->LongHomePressedEscaped:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasCallbacks(Ljava/lang/Runnable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 777
    sget-object v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    const-string v1, "LongHomePressedR removed()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 778
    iget-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->LongHomePressedEscaped:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 780
    :cond_0
    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;)Ljava/util/ArrayList;
    .locals 1
    .parameter "x0"

    .prologue
    .line 101
    iget-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mKeyLockIntentProcess:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 101
    iget-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->object:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mProximitySensorActive:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mProximitySensorActive:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;)Landroid/hardware/Sensor;
    .locals 1
    .parameter "x0"

    .prologue
    .line 101
    iget-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mProximitySensor:Landroid/hardware/Sensor;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;I)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 101
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->setVideoMode(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mPauseForChangingTheme:Z

    return v0
.end method

.method static synthetic access$302(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mPauseForChangingTheme:Z

    return p1
.end method

.method static synthetic access$400(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;)J
    .locals 2
    .parameter "x0"

    .prologue
    .line 101
    iget-wide v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->FORCE_RESUME_FOR_CHANGING_THEME:J

    return-wide v0
.end method

.method static synthetic access$500(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;)Landroid/content/Context;
    .locals 1
    .parameter "x0"

    .prologue
    .line 101
    iget-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$602(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->isMute:Z

    return p1
.end method

.method static synthetic access$700()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$802(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->longPressMenuEnable:Z

    return p1
.end method

.method static synthetic access$902(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;J)J
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 101
    iput-wide p1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mRingingTime:J

    return-wide p1
.end method

.method private adjustKey(Landroid/view/KeyEvent;)Landroid/view/KeyEvent;
    .locals 4
    .parameter "event"

    .prologue
    .line 819
    move-object v1, p1

    .line 820
    .local v1, newEvent:Landroid/view/KeyEvent;
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 821
    .local v0, keyCode:I
    const/4 v2, 0x1

    iget v3, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mCurrentKeyMode:I

    if-ne v2, v3, :cond_1

    .line 822
    packed-switch v0, :pswitch_data_0

    .line 847
    :cond_0
    :goto_0
    return-object v1

    .line 824
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->offsetKey(Landroid/view/KeyEvent;)Landroid/view/KeyEvent;

    move-result-object v1

    .line 826
    goto :goto_0

    .line 829
    :cond_1
    const/4 v2, 0x2

    iget v3, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mCurrentKeyMode:I

    if-ne v2, v3, :cond_2

    .line 830
    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 833
    :sswitch_0
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->offsetKey(Landroid/view/KeyEvent;)Landroid/view/KeyEvent;

    move-result-object v1

    .line 835
    goto :goto_0

    .line 838
    :cond_2
    const/4 v2, 0x3

    iget v3, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mCurrentKeyMode:I

    if-ne v2, v3, :cond_0

    .line 839
    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 841
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->offsetKey(Landroid/view/KeyEvent;)Landroid/view/KeyEvent;

    move-result-object v1

    .line 843
    goto :goto_0

    .line 822
    nop

    :pswitch_data_0
    .packed-switch 0x1a
        :pswitch_0
    .end packed-switch

    .line 830
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x1a -> :sswitch_0
    .end sparse-switch

    .line 839
    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_1
    .end packed-switch
.end method

.method private cancelPendingPowerKeyAction()V
    .locals 2

    .prologue
    .line 886
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mPowerKeyHandled:Z

    if-nez v0, :cond_0

    .line 887
    iget-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mPowerLongPress:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 889
    :cond_0
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoPowerKeyTriggered:Z

    if-eqz v0, :cond_1

    .line 890
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mPendingPowerKeyUpCanceled:Z

    .line 892
    :cond_1
    return-void
.end method

.method private cancelPendingScreenshotChordAction()V
    .locals 2

    .prologue
    .line 880
    sget-object v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    const-string v1, "cancelPendingScreenshotChordAction"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 882
    iget-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mScreenshotChordLongPress:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 883
    return-void
.end method

.method private closeFlashApp()V
    .locals 3

    .prologue
    .line 782
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->flashlights:Z

    if-nez v1, :cond_0

    .line 790
    :goto_0
    return-void

    .line 784
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->flashlights:Z

    .line 785
    new-instance v0, Landroid/content/Intent;

    const-string v1, "oppo.intent.action.STOP_LOCK_FLASHLIGHT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 786
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_1

    .line 787
    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 789
    :cond_1
    invoke-direct {p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->RemoveHomeLongPressedR()V

    goto :goto_0
.end method

.method private disableNotificationAlert()V
    .locals 3

    .prologue
    .line 1010
    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;

    const-string v2, "statusbar"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    .line 1012
    .local v0, statusBarManager:Landroid/app/StatusBarManager;
    const/high16 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    .line 1013
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    .line 1014
    return-void
.end method

.method private disableProximitySensor()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1270
    sget-object v2, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    const-string v3, "disableProximitySensor"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1271
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mProximitySensorEnabled:Z

    if-eqz v2, :cond_0

    .line 1273
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 1275
    .local v0, identity:J
    :try_start_0
 #   iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mSensorManager:Landroid/hardware/SensorManager;

#    iget-object v3, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mProximityListener:Landroid/hardware/SensorEventListener;

#    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 1276
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mProximitySensorEnabled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1278
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1280
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mProximitySensorActive:Z

    if-eqz v2, :cond_0

    .line 1281
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mProximitySensorActive:Z

    .line 1284
    .end local v0           #identity:J
    :cond_0
    return-void

    .line 1278
    .restart local v0       #identity:J
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v2
.end method

.method private dumpWindowState(Landroid/view/WindowManagerPolicy$WindowState;)V
    .locals 3
    .parameter "win"

    .prologue
    .line 1564
    sget-object v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "====getAttrs().packageName :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget-object v2, v2, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1565
    sget-object v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " getAttrs :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1566
    sget-object v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSurfaceLayer :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getSurfaceLayer()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1567
    sget-object v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hasAppShownWindows :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->hasAppShownWindows()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1568
    sget-object v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isVisibleLw :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->isVisibleLw()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1569
    sget-object v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isVisibleOrBehindKeyguardLw :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->isVisibleOrBehindKeyguardLw()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1570
    sget-object v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isDisplayedLw :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->isDisplayedLw()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1571
    sget-object v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isAnimatingLw :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->isAnimatingLw()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1572
    sget-object v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isGoneForLayoutLw :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->isGoneForLayoutLw()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1573
    sget-object v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hasDrawnLw :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->hasDrawnLw()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1574
    sget-object v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isAlive :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->isAlive()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1575
    return-void
.end method

.method private enableProximitySensor()V
    .locals 6

    .prologue
    .line 1254
    sget-object v2, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    const-string v3, "enableProximitySensor"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1255
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mProximitySensorEnabled:Z

    if-nez v2, :cond_0

    .line 1257
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 1259
    .local v0, identity:J
    :try_start_0
#    iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mSensorManager:Landroid/hardware/SensorManager;

#    iget-object v3, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mProximityListener:Landroid/hardware/SensorEventListener;

 #   iget-object v4, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mProximitySensor:Landroid/hardware/Sensor;

#    const/4 v5, 0x0

#    invoke-virtual {v2, v3, v4, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 1262
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mProximitySensorEnabled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1264
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1267
    .end local v0           #identity:J
    :cond_0
    return-void

    .line 1264
    .restart local v0       #identity:J
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v2
.end method

.method public static getFieldName(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    .locals 4
    .parameter "instance"
    .parameter "variableName"

    .prologue
    .line 1579
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 1583
    .local v2, targetClass:Ljava/lang/Class;
    :try_start_0
    invoke-virtual {v2, p1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 1584
    .local v1, field:Ljava/lang/reflect/Field;
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 1585
    invoke-virtual {v1, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 1588
    .end local v1           #field:Ljava/lang/reflect/Field;
    :goto_0
    return-object v3

    .line 1586
    :catch_0
    move-exception v0

    .line 1587
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1588
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private handleDoubleKeyEvents()V
    .locals 6

    .prologue
    const-wide/16 v4, 0xc8

    .line 795
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 797
    .local v0, now:J
    iget-wide v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->lasDoubletHomeKeyTime:J

    sub-long v2, v0, v2

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 798
    iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHomeKeyTap:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 799
    iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Landroid/os/PowerManager;->goToSleep(J)V

    .line 803
    :goto_0
    iput-wide v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->lasDoubletHomeKeyTime:J

    .line 804
    return-void

    .line 801
    :cond_0
    iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHomeKeyTap:Ljava/lang/Runnable;

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private handleTelephone(Z)Z
    .locals 11
    .parameter "isScreenOn"

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 966
    invoke-static {}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    move-result-object v5

    .line 967
    .local v5, telephonyService:Lcom/android/internal/telephony/ITelephony;
    const/4 v2, 0x0

    .line 968
    .local v2, hungUp:Z
    sget-object v9, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "telephonyService = "

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    if-eqz v5, :cond_3

    move v6, v7

    :goto_0
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v9, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 969
    if-eqz v5, :cond_2

    .line 971
    :try_start_0
    invoke-interface {v5}, Lcom/android/internal/telephony/ITelephony;->isRinging()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 972
    iget-object v6, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;

    const-string v9, "audio"

    invoke-virtual {v6, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 974
    .local v0, am:Landroid/media/AudioManager;
    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v6

    if-nez v6, :cond_4

    move v3, v7

    .line 975
    .local v3, inSilence:Z
    :goto_1
    iget-object v6, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v9, "call_vibrate_method"

    const/4 v10, 0x1

    invoke-static {v6, v9, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-ne v6, v7, :cond_5

    move v4, v7

    .line 977
    .local v4, isNeverVibrate:Z
    :goto_2
    sget-object v6, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "inSilence::"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", isNeverVibrate::"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " isMute="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->isMute:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 978
    iget-boolean v6, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->isMute:Z

    if-nez v6, :cond_6

    if-eqz v3, :cond_0

    if-nez v4, :cond_6

    .line 979
    :cond_0
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->isMute:Z

    .line 980
    const/4 v2, 0x1

    .line 981
    invoke-interface {v5}, Lcom/android/internal/telephony/ITelephony;->silenceRinger()V

    .line 996
    .end local v0           #am:Landroid/media/AudioManager;
    .end local v3           #inSilence:Z
    .end local v4           #isNeverVibrate:Z
    :goto_3
    const/4 v6, 0x5

    const/4 v7, 0x0

    invoke-static {v6, v7}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 997
    invoke-direct {p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->disableNotificationAlert()V

    :cond_1
    move v8, v2

    .line 1006
    :cond_2
    :goto_4
    return v8

    :cond_3
    move v6, v8

    .line 968
    goto :goto_0

    .restart local v0       #am:Landroid/media/AudioManager;
    :cond_4
    move v3, v8

    .line 974
    goto :goto_1

    .restart local v3       #inSilence:Z
    :cond_5
    move v4, v8

    .line 975
    goto :goto_2

    .line 983
    .restart local v4       #isNeverVibrate:Z
    :cond_6
    invoke-interface {v5}, Lcom/android/internal/telephony/ITelephony;->endCall()Z

    move-result v2

    .line 984
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->isMute:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 1000
    .end local v0           #am:Landroid/media/AudioManager;
    .end local v3           #inSilence:Z
    .end local v4           #isNeverVibrate:Z
    :catch_0
    move-exception v1

    .line 1001
    .local v1, ex:Landroid/os/RemoteException;
    :try_start_1
    sget-object v6, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    const-string v7, "ITelephony threw RemoteException"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v8, v2

    .line 1003
    goto :goto_4

    .line 986
    .end local v1           #ex:Landroid/os/RemoteException;
    :cond_7
    :try_start_2
    invoke-interface {v5}, Lcom/android/internal/telephony/ITelephony;->isOffhook()Z

    move-result v6

    if-eqz v6, :cond_8

    if-eqz p1, :cond_8

    iget v6, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mIncallPowerBehavior:I

    and-int/lit8 v6, v6, 0x2

    if-eqz v6, :cond_8

    .line 989
    invoke-interface {v5}, Lcom/android/internal/telephony/ITelephony;->endCall()Z

    move-result v2

    .line 990
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->isMute:Z

    goto :goto_3

    .line 1003
    :catchall_0
    move-exception v6

    move v8, v2

    goto :goto_4

    .line 992
    :cond_8
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->isMute:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3
.end method

.method private interceptPowerKeyForTelephone(Landroid/view/KeyEvent;IZI)I
    .locals 7
    .parameter "event"
    .parameter "policyFlags"
    .parameter "isScreenOn"
    .parameter "result"

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 897
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    if-nez v5, :cond_3

    move v1, v3

    .line 899
    .local v1, down:Z
    :goto_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v0

    .line 901
    .local v0, canceled:Z
    and-int/lit8 p4, p4, -0x2

    .line 902
    if-eqz v1, :cond_4

    .line 906
    new-instance v2, Landroid/content/Intent;

    const-string v5, "SILENCE_ACTION_FOR_OPPO_SPEECH"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 907
    .local v2, it:Landroid/content/Intent;
    iget-object v5, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 909
    if-eqz p3, :cond_0

    iget-boolean v5, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoPowerKeyTriggered:Z

    if-nez v5, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getFlags()I

    move-result v5

    and-int/lit16 v5, v5, 0x400

    if-nez v5, :cond_0

    .line 911
    iput-boolean v3, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoPowerKeyTriggered:Z

    .line 912
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoPowerKeyTime:J

    .line 913
    invoke-direct {p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->interceptScreenshotChord()V

    .line 942
    :cond_0
    if-eqz p3, :cond_1

    iget-boolean v5, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoVolumeDownKeyTriggered:Z

    if-nez v5, :cond_1

    iget-boolean v5, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoVolumeUpKeyTriggered:Z

    if-eqz v5, :cond_2

    :cond_1
    move v4, v3

    :cond_2
    invoke-virtual {p0, v4}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->interceptPowerKeyDown(Z)V

    .line 962
    .end local v2           #it:Landroid/content/Intent;
    :goto_1
    return p4

    .end local v0           #canceled:Z
    .end local v1           #down:Z
    :cond_3
    move v1, v4

    .line 897
    goto :goto_0

    .line 947
    .restart local v0       #canceled:Z
    .restart local v1       #down:Z
    :cond_4
    iget-boolean v5, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mPowerKeyHandled:Z

    if-nez v5, :cond_5

    iget-boolean v5, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mPendingPowerKeyUpCanceled:Z

    if-nez v5, :cond_5

    .line 948
    iget-object v5, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mPowerLongPress:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 949
    invoke-direct {p0, p3}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->handleTelephone(Z)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 950
    iput-boolean v3, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mPowerKeyHandled:Z

    .line 954
    :cond_5
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoPowerKeyTriggered:Z

    .line 955
    invoke-direct {p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->cancelPendingScreenshotChordAction()V

    .line 956
    if-nez v0, :cond_6

    iget-boolean v5, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mPendingPowerKeyUpCanceled:Z

    if-eqz v5, :cond_8

    :cond_6
    :goto_2
    invoke-virtual {p0, v3}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->interceptPowerKeyUp(Z)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 957
    and-int/lit8 v3, p4, -0x3

    or-int/lit8 p4, v3, 0x4

    .line 959
    :cond_7
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mPendingPowerKeyUpCanceled:Z

    goto :goto_1

    :cond_8
    move v3, v4

    .line 956
    goto :goto_2
.end method

.method private interceptScreenshotChord()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 859
    sget-object v2, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "interceptScreenshotChord  mOppoVolumeDownKeyTriggered"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoVolumeDownKeyTriggered:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  mOppoPowerKeyTriggered"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoPowerKeyTriggered:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  mOppoVolumeUpKeyTriggered"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoVolumeUpKeyTriggered:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  mOppoVolumeDownKeyTime"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoVolumeDownKeyTime:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  mOppoPowerKeyTime"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoPowerKeyTime:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 865
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoScreenshotChordEnabled:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoVolumeDownKeyTriggered:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoPowerKeyTriggered:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoVolumeUpKeyTriggered:Z

    if-nez v2, :cond_0

    .line 867
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 868
    .local v0, now:J
    iget-wide v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoVolumeDownKeyTime:J

    const-wide/16 v4, 0x96

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    iget-wide v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoPowerKeyTime:J

    const-wide/16 v4, 0x3e8

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 870
    iput-boolean v6, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoVolumeDownKeyConsumedByScreenshotChord:Z

    .line 871
    iput-boolean v6, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoPowerDownKeyConsumedByScreenshotChord:Z

    .line 873
    invoke-direct {p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->cancelPendingPowerKeyAction()V

    .line 874
    iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mScreenshotChordLongPress:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 877
    .end local v0           #now:J
    :cond_0
    return-void
.end method

.method private isLogKey(I)Z
    .locals 1
    .parameter "keyCode"

    .prologue
    .line 1443
    const/16 v0, 0x1a

    if-eq v0, p1, :cond_0

    const/16 v0, 0x19

    if-eq v0, p1, :cond_0

    const/16 v0, 0x18

    if-eq v0, p1, :cond_0

    const/16 v0, 0xa4

    if-eq v0, p1, :cond_0

    const/16 v0, 0x52

    if-eq v0, p1, :cond_0

    const/4 v0, 0x3

    if-eq v0, p1, :cond_0

    const/16 v0, 0x4f

    if-eq v0, p1, :cond_0

    const/4 v0, 0x4

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private longPressMenuKey()V
    .locals 3

    .prologue
    .line 1018
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1019
    .local v0, intentOppoAssist:Landroid/content/Intent;
    const/high16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1020
    const-string v1, "com.oppo.speechassist.start_action"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1021
    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1022
    sget-object v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    const-string v2, "send broadcast com.oppo.speechassist.start_action to start speechassist."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1023
    return-void
.end method

.method private offsetKey(Landroid/view/KeyEvent;)Landroid/view/KeyEvent;
    .locals 13
    .parameter "event"

    .prologue
    .line 851
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v5

    add-int/lit16 v5, v5, 0x320

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v6

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v7

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDeviceId()I

    move-result v8

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getScanCode()I

    move-result v9

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getFlags()I

    move-result v10

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getSource()I

    move-result v11

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getCharacters()Ljava/lang/String;

    move-result-object v12

    invoke-static/range {v0 .. v12}, Landroid/view/KeyEvent;->obtain(JJIIIIIIIILjava/lang/String;)Landroid/view/KeyEvent;

    move-result-object v0

    return-object v0
.end method

.method private openFlashApp()V
    .locals 5

    .prologue
    .line 760
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->flashlights:Z

    if-eqz v1, :cond_1

    .line 773
    :cond_0
    :goto_0
    return-void

    .line 762
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->flashlights:Z

    .line 763
    new-instance v0, Landroid/content/Intent;

    const-string v1, "oppo.intent.action.START_LOCK_FLASHLIGHT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 764
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 765
    const/high16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 766
    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_2

    .line 767
    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 770
    :cond_2
    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternUtils;->usingBiometricWeak()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternUtils;->isBiometricWeakInstalled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 771
    :cond_3
    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->LongHomePressedEscaped:Ljava/lang/Runnable;

    const-wide/16 v3, 0xfa0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private setVideoMode(I)V
    .locals 0
    .parameter "mode"

    .prologue
    .line 815
    iput p1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mCurrentKeyMode:I

    .line 816
    return-void
.end method


# virtual methods
.method public allowAppAnimationsLw()Z
    .locals 7
    .annotation build Landroid/annotation/OppoHook;
        level = .enum Landroid/annotation/OppoHook$OppoHookType;->NEW_METHOD:Landroid/annotation/OppoHook$OppoHookType;
        note = "zhangkai@Plf.DesktopApp.Keyguard add for apklock"
        property = .enum Landroid/annotation/OppoHook$OppoRomType;->OPPO:Landroid/annotation/OppoHook$OppoRomType;
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1411
    const/4 v1, 0x0

    .line 1412
    .local v1, isAnim:Z
    const/4 v3, 0x0

    .line 1413
    .local v3, temp:Z
    iget-object v6, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mApkLockScreens:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManagerPolicy$WindowState;

    .line 1414
    .local v2, mApkLockScreen:Landroid/view/WindowManagerPolicy$WindowState;
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/view/WindowManagerPolicy$WindowState;->isVisibleLw()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Landroid/view/WindowManagerPolicy$WindowState;->isAnimatingLw()Z

    move-result v6

    if-nez v6, :cond_1

    move v3, v5

    .line 1415
    :goto_1
    if-nez v1, :cond_0

    if-eqz v3, :cond_2

    :cond_0
    move v1, v5

    :goto_2
    goto :goto_0

    :cond_1
    move v3, v4

    .line 1414
    goto :goto_1

    :cond_2
    move v1, v4

    .line 1415
    goto :goto_2

    .line 1418
    .end local v2           #mApkLockScreen:Landroid/view/WindowManagerPolicy$WindowState;
    :cond_3
    if-eqz v1, :cond_4

    .line 1421
    :goto_3
    return v4

    :cond_4
    invoke-super {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->allowAppAnimationsLw()Z

    move-result v4

    goto :goto_3
.end method

.method public applyPostLayoutPolicyLw(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)V
    .locals 4
    .parameter "win"
    .parameter "attrs"

    .prologue
    .line 1539
    invoke-super {p0, p1, p2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->applyPostLayoutPolicyLw(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)V

    .line 1542
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v3, 0x63

    if-gt v2, v3, :cond_0

    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->isVisibleLw()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1544
    iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mTopAppWin:Landroid/view/WindowManagerPolicy$WindowState;

    if-nez v2, :cond_2

    .line 1545
    iput-object p1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mTopAppWin:Landroid/view/WindowManagerPolicy$WindowState;

    .line 1553
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1554
    .local v1, pkg:Ljava/lang/String;
    const-string v2, "launcher"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 1556
    .local v0, isLauncher:Z
    if-eqz v0, :cond_3

    .line 1557
    iput-object p1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mLauncherWin:Landroid/view/WindowManagerPolicy$WindowState;

    .line 1561
    :cond_1
    :goto_1
    return-void

    .line 1547
    .end local v0           #isLauncher:Z
    .end local v1           #pkg:Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mTopAppWin:Landroid/view/WindowManagerPolicy$WindowState;

    invoke-interface {v2}, Landroid/view/WindowManagerPolicy$WindowState;->getSurfaceLayer()I

    move-result v2

    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getSurfaceLayer()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 1548
    iput-object p1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mTopAppWin:Landroid/view/WindowManagerPolicy$WindowState;

    goto :goto_0

    .line 1558
    .restart local v0       #isLauncher:Z
    .restart local v1       #pkg:Ljava/lang/String;
    :cond_3
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v3, 0x7dd

    if-ne v2, v3, :cond_1

    .line 1559
    iput-object p1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mWallpaperWin:Landroid/view/WindowManagerPolicy$WindowState;

    goto :goto_1
.end method

.method public beginPostLayoutPolicyLw(II)V
    .locals 1
    .parameter "displayWidth"
    .parameter "displayHeight"

    .prologue
    const/4 v0, 0x0

    .line 1530
    invoke-super {p0, p1, p2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->beginPostLayoutPolicyLw(II)V

    .line 1531
    iput-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mWallpaperWin:Landroid/view/WindowManagerPolicy$WindowState;

    .line 1532
    iput-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mLauncherWin:Landroid/view/WindowManagerPolicy$WindowState;

    .line 1533
    iput-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mTopAppWin:Landroid/view/WindowManagerPolicy$WindowState;

    .line 1534
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mIsShowGaussFlag:Z

    .line 1535
    return-void
.end method

.method public canBeForceHidden(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)Z
    .locals 2
    .parameter "win"
    .parameter "attrs"
    .annotation build Landroid/annotation/OppoHook;
        level = .enum Landroid/annotation/OppoHook$OppoHookType;->NEW_METHOD:Landroid/annotation/OppoHook$OppoHookType;
        note = "zhangkai@Plf.DesktopApp.Keyguard add for apklock"
        property = .enum Landroid/annotation/OppoHook$OppoRomType;->OPPO:Landroid/annotation/OppoHook$OppoRomType;
    .end annotation

    .prologue
    .line 1366
    const/16 v0, 0xa10

    iget v1, p2, Landroid/view/WindowManager$LayoutParams;->type:I

    if-ne v0, v1, :cond_1

    .line 1367
    const-string v0, "oppo lockscreen hostview colorLock"

    invoke-virtual {p2}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1368
    const/4 v0, 0x1

    .line 1372
    :goto_0
    return v0

    .line 1370
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1372
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->canBeForceHidden(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)Z

    move-result v0

    goto :goto_0
.end method

.method public checkAddPermission(Landroid/view/WindowManager$LayoutParams;[I)I
    .locals 4
    .parameter "attrs"
    .parameter "outAppOp"

    .annotation build Landroid/annotation/OppoHook;
        level = .enum Landroid/annotation/OppoHook$OppoHookType;->NEW_METHOD:Landroid/annotation/OppoHook$OppoHookType;
        note = "zhangkai@Plf.DesktopApp.Keyguard add for apklock"
        property = .enum Landroid/annotation/OppoHook$OppoRomType;->OPPO:Landroid/annotation/OppoHook$OppoRomType;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1311
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 1313
    .local v1, type:I
    const/16 v3, 0x7d0

    if-lt v1, v3, :cond_0

    const/16 v3, 0xbb7

    if-le v1, v3, :cond_1

    .line 1322
    :cond_0
    :goto_0
    return v2

    .line 1317
    :cond_1
    const/4 v0, 0x0

    .line 1318
    .local v0, permission:Ljava/lang/String;
    const/16 v3, 0xa10

    if-eq v3, v1, :cond_0

    const/16 v3, 0x7da

    if-eq v3, v1, :cond_0

    .line 1322
    invoke-super {p0, p1, p2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->checkAddPermission(Landroid/view/WindowManager$LayoutParams;[I)I

    move-result v2

    goto :goto_0
.end method

.method public doesForceHide(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)Z
    .locals 7
    .parameter "win"
    .parameter "attrs"
    .annotation build Landroid/annotation/OppoHook;
        level = .enum Landroid/annotation/OppoHook$OppoHookType;->NEW_METHOD:Landroid/annotation/OppoHook$OppoHookType;
        note = "zhangkai@Plf.DesktopApp.Keyguard add for apklock"
        property = .enum Landroid/annotation/OppoHook$OppoRomType;->OPPO:Landroid/annotation/OppoHook$OppoRomType;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/16 v6, 0xa10

    const/4 v3, 0x0

    .line 1345
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttachedWindow()Landroid/view/WindowManagerPolicy$WindowState;

    move-result-object v1

    .line 1346
    .local v1, parent:Landroid/view/WindowManagerPolicy$WindowState;
    const/4 v0, 0x0

    .line 1347
    .local v0, isParentLock:Z
    if-eqz v1, :cond_0

    .line 1348
    invoke-interface {v1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/WindowManager$LayoutParams;->type:I

    if-ne v4, v6, :cond_2

    move v0, v2

    .line 1351
    :cond_0
    :goto_0
    iget v4, p2, Landroid/view/WindowManager$LayoutParams;->type:I

    if-ne v6, v4, :cond_3

    .line 1352
    iget-boolean v4, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHideLockScreen:Z

    if-nez v4, :cond_3

    const-string v4, "oppo lockscreen hostview colorLock"

    invoke-virtual {p2}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1357
    :cond_1
    :goto_1
    return v3

    :cond_2
    move v0, v3

    .line 1348
    goto :goto_0

    .line 1357
    :cond_3
    iget v4, p2, Landroid/view/WindowManager$LayoutParams;->type:I

    if-eq v4, v6, :cond_4

    if-nez v0, :cond_4

    invoke-super {p0, p1, p2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->doesForceHide(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_4
    move v3, v2

    goto :goto_1
.end method

.method public finishPostLayoutPolicyLw()I
    .locals 21
    .annotation build Landroid/annotation/OppoHook;
        level = .enum Landroid/annotation/OppoHook$OppoHookType;->CHANGE_CODE:Landroid/annotation/OppoHook$OppoHookType;
        note = "liumei@Plf.Framework add for narrow frame 2013/05/17,zhangkai@Plf.DesktopApp.Keyguard add for apklock"
        property = .enum Landroid/annotation/OppoHook$OppoRomType;->OPPO:Landroid/annotation/OppoHook$OppoRomType;
    .end annotation

    .prologue
    .line 1061
    const/16 v20, -0x1

    .line 1062
    .local v20, wallpaperLayer:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mLauncherWin:Landroid/view/WindowManagerPolicy$WindowState;

    if-eqz v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mWallpaperWin:Landroid/view/WindowManagerPolicy$WindowState;

    if-eqz v3, :cond_7

    .line 1063
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mLauncherWin:Landroid/view/WindowManagerPolicy$WindowState;

    invoke-interface {v3}, Landroid/view/WindowManagerPolicy$WindowState;->getSurfaceLayer()I

    move-result v16

    .line 1064
    .local v16, launcherLayer:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mWallpaperWin:Landroid/view/WindowManagerPolicy$WindowState;

    invoke-interface {v3}, Landroid/view/WindowManagerPolicy$WindowState;->getSurfaceLayer()I

    move-result v20

    .line 1066
    move/from16 v0, v16

    move/from16 v1, v20

    if-lt v0, v1, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mLauncherWin:Landroid/view/WindowManagerPolicy$WindowState;

    invoke-interface {v3}, Landroid/view/WindowManagerPolicy$WindowState;->isVisibleLw()Z

    move-result v3

    if-nez v3, :cond_6

    .line 1067
    :cond_0
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mIsShowGaussFlag:Z

    .line 1075
    .end local v16           #launcherLayer:I
    :goto_0
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mWallpaperLayer:I

    move/from16 v0, v20

    if-eq v3, v0, :cond_1

    if-lez v20, :cond_1

    .line 1076
    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mWallpaperLayer:I

    .line 1078
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v5, "LAYER_WALLPAPER"

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mWallpaperLayer:I

    invoke-static {v3, v5, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1085
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mIsShowGaussFlag:Z

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mIsGaussFlag:Z

    if-eq v3, v5, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mWallpaperWin:Landroid/view/WindowManagerPolicy$WindowState;

    if-eqz v3, :cond_2

    .line 1087
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mWallpaperWin:Landroid/view/WindowManagerPolicy$WindowState;

    const-string v5, "mClient"

    invoke-static {v3, v5}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->getFieldName(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/IWindow;

    .line 1089
    .local v2, mClient:Landroid/view/IWindow;
    if-eqz v2, :cond_2

    .line 1091
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mIsShowGaussFlag:Z

    if-eqz v3, :cond_8

    const/4 v4, 0x1

    .line 1092
    .local v4, i:I
    :goto_1
    const-string v3, "wallpaper_change"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-interface/range {v2 .. v8}, Landroid/view/IWindow;->dispatchWallpaperCommand(Ljava/lang/String;IIILandroid/os/Bundle;Z)V

    .line 1094
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mIsShowGaussFlag:Z

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mIsGaussFlag:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1104
    .end local v2           #mClient:Landroid/view/IWindow;
    .end local v4           #i:I
    :cond_2
    :goto_2
    const/4 v9, 0x0

    .line 1105
    .local v9, changes:I
    const/4 v15, 0x0

    .line 1109
    .local v15, isShowLock:Z
    sget-object v3, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "finishPostLayoutPolicyLw size ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mApkLockScreens:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1110
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mApkLockScreens:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, i$:Ljava/util/Iterator;
    :cond_3
    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/view/WindowManagerPolicy$WindowState;

    .line 1112
    .local v17, mApkLockScreen:Landroid/view/WindowManagerPolicy$WindowState;
    if-eqz v17, :cond_3

    .line 1113
    if-nez v15, :cond_4

    invoke-interface/range {v17 .. v17}, Landroid/view/WindowManagerPolicy$WindowState;->isVisibleLw()Z

    move-result v3

    if-eqz v3, :cond_9

    :cond_4
    const/4 v15, 0x1

    .line 1117
    :goto_4
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mDismissKeyguard:I

    if-eqz v3, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;

    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;->isSecure()Z

    move-result v3

    if-nez v3, :cond_a

    .line 1118
    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/view/WindowManagerPolicy$WindowState;->hideLw(Z)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1119
    or-int/lit8 v9, v9, 0x7

    .line 1123
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;

    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1124
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHandler:Landroid/os/Handler;

    new-instance v5, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$4;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$4;-><init>(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;)V

    invoke-virtual {v3, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_3

    .line 1069
    .end local v9           #changes:I
    .end local v11           #i$:Ljava/util/Iterator;
    .end local v15           #isShowLock:Z
    .end local v17           #mApkLockScreen:Landroid/view/WindowManagerPolicy$WindowState;
    .restart local v16       #launcherLayer:I
    :cond_6
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mIsShowGaussFlag:Z

    goto/16 :goto_0

    .line 1072
    .end local v16           #launcherLayer:I
    :cond_7
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mIsShowGaussFlag:Z

    goto/16 :goto_0

    .line 1091
    .restart local v2       #mClient:Landroid/view/IWindow;
    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 1097
    :catch_0
    move-exception v10

    .line 1098
    .local v10, e:Landroid/os/RemoteException;
    invoke-virtual {v10}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_2

    .line 1113
    .end local v2           #mClient:Landroid/view/IWindow;
    .end local v10           #e:Landroid/os/RemoteException;
    .restart local v9       #changes:I
    .restart local v11       #i$:Ljava/util/Iterator;
    .restart local v15       #isShowLock:Z
    .restart local v17       #mApkLockScreen:Landroid/view/WindowManagerPolicy$WindowState;
    :cond_9
    const/4 v15, 0x0

    goto :goto_4

    .line 1130
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHideLockScreen:Z

    if-eqz v3, :cond_c

    .line 1133
    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/view/WindowManagerPolicy$WindowState;->hideLw(Z)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 1134
    or-int/lit8 v9, v9, 0x7

    .line 1138
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHideLockScreen:Z

    if-eqz v3, :cond_3

    .line 1139
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;->setHidden(Z)V

    goto/16 :goto_3

    .line 1145
    :cond_c
    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/view/WindowManagerPolicy$WindowState;->showLw(Z)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 1146
    or-int/lit8 v9, v9, 0x7

    .line 1150
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;->setHidden(Z)V

    goto/16 :goto_3

    .line 1157
    .end local v17           #mApkLockScreen:Landroid/view/WindowManagerPolicy$WindowState;
    :cond_e
    const/4 v14, 0x0

    .line 1158
    .local v14, isLauncherShow:Z
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mLauncherWin:Landroid/view/WindowManagerPolicy$WindowState;

    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mLauncherWin:Landroid/view/WindowManagerPolicy$WindowState;

    invoke-interface {v3}, Landroid/view/WindowManagerPolicy$WindowState;->isVisibleLw()Z

    move-result v3

    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mTopAppWin:Landroid/view/WindowManagerPolicy$WindowState;

    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mTopAppWin:Landroid/view/WindowManagerPolicy$WindowState;

    invoke-interface {v3}, Landroid/view/WindowManagerPolicy$WindowState;->getSurfaceLayer()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mLauncherWin:Landroid/view/WindowManagerPolicy$WindowState;

    invoke-interface {v5}, Landroid/view/WindowManagerPolicy$WindowState;->getSurfaceLayer()I

    move-result v5

    if-gt v3, v5, :cond_f

    .line 1160
    const/4 v14, 0x1

    .line 1163
    :cond_f
    const/4 v13, 0x0

    .line 1164
    .local v13, isHomeModeChange:Z
    if-nez v15, :cond_10

    if-eqz v14, :cond_15

    :cond_10
    const/16 v19, 0x1

    .line 1165
    .local v19, shouldTans:Z
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mIsStatusBarTans:Z

    move/from16 v0, v19

    if-eq v0, v3, :cond_11

    .line 1168
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mIsStatusBarTans:Z

    .line 1169
    const/4 v13, 0x1

    .line 1194
    :cond_11
    if-eqz v13, :cond_12

    .line 1197
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mMyHandler:Landroid/os/Handler;

    const/16 v5, 0x10

    invoke-virtual {v3, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 1198
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mMyHandler:Landroid/os/Handler;

    const/16 v7, 0x10

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mIsStatusBarTans:Z

    if-eqz v3, :cond_16

    const/4 v3, 0x1

    :goto_6
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mIsStatusBarSemipermeable:Z

    if-eqz v5, :cond_17

    const/4 v5, 0x1

    :goto_7
    invoke-static {v6, v7, v3, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v18

    .line 1200
    .local v18, msg:Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mMyHandler:Landroid/os/Handler;

    const-wide/16 v5, 0x32

    move-object/from16 v0, v18

    invoke-virtual {v3, v0, v5, v6}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1203
    .end local v18           #msg:Landroid/os/Message;
    :cond_12
    const/4 v12, 0x0

    .line 1205
    .local v12, isCameraShow:Z
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mTopAppWin:Landroid/view/WindowManagerPolicy$WindowState;

    if-eqz v3, :cond_13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mTopAppWin:Landroid/view/WindowManagerPolicy$WindowState;

    invoke-interface {v3}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    iget-object v3, v3, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    const-string v5, "com.oppo.camera"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 1206
    const/4 v12, 0x1

    .line 1209
    :cond_13
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mIsCameraShow:Z

    if-eq v12, v3, :cond_14

    .line 1210
    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mIsCameraShow:Z

    .line 1211
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mMyHandler:Landroid/os/Handler;

    const/16 v5, 0x11

    invoke-virtual {v3, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 1212
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mMyHandler:Landroid/os/Handler;

    const/16 v6, 0x11

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mIsCameraShow:Z

    if-eqz v3, :cond_18

    const/4 v3, 0x1

    :goto_8
    const/4 v7, 0x0

    invoke-static {v5, v6, v3, v7}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v18

    .line 1214
    .restart local v18       #msg:Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mMyHandler:Landroid/os/Handler;

    const-wide/16 v5, 0x32

    move-object/from16 v0, v18

    invoke-virtual {v3, v0, v5, v6}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1218
    .end local v18           #msg:Landroid/os/Message;
    :cond_14
    invoke-super/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->finishPostLayoutPolicyLw()I

    move-result v3

    or-int/2addr v9, v3

    .line 1220
    return v9

    .line 1164
    .end local v12           #isCameraShow:Z
    .end local v19           #shouldTans:Z
    :cond_15
    const/16 v19, 0x0

    goto/16 :goto_5

    .line 1198
    .restart local v19       #shouldTans:Z
    :cond_16
    const/4 v3, 0x0

    goto :goto_6

    :cond_17
    const/4 v5, 0x0

    goto :goto_7

    .line 1212
    .restart local v12       #isCameraShow:Z
    :cond_18
    const/4 v3, 0x0

    goto :goto_8
.end method

.method public init(Landroid/content/Context;Landroid/view/IWindowManager;Landroid/view/WindowManagerPolicy$WindowManagerFuncs;)V
    .locals 5
    .parameter "context"
    .parameter "windowManager"
    .parameter "windowManagerFuncs"

    .prologue
    .line 297
    invoke-super {p0, p1, p2, p3}, Lcom/android/internal/policy/impl/PhoneWindowManager;->init(Landroid/content/Context;Landroid/view/IWindowManager;Landroid/view/WindowManagerPolicy$WindowManagerFuncs;)V

    .line 299
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHeadless:Z

    if-nez v2, :cond_0

    .line 301
    new-instance v2, Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;

    const/4 v3, 0x0

    invoke-direct {v2, p1, v3}, Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;-><init>(Landroid/content/Context;Lcom/android/internal/widget/LockPatternUtils;)V

    iput-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;

    .line 304
    :cond_0
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 305
    .local v1, keyModeFilter:Landroid/content/IntentFilter;
    const-string v2, "com.oppo.intent.action.KEY_LOCK_MODE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 306
    iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mKeyLockModeReceiver:Lcom/android/internal/policy/impl/OppoPhoneWindowManager$KeyLockModeReceiver;

    invoke-virtual {p1, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 308
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 309
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.END_CALL"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 310
    const-string v2, "com.oppo.intent.action.START_SPEECH_ENABLE"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 311
    const-string v2, "com.oppo.intent.action.START_SPEECH_DISABLE"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 314
    const-string v2, "oppo.intent.action.SCREEN_SHOT"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 317
    const-string v2, "android.intent.action.PHONE_STATE"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 319
    iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoBaseReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 321
    iput-object p1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;

    .line 324
    new-instance v2, Lcom/android/internal/widget/LockPatternUtils;

    iget-object v3, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    .line 327
    iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mShotScreenHelper:Lcom/android/internal/policy/impl/OppoShotScreenHelper;

    iget-object v3, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3, v4}, Lcom/android/internal/policy/impl/OppoShotScreenHelper;->init(Landroid/os/Handler;Landroid/content/Context;)V

    .line 328
    return-void
.end method

.method public interceptKeyBeforeDispatching(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/KeyEvent;I)J
    .locals 28
    .parameter "win"
    .parameter "event"
    .parameter "policyFlags"
    .annotation build Landroid/annotation/OppoHook;
        level = .enum Landroid/annotation/OppoHook$OppoHookType;->NEW_METHOD:Landroid/annotation/OppoHook$OppoHookType;
        note = "zhangkai@Plf.DesktopApp.Keyguard add for FlashLight"
        property = .enum Landroid/annotation/OppoHook$OppoRomType;->OPPO:Landroid/annotation/OppoHook$OppoRomType;
    .end annotation

    .prologue
    .line 550
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v24

    if-nez v24, :cond_0

    const/4 v5, 0x1

    .line 559
    .local v5, down1:Z
    :goto_0
    sget-object v24, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "interceptKeyBeforeDispatching key: win="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ", action="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ", flags="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getFlags()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ", keyCode="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ", scanCode="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getScanCode()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ", metaState="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ", repeatCount="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ", policyFlags="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v24

    if-nez v24, :cond_1

    const/4 v4, 0x1

    .line 568
    .local v4, down:Z
    :goto_1
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v12

    .line 569
    .local v12, keyCode:I
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getFlags()I

    move-result v8

    .line 570
    .local v8, flags:I
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v3

    .line 571
    .local v3, canceled:Z
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v19

    .line 576
    .local v19, repeatCount:I
    if-nez v5, :cond_2

    .line 577
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->flashlights:Z

    move/from16 v24, v0

    if-eqz v24, :cond_2

    const/16 v24, 0x3

    move/from16 v0, v24

    if-ne v12, v0, :cond_2

    .line 578
    sget-object v24, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    const-string v25, "interceptKeyBeforeDispatching disabled flashlights"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->closeFlashApp()V

    .line 580
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->flashlights:Z

    .line 581
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->RemoveHomeLongPressedR()V

    .line 582
    const-wide/16 v24, -0x1

    .line 743
    :goto_2
    return-wide v24

    .line 550
    .end local v3           #canceled:Z
    .end local v4           #down:Z
    .end local v5           #down1:Z
    .end local v8           #flags:I
    .end local v12           #keyCode:I
    .end local v19           #repeatCount:I
    :cond_0
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 567
    .restart local v5       #down1:Z
    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 587
    .restart local v3       #canceled:Z
    .restart local v4       #down:Z
    .restart local v8       #flags:I
    .restart local v12       #keyCode:I
    .restart local v19       #repeatCount:I
    :cond_2
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->isLogKey(I)Z

    move-result v24

    if-eqz v24, :cond_3

    .line 588
    sget-object v24, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "interceptKeyBeforeDispatching key: win="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "  event = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 591
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoScreenshotChordEnabled:Z

    move/from16 v24, v0

    if-eqz v24, :cond_9

    and-int/lit16 v0, v8, 0x400

    move/from16 v24, v0

    if-nez v24, :cond_9

    .line 592
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoVolumeDownKeyTriggered:Z

    move/from16 v24, v0

    if-eqz v24, :cond_4

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoPowerKeyTriggered:Z

    move/from16 v24, v0

    if-nez v24, :cond_4

    .line 593
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v15

    .line 594
    .local v15, now:J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoVolumeDownKeyTime:J

    move-wide/from16 v24, v0

    const-wide/16 v26, 0x96

    add-long v22, v24, v26

    .line 596
    .local v22, timeoutTime:J
    cmp-long v24, v15, v22

    if-gez v24, :cond_4

    .line 597
    sub-long v24, v22, v15

    goto :goto_2

    .line 601
    .end local v15           #now:J
    .end local v22           #timeoutTime:J
    :cond_4
    const/16 v24, 0x1a

    move/from16 v0, v24

    if-ne v12, v0, :cond_5

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoPowerKeyTriggered:Z

    move/from16 v24, v0

    if-eqz v24, :cond_5

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoVolumeDownKeyTriggered:Z

    move/from16 v24, v0

    if-nez v24, :cond_5

    .line 603
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v15

    .line 604
    .restart local v15       #now:J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoPowerKeyTime:J

    move-wide/from16 v24, v0

    const-wide/16 v26, 0x3e8

    add-long v22, v24, v26

    .line 605
    .restart local v22       #timeoutTime:J
    cmp-long v24, v15, v22

    if-gez v24, :cond_5

    .line 606
    sub-long v24, v22, v15

    goto/16 :goto_2

    .line 610
    .end local v15           #now:J
    .end local v22           #timeoutTime:J
    :cond_5
    const/16 v24, 0x19

    move/from16 v0, v24

    if-ne v12, v0, :cond_7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoVolumeDownKeyConsumedByScreenshotChord:Z

    move/from16 v24, v0

    if-eqz v24, :cond_7

    .line 612
    if-nez v4, :cond_6

    .line 613
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoVolumeDownKeyConsumedByScreenshotChord:Z

    .line 615
    :cond_6
    const-wide/16 v24, -0x1

    goto/16 :goto_2

    .line 618
    :cond_7
    const/16 v24, 0x1a

    move/from16 v0, v24

    if-ne v12, v0, :cond_9

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoPowerDownKeyConsumedByScreenshotChord:Z

    move/from16 v24, v0

    if-eqz v24, :cond_9

    .line 620
    if-nez v4, :cond_8

    .line 621
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoPowerDownKeyConsumedByScreenshotChord:Z

    .line 623
    :cond_8
    const-wide/16 v24, -0x1

    goto/16 :goto_2

    .line 628
    :cond_9
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->adjustKey(Landroid/view/KeyEvent;)Landroid/view/KeyEvent;

    move-result-object v14

    .line 631
    .local v14, newEvent:Landroid/view/KeyEvent;
    invoke-virtual {v14}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v24

    const/16 v25, 0x3

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_16

    .line 633
    if-nez v4, :cond_12

    .line 634
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHomeLongPressed:Z

    .line 640
    .local v9, homeWasLongPressed:Z
    if-nez v9, :cond_12

    .line 641
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHomePressed:Z

    .line 642
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHomeLongPressed:Z

    .line 644
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->getStatusBarService()Lcom/android/internal/statusbar/IStatusBarService;

    move-result-object v20

    .line 645
    .local v20, statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v20, :cond_a

    .line 646
    invoke-interface/range {v20 .. v20}, Lcom/android/internal/statusbar/IStatusBarService;->cancelPreloadRecentApps()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 654
    .end local v20           #statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    :cond_a
    :goto_3
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHomePressed:Z

    .line 655
    if-nez v3, :cond_11

    .line 659
    const/4 v10, 0x0

    .line 661
    .local v10, incomingRinging:Z
    :try_start_1
    invoke-static {}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    move-result-object v21

    .line 662
    .local v21, telephonyService:Lcom/android/internal/telephony/ITelephony;
    if-eqz v21, :cond_b

    .line 663
    invoke-interface/range {v21 .. v21}, Lcom/android/internal/telephony/ITelephony;->isRinging()Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v10

    .line 669
    .end local v21           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :cond_b
    :goto_4
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v17

    .line 670
    .local v17, nowTime:J
    if-eqz v10, :cond_e

    if-eqz p1, :cond_c

    const-string v24, "com.android.phone/com.android.phone.OppoInCallScreen"

    invoke-interface/range {p1 .. p1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_d

    const-string v24, "com.android.phone/com.android.phone.OppoIgnoreInComingCallScreen"

    invoke-interface/range {p1 .. p1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_d

    :cond_c
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mRingingTime:J

    move-wide/from16 v24, v0

    sub-long v24, v17, v24

    const-wide/16 v26, 0x7d0

    cmp-long v24, v24, v26

    if-gez v24, :cond_e

    .line 674
    :cond_d
    sget-object v24, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    const-string v25, "Ignoring HOME; there\'s a ringing incoming call."

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 705
    .end local v10           #incomingRinging:Z
    .end local v17           #nowTime:J
    :goto_5
    const-wide/16 v24, -0x1

    goto/16 :goto_2

    .line 648
    :catch_0
    move-exception v6

    .line 649
    .local v6, e:Landroid/os/RemoteException;
    sget-object v24, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    const-string v25, "RemoteException when showing recent apps"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-static {v0, v1, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 651
    const/16 v24, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mStatusBarService:Lcom/android/internal/statusbar/IStatusBarService;

    goto :goto_3

    .line 665
    .end local v6           #e:Landroid/os/RemoteException;
    .restart local v10       #incomingRinging:Z
    :catch_1
    move-exception v7

    .line 666
    .local v7, ex:Landroid/os/RemoteException;
    sget-object v24, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    const-string v25, "RemoteException from getPhoneInterface()"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-static {v0, v1, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 691
    .end local v7           #ex:Landroid/os/RemoteException;
    .restart local v17       #nowTime:J
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v24

    const-string v25, "oppo_double_press_home_lock_screen"

    const/16 v26, 0x1

    invoke-static/range {v24 .. v26}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v24

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_f

    const/4 v11, 0x1

    .line 693
    .local v11, isDoubleHomeEnable:Z
    :goto_6
    if-eqz v11, :cond_10

    .line 694
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->handleDoubleKeyEvents()V

    goto :goto_5

    .line 691
    .end local v11           #isDoubleHomeEnable:Z
    :cond_f
    const/4 v11, 0x0

    goto :goto_6

    .line 696
    .restart local v11       #isDoubleHomeEnable:Z
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->launchHomeFromHotKey()V

    goto :goto_5

    .line 703
    .end local v10           #incomingRinging:Z
    .end local v11           #isDoubleHomeEnable:Z
    .end local v17           #nowTime:J
    :cond_11
    sget-object v24, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    const-string v25, "Ignoring HOME; event canceled."

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 710
    .end local v9           #homeWasLongPressed:Z
    :cond_12
    const/4 v13, 0x0

    .line 711
    .local v13, keyguardOn:Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;

    move-object/from16 v24, v0

    if-eqz v24, :cond_13

    .line 712
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;->isShowingAndNotHidden()Z

    move-result v13

    .line 715
    :cond_13
    if-eqz v4, :cond_14

    if-eqz v13, :cond_14

    .line 716
    if-nez v19, :cond_15

    .line 728
    :cond_14
    invoke-super/range {p0 .. p3}, Lcom/android/internal/policy/impl/PhoneWindowManager;->interceptKeyBeforeDispatching(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/KeyEvent;I)J

    move-result-wide v24

    goto/16 :goto_2

    .line 718
    :cond_15
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getFlags()I

    move-result v24

    move/from16 v0, v24

    and-int/lit16 v0, v0, 0x80

    move/from16 v24, v0

    if-eqz v24, :cond_14

    .line 721
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->closeFlashApp()V

    .line 722
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->openFlashApp()V

    .line 723
    const-wide/16 v24, -0x1

    goto/16 :goto_2

    .line 732
    .end local v13           #keyguardOn:Z
    :cond_16
    const/16 v24, 0x52

    move/from16 v0, v24

    if-ne v12, v0, :cond_17

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->longPressMenuEnable:Z

    move/from16 v24, v0

    if-eqz v24, :cond_17

    .line 733
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->keyguardOn()Z

    move-result v13

    .line 734
    .restart local v13       #keyguardOn:Z
    if-eqz v4, :cond_17

    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getFlags()I

    move-result v24

    move/from16 v0, v24

    and-int/lit16 v0, v0, 0x80

    move/from16 v24, v0

    if-eqz v24, :cond_17

    if-nez v13, :cond_17

    .line 736
    if-eqz p1, :cond_17

    const-string v24, "com.oppo.speechassist"

    invoke-interface/range {p1 .. p1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v25

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_17

    .line 737
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->longPressMenuKey()V

    .line 742
    .end local v13           #keyguardOn:Z
    :cond_17
    sget-object v24, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "interceptKeyBeforeDispatching"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual {v14}, Landroid/view/KeyEvent;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 743
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p3

    invoke-super {v0, v1, v14, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->interceptKeyBeforeDispatching(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/KeyEvent;I)J

    move-result-wide v24

    goto/16 :goto_2
.end method

.method public interceptKeyBeforeQueueing(Landroid/view/KeyEvent;IZ)I
    .locals 20
    .parameter "event"
    .parameter "policyFlags"
    .parameter "isScreenOn"

    .prologue
    .line 333
    sget-object v16, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    const-string v17, "OppoPhoneWindowManager interceptKeyBeforeQueueing begin"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v10

    .line 335
    .local v10, keyCode:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v16

    if-nez v16, :cond_1

    const/4 v5, 0x1

    .line 337
    .local v5, down:Z
    :goto_0
    const/4 v14, 0x0

    .line 340
    .local v14, result:I
    const/16 v16, 0x1a

    move/from16 v0, v16

    if-ne v0, v10, :cond_d

    .line 341
    invoke-static {}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    move-result-object v15

    .line 342
    .local v15, telephonyService:Lcom/android/internal/telephony/ITelephony;
    if-eqz v15, :cond_d

    .line 344
    :try_start_0
    invoke-interface {v15}, Lcom/android/internal/telephony/ITelephony;->isRinging()Z

    move-result v16

    if-nez v16, :cond_0

    invoke-interface {v15}, Lcom/android/internal/telephony/ITelephony;->isOffhook()Z

    move-result v16

    if-nez v16, :cond_0

    const/16 v16, 0x5

    const/16 v17, 0x0

    invoke-static/range {v16 .. v17}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    move-result v16

    if-eqz v16, :cond_d

    .line 347
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v4

    .line 349
    .local v4, canceled:Z
    const/high16 v16, 0x100

    and-int v16, v16, p2

    if-eqz v16, :cond_2

    const/4 v7, 0x1

    .line 357
    .local v7, isInjected:Z
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;

    move-object/from16 v16, v0

    if-nez v16, :cond_3

    const/4 v11, 0x0

    .line 361
    .local v11, keyguardActive:Z
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mSystemBooted:Z

    move/from16 v16, v0

    if-nez v16, :cond_5

    .line 363
    const/16 v16, 0x0

    .line 542
    .end local v4           #canceled:Z
    .end local v7           #isInjected:Z
    .end local v11           #keyguardActive:Z
    .end local v15           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :goto_3
    return v16

    .line 335
    .end local v5           #down:Z
    .end local v14           #result:I
    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    .line 349
    .restart local v4       #canceled:Z
    .restart local v5       #down:Z
    .restart local v14       #result:I
    .restart local v15       #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :cond_2
    const/4 v7, 0x0

    goto :goto_1

    .line 357
    .restart local v7       #isInjected:Z
    :cond_3
    if-eqz p3, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;->isShowingAndNotHidden()Z

    move-result v11

    goto :goto_2

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;->isShowing()Z

    move-result v11

    goto :goto_2

    .line 371
    .restart local v11       #keyguardActive:Z
    :cond_5
    if-eqz v5, :cond_6

    move/from16 v0, p2

    and-int/lit16 v0, v0, 0x100

    move/from16 v16, v0

    if-eqz v16, :cond_6

    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v16

    if-nez v16, :cond_6

    .line 373
    const/16 v16, 0x0

    const/16 v17, 0x1

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move/from16 v2, v17

    move/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->performHapticFeedbackLw(Landroid/view/WindowManagerPolicy$WindowState;IZ)Z

    .line 377
    :cond_6
    const/16 v16, 0x1a

    move/from16 v0, v16

    if-ne v10, v0, :cond_7

    .line 378
    or-int/lit8 p2, p2, 0x1

    .line 380
    :cond_7
    and-int/lit8 v16, p2, 0x3

    if-eqz v16, :cond_b

    const/4 v8, 0x1

    .line 394
    .local v8, isWakeKey:Z
    :goto_4
    if-eqz p3, :cond_8

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHeadless:Z

    move/from16 v16, v0

    if-eqz v16, :cond_9

    :cond_8
    if-eqz v7, :cond_c

    if-nez v8, :cond_c

    .line 397
    :cond_9
    const/4 v14, 0x1

    .line 415
    :cond_a
    :goto_5
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    invoke-direct {v0, v1, v2, v3, v14}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->interceptPowerKeyForTelephone(Landroid/view/KeyEvent;IZI)I

    move-result v14

    move/from16 v16, v14

    .line 417
    goto :goto_3

    .line 380
    .end local v8           #isWakeKey:Z
    :cond_b
    const/4 v8, 0x0

    goto :goto_4

    .line 401
    .restart local v8       #isWakeKey:Z
    :cond_c
    const/4 v14, 0x0

    .line 402
    if-eqz v5, :cond_a

    if-eqz v8, :cond_a

    .line 403
    if-eqz v11, :cond_f

    .line 406
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;->onWakeKeyWhenKeyguardShowingTq(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    .line 419
    .end local v4           #canceled:Z
    .end local v7           #isInjected:Z
    .end local v8           #isWakeKey:Z
    .end local v11           #keyguardActive:Z
    :catch_0
    move-exception v6

    .line 420
    .local v6, ex:Landroid/os/RemoteException;
    sget-object v16, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    const-string v17, "ITelephony threw RemoteException"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 426
    .end local v6           #ex:Landroid/os/RemoteException;
    .end local v15           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :cond_d
    const/16 v16, 0x19

    move/from16 v0, v16

    if-eq v0, v10, :cond_e

    const/16 v16, 0x18

    move/from16 v0, v16

    if-eq v0, v10, :cond_e

    const/16 v16, 0xa4

    move/from16 v0, v16

    if-ne v0, v10, :cond_11

    .line 429
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mSystemBooted:Z

    move/from16 v16, v0

    if-nez v16, :cond_10

    .line 431
    const/16 v16, 0x0

    goto/16 :goto_3

    .line 409
    .restart local v4       #canceled:Z
    .restart local v7       #isInjected:Z
    .restart local v8       #isWakeKey:Z
    .restart local v11       #keyguardActive:Z
    .restart local v15       #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :cond_f
    or-int/lit8 v14, v14, 0x2

    goto :goto_5

    .line 434
    .end local v4           #canceled:Z
    .end local v7           #isInjected:Z
    .end local v8           #isWakeKey:Z
    .end local v11           #keyguardActive:Z
    .end local v15           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :cond_10
    if-eqz v5, :cond_11

    .line 435
    invoke-static {}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    move-result-object v15

    .line 436
    .restart local v15       #telephonyService:Lcom/android/internal/telephony/ITelephony;
    if-eqz v15, :cond_11

    .line 440
    new-instance v9, Landroid/content/Intent;

    const-string v16, "SILENCE_ACTION_FOR_OPPO_SPEECH"

    move-object/from16 v0, v16

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 441
    .local v9, it:Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 442
    sget-object v16, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    const-string v17, "send broadcast silence action for speechassist."

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    :try_start_1
    invoke-interface {v15}, Lcom/android/internal/telephony/ITelephony;->isRinging()Z

    move-result v16

    if-eqz v16, :cond_11

    .line 446
    const/16 v16, 0x1

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->isMute:Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 456
    .end local v9           #it:Landroid/content/Intent;
    .end local v15           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :cond_11
    :goto_6
    const/16 v16, 0xda

    move/from16 v0, v16

    if-ne v0, v10, :cond_13

    if-eqz v5, :cond_13

    .line 457
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->object:Ljava/lang/Object;

    move-object/from16 v17, v0

    monitor-enter v17

    .line 458
    :try_start_2
    sget-object v16, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    const-string v18, "KEYCODE_KANA"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->enableProximitySensor()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 462
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->object:Ljava/lang/Object;

    move-object/from16 v16, v0

    const-wide/16 v18, 0x3e8

    move-object/from16 v0, v16

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2

    .line 467
    :goto_7
    :try_start_4
    sget-object v16, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "mProximitySensorActive = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mProximitySensorActive:Z

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mProximitySensorActive:Z

    move/from16 v16, v0

    if-nez v16, :cond_12

    .line 469
    or-int/lit8 p2, p2, 0x1

    .line 470
    and-int/lit8 v14, v14, -0x2

    .line 472
    const-string v16, "oppo.dt.wakeupnum"

    const-string v18, "oppo.dt.wakeupnum"

    const/16 v19, 0x0

    invoke-static/range {v18 .. v19}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v18

    add-int/lit8 v18, v18, 0x1

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    :cond_12
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->disableProximitySensor()V

    .line 477
    monitor-exit v17
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 481
    :cond_13
    invoke-direct/range {p0 .. p1}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->adjustKey(Landroid/view/KeyEvent;)Landroid/view/KeyEvent;

    move-result-object v12

    .line 483
    .local v12, newEvent:Landroid/view/KeyEvent;
    sget-object v16, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "interceptKeyBeforeQueueing"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v12}, Landroid/view/KeyEvent;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-super {v0, v12, v1, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->interceptKeyBeforeQueueing(Landroid/view/KeyEvent;IZ)I

    move-result v14

    .line 492
    invoke-virtual {v12}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v13

    .line 493
    .local v13, newKeyCode:I
    invoke-virtual {v12}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v4

    .line 494
    .restart local v4       #canceled:Z
    const/16 v16, 0x33a

    move/from16 v0, v16

    if-ne v0, v13, :cond_1b

    .line 495
    and-int/lit8 v14, v14, 0x1

    .line 496
    if-eqz v5, :cond_17

    .line 497
    if-eqz p3, :cond_14

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoPowerKeyTriggered:Z

    move/from16 v16, v0

    if-nez v16, :cond_14

    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getFlags()I

    move-result v16

    move/from16 v0, v16

    and-int/lit16 v0, v0, 0x400

    move/from16 v16, v0

    if-nez v16, :cond_14

    .line 499
    const/16 v16, 0x1

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoPowerKeyTriggered:Z

    .line 500
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v16

    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoPowerKeyTime:J

    .line 501
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoPowerDownKeyConsumedByScreenshotChord:Z

    .line 503
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->interceptScreenshotChord()V

    .line 505
    :cond_14
    if-nez p3, :cond_16

    const/16 v16, 0x1

    :goto_8
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->interceptPowerKeyDown(Z)V

    :cond_15
    :goto_9
    move/from16 v16, v14

    .line 542
    goto/16 :goto_3

    .line 448
    .end local v4           #canceled:Z
    .end local v12           #newEvent:Landroid/view/KeyEvent;
    .end local v13           #newKeyCode:I
    .restart local v9       #it:Landroid/content/Intent;
    .restart local v15       #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :catch_1
    move-exception v6

    .line 449
    .restart local v6       #ex:Landroid/os/RemoteException;
    sget-object v16, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    const-string v17, "ITelephony threw RemoteException"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_6

    .line 477
    .end local v6           #ex:Landroid/os/RemoteException;
    .end local v9           #it:Landroid/content/Intent;
    .end local v15           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :catchall_0
    move-exception v16

    :try_start_5
    monitor-exit v17
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v16

    .line 505
    .restart local v4       #canceled:Z
    .restart local v12       #newEvent:Landroid/view/KeyEvent;
    .restart local v13       #newKeyCode:I
    :cond_16
    const/16 v16, 0x0

    goto :goto_8

    .line 509
    :cond_17
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoPowerKeyTriggered:Z

    .line 510
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->cancelPendingScreenshotChordAction()V

    .line 511
    if-nez v4, :cond_18

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mPendingPowerKeyUpCanceled:Z

    move/from16 v16, v0

    if-eqz v16, :cond_1a

    :cond_18
    const/16 v16, 0x1

    :goto_a
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->interceptPowerKeyUp(Z)Z

    move-result v16

    if-eqz v16, :cond_19

    .line 512
    :cond_19
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mPendingPowerKeyUpCanceled:Z

    goto :goto_9

    .line 511
    :cond_1a
    const/16 v16, 0x0

    goto :goto_a

    .line 514
    :cond_1b
    const/16 v16, 0x19

    move/from16 v0, v16

    if-eq v0, v13, :cond_1c

    const/16 v16, 0x18

    move/from16 v0, v16

    if-eq v0, v13, :cond_1c

    const/16 v16, 0xa4

    move/from16 v0, v16

    if-ne v0, v13, :cond_15

    .line 518
    :cond_1c
    const/16 v16, 0x19

    move/from16 v0, v16

    if-ne v0, v13, :cond_1d

    .line 519
    if-eqz v5, :cond_1e

    .line 520
    if-eqz p3, :cond_1d

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoVolumeDownKeyTriggered:Z

    move/from16 v16, v0

    if-nez v16, :cond_1d

    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getFlags()I

    move-result v16

    move/from16 v0, v16

    and-int/lit16 v0, v0, 0x400

    move/from16 v16, v0

    if-nez v16, :cond_1d

    .line 522
    const/16 v16, 0x1

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoVolumeDownKeyTriggered:Z

    .line 523
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v16

    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoVolumeDownKeyTime:J

    .line 524
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoVolumeDownKeyConsumedByScreenshotChord:Z

    .line 525
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->cancelPendingPowerKeyAction()V

    .line 526
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->interceptScreenshotChord()V

    .line 534
    :cond_1d
    :goto_b
    if-eqz v5, :cond_15

    .line 536
    const/16 v16, 0x5

    const/16 v17, 0x0

    invoke-static/range {v16 .. v17}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    move-result v16

    if-eqz v16, :cond_15

    .line 537
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->disableNotificationAlert()V

    goto/16 :goto_9

    .line 529
    :cond_1e
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mOppoVolumeDownKeyTriggered:Z

    .line 530
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->cancelPendingScreenshotChordAction()V

    goto :goto_b

    .line 463
    .end local v4           #canceled:Z
    .end local v12           #newEvent:Landroid/view/KeyEvent;
    .end local v13           #newKeyCode:I
    :catch_2
    move-exception v16

    goto/16 :goto_7
.end method

.method public keyguardSetApkLockScreenShowing(Z)V
    .locals 2
    .parameter "showing"
    .annotation build Landroid/annotation/OppoHook;
        level = .enum Landroid/annotation/OppoHook$OppoHookType;->NEW_METHOD:Landroid/annotation/OppoHook$OppoHookType;
        note = "zhangkai add for apklock"
    .end annotation

    .prologue
    .line 1477
    iget-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;

    if-eqz v0, :cond_0

    .line 1478
    iget-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;->keyguardSetApkLockScreenShowing(Z)V

    .line 1483
    :goto_0
    return-void

    .line 1481
    :cond_0
    sget-object v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    const-string v1, "mKeyguardMediator == NULL!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public keyguardShowSecureApkLock(Z)V
    .locals 2
    .parameter "show"
    .annotation build Landroid/annotation/OppoHook;
        level = .enum Landroid/annotation/OppoHook$OppoHookType;->NEW_METHOD:Landroid/annotation/OppoHook$OppoHookType;
        note = "zhangkai add for apklock"
    .end annotation

    .prologue
    .line 1490
    iget-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;

    if-eqz v0, :cond_0

    .line 1491
    iget-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;

    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;->keyguardShowSecureApkLock(Z)V

    .line 1495
    :goto_0
    return-void

    .line 1493
    :cond_0
    sget-object v0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    const-string v1, "keyguardShowSecureApkLock mKeyguardMediator == NULL!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public performHapticFeedbackLw(Landroid/view/WindowManagerPolicy$WindowState;IZ)Z
    .locals 4
    .parameter "win"
    .parameter "effectId"
    .parameter "always"

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1046
    iget-object v2, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "haptic_feedback_enabled"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1048
    .local v0, hapticsDisabled:Z
    :cond_0
    if-nez v0, :cond_1

    if-ne p2, v1, :cond_1

    .line 1049
    invoke-super {p0, p1, p2, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->performHapticFeedbackLw(Landroid/view/WindowManagerPolicy$WindowState;IZ)Z

    move-result v1

    .line 1051
    :goto_0
    return v1

    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/android/internal/policy/impl/PhoneWindowManager;->performHapticFeedbackLw(Landroid/view/WindowManagerPolicy$WindowState;IZ)Z

    move-result v1

    goto :goto_0
.end method

.method public prepareAddWindowLw(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)I
    .locals 3
    .parameter "win"
    .parameter "attrs"
    .annotation build Landroid/annotation/OppoHook;
        level = .enum Landroid/annotation/OppoHook$OppoHookType;->NEW_METHOD:Landroid/annotation/OppoHook$OppoHookType;
        note = "zhangkai@Plf.DesktopApp.Keyguard add for apklock"
        property = .enum Landroid/annotation/OppoHook$OppoRomType;->OPPO:Landroid/annotation/OppoHook$OppoRomType;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1380
    const/16 v1, 0xa10

    iget v2, p2, Landroid/view/WindowManager$LayoutParams;->type:I

    if-ne v1, v2, :cond_1

    .line 1381
    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mApkLockScreens:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1382
    if-eqz p1, :cond_0

    .line 1388
    :cond_0
    :goto_0
    return v0

    .line 1386
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->prepareAddWindowLw(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)I

    move-result v0

    goto :goto_0
.end method

.method public removeWindowLw(Landroid/view/WindowManagerPolicy$WindowState;)V
    .locals 4
    .parameter "win"
    .annotation build Landroid/annotation/OppoHook;
        level = .enum Landroid/annotation/OppoHook$OppoHookType;->NEW_METHOD:Landroid/annotation/OppoHook$OppoHookType;
        note = "zhangkai@Plf.DesktopApp.Keyguard add for apklock"
        property = .enum Landroid/annotation/OppoHook$OppoRomType;->OPPO:Landroid/annotation/OppoHook$OppoRomType;
    .end annotation

    .prologue
    .line 1395
    const/16 v1, 0xa10

    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    if-ne v1, v2, :cond_1

    .line 1396
    sget-object v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "removeWindowLw win ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1397
    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mApkLockScreens:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1398
    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;

    if-eqz v1, :cond_0

    .line 1399
    const-string v1, "oppo simlock hostview"

    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1400
    .local v0, isSimUnlockWindow:Z
    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;

    invoke-virtual {v1, v0}, Lcom/android/internal/policy/impl/keyguard/OppoKeyguardViewMediator;->checkApkKeyguardStatus(Z)V

    .line 1405
    .end local v0           #isSimUnlockWindow:Z
    :cond_0
    :goto_0
    return-void

    .line 1403
    :cond_1
    invoke-super {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->removeWindowLw(Landroid/view/WindowManagerPolicy$WindowState;)V

    goto :goto_0
.end method

.method public screenTurnedOff(I)V
    .locals 0
    .parameter "why"

    .prologue
    .line 1034
    invoke-super {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->screenTurnedOff(I)V

    .line 1035
    return-void
.end method

.method public screenTurningOn(Landroid/view/WindowManagerPolicy$ScreenOnListener;)V
    .locals 0
    .parameter "screenOnListener"

    .prologue
    .line 1038
    invoke-super {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->screenTurningOn(Landroid/view/WindowManagerPolicy$ScreenOnListener;)V

    .line 1041
    invoke-direct {p0}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->closeFlashApp()V

    .line 1043
    return-void
.end method

.method final sendIsCameraModeIntent(Z)V
    .locals 4
    .parameter "isCameraMode"

    .prologue
    .line 1611
    sget-object v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "===sendIsCameraModeIntent=="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1612
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mSystemReady:Z

    if-nez v1, :cond_1

    .line 1613
    sget-object v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendIsCameraModeIntent:isCameraMode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", can\'t send broadcast before boot completed!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1623
    :cond_0
    :goto_0
    return-void

    .line 1618
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CAMERA_MODE_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1619
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "iscameramode"

    if-eqz p1, :cond_2

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1620
    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 1621
    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_0

    .line 1619
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method final sendIsHomeModeIntent(ZZ)V
    .locals 5
    .parameter "isHomeMode"
    .parameter "isSemipermeable"

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1596
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mSystemReady:Z

    if-nez v1, :cond_1

    .line 1597
    sget-object v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendIsHomeModeIntent:isHomeMode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", can\'t send broadcast before boot completed!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1608
    :cond_0
    :goto_0
    return-void

    .line 1602
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.HOME_MODE_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1603
    .local v0, intent:Landroid/content/Intent;
    const-string v4, "ishomemode"

    if-eqz p1, :cond_2

    move v1, v2

    :goto_1
    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1604
    const-string v1, "isSemipermeable"

    if-eqz p2, :cond_3

    :goto_2
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1605
    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 1606
    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_0

    :cond_2
    move v1, v3

    .line 1603
    goto :goto_1

    :cond_3
    move v2, v3

    .line 1604
    goto :goto_2
.end method

.method public showBootMessage(Ljava/lang/CharSequence;Z)V
    .locals 2
    .parameter "msg"
    .parameter "always"

    .prologue
    .line 1629
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHeadless:Z

    if-eqz v0, :cond_0

    .line 1680
    :goto_0
    return-void

    .line 1630
    :cond_0
    iget-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$9;

    invoke-direct {v1, p0, p1}, Lcom/android/internal/policy/impl/OppoPhoneWindowManager$9;-><init>(Lcom/android/internal/policy/impl/OppoPhoneWindowManager;Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public systemReady()V
    .locals 2

    .prologue
    .line 1027
    invoke-super {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->systemReady()V

    .line 1029
    new-instance v0, Landroid/hardware/SystemSensorManager;

    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

#    invoke-direct {v0, v1}, Landroid/hardware/SystemSensorManager;-><init>(Landroid/os/Looper;)V

#    iput-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 1030
#    iget-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v1, 0x8

#    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

#    move-result-object v0

#    iput-object v0, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mProximitySensor:Landroid/hardware/Sensor;

    .line 1031
    return-void
.end method

.method updateSystemUiVisibilityLw()I
    .locals 3
    .annotation build Landroid/annotation/OppoHook;
        level = .enum Landroid/annotation/OppoHook$OppoHookType;->NEW_METHOD:Landroid/annotation/OppoHook$OppoHookType;
        note = "zhangkai@Plf.DesktopApp.Keyguard add for apklock"
        property = .enum Landroid/annotation/OppoHook$OppoRomType;->OPPO:Landroid/annotation/OppoHook$OppoRomType;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1430
    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mFocusedWindow:Landroid/view/WindowManagerPolicy$WindowState;

    if-nez v1, :cond_1

    .line 1438
    :cond_0
    :goto_0
    return v0

    .line 1434
    :cond_1
    iget-object v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mFocusedWindow:Landroid/view/WindowManagerPolicy$WindowState;

    invoke-interface {v1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v2, 0xa10

    if-ne v1, v2, :cond_2

    iget-boolean v1, p0, Lcom/android/internal/policy/impl/OppoPhoneWindowManager;->mHideLockScreen:Z

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 1438
    :cond_2
    invoke-super {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->updateSystemUiVisibilityLw()I

    move-result v0

    goto :goto_0
.end method

.method public windowTypeToLayerLw(I)I
    .locals 1
    .parameter "type"
    .annotation build Landroid/annotation/OppoHook;
        level = .enum Landroid/annotation/OppoHook$OppoHookType;->NEW_METHOD:Landroid/annotation/OppoHook$OppoHookType;
        note = "zhangkai@Plf.DesktopApp.Keyguard add for apklock"
        property = .enum Landroid/annotation/OppoHook$OppoRomType;->OPPO:Landroid/annotation/OppoHook$OppoRomType;
    .end annotation

    .prologue
    .line 1330
    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    const/16 v0, 0x63

    if-gt p1, v0, :cond_0

    .line 1331
    const/4 v0, 0x2

    .line 1336
    :goto_0
    return v0

    .line 1333
    :cond_0
    const/16 v0, 0xa10

    if-ne v0, p1, :cond_1

    .line 1334
    const/16 v0, 0x7d9

    invoke-super {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->windowTypeToLayerLw(I)I

    move-result v0

    goto :goto_0

    .line 1336
    :cond_1
    invoke-super {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->windowTypeToLayerLw(I)I

    move-result v0

    goto :goto_0
.end method
