.class public Lcom/android/internal/widget/OppoActionBarView;
.super Lcom/android/internal/widget/ActionBarView;
.source "OppoActionBarView.java"


# static fields
.field private static final DBG:Z = false

.field private static final TAG:Ljava/lang/String; = "OppoActionBarView"


# instance fields
.field private mBackground:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/android/internal/widget/ActionBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/internal/widget/OppoActionBarView;->mBackground:Landroid/graphics/drawable/Drawable;

    .line 50
    const/4 v1, 0x0

    const-string v2, "OppoActionBarView"

    const-string v3, "OppoActionBarView"

    invoke-static {v1, v2, v3}, Lcom/oppo/util/OppoLog;->e(ZLjava/lang/String;Ljava/lang/String;)V

    .line 52
    sget-object v1, Lcom/android/internal/R$styleable;->ActionBar:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 54
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/internal/widget/OppoActionBarView;->mBackground:Landroid/graphics/drawable/Drawable;

    .line 55
    iget-object v1, p0, Lcom/android/internal/widget/OppoActionBarView;->mBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 56
    iget-object v1, p0, Lcom/android/internal/widget/OppoActionBarView;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v1}, Lcom/android/internal/widget/OppoActionBarView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 58
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 59
    return-void
.end method

.method private doCustomViewAnim(Landroid/view/View;)V
    .locals 4
    .parameter "customView"

    .prologue
    .line 106
    if-nez p1, :cond_1

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 109
    :cond_1
    iget-object v2, p0, Lcom/android/internal/widget/OppoActionBarView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v1, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 110
    .local v1, packageName:Ljava/lang/String;
    const-string v2, "com.android.settings"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "com.qualcomm.wfd.client"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 113
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    if-nez v2, :cond_0

    .line 116
    iget-object v2, p0, Lcom/android/internal/widget/OppoActionBarView;->mContext:Landroid/content/Context;

    const v3, 0xc0a0407

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 117
    .local v0, animation:Landroid/view/animation/Animation;
    invoke-virtual {p1, v0}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private doHomeAsUpAnim(Z)V
    .locals 3
    .parameter "isUp"

    .prologue
    .line 80
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/android/internal/widget/OppoActionBarView;->mTitleUpView:Landroid/view/View;

    if-nez v1, :cond_1

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 83
    :cond_1
    iget-object v1, p0, Lcom/android/internal/widget/OppoActionBarView;->mTitleUpView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    if-nez v1, :cond_0

    .line 86
    iget-object v1, p0, Lcom/android/internal/widget/OppoActionBarView;->mContext:Landroid/content/Context;

    const v2, 0xc0a0406

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 87
    .local v0, animation:Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/android/internal/widget/OppoActionBarView;->mTitleUpView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method


# virtual methods
.method hookSetTitleItem(ZZ)V
    .locals 7
    .parameter "showHome"
    .parameter "homeAsUp"

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 66
    iget-object v2, p0, Lcom/android/internal/widget/OppoActionBarView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0xc05041a

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 68
    .local v1, paddingLeft:I
    iget-object v5, p0, Lcom/android/internal/widget/OppoActionBarView;->mTitleUpView:Landroid/view/View;

    if-nez p1, :cond_1

    if-eqz p2, :cond_1

    move v2, v3

    :goto_0
    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    .line 69
    iget-object v2, p0, Lcom/android/internal/widget/OppoActionBarView;->mTitleUpView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-ne v2, v4, :cond_2

    if-nez p1, :cond_2

    move v0, v1

    .line 70
    .local v0, left:I
    :goto_1
    iget-object v2, p0, Lcom/android/internal/widget/OppoActionBarView;->mTitleLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/android/internal/widget/OppoActionBarView;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Lcom/android/internal/widget/OppoActionBarView;->getPaddingRight()I

    move-result v5

    invoke-virtual {p0}, Lcom/android/internal/widget/OppoActionBarView;->getPaddingBottom()I

    move-result v6

    invoke-virtual {v2, v0, v4, v5, v6}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 73
    if-nez p1, :cond_0

    if-eqz p2, :cond_0

    const/4 v3, 0x1

    :cond_0
    invoke-direct {p0, v3}, Lcom/android/internal/widget/OppoActionBarView;->doHomeAsUpAnim(Z)V

    .line 75
    return-void

    .end local v0           #left:I
    :cond_1
    move v2, v4

    .line 68
    goto :goto_0

    :cond_2
    move v0, v3

    .line 69
    goto :goto_1
.end method

.method public setCustomNavigationView(Landroid/view/View;)V
    .locals 0
    .parameter "view"

    .prologue
    .line 101
    invoke-super {p0, p1}, Lcom/android/internal/widget/ActionBarView;->setCustomNavigationView(Landroid/view/View;)V

    .line 102
    invoke-direct {p0, p1}, Lcom/android/internal/widget/OppoActionBarView;->doCustomViewAnim(Landroid/view/View;)V

    .line 103
    return-void
.end method
