.class Lcom/android/server/am/OppoActivityStackHelper;
.super Ljava/lang/Object;
.source "OppoActivityStackHelper.java"


# annotations
.annotation build Landroid/annotation/OppoHook;
    level = .enum Landroid/annotation/OppoHook$OppoHookType;->NEW_CLASS:Landroid/annotation/OppoHook$OppoHookType;
    note = "ZhiYong.Lin@Plf.Framework add for activity stack helper"
    property = .enum Landroid/annotation/OppoHook$OppoRomType;->ROM:Landroid/annotation/OppoHook$OppoRomType;
.end annotation


# static fields
.field private static final CAMERA_PACKAGE_NAME:Ljava/lang/String; = "com.oppo.camera"

.field private static final TAG:Ljava/lang/String; = "OppoActivityStackHelper"


# instance fields
.field private mIsProcessesReady:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/am/OppoActivityStackHelper;->mIsProcessesReady:Z

    return-void
.end method


# virtual methods
.method final handleSendCameraMode(Landroid/content/Context;Lcom/android/server/am/ActivityRecord;Lcom/android/server/am/ActivityRecord;)V
    .locals 4
    .parameter "context"
    .parameter "prev"
    .parameter "next"

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 109
    if-nez p2, :cond_2

    .line 110
    const-string v0, "com.oppo.camera"

    iget-object v1, p3, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    const-string v0, "OppoActivityStackHelper"

    const-string v1, "sendIsCameraModeIntent true1!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    invoke-virtual {p0, p1, v3}, Lcom/android/server/am/OppoActivityStackHelper;->sendIsCameraModeIntent(Landroid/content/Context;Z)V

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    invoke-virtual {p0, p1, v2}, Lcom/android/server/am/OppoActivityStackHelper;->sendIsCameraModeIntent(Landroid/content/Context;Z)V

    goto :goto_0

    .line 117
    :cond_2
    const-string v0, "com.oppo.camera"

    iget-object v1, p3, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "com.oppo.camera"

    iget-object v1, p2, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 119
    const-string v0, "OppoActivityStackHelper"

    const-string v1, "sendIsCameraModeIntent true2!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    invoke-virtual {p0, p1, v3}, Lcom/android/server/am/OppoActivityStackHelper;->sendIsCameraModeIntent(Landroid/content/Context;Z)V

    goto :goto_0

    .line 121
    :cond_3
    const-string v0, "com.oppo.camera"

    iget-object v1, p3, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.oppo.camera"

    iget-object v1, p2, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    invoke-virtual {p0, p1, v2}, Lcom/android/server/am/OppoActivityStackHelper;->sendIsCameraModeIntent(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method final handleSendHomeMode(Landroid/content/Context;Lcom/android/server/am/ActivityRecord;Lcom/android/server/am/ActivityRecord;)V
    .locals 3
    .parameter "context"
    .parameter "prev"
    .parameter "next"

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 71
    if-nez p2, :cond_2

    .line 72
    iget-boolean v0, p3, Lcom/android/server/am/ActivityRecord;->isHomeActivity:Z

    if-eqz v0, :cond_1

    .line 73
    invoke-virtual {p0, p1, v2}, Lcom/android/server/am/OppoActivityStackHelper;->sendIsHomeModeIntent(Landroid/content/Context;Z)V

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    invoke-virtual {p0, p1, v1}, Lcom/android/server/am/OppoActivityStackHelper;->sendIsHomeModeIntent(Landroid/content/Context;Z)V

    goto :goto_0

    .line 80
    :cond_2
    iget-boolean v0, p3, Lcom/android/server/am/ActivityRecord;->isHomeActivity:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p2, Lcom/android/server/am/ActivityRecord;->isHomeActivity:Z

    if-nez v0, :cond_3

    .line 81
    invoke-virtual {p0, p1, v2}, Lcom/android/server/am/OppoActivityStackHelper;->sendIsHomeModeIntent(Landroid/content/Context;Z)V

    goto :goto_0

    .line 83
    :cond_3
    iget-boolean v0, p3, Lcom/android/server/am/ActivityRecord;->isHomeActivity:Z

    if-nez v0, :cond_0

    iget-boolean v0, p2, Lcom/android/server/am/ActivityRecord;->isHomeActivity:Z

    if-eqz v0, :cond_0

    .line 84
    invoke-virtual {p0, p1, v1}, Lcom/android/server/am/OppoActivityStackHelper;->sendIsHomeModeIntent(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method final informProcessesReady()V
    .locals 2

    .prologue
    .line 132
    const-string v0, "OppoActivityStackHelper"

    const-string v1, "informProcessesReady begin:"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/am/OppoActivityStackHelper;->mIsProcessesReady:Z

    .line 134
    const-string v0, "OppoActivityStackHelper"

    const-string v1, "informProcessesReady end."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    return-void
.end method

.method final sendIsCameraModeIntent(Landroid/content/Context;Z)V
    .locals 4
    .parameter "context"
    .parameter "isCameraMode"

    .prologue
    .line 94
    iget-boolean v1, p0, Lcom/android/server/am/OppoActivityStackHelper;->mIsProcessesReady:Z

    if-nez v1, :cond_0

    .line 95
    const-string v1, "OppoActivityStackHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendIsCameraModeIntent:isCameraMode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", can\'t send broadcast before boot completed!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    :goto_0
    return-void

    .line 100
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CAMERA_MODE_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 101
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "iscameramode"

    if-eqz p2, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 102
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 101
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method final sendIsHomeModeIntent(Landroid/content/Context;Z)V
    .locals 4
    .parameter "context"
    .parameter "isHomeMode"

    .prologue
    .line 56
    iget-boolean v1, p0, Lcom/android/server/am/OppoActivityStackHelper;->mIsProcessesReady:Z

    if-nez v1, :cond_0

    .line 57
    const-string v1, "OppoActivityStackHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendIsHomeModeIntent:isHomeMode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", can\'t send broadcast before boot completed!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    :goto_0
    return-void

    .line 62
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.HOME_MODE_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 63
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "ishomemode"

    if-eqz p2, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 64
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 63
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
