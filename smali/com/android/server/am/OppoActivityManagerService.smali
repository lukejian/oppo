.class public Lcom/android/server/am/OppoActivityManagerService;
.super Lcom/android/server/am/ActivityManagerService;
.source "OppoActivityManagerService.java"

# interfaces
.implements Landroid/app/IOppoActivityManager;


# static fields
.field private static final TAG:Ljava/lang/String; = "OppoActivityManagerService"


# instance fields
.field mService:Lcom/android/server/am/ActivityManagerService;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/android/server/am/ActivityManagerService;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/am/OppoActivityManagerService;->mService:Lcom/android/server/am/ActivityManagerService;

    return-void
.end method


# virtual methods
.method public getTopActivityComponentName()Landroid/content/ComponentName;
    .locals 6

    .prologue
    .line 82
    iget-object v4, p0, Lcom/android/server/am/OppoActivityManagerService;->mMainStack:Lcom/android/server/am/ActivityStack;

    iget-object v0, v4, Lcom/android/server/am/ActivityStack;->mLRUActivities:Ljava/util/ArrayList;

    .line 84
    .local v0, array:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/android/server/am/ActivityRecord;>;"
    :try_start_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/ActivityRecord;

    .line 85
    .local v3, top:Lcom/android/server/am/ActivityRecord;
    new-instance v2, Landroid/content/ComponentName;

    iget-object v4, v3, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v5, v3, Lcom/android/server/am/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v2, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    .end local v3           #top:Lcom/android/server/am/ActivityRecord;
    :goto_0
    return-object v2

    .line 87
    :catch_0
    move-exception v1

    .line 88
    .local v1, e:Ljava/lang/Exception;
    new-instance v2, Landroid/content/ComponentName;

    const-string v4, ""

    const-string v5, ""

    invoke-direct {v2, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public killPidForce(I)V
    .locals 1
    .parameter "pid"

    .prologue
    .line 93
    const/16 v0, 0x9

    invoke-static {p1, v0}, Landroid/os/Process;->sendSignal(II)V

    .line 94
    return-void
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 6
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 42
    iget-object v5, p0, Lcom/android/server/am/OppoActivityManagerService;->mService:Lcom/android/server/am/ActivityManagerService;

    if-nez v5, :cond_0

    .line 44
    invoke-static {}, Lcom/android/server/am/ActivityManagerService;->self()Lcom/android/server/am/ActivityManagerService;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/am/OppoActivityManagerService;->mService:Lcom/android/server/am/ActivityManagerService;

    .line 47
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 73
    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/server/am/ActivityManagerService;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v4

    :goto_0
    return v4

    .line 50
    :pswitch_0
    const-string v5, "android.app.IActivityManager"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 52
    .local v2, properties:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 53
    .local v3, value:Ljava/lang/String;
    invoke-virtual {p0, v2, v3}, Lcom/android/server/am/OppoActivityManagerService;->setSystemProperties(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 59
    .end local v2           #properties:Ljava/lang/String;
    .end local v3           #value:Ljava/lang/String;
    :pswitch_1
    const-string v5, "android.app.IActivityManager"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p0}, Lcom/android/server/am/OppoActivityManagerService;->getTopActivityComponentName()Landroid/content/ComponentName;

    move-result-object v0

    .line 61
    .local v0, name:Landroid/content/ComponentName;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 62
    invoke-static {v0, p3}, Landroid/content/ComponentName;->writeToParcel(Landroid/content/ComponentName;Landroid/os/Parcel;)V

    goto :goto_0

    .line 66
    .end local v0           #name:Landroid/content/ComponentName;
    :pswitch_2
    const-string v5, "android.app.IActivityManager"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 68
    .local v1, pid:I
    invoke-virtual {p0, v1}, Lcom/android/server/am/OppoActivityManagerService;->killPidForce(I)V

    .line 69
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 47
    nop

    :pswitch_data_0
    .packed-switch 0x2716
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setSystemProperties(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter "properties"
    .parameter "value"

    .prologue
    .line 78
    invoke-static {p1, p2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    return-void
.end method
