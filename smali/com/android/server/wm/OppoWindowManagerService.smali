.class public Lcom/android/server/wm/OppoWindowManagerService;
.super Lcom/android/server/wm/WindowManagerService;
.source "OppoWindowManagerService.java"

# interfaces
.implements Landroid/view/IOppoWindowManager;


# static fields
.field private static final DEFAULT_APP_TRANSITION_DURATION:J = 0x15eL

.field private static final RECENTS_THUMBNAIL_FADEOUT_FRACTION:F = 0.25f

.field private static final TAG:Ljava/lang/String; = "OppoWindowManagerService"


# instance fields
.field private final mAccelerateDecelerateInterpolator:Landroid/view/animation/Interpolator;

.field private final mAccelerateInterpolator:Landroid/view/animation/Interpolator;

.field private mApkLockScreenSyncObj:Ljava/lang/Object;

.field private final mDecelerateInterpolator:Landroid/view/animation/Interpolator;

.field private final mThumbnailFadeoutInterpolator:Landroid/view/animation/Interpolator;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/server/power/PowerManagerService;Lcom/android/server/display/DisplayManagerService;Lcom/android/server/input/InputManagerService;Landroid/os/Handler;ZZZ)V
    .locals 1
    .parameter "context"
    .parameter "pm"
    .parameter "displayManager"
    .parameter "inputManager"
    .parameter "uiHandler"
    .parameter "haveInputMethods"
    .parameter "showBootMsgs"
    .parameter "onlyCore"

    .prologue
    .line 75
    invoke-direct/range {p0 .. p8}, Lcom/android/server/wm/WindowManagerService;-><init>(Landroid/content/Context;Lcom/android/server/power/PowerManagerService;Lcom/android/server/display/DisplayManagerService;Lcom/android/server/input/InputManagerService;Landroid/os/Handler;ZZZ)V

    .line 61
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/OppoWindowManagerService;->mApkLockScreenSyncObj:Ljava/lang/Object;

    .line 76
    const v0, 0x10c0003

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/OppoWindowManagerService;->mDecelerateInterpolator:Landroid/view/animation/Interpolator;

    .line 78
    const v0, 0x10c0002

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/OppoWindowManagerService;->mAccelerateInterpolator:Landroid/view/animation/Interpolator;

    .line 80
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/OppoWindowManagerService;->mAccelerateDecelerateInterpolator:Landroid/view/animation/Interpolator;

    .line 81
    new-instance v0, Lcom/android/server/wm/OppoWindowManagerService$1;

    invoke-direct {v0, p0}, Lcom/android/server/wm/OppoWindowManagerService$1;-><init>(Lcom/android/server/wm/OppoWindowManagerService;)V

    iput-object v0, p0, Lcom/android/server/wm/OppoWindowManagerService;->mThumbnailFadeoutInterpolator:Landroid/view/animation/Interpolator;

    .line 91
    return-void
.end method

.method private static computePivot(IF)F
    .locals 3
    .parameter "startPos"
    .parameter "finalScale"

    .prologue
    .line 480
    const/high16 v1, 0x3f80

    sub-float v0, p1, v1

    .line 481
    .local v0, denom:F
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x38d1b717

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 482
    int-to-float v1, p0

    .line 484
    :goto_0
    return v1

    :cond_0
    neg-int v1, p0

    int-to-float v1, v1

    div-float/2addr v1, v0

    goto :goto_0
.end method

.method private createOppoCustomAnimLocked(Lcom/android/server/wm/AppWindowToken;Ljava/lang/String;Z)Landroid/view/animation/Animation;
    .locals 23
    .parameter "atoken"
    .parameter "packageName"
    .parameter "enter"

    .prologue
    .line 403
    const/4 v9, 0x0

    .line 404
    .local v9, a:Landroid/view/animation/Animation;
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/OppoWindowManagerService;->getDefaultDisplayInfoLocked()Landroid/view/DisplayInfo;

    move-result-object v13

    .line 405
    .local v13, displayInfo:Landroid/view/DisplayInfo;
    iget v12, v13, Landroid/view/DisplayInfo;->appWidth:I

    .line 406
    .local v12, appWidth:I
    iget v11, v13, Landroid/view/DisplayInfo;->appHeight:I

    .line 407
    .local v11, appHeight:I
    move-object/from16 v0, p0

 #   iget v3, v0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionEnter:I

    invoke-virtual {v0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionEnter()I

    move-result v3

    invoke-static {v3}, Lcom/oppo/util/OppoAnimSynthesisNumber;->isSynthesisNumber(I)Z

    move-result v3

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

 #   iget v3, v0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionExit:I
    invoke-virtual {v0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionExit()I

    move-result v3

    invoke-static {v3}, Lcom/oppo/util/OppoAnimSynthesisNumber;->isSynthesisNumber(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 409
    const-wide/16 v14, 0xfa

    .line 410
    .local v14, duration:J
    move-object/from16 v0, p0

 #   iget v3, v0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionEnter:I
    invoke-virtual {v0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionEnter()I

    move-result v3

    invoke-static {v3}, Lcom/oppo/util/OppoAnimSynthesisNumber;->getLowerDigit(I)I

    move-result v20

    .line 411
    .local v20, startWidth:I
    move-object/from16 v0, p0

#    iget v3, v0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionExit:I
    invoke-virtual {v0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionExit()I

    move-result v3

    invoke-static {v3}, Lcom/oppo/util/OppoAnimSynthesisNumber;->getLowerDigit(I)I

    move-result v19

    .line 414
    .local v19, startHight:I
    if-nez v20, :cond_0

    if-eqz v19, :cond_1

    :cond_0
    move-object/from16 v0, p0

 #   iget v3, v0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionEnter:I
    invoke-virtual {v0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionEnter()I

    move-result v3

    invoke-static {v3}, Lcom/oppo/util/OppoAnimSynthesisNumber;->getHighDigit(I)I

    move-result v3

    if-ge v3, v12, :cond_1

    move-object/from16 v0, p0

#    iget v3, v0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionExit:I
    invoke-virtual {v0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionExit()I

    move-result v3

    invoke-static {v3}, Lcom/oppo/util/OppoAnimSynthesisNumber;->getHighDigit(I)I

    move-result v3

    if-lt v3, v11, :cond_2

    .line 417
    :cond_1
    div-int/lit8 v21, v12, 0x2

    .line 418
    .local v21, startX:I
    div-int/lit8 v22, v11, 0x2

    .line 426
    .local v22, startY:I
    :goto_0
    move/from16 v0, v20

    int-to-float v3, v0

    int-to-float v4, v12

    div-float v17, v3, v4

    .line 427
    .local v17, scaleW:F
    move/from16 v0, v19

    int-to-float v3, v0

    int-to-float v4, v11

    div-float v16, v3, v4

    .line 432
    .local v16, scaleH:F
    const/4 v2, 0x0

    .line 433
    .local v2, scale:Landroid/view/animation/Animation;
    const/16 v18, 0x0

    .line 434
    .local v18, set:Landroid/view/animation/AnimationSet;
    const/4 v10, 0x0

    .line 435
    .local v10, alpha:Landroid/view/animation/Animation;
    if-eqz p3, :cond_3

    .line 436
    new-instance v18, Landroid/view/animation/AnimationSet;

    .end local v18           #set:Landroid/view/animation/AnimationSet;
    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 438
    .restart local v18       #set:Landroid/view/animation/AnimationSet;
    new-instance v2, Landroid/view/animation/ScaleAnimation;

    .end local v2           #scale:Landroid/view/animation/Animation;
    const v3, 0x3f99999a

    const/high16 v4, 0x3f80

    const v5, 0x3f99999a

    const/high16 v6, 0x3f80

    move-object/from16 v0, p0

#    iget v7, v0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionStartX:I
    invoke-virtual {v0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionStartX()I

    move-result v7

    move/from16 v0, v17

    invoke-static {v7, v0}, Lcom/android/server/wm/OppoWindowManagerService;->computePivot(IF)F

    move-result v7

    move-object/from16 v0, p0

#    iget v8, v0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionStartY:I
    invoke-virtual {v0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionStartX()I

    move-result v8

    move/from16 v0, v16

    invoke-static {v8, v0}, Lcom/android/server/wm/OppoWindowManagerService;->computePivot(IF)F

    move-result v8

    invoke-direct/range {v2 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    .line 441
    .restart local v2       #scale:Landroid/view/animation/Animation;
    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 443
    new-instance v10, Landroid/view/animation/AlphaAnimation;

    .end local v10           #alpha:Landroid/view/animation/Animation;
    const/4 v3, 0x0

    const/high16 v4, 0x3f80

    invoke-direct {v10, v3, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 444
    .restart local v10       #alpha:Landroid/view/animation/Animation;
    const-wide/16 v3, 0xaf

    invoke-virtual {v10, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 445
    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 446
    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/view/animation/AnimationSet;->setDetachWallpaper(Z)V

    .line 447
    move-object/from16 v9, v18

    .line 460
    :goto_1
    const-wide/16 v3, 0xfa

    invoke-virtual {v9, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 461
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/server/wm/OppoWindowManagerService;->mAccelerateDecelerateInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v9, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 466
    .end local v2           #scale:Landroid/view/animation/Animation;
    .end local v10           #alpha:Landroid/view/animation/Animation;
    .end local v14           #duration:J
    .end local v16           #scaleH:F
    .end local v17           #scaleW:F
    .end local v18           #set:Landroid/view/animation/AnimationSet;
    .end local v19           #startHight:I
    .end local v20           #startWidth:I
    .end local v21           #startX:I
    .end local v22           #startY:I
    :goto_2
    return-object v9

    .line 423
    .restart local v14       #duration:J
    .restart local v19       #startHight:I
    .restart local v20       #startWidth:I
    :cond_2
    move-object/from16 v0, p0

#    iget v3, v0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionEnter:I
    invoke-virtual {v0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionEnter()I

    move-result v3

    invoke-static {v3}, Lcom/oppo/util/OppoAnimSynthesisNumber;->getHighDigit(I)I

    move-result v21

    .line 424
    .restart local v21       #startX:I
    move-object/from16 v0, p0

#    iget v3, v0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionExit:I
    invoke-virtual {v0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionExit()I

    move-result v3

    invoke-static {v3}, Lcom/oppo/util/OppoAnimSynthesisNumber;->getHighDigit(I)I

    move-result v22

    .restart local v22       #startY:I
    goto :goto_0

    .line 449
    .restart local v2       #scale:Landroid/view/animation/Animation;
    .restart local v10       #alpha:Landroid/view/animation/Animation;
    .restart local v16       #scaleH:F
    .restart local v17       #scaleW:F
    .restart local v18       #set:Landroid/view/animation/AnimationSet;
    :cond_3
    new-instance v2, Landroid/view/animation/ScaleAnimation;

    .end local v2           #scale:Landroid/view/animation/Animation;
    const/high16 v3, 0x3f80

    const/high16 v5, 0x3f80

    move/from16 v0, v21

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/server/wm/OppoWindowManagerService;->computePivot(IF)F

    move-result v7

    move/from16 v0, v22

    move/from16 v1, v16

    invoke-static {v0, v1}, Lcom/android/server/wm/OppoWindowManagerService;->computePivot(IF)F

    move-result v8

    move/from16 v4, v17

    move/from16 v6, v16

    invoke-direct/range {v2 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    .line 452
    .restart local v2       #scale:Landroid/view/animation/Animation;
    new-instance v18, Landroid/view/animation/AnimationSet;

    .end local v18           #set:Landroid/view/animation/AnimationSet;
    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 453
    .restart local v18       #set:Landroid/view/animation/AnimationSet;
    new-instance v10, Landroid/view/animation/AlphaAnimation;

    .end local v10           #alpha:Landroid/view/animation/Animation;
    const/high16 v3, 0x3f80

    const v4, 0x0

    invoke-direct {v10, v3, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 454
    .restart local v10       #alpha:Landroid/view/animation/Animation;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/server/wm/OppoWindowManagerService;->mAccelerateInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v10, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 455
    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 456
    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 457
    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V

    .line 458
    move-object/from16 v9, v18

    goto :goto_1

    .line 463
    .end local v2           #scale:Landroid/view/animation/Animation;
    .end local v10           #alpha:Landroid/view/animation/Animation;
    .end local v14           #duration:J
    .end local v16           #scaleH:F
    .end local v17           #scaleW:F
    .end local v18           #set:Landroid/view/animation/AnimationSet;
    .end local v19           #startHight:I
    .end local v20           #startWidth:I
    .end local v21           #startX:I
    .end local v22           #startY:I
    :cond_4
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x0

    move-object/from16 v0, p0

 #   iget-object v5, v0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionPackage:Ljava/lang/String;
    invoke-virtual {v0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionPackage()Ljava/lang/String;

    move-result-object v5

    if-eqz p3, :cond_5

    move-object/from16 v0, p0

#    iget v3, v0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionEnter:I
    invoke-virtual {v0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionEnter()I

    move-result v3

    :goto_3
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v3}, Lcom/android/server/wm/OppoWindowManagerService;->loadAnimation(ILjava/lang/String;I)Landroid/view/animation/Animation;

    move-result-object v9

    goto :goto_2

    :cond_5
    move-object/from16 v0, p0

 #   iget v3, v0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionExit:I
    invoke-virtual {v0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionExit()I

    move-result v3

    goto :goto_3
.end method

.method private createOppoScaleUpAnimationLocked(Lcom/android/server/wm/AppWindowToken;IZ)Landroid/view/animation/Animation;
    .locals 17
    .parameter "atoken"
    .parameter "transit"
    .parameter "enter"

    .prologue
    .line 314
    const/4 v11, 0x0

    .line 315
    .local v11, a:Landroid/view/animation/Animation;
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/OppoWindowManagerService;->getDefaultDisplayInfoLocked()Landroid/view/DisplayInfo;

    move-result-object v13

    .line 316
    .local v13, displayInfo:Landroid/view/DisplayInfo;
    iget v9, v13, Landroid/view/DisplayInfo;->appWidth:I

    .line 317
    .local v9, appWidth:I
    iget v10, v13, Landroid/view/DisplayInfo;->appHeight:I

    .line 318
    .local v10, appHeight:I
    if-eqz p3, :cond_0

    .line 320
    move-object/from16 v0, p0

 #   iget v3, v0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionStartWidth:I
    invoke-virtual {v0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionStartWidth()I

    move-result v3

    int-to-float v3, v3

    int-to-float v5, v9

    div-float v2, v3, v5

    .line 321
    .local v2, scaleW:F
    move-object/from16 v0, p0

 #   iget v3, v0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionStartHeight:I
    invoke-virtual {v0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionStartHeight()I

    move-result v3

    int-to-float v3, v3

    int-to-float v5, v10

    div-float v4, v3, v5

    .line 322
    .local v4, scaleH:F
    new-instance v1, Landroid/view/animation/ScaleAnimation;

    const/high16 v3, 0x3f80

    const/high16 v5, 0x3f80

    move-object/from16 v0, p0

#    iget v6, v0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionStartX:I
    invoke-virtual {v0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionStartX()I

    move-result v6

    invoke-static {v6, v2}, Lcom/android/server/wm/OppoWindowManagerService;->computePivot(IF)F

    move-result v6

    move-object/from16 v0, p0

 #   iget v7, v0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionStartY:I
    invoke-virtual {v0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionStartY()I

    move-result v7

    invoke-static {v7, v4}, Lcom/android/server/wm/OppoWindowManagerService;->computePivot(IF)F

    move-result v7

    invoke-direct/range {v1 .. v7}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    .line 325
    .local v1, scale:Landroid/view/animation/Animation;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/server/wm/OppoWindowManagerService;->mDecelerateInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 327
    new-instance v12, Landroid/view/animation/AlphaAnimation;

    const/4 v3, 0x0

    const/high16 v5, 0x3f80

    invoke-direct {v12, v3, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 328
    .local v12, alpha:Landroid/view/animation/Animation;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/server/wm/OppoWindowManagerService;->mThumbnailFadeoutInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v12, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 330
    new-instance v16, Landroid/view/animation/AnimationSet;

    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 331
    .local v16, set:Landroid/view/animation/AnimationSet;
    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 332
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 333
    const/4 v3, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/view/animation/AnimationSet;->setDetachWallpaper(Z)V

    .line 334
    move-object/from16 v11, v16

    .line 344
    .end local v1           #scale:Landroid/view/animation/Animation;
    .end local v2           #scaleW:F
    .end local v4           #scaleH:F
    .end local v12           #alpha:Landroid/view/animation/Animation;
    .end local v16           #set:Landroid/view/animation/AnimationSet;
    :goto_0
    sparse-switch p2, :sswitch_data_0

    .line 351
    const-wide/16 v14, 0xfa

    .line 354
    .local v14, duration:J
    :goto_1
    invoke-virtual {v11, v14, v15}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 355
    const/4 v3, 0x1

    invoke-virtual {v11, v3}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 356
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/server/wm/OppoWindowManagerService;->mDecelerateInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v11, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 357
    invoke-virtual {v11, v9, v10, v9, v10}, Landroid/view/animation/Animation;->initialize(IIII)V

    .line 358
    return-object v11

    .line 337
    .end local v14           #duration:J
    :cond_0
    const/4 v8, 0x0

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    invoke-direct/range {v5 .. v10}, Lcom/android/server/wm/OppoWindowManagerService;->createOppoScaleUpExitAnimationLocked(Lcom/android/server/wm/AppWindowToken;IZII)Landroid/view/animation/Animation;

    move-result-object v11

    goto :goto_0

    .line 347
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/server/wm/OppoWindowManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v5, 0x10e

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

#    iget-object v5, v0, Lcom/android/server/wm/WindowManagerService;->mAppTransition:Lcom/android/server/wm/AppTransition;

#    iget v3, v5, Lcom/android/server/wm/AppTransition;->mConfigShortAnimTime:I

    int-to-long v14, v3

    .line 349
    .restart local v14       #duration:J
    goto :goto_1

    .line 344
    nop

    :sswitch_data_0
    .sparse-switch
        0x1006 -> :sswitch_0
        0x2007 -> :sswitch_0
    .end sparse-switch
.end method

.method private createOppoScaleUpExitAnimationLocked(Lcom/android/server/wm/AppWindowToken;IZII)Landroid/view/animation/Animation;
    .locals 13
    .parameter "atoken"
    .parameter "transit"
    .parameter "enter"
    .parameter "appWidth"
    .parameter "appHeight"

    .prologue
    .line 366
    invoke-virtual {p1}, Lcom/android/server/wm/AppWindowToken;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.oppo.launcher"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 368
    const/16 v2, 0x100e

    if-eq p2, v2, :cond_0

    const/16 v2, 0x200f

    if-ne p2, v2, :cond_1

    .line 374
    :cond_0
    new-instance v8, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f80

    const/4 v3, 0x0

    invoke-direct {v8, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 375
    .local v8, a:Landroid/view/animation/Animation;
    const/4 v2, 0x1

    invoke-virtual {v8, v2}, Landroid/view/animation/Animation;->setDetachWallpaper(Z)V

    .line 398
    .end local v8           #a:Landroid/view/animation/Animation;
    :goto_0
    return-object v8

    .line 378
    :cond_1
    new-instance v8, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f80

    const/high16 v3, 0x3f80

    invoke-direct {v8, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .restart local v8       #a:Landroid/view/animation/Animation;
    goto :goto_0

    .line 382
    .end local v8           #a:Landroid/view/animation/Animation;
    :cond_2
    new-instance v12, Landroid/view/animation/AnimationSet;

    const/4 v2, 0x1

    invoke-direct {v12, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 384
    .local v12, set:Landroid/view/animation/AnimationSet;
#    iget v2, p0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionStartWidth:I
    invoke-virtual {p0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionStartWidth()I

    move-result v2

    int-to-float v2, v2

    move/from16 v0, p4

    int-to-float v3, v0

    div-float v11, v2, v3

    .line 385
    .local v11, scaleW:F
#    iget v2, p0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionStartHeight:I
    invoke-virtual {p0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionStartHeight()I

    move-result v2

    int-to-float v2, v2

    move/from16 v0, p5

    int-to-float v3, v0

    div-float v10, v2, v3

    .line 386
    .local v10, scaleH:F
    new-instance v1, Landroid/view/animation/ScaleAnimation;

    const/high16 v2, 0x3f80

    const v3, 0x3f99999a

    const/high16 v4, 0x3f80

    const v5, 0x3f99999a

#    iget v6, p0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionStartX:I
    invoke-virtual {p0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionStartX()I

    move-result v6

    invoke-static {v6, v11}, Lcom/android/server/wm/OppoWindowManagerService;->computePivot(IF)F

    move-result v6

 #   iget v7, p0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionStartY:I
    invoke-virtual {p0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionStartY()I

    move-result v7

    invoke-static {v7, v10}, Lcom/android/server/wm/OppoWindowManagerService;->computePivot(IF)F

    move-result v7

    invoke-direct/range {v1 .. v7}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    .line 390
    .local v1, scale:Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/android/server/wm/OppoWindowManagerService;->mAccelerateInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 391
    invoke-virtual {v12, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 393
    new-instance v9, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f80

    const/4 v3, 0x0

    invoke-direct {v9, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 394
    .local v9, alpha:Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/android/server/wm/OppoWindowManagerService;->mAccelerateInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v9, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 395
    const-wide/16 v2, 0xaf

    invoke-virtual {v9, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 396
    const/4 v2, 0x1

    invoke-virtual {v12, v2}, Landroid/view/animation/AnimationSet;->setDetachWallpaper(Z)V

    .line 397
    invoke-virtual {v12, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    move-object v8, v12

    .line 398
    goto :goto_0
.end method

.method private getCachedAnimations(ILjava/lang/String;I)Lcom/android/server/AttributeCache$Entry;
    .locals 2
    .parameter "userId"
    .parameter "packageName"
    .parameter "resId"

    .prologue
    .line 506
    if-eqz p2, :cond_1

    .line 507
    const/high16 v0, -0x100

    and-int/2addr v0, p3

    const/high16 v1, 0x100

    if-ne v0, v1, :cond_0

    .line 508
    const-string p2, "android"

    .line 512
    :cond_0
    invoke-static {}, Lcom/android/server/AttributeCache;->instance()Lcom/android/server/AttributeCache;

    move-result-object v0

    sget-object v1, Landroid/R$styleable;->WindowAnimation:[I

    invoke-virtual {v0, p2, p3, v1, p1}, Lcom/android/server/AttributeCache;->get(Ljava/lang/String;I[II)Lcom/android/server/AttributeCache$Entry;

    move-result-object v0

    .line 515
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadAnimation(ILjava/lang/String;I)Landroid/view/animation/Animation;
    .locals 4
    .parameter "userId"
    .parameter "packageName"
    .parameter "resId"

    .prologue
    .line 488
    const/4 v0, 0x0

    .line 489
    .local v0, anim:I
    iget-object v1, p0, Lcom/android/server/wm/OppoWindowManagerService;->mContext:Landroid/content/Context;

    .line 490
    .local v1, context:Landroid/content/Context;
    if-ltz p3, :cond_0

    .line 491
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/wm/OppoWindowManagerService;->getCachedAnimations(ILjava/lang/String;I)Lcom/android/server/AttributeCache$Entry;

    move-result-object v2

    .line 492
    .local v2, ent:Lcom/android/server/AttributeCache$Entry;
    if-eqz v2, :cond_0

    .line 493
    iget-object v1, v2, Lcom/android/server/AttributeCache$Entry;->context:Landroid/content/Context;

    .line 494
    move v0, p3

    .line 497
    .end local v2           #ent:Lcom/android/server/AttributeCache$Entry;
    :cond_0
    if-eqz v0, :cond_1

    .line 498
    invoke-static {v1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    .line 500
    :goto_0
    return-object v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected applyAnimationLocked(Lcom/android/server/wm/AppWindowToken;Landroid/view/WindowManager$LayoutParams;IZ)Z
    .locals 5
    .parameter "atoken"
    .parameter "lp"
    .parameter "transit"
    .parameter "enter"

    .prologue
    const/4 v2, 0x1

    .line 269
    invoke-virtual {p0}, Lcom/android/server/wm/OppoWindowManagerService;->okToDisplay()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 270
    const/4 v0, 0x0

    .line 271
    .local v0, a:Landroid/view/animation/Animation;
    const/4 v1, 0x0

    .line 272
    .local v1, initialized:Z
#    iget v3, p0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionType:I
    invoke-virtual {p0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionType()I

    move-result v3

    if-ne v3, v2, :cond_1

    .line 275
#    iget-object v3, p0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionPackage:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionPackage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p1, v3, p4}, Lcom/android/server/wm/OppoWindowManagerService;->createOppoCustomAnimLocked(Lcom/android/server/wm/AppWindowToken;Ljava/lang/String;Z)Landroid/view/animation/Animation;

    move-result-object v0

    .line 293
    :goto_0
    if-eqz v0, :cond_0

    .line 302
    iget-object v3, p1, Lcom/android/server/wm/AppWindowToken;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

########
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/OppoWindowManagerService;->getDefaultDisplayInfoLocked()Landroid/view/DisplayInfo;

    move-result-object v1

    iget v2, v1, Landroid/view/DisplayInfo;->appWidth:I

    iget v4, v1, Landroid/view/DisplayInfo;->appHeight:I
#########

    invoke-virtual {v3, v0, v2, v4}, Lcom/android/server/wm/AppWindowAnimator;->setAnimation(Landroid/view/animation/Animation;II)V

    .line 308
    .end local v0           #a:Landroid/view/animation/Animation;
    .end local v1           #initialized:Z
    :cond_0
    :goto_1
    iget-object v3, p1, Lcom/android/server/wm/AppWindowToken;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    iget-object v3, v3, Lcom/android/server/wm/AppWindowAnimator;->animation:Landroid/view/animation/Animation;

    if-eqz v3, :cond_4

    :goto_2
    return v2

    .line 281
    .restart local v0       #a:Landroid/view/animation/Animation;
    .restart local v1       #initialized:Z
    :cond_1
#    iget v3, p0, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionType:I
    invoke-virtual {p0}, Lcom/android/server/wm/OppoWindowManagerService;->mNextAppTransitionType()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 283
    invoke-direct {p0, p1, p3, p4}, Lcom/android/server/wm/OppoWindowManagerService;->createOppoScaleUpAnimationLocked(Lcom/android/server/wm/AppWindowToken;IZ)Landroid/view/animation/Animation;

    move-result-object v0

    .line 284
    const/4 v1, 0x1

    goto :goto_0

    .line 291
    :cond_2
    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/server/wm/WindowManagerService;->applyAnimationLocked(Lcom/android/server/wm/AppWindowToken;Landroid/view/WindowManager$LayoutParams;IZ)Z

    move-result v2

    goto :goto_2

    .line 305
    .end local v0           #a:Landroid/view/animation/Animation;
    .end local v1           #initialized:Z
    :cond_3
    iget-object v3, p1, Lcom/android/server/wm/AppWindowToken;->mAppAnimator:Lcom/android/server/wm/AppWindowAnimator;

    invoke-virtual {v3}, Lcom/android/server/wm/AppWindowAnimator;->clearAnimation()V

    goto :goto_1

    .line 308
    :cond_4
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public getApkUnlockWindow()Landroid/os/IBinder;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 190
    invoke-virtual {p0}, Lcom/android/server/wm/OppoWindowManagerService;->getDefaultWindowListLocked()Lcom/android/server/wm/WindowList;

    move-result-object v4

    .line 191
    .local v4, windows:Lcom/android/server/wm/WindowList;
    invoke-virtual {v4}, Lcom/android/server/wm/WindowList;->size()I

    move-result v0

    .line 192
    .local v0, N:I
    const/4 v3, 0x0

    .line 194
    .local v3, wTemp:Lcom/android/server/wm/WindowState;
    const/4 v2, 0x0

    .local v2, j:I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 196
    :try_start_0
    invoke-virtual {v4, v2}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3           #wTemp:Lcom/android/server/wm/WindowState;
    check-cast v3, Lcom/android/server/wm/WindowState;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    .restart local v3       #wTemp:Lcom/android/server/wm/WindowState;
    iget-object v5, v3, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget v5, v5, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v6, 0xa10

    if-ne v5, v6, :cond_0

    .line 194
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 197
    .end local v3           #wTemp:Lcom/android/server/wm/WindowState;
    :catch_0
    move-exception v1

    .line 198
    .local v1, e:Ljava/lang/Exception;
    const-string v5, "OppoWindowManagerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception: Invalid index "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", size is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/RuntimeException;

    invoke-direct {v7}, Ljava/lang/RuntimeException;-><init>()V

    invoke-static {v5, v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 207
    .end local v1           #e:Ljava/lang/Exception;
    :cond_1
    return-object v8
.end method

.method public isLockOnShow()Z
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 243
    invoke-virtual {p0}, Lcom/android/server/wm/OppoWindowManagerService;->getDefaultWindowListLocked()Lcom/android/server/wm/WindowList;

    move-result-object v5

    .line 244
    .local v5, windows:Lcom/android/server/wm/WindowList;
    invoke-virtual {v5}, Lcom/android/server/wm/WindowList;->size()I

    move-result v1

    .line 245
    .local v1, N:I
    const/4 v4, 0x0

    .line 247
    .local v4, wTemp:Lcom/android/server/wm/WindowState;
    const/4 v3, 0x0

    .local v3, j:I
    :goto_0
    if-ge v3, v1, :cond_1

    .line 249
    :try_start_0
    invoke-virtual {v5, v3}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/android/server/wm/WindowState;

    move-object v4, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 255
    iget-object v6, v4, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v8, 0xa10

    if-ne v6, v8, :cond_0

    .line 256
    const/4 v6, 0x1

    .line 259
    :goto_1
    return v6

    .line 250
    :catch_0
    move-exception v2

    .line 251
    .local v2, e:Ljava/lang/Exception;
    const-string v6, "OppoWindowManagerService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception: Invalid index "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", size is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/RuntimeException;

    invoke-direct {v9}, Ljava/lang/RuntimeException;-><init>()V

    invoke-static {v6, v8, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v6, v7

    .line 253
    goto :goto_1

    .line 247
    .end local v2           #e:Ljava/lang/Exception;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v6, v7

    .line 259
    goto :goto_1
.end method

.method public isLockWndShow()Z
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 163
    invoke-virtual {p0}, Lcom/android/server/wm/OppoWindowManagerService;->getDefaultWindowListLocked()Lcom/android/server/wm/WindowList;

    move-result-object v5

    .line 164
    .local v5, windows:Lcom/android/server/wm/WindowList;
    invoke-virtual {v5}, Lcom/android/server/wm/WindowList;->size()I

    move-result v1

    .line 165
    .local v1, N:I
    const/4 v4, 0x0

    .line 167
    .local v4, wTemp:Lcom/android/server/wm/WindowState;
    const/4 v3, 0x0

    .local v3, j:I
    :goto_0
    if-ge v3, v1, :cond_1

    .line 169
    :try_start_0
    invoke-virtual {v5, v3}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/android/server/wm/WindowState;

    move-object v4, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    iget-object v6, v4, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v8, 0xa10

    if-ne v6, v8, :cond_0

    iget-object v6, v4, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    iget-boolean v6, v6, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceShown:Z

    if-eqz v6, :cond_0

    .line 178
    const-string v6, "OppoWindowManagerService"

    const-string v7, "return true"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    const/4 v6, 0x1

    .line 182
    :goto_1
    return v6

    .line 170
    :catch_0
    move-exception v2

    .line 171
    .local v2, e:Ljava/lang/Exception;
    const-string v6, "OppoWindowManagerService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception: Invalid index "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", size is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/RuntimeException;

    invoke-direct {v9}, Ljava/lang/RuntimeException;-><init>()V

    invoke-static {v6, v8, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v6, v7

    .line 173
    goto :goto_1

    .line 167
    .end local v2           #e:Ljava/lang/Exception;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v6, v7

    .line 182
    goto :goto_1
.end method

.method public isSIMUnlockRunning()Z
    .locals 9

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/android/server/wm/OppoWindowManagerService;->getDefaultWindowListLocked()Lcom/android/server/wm/WindowList;

    move-result-object v5

    .line 215
    .local v5, windows:Lcom/android/server/wm/WindowList;
    invoke-virtual {v5}, Lcom/android/server/wm/WindowList;->size()I

    move-result v1

    .line 216
    .local v1, N:I
    const/4 v4, 0x0

    .line 218
    .local v4, wTemp:Lcom/android/server/wm/WindowState;
    const/4 v3, 0x0

    .local v3, j:I
    :goto_0
    if-ge v3, v1, :cond_1

    .line 220
    :try_start_0
    invoke-virtual {v5, v3}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/android/server/wm/WindowState;

    move-object v4, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    iget-object v6, v4, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget v6, v6, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v7, 0xa10

    if-ne v6, v7, :cond_0

    const-string v6, "oppo simlock hostview"

    iget-object v7, v4, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v7}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 228
    const/4 v6, 0x1

    .line 231
    :goto_1
    return v6

    .line 221
    :catch_0
    move-exception v2

    .line 222
    .local v2, e:Ljava/lang/Exception;
    const-string v6, "OppoWindowManagerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception: Invalid index "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", size is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/RuntimeException;

    invoke-direct {v8}, Ljava/lang/RuntimeException;-><init>()V

    invoke-static {v6, v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 218
    .end local v2           #e:Ljava/lang/Exception;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 231
    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public keyguardSetApkLockScreenShowing(Z)V
    .locals 3
    .parameter "showing"

    .prologue
    .line 150
    iget-object v1, p0, Lcom/android/server/wm/OppoWindowManagerService;->mApkLockScreenSyncObj:Ljava/lang/Object;

    monitor-enter v1

    .line 151
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/OppoWindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/android/server/wm/OppoWindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    check-cast v0, Landroid/view/OppoWindowManagerPolicy;

    invoke-interface {v0, p1}, Landroid/view/OppoWindowManagerPolicy;->keyguardSetApkLockScreenShowing(Z)V

    .line 156
    :goto_0
    monitor-exit v1

    .line 157
    return-void

    .line 154
    :cond_0
    const-string v0, "OppoWindowManagerService"

    const-string v2, "mPolicy == NULL!!!!"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 156
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public keyguardShowSecureApkLock(Z)V
    .locals 2
    .parameter "show"

    .prologue
    .line 235
    iget-object v0, p0, Lcom/android/server/wm/OppoWindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/android/server/wm/OppoWindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    check-cast v0, Landroid/view/OppoWindowManagerPolicy;

    invoke-interface {v0, p1}, Landroid/view/OppoWindowManagerPolicy;->keyguardShowSecureApkLock(Z)V

    .line 240
    :goto_0
    return-void

    .line 238
    :cond_0
    const-string v0, "OppoWindowManagerService"

    const-string v1, "mPolicy == null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 5
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 95
    packed-switch p1, :pswitch_data_0

    .line 143
    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/server/wm/WindowManagerService;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v3

    :goto_0
    return v3

    .line 97
    :pswitch_0
    const-string v4, "android.view.IWindowManager"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 98
    invoke-virtual {p0}, Lcom/android/server/wm/OppoWindowManagerService;->isLockWndShow()Z

    move-result v1

    .line 99
    .local v1, result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 100
    if-eqz v1, :cond_0

    move v2, v3

    :cond_0
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 104
    .end local v1           #result:Z
    :pswitch_1
    const-string v4, "android.view.IWindowManager"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 106
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1

    move v0, v3

    .line 107
    .local v0, arg0:Z
    :goto_1
    invoke-virtual {p0, v0}, Lcom/android/server/wm/OppoWindowManagerService;->keyguardSetApkLockScreenShowing(Z)V

    .line 108
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .end local v0           #arg0:Z
    :cond_1
    move v0, v2

    .line 106
    goto :goto_1

    .line 112
    :pswitch_2
    const-string v2, "android.view.IWindowManager"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 113
    invoke-virtual {p0}, Lcom/android/server/wm/OppoWindowManagerService;->getApkUnlockWindow()Landroid/os/IBinder;

    move-result-object v1

    .line 114
    .local v1, result:Landroid/os/IBinder;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 115
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    goto :goto_0

    .line 119
    .end local v1           #result:Landroid/os/IBinder;
    :pswitch_3
    const-string v4, "android.view.IWindowManager"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 121
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_2

    move v0, v3

    .line 122
    .restart local v0       #arg0:Z
    :goto_2
    invoke-virtual {p0, v0}, Lcom/android/server/wm/OppoWindowManagerService;->keyguardShowSecureApkLock(Z)V

    .line 123
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .end local v0           #arg0:Z
    :cond_2
    move v0, v2

    .line 121
    goto :goto_2

    .line 127
    :pswitch_4
    const-string v4, "android.view.IWindowManager"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 128
    invoke-virtual {p0}, Lcom/android/server/wm/OppoWindowManagerService;->isLockOnShow()Z

    move-result v1

    .line 129
    .local v1, result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 130
    if-eqz v1, :cond_3

    move v2, v3

    :cond_3
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 135
    .end local v1           #result:Z
    :pswitch_5
    const-string v4, "android.view.IWindowManager"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 136
    invoke-virtual {p0}, Lcom/android/server/wm/OppoWindowManagerService;->isSIMUnlockRunning()Z

    move-result v1

    .line 137
    .restart local v1       #result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 138
    if-eqz v1, :cond_4

    move v2, v3

    :cond_4
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 95
    nop

    :pswitch_data_0
    .packed-switch 0x2712
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public screenshotApplications(Landroid/os/IBinder;III)Landroid/graphics/Bitmap;
    .locals 41
    .parameter "appToken"
    .parameter "displayId"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 529
    const-string v5, "android.permission.READ_FRAME_BUFFER"

    const-string v6, "screenshotApplications()"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Lcom/android/server/wm/OppoWindowManagerService;->checkCallingPermission(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 531
    new-instance v5, Ljava/lang/SecurityException;

    const-string v6, "Requires READ_FRAME_BUFFER permission"

    invoke-direct {v5, v6}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 536
    :cond_0
    const/16 v30, 0x0

    .line 537
    .local v30, maxLayer:I
    new-instance v21, Landroid/graphics/Rect;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/Rect;-><init>()V

    .line 543
    .local v21, frame:Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/server/wm/OppoWindowManagerService;->mWindowMap:Ljava/util/HashMap;

    monitor-enter v6

    .line 544
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v24

    .line 546
    .local v24, ident:J
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/android/server/wm/OppoWindowManagerService;->getDisplayContentLocked(I)Lcom/android/server/wm/DisplayContent;

    move-result-object v17

    .line 547
    .local v17, displayContent:Lcom/android/server/wm/DisplayContent;
    if-nez v17, :cond_1

    .line 548
    const/4 v4, 0x0

    monitor-exit v6

    .line 688
    :goto_0
    return-object v4

    .line 550
    :cond_1
    invoke-virtual/range {v17 .. v17}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;

    move-result-object v18

    .line 551
    .local v18, displayInfo:Landroid/view/DisplayInfo;
    move-object/from16 v0, v18

    iget v0, v0, Landroid/view/DisplayInfo;->logicalWidth:I

    move/from16 v19, v0

    .line 552
    .local v19, dw:I
    move-object/from16 v0, v18

    iget v0, v0, Landroid/view/DisplayInfo;->logicalHeight:I

    move/from16 v16, v0

    .line 554
    .local v16, dh:I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/wm/OppoWindowManagerService;->mPolicy:Landroid/view/WindowManagerPolicy;

    const/4 v7, 0x2

    invoke-interface {v5, v7}, Landroid/view/WindowManagerPolicy;->windowTypeToLayerLw(I)I

    move-result v5

    mul-int/lit16 v5, v5, 0x2710

    add-int/lit16 v11, v5, 0x3e8

    .line 556
    .local v11, aboveAppLayer:I
    add-int/lit16 v11, v11, 0x2710

    .line 558
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/wm/OppoWindowManagerService;->mInputMethodTarget:Lcom/android/server/wm/WindowState;

    if-eqz v5, :cond_3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/wm/OppoWindowManagerService;->mInputMethodTarget:Lcom/android/server/wm/WindowState;

    iget-object v5, v5, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    if-eqz v5, :cond_3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/wm/OppoWindowManagerService;->mInputMethodTarget:Lcom/android/server/wm/WindowState;

    iget-object v5, v5, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    iget-object v5, v5, Lcom/android/server/wm/AppWindowToken;->appToken:Landroid/view/IApplicationToken;

    if-eqz v5, :cond_3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/wm/OppoWindowManagerService;->mInputMethodTarget:Lcom/android/server/wm/WindowState;

    iget-object v5, v5, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    iget-object v5, v5, Lcom/android/server/wm/AppWindowToken;->appToken:Landroid/view/IApplicationToken;

    invoke-interface {v5}, Landroid/view/IApplicationToken;->asBinder()Landroid/os/IBinder;

    move-result-object v5

    move-object/from16 v0, p1

    if-ne v5, v0, :cond_3

    const/16 v27, 0x1

    .line 564
    .local v27, isImeTarget:Z
    :goto_1
    const/16 v26, 0x0

    .line 565
    .local v26, including:Z
    invoke-virtual/range {v17 .. v17}, Lcom/android/server/wm/DisplayContent;->getWindowList()Lcom/android/server/wm/WindowList;

    move-result-object v39

    .line 566
    .local v39, windows:Lcom/android/server/wm/WindowList;
    invoke-virtual/range {v39 .. v39}, Lcom/android/server/wm/WindowList;->size()I

    move-result v5

    add-int/lit8 v23, v5, -0x1

    .local v23, i:I
    :goto_2
    if-ltz v23, :cond_9

    .line 567
    move-object/from16 v0, v39

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/android/server/wm/WindowState;

    .line 568
    .local v40, ws:Lcom/android/server/wm/WindowState;
    move-object/from16 v0, v40

    iget-boolean v5, v0, Lcom/android/server/wm/WindowState;->mHasSurface:Z

    if-nez v5, :cond_4

    .line 566
    :cond_2
    :goto_3
    add-int/lit8 v23, v23, -0x1

    goto :goto_2

    .line 558
    .end local v23           #i:I
    .end local v26           #including:Z
    .end local v27           #isImeTarget:Z
    .end local v39           #windows:Lcom/android/server/wm/WindowList;
    .end local v40           #ws:Lcom/android/server/wm/WindowState;
    :cond_3
    const/16 v27, 0x0

    goto :goto_1

    .line 571
    .restart local v23       #i:I
    .restart local v26       #including:Z
    .restart local v27       #isImeTarget:Z
    .restart local v39       #windows:Lcom/android/server/wm/WindowList;
    .restart local v40       #ws:Lcom/android/server/wm/WindowState;
    :cond_4
    move-object/from16 v0, v40

    iget v5, v0, Lcom/android/server/wm/WindowState;->mLayer:I

    if-ge v5, v11, :cond_2

    .line 577
    if-nez v26, :cond_6

    if-eqz p1, :cond_6

    .line 581
    move-object/from16 v0, v40

    iget-boolean v5, v0, Lcom/android/server/wm/WindowState;->mIsImWindow:Z

    if-eqz v5, :cond_5

    if-nez v27, :cond_6

    .line 584
    :cond_5
    move-object/from16 v0, v40

    iget-object v5, v0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    if-eqz v5, :cond_2

    move-object/from16 v0, v40

    iget-object v5, v0, Lcom/android/server/wm/WindowState;->mAppToken:Lcom/android/server/wm/AppWindowToken;

    iget-object v5, v5, Lcom/android/server/wm/AppWindowToken;->token:Landroid/os/IBinder;

    move-object/from16 v0, p1

    if-ne v5, v0, :cond_2

    .line 592
    :cond_6
    move-object/from16 v0, v40

    iget-boolean v5, v0, Lcom/android/server/wm/WindowState;->mIsImWindow:Z

    if-nez v5, :cond_8

    move-object/from16 v0, v40

    move/from16 v1, v19

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/WindowState;->isFullscreen(II)Z

    move-result v5

    if-nez v5, :cond_8

    const/16 v26, 0x1

    .line 594
    :goto_4
    move-object/from16 v0, v40

    iget-object v5, v0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    iget v5, v5, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceLayer:I

    move/from16 v0, v30

    if-ge v0, v5, :cond_7

    .line 595
    move-object/from16 v0, v40

    iget-object v5, v0, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    iget v0, v5, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceLayer:I

    move/from16 v30, v0

    .line 599
    :cond_7
    move-object/from16 v0, v40

    iget-boolean v5, v0, Lcom/android/server/wm/WindowState;->mIsWallpaper:Z

    if-nez v5, :cond_2

    .line 600
    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/android/server/wm/WindowState;->mFrame:Landroid/graphics/Rect;

    move-object/from16 v38, v0

    .line 601
    .local v38, wf:Landroid/graphics/Rect;
    move-object/from16 v0, v40

    iget-object v15, v0, Lcom/android/server/wm/WindowState;->mContentInsets:Landroid/graphics/Rect;

    .line 602
    .local v15, cr:Landroid/graphics/Rect;
    move-object/from16 v0, v38

    iget v5, v0, Landroid/graphics/Rect;->left:I

    iget v7, v15, Landroid/graphics/Rect;->left:I

    add-int v28, v5, v7

    .line 603
    .local v28, left:I
    move-object/from16 v0, v38

    iget v5, v0, Landroid/graphics/Rect;->top:I

    iget v7, v15, Landroid/graphics/Rect;->top:I

    add-int v37, v5, v7

    .line 604
    .local v37, top:I
    move-object/from16 v0, v38

    iget v5, v0, Landroid/graphics/Rect;->right:I

    iget v7, v15, Landroid/graphics/Rect;->right:I

    sub-int v31, v5, v7

    .line 605
    .local v31, right:I
    move-object/from16 v0, v38

    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    iget v7, v15, Landroid/graphics/Rect;->bottom:I

    sub-int v13, v5, v7

    .line 606
    .local v13, bottom:I
    move-object/from16 v0, v21

    move/from16 v1, v28

    move/from16 v2, v37

    move/from16 v3, v31

    invoke-virtual {v0, v1, v2, v3, v13}, Landroid/graphics/Rect;->union(IIII)V

    goto/16 :goto_3

    .line 662
    .end local v11           #aboveAppLayer:I
    .end local v13           #bottom:I
    .end local v15           #cr:Landroid/graphics/Rect;
    .end local v16           #dh:I
    .end local v17           #displayContent:Lcom/android/server/wm/DisplayContent;
    .end local v18           #displayInfo:Landroid/view/DisplayInfo;
    .end local v19           #dw:I
    .end local v23           #i:I
    .end local v24           #ident:J
    .end local v26           #including:Z
    .end local v27           #isImeTarget:Z
    .end local v28           #left:I
    .end local v31           #right:I
    .end local v37           #top:I
    .end local v38           #wf:Landroid/graphics/Rect;
    .end local v39           #windows:Lcom/android/server/wm/WindowList;
    .end local v40           #ws:Lcom/android/server/wm/WindowState;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 592
    .restart local v11       #aboveAppLayer:I
    .restart local v16       #dh:I
    .restart local v17       #displayContent:Lcom/android/server/wm/DisplayContent;
    .restart local v18       #displayInfo:Landroid/view/DisplayInfo;
    .restart local v19       #dw:I
    .restart local v23       #i:I
    .restart local v24       #ident:J
    .restart local v26       #including:Z
    .restart local v27       #isImeTarget:Z
    .restart local v39       #windows:Lcom/android/server/wm/WindowList;
    .restart local v40       #ws:Lcom/android/server/wm/WindowState;
    :cond_8
    const/16 v26, 0x0

    goto :goto_4

    .line 609
    .end local v40           #ws:Lcom/android/server/wm/WindowState;
    :cond_9
    :try_start_1
    invoke-static/range {v24 .. v25}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 612
    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v19

    move/from16 v2, v16

    invoke-virtual {v0, v5, v7, v1, v2}, Landroid/graphics/Rect;->intersect(IIII)Z

    .line 614
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_a

    if-nez v30, :cond_b

    .line 615
    :cond_a
    const/4 v4, 0x0

    monitor-exit v6

    goto/16 :goto_0

    .line 619
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/wm/OppoWindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/wm/DisplayContent;->getDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Display;->getRotation()I

    move-result v32

    .line 620
    .local v32, rot:I
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->width()I

    move-result v22

    .line 621
    .local v22, fw:I
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->height()I

    move-result v20

    .line 625
    .local v20, fh:I
    move/from16 v0, p3

    int-to-float v5, v0

    move/from16 v0, v22

    int-to-float v7, v0

    div-float v35, v5, v7

    .line 626
    .local v35, targetWidthScale:F
    move/from16 v0, p4

    int-to-float v5, v0

    move/from16 v0, v20

    int-to-float v7, v0

    div-float v34, v5, v7

    .line 627
    .local v34, targetHeightScale:F
    move/from16 v0, v19

    move/from16 v1, v16

    if-gt v0, v1, :cond_f

    .line 628
    move/from16 v33, v35

    .line 631
    .local v33, scale:F
    cmpl-float v5, v34, v33

    if-lez v5, :cond_c

    move/from16 v0, v22

    int-to-float v5, v0

    mul-float v5, v5, v34

    float-to-int v5, v5

    move/from16 v0, p3

    if-ne v5, v0, :cond_c

    .line 632
    move/from16 v33, v34

    .line 644
    :cond_c
    :goto_5
    move/from16 v0, v19

    int-to-float v5, v0

    mul-float v5, v5, v33

    float-to-int v0, v5

    move/from16 v19, v0

    .line 645
    move/from16 v0, v16

    int-to-float v5, v0

    mul-float v5, v5, v33

    float-to-int v0, v5

    move/from16 v16, v0

    .line 646
    const/4 v5, 0x1

    move/from16 v0, v32

    if-eq v0, v5, :cond_d

    const/4 v5, 0x3

    move/from16 v0, v32

    if-ne v0, v5, :cond_e

    .line 647
    :cond_d
    move/from16 v36, v19

    .line 648
    .local v36, tmp:I
    move/from16 v19, v16

    .line 649
    move/from16 v16, v36

    .line 650
    const/4 v5, 0x1

    move/from16 v0, v32

    if-ne v0, v5, :cond_10

    const/16 v32, 0x3

    .line 661
    .end local v36           #tmp:I
    :cond_e
    :goto_6
    const/4 v5, 0x0

    add-int/lit8 v7, v30, 0x5

    move/from16 v0, v19

    move/from16 v1, v16

    invoke-static {v0, v1, v5, v7}, Landroid/view/SurfaceControl;->screenshot(IIII)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 662
    .local v4, rawss:Landroid/graphics/Bitmap;
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 664
    if-nez v4, :cond_11

    .line 665
    const-string v5, "OppoWindowManagerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failure taking screenshot for ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "x"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") to layer "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v30

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 635
    .end local v4           #rawss:Landroid/graphics/Bitmap;
    .end local v33           #scale:F
    :cond_f
    move/from16 v33, v34

    .line 638
    .restart local v33       #scale:F
    cmpl-float v5, v35, v33

    if-lez v5, :cond_c

    move/from16 v0, v20

    int-to-float v5, v0

    mul-float v5, v5, v35

    float-to-int v5, v5

    move/from16 v0, p4

    if-ne v5, v0, :cond_c

    .line 639
    move/from16 v33, v35

    goto :goto_5

    .line 650
    .restart local v36       #tmp:I
    :cond_10
    const/16 v32, 0x1

    goto :goto_6

    .line 671
    .end local v36           #tmp:I
    .restart local v4       #rawss:Landroid/graphics/Bitmap;
    :cond_11
    if-eqz v4, :cond_12

    .line 672
    new-instance v9, Landroid/graphics/Matrix;

    invoke-direct {v9}, Landroid/graphics/Matrix;-><init>()V

    .line 673
    .local v9, ma:Landroid/graphics/Matrix;
    mul-int/lit8 v5, v32, 0x5a

    int-to-float v5, v5

    invoke-virtual {v9, v5}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 674
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    const/4 v10, 0x1

    invoke-static/range {v4 .. v10}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 676
    goto/16 :goto_0

    .line 679
    .end local v9           #ma:Landroid/graphics/Matrix;
    :cond_12
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v5

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-static {v0, v1, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 680
    .local v12, bm:Landroid/graphics/Bitmap;
    new-instance v29, Landroid/graphics/Matrix;

    invoke-direct/range {v29 .. v29}, Landroid/graphics/Matrix;-><init>()V

    .line 681
    .local v29, matrix:Landroid/graphics/Matrix;
    move/from16 v0, v32

    move/from16 v1, v19

    move/from16 v2, v16

    move-object/from16 v3, v29

    invoke-static {v0, v1, v2, v3}, Lcom/android/server/wm/ScreenRotationAnimation;->createRotationMatrix(IIILandroid/graphics/Matrix;)V

    .line 682
    move-object/from16 v0, v21

    iget v5, v0, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    mul-float v5, v5, v33

    invoke-static {v5}, Landroid/util/FloatMath;->ceil(F)F

    move-result v5

    neg-float v5, v5

    move-object/from16 v0, v21

    iget v6, v0, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    mul-float v6, v6, v33

    invoke-static {v6}, Landroid/util/FloatMath;->ceil(F)F

    move-result v6

    neg-float v6, v6

    move-object/from16 v0, v29

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 683
    new-instance v14, Landroid/graphics/Canvas;

    invoke-direct {v14, v12}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 684
    .local v14, canvas:Landroid/graphics/Canvas;
    const/4 v5, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v14, v4, v0, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 685
    const/4 v5, 0x0

    invoke-virtual {v14, v5}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 687
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    move-object v4, v12

    .line 688
    goto/16 :goto_0
.end method
